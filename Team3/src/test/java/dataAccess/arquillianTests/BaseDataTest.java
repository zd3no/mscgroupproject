
package dataAccess.arquillianTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.File;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.ejb.EJB;

import model.BaseData;
import model.CountryNetworkCode;
import model.CountryNetworkCodePK;
import model.EventCause;
import model.EventCausePK;
import model.FailureClass;
import model.UserEquipment;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import restMapping.CellExtractor;
import restMapping.JaxRsActivator;
import serviceLayer.baseData.BaseDataService;
import serviceLayer.baseData.BaseDataServiceBean;
import serviceLayer.countryNetworkCode.CountryNetworkCodeService;
import serviceLayer.eventCause.EventCauseService;
import serviceLayer.failureClass.FailureClassService;
import serviceLayer.userEquipment.UserEquipmentService;
import dataAccess.baseData.BaseDataDAO;
import dataAccess.insertionConflict.InsertionConflictDAOBean;
import dataAccess.junitTests.CreateData;

@RunWith(Arquillian.class)
public class BaseDataTest {

    @Deployment
    public static Archive<?> createTestArchive() {
        PomEquippedResolveStage pom = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies();
        File[] libs = pom.resolve("org.apache.poi:poi").withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(InsertionConflictDAOBean.class.getPackage())
                .addPackage(BaseDataDAO.class.getPackage())
                .addPackage(BaseData.class.getPackage())
                .addPackage(CellExtractor.class.getPackage())
                .addPackage(JaxRsActivator.class.getPackage())
                .addPackage(BaseDataServiceBean.class.getPackage())
                .addPackage(DataFormatter.class.getPackage())
                .addPackage(HSSFRow.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"))
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("testSheet.xls");
        war.addAsLibraries(libs);
        System.out.println(war.toString(true));
        return war;
    }

    @EJB
    private BaseDataService baseDataService;
    @EJB
    private FailureClassService failureClassService;
    @EJB
    private UserEquipmentService userEquipmentService;
    @EJB
    private EventCauseService eventCauseService;
    @EJB
    private CountryNetworkCodeService countryNetworkCodeService;

    private CreateData createData = new CreateData();

    private Collection<BaseData> baseData = new ArrayList<BaseData>();
    private FailureClass failureClass;
    private UserEquipment userEquipment;
    private EventCause eventCause;
    private CountryNetworkCode countryNetworkCode;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");

    public BaseData createBaseData(int duration, Date dateTime,
            String neVersion, int cellId, BigInteger imsi, BigInteger hier3Id,
            BigInteger hier32Id, BigInteger hier321Id) {
        BaseData basedata = new BaseData();
        basedata.setDuration(duration);
        basedata.setTimeStamp(dateTime);
        basedata.setNetworkElementVersion(neVersion);
        basedata.setCellId(cellId);
        basedata.setImsi(imsi);
        basedata.setHier3Id(hier3Id);
        basedata.setHier32Id(hier32Id);
        basedata.setHier321Id(hier321Id);
        basedata.setEventCause(eventCause);
        basedata.setFailureClass(failureClass);
        basedata.setUserEquipment(userEquipment);
        basedata.setCountryNetworkCode(countryNetworkCode);
        return basedata;
    }

    public void createFailureData() {
        failureClass = new FailureClass(0);
        failureClass.setDescription("EMERGENCY");
    }

    public void createUserEquipmentData() {
        userEquipment = new UserEquipment(new BigInteger("21060800"));
        userEquipment.setMarketingName("VEA3");
        userEquipment.setManufacturer("S.A.R.L. B  & B International");
        userEquipment.setAccessCapability("GSM 1800, GSM 900");
        userEquipment.setModel("VEA3");
        userEquipment.setVendorName("S.A.R.L. B  & B International");
        userEquipment.setUserEquipmentType("");
        userEquipment.setOperatingSystem("");
        userEquipment.setInputMode("");
    }

    public void createEvenCauseData() {
        EventCausePK eventCausePK = new EventCausePK(0, 4098);
        eventCause = new EventCause(eventCausePK);
        eventCause.setDescription("S1 SIG CONN SETUP-SUCCESS");
    }

    public void createCountryNetworkCodeData() {
        CountryNetworkCodePK countryNetworkCodePK = new CountryNetworkCodePK(
                344, 930);
        countryNetworkCode = new CountryNetworkCode(countryNetworkCodePK);
        countryNetworkCode.setCountry("Antigua and Barbuda");
        countryNetworkCode.setOperator("AT&T Wireless-Antigua AG");
    }

    public void addCheckingData() {
        countryNetworkCode = createData.newCountryNetworkCodeData(344, 930, "Antigua and Barbuda", "AT&T Wireless-Antigua AG");
        // createCountryNetworkCodeData();
        countryNetworkCodeService.persistCountryNetworkCode(countryNetworkCode);
        eventCause = createData.newEvenCauseData(0, 4098, "S1 SIG CONN SETUP-SUCCESS");
        // createEvenCauseData();
        eventCauseService.persistEventCode(eventCause);
        failureClass = createData.newFailureData(0, "EMERGENCY");
        // createFailureData();
        failureClassService.persistFailureClass(failureClass);
        userEquipment =
                createData.newUserEquipmentData(new BigInteger("21060800"), "VEA3", "S.A.R.L. B  & B International", "GSM 1800, GSM 900", "VEA3",
                        "S.A.R.L. B  & B International", "", "", "");
        // createUserEquipmentData();
        userEquipmentService.persistUserEquipment(userEquipment);
    }

    public Date convertStringToDate(String date) throws ParseException {
        return sdf.parse(date);
    }

    @After
    public void removeDataFromDataBase() {
        baseData = baseDataService.getAllBaseData();
        for (BaseData basedata : baseData) {
            baseDataService.removeBaseData(basedata);
        }
        eventCauseService.removeEventCode(eventCause);
        failureClassService.removeFailureClass(failureClass);
        userEquipmentService.removeUserEquipment(userEquipment);
        countryNetworkCodeService.removeCountryNetworkCode(countryNetworkCode);
    }

    @Before
    public void addDataToBaseData() throws ParseException {
        addCheckingData();
        baseData.add(createBaseData(1000,
                convertStringToDate("11/01/2013 17:15"), "11B", 4,
                new BigInteger("344930000000011"), new BigInteger(
                        "4809532081614990000"), new BigInteger(
                        "8226896360947470000"), new BigInteger(
                        "1150444940909480000")));
        baseData.add(createBaseData(1000,
                convertStringToDate("11/01/2013 17:16"), "11B", 3,
                new BigInteger("344930000000011"), new BigInteger(
                        "4809532081614990000"), new BigInteger(
                        "8226896360947470000"), new BigInteger(
                        "1150444940909480000")));
        baseData.add(createBaseData(1000,
                convertStringToDate("11/01/2013 17:15"), "11B", 2,
                new BigInteger("344930000000011"), new BigInteger(
                        "4809532081614990000"), new BigInteger(
                        "8226896360947470000"), new BigInteger(
                        "1150444940909480000")));
        baseDataService.persistBaseData(baseData);
    }

    @Test
    public void addDataToDataBaseTest() {
        assertEquals(3, baseDataService.getAllBaseData().size());
    }

    @Test
    public void addDuplicateDataToDataBaseTest() {
        baseDataService.persistBaseData((BaseData) baseData.toArray()[1]);
        assertEquals(4, baseDataService.getAllBaseData().size());
    }

    @Test
    public void removeValidDataFromDataBaseTest() {
        baseDataService.removeBaseData((BaseData) baseData.toArray()[1]);
        assertEquals(2, baseDataService.getAllBaseData().size());
    }

    @Test
    public void removeInvalidDataFromDataBaseTest() {
        baseDataService.removeBaseData(new BaseData());
        assertEquals(3, baseDataService.getAllBaseData().size());
    }

    @Test
    public void removeNullDataFromDataBaseTest() {
        baseDataService.removeBaseData(null);
        assertFalse(baseDataService.removeBaseData(null));
    }

    @Test
    public void mergeUpdateDataToDataBaseTest() {
        BaseData basedata = (BaseData) baseDataService.getAllBaseData().toArray()[0];
        basedata.setDuration(2000);
        baseDataService.mergeBaseData(basedata);
        assertEquals(2000, (int) baseDataService.getAllBaseData().iterator().next().getDuration());
    }

    @Test
    public void mergeNewDataToDataBaseTest() throws ParseException {
        BaseData basedata = createBaseData(1000,
                convertStringToDate("11/01/2013 17:20"), "11B", 4,
                new BigInteger("240210000000013"), new BigInteger(
                        "4809532081614990000"), new BigInteger(
                        "8226896360947470000"), new BigInteger(
                        "1150444940909480000"));
        basedata.setId(50);
        baseDataService.mergeBaseData(basedata);
        assertEquals(4, baseDataService.getAllBaseData().size());
    }

    @Test
    public void mergeInvalidDataBaseTest() {
        assertFalse(baseDataService.mergeBaseData(new BaseData()));
    }

    @Test
    public void mergeNullDataBaseTest() {
        baseDataService.mergeBaseData(null);
        assertEquals(3, baseDataService.getAllBaseData().size());
    }

    @Test
    public void mergeDuplicateDataBaseTest() {
        baseDataService.mergeBaseData((BaseData) baseData.toArray()[1]);
        assertEquals(3, baseDataService.getAllBaseData().size());
    }

    @Test
    public void getNumberOfFailuresAndDurationForEachImsiBetweenPeriodTime_testValidData_returnsTheArrayWithObjects() throws Exception {
        assertEquals(
                1,
                baseDataService.getNumberOfFailuresAndDurationForEachImsiBetweenPeriodTime(sdf.parse("22/09/1987 00:00"),
                        sdf.parse("22/09/2015 00:00")).size());
    }

    @Test
    public void getNumberOfFailuresAndDurationForEachImsiBetweenPeriodTime_testNoRecords_returnsEmptyArray() throws Exception {
        assertEquals(
                0,
                baseDataService.getNumberOfFailuresAndDurationForEachImsiBetweenPeriodTime(sdf.parse("22/09/2015 00:00"),
                        sdf.parse("22/09/2016 00:00")).size());
    }

    @Test
    public void getImsiWithFailuresForGivenDate_testNoRecords_returnsArrayOfObjects() throws Exception {
        assertEquals(0, baseDataService.getImsiForGivenDate(sdf.parse("22/09/2015 00:00"), sdf.parse("22/09/2015 00:00")).size());
    }

    @Test
    public void getAllUniqueCauseCodeAndFailureClassByImsi_testValidData_returnsArrayOfObjects() {
        assertEquals(1, baseDataService.getAllUniqueCauseCodeAndFailureClassByImsi(new BigInteger("344930000000011")).size());
    }

    @Test
    public void getAllUniqueCauseCodeAndFailureClassByImsi_testNonExistingIMSI_returnsEmptyArrayOfObjects() {
        assertEquals(0, baseDataService.getAllUniqueCauseCodeAndFailureClassByImsi(new BigInteger("344930000000031")).size());
    }

    @Test
    public void getAllImsiByFailureClass_testValidData_returnsArrayOfObjects() {
        assertEquals(1, baseDataService.getAllImsiByFailureClass(0).size());
    }

    @Test
    public void getAllImsiByFailureClass_testNonExistingFailure_returnsEmptyArrayOfObjects() {
        assertEquals(0, baseDataService.getAllImsiByFailureClass(4).size());
    }

    @Test
    public void getAllImsiByFailureClass_testNonExistingFailure1_returnsEmptyArrayOfObjects() {
        assertEquals(0, baseDataService.getAllImsiByFailureClass(-1).size());
    }

    @Test
    public void getAllEventCauseByImsi_testValidIMSI_returnsArrayOfObjects() {
        assertEquals(1, baseDataService.getAllEventCauseByImsi(new BigInteger("344930000000011")).size());
    }

    @Test
    public void getAllEventCauseByImsi_testNonExistingIMSI_returnsEmptyArrayOfObjects() {
        assertEquals(0, baseDataService.getAllEventCauseByImsi(new BigInteger("344930000000031")).size());
    }

    @Test
    public void getAllImsiAndFailureIdBetweenPeriodTime_testValidData_returnsArrayOfObjects() throws Exception {
        assertEquals(3, baseDataService.getAllImsiBetweenPeriodTime(sdf.parse("22/09/1987 00:00"), sdf.parse("22/09/2015 00:00")).size());
    }

    @Test
    public void getAllImsiAndFailureIdBetweenPeriodTime_testNotExistingData_returnsEmptyArrayOfObjects() throws Exception {
        assertEquals(0, baseDataService.getAllImsiBetweenPeriodTime(sdf.parse("22/09/2014 00:00"), sdf.parse("22/09/2015 00:00")).size());
    }

    @Test
    public void getNumberOfFailuresByModelBetweenPeriodTime_testExistingModel_returnsArrayOfObjects() throws Exception {
        assertEquals(3,
                baseDataService.getNumberOfFailuresByModelBetweenPeriodTime("VEA3", sdf.parse("22/09/1987 00:00"), sdf.parse("22/09/2015 00:00")),
                0.0);
    }

    @Test
    public void getNumberOfFailuresByModelBetweenPeriodTime_testNonExistingModel_returnsEmptyArrayOfObjects() throws Exception {
        assertEquals(0,
                baseDataService.getNumberOfFailuresByModelBetweenPeriodTime("AS3", sdf.parse("22/09/1987 00:00"), sdf.parse("22/09/2015 00:00")), 0.0);
    }

    @Test
    public void getNumberOfFailuresByModelBetweenPeriodTime_testExistingModelWithEsxcludingDates_returnsEmptyArrayOfObjects() throws Exception {
        assertEquals(0,
                baseDataService.getNumberOfFailuresByModelBetweenPeriodTime("VEA3", sdf.parse("22/09/2014 00:00"), sdf.parse("22/09/2015 00:00")),
                0.0);
    }

    @Test
    public void getUniqureFailureIdEventCauseAndNumberOccurrencesByPhoneModel_testExistingModel_returnsArrayOfObjects() {
        assertEquals(1, baseDataService.getUniqueFailureIdEventCauseAndNumberOccurrencesByPhoneModel("VEA3").size());
    }

    @Test
    public void getUniqureFailureIdEventCauseAndNumberOccurrencesByPhoneModel_testEmptyStringModel_returnsEmptyArrayOfObjects() {
        assertEquals(0, baseDataService.getUniqueFailureIdEventCauseAndNumberOccurrencesByPhoneModel("").size());
    }

    @Test
    public void getUniqureFailureIdEventCauseAndNumberOccurrencesByPhoneModel_testNonExistingModel_returnsEmptyArrayOfObjects() {
        assertEquals(0, baseDataService.getUniqueFailureIdEventCauseAndNumberOccurrencesByPhoneModel("VEA4").size());
    }

    @Test
    public void getNumberOfFailuresByImsiBetweenPeriodTime_testExistImsiAndExistValidDates_returnsNumber() throws Exception {
        assertEquals(
                new Long(1),
                baseDataService.getNumberOfFailuresByImsiBetweenPeriodTime(new BigInteger("344930000000011"), sdf.parse("01/01/2012 00:00"),
                        sdf.parse("01/01/2015 00:00")).size(), 0.00001);
    }

    @Test
    public void getNumberOfFailuresByImsiBetweenPeriodTime_testExistImsiAndNoneExistValidDates_returnsZero() throws Exception {
        assertEquals(
                0,
                baseDataService.getNumberOfFailuresByImsiBetweenPeriodTime(new BigInteger("344930000000011"), sdf.parse("01/01/2014 00:00"),
                        sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getNumberOfFailuresByImsiBetweenPeriodTime_testInvalidImsi_returnsZero() throws Exception {
        assertEquals(
                0,
                baseDataService.getNumberOfFailuresByImsiBetweenPeriodTime(new BigInteger("-344930000000011"), sdf.parse("01/01/2012 00:00"),
                        sdf.parse("01/01/2015 00:00")).size(), 0.00001);
    }

    @Test
    public void getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime_testExistingDates_returnsArrayOfObjects() throws Exception {
        assertEquals(3,
                baseDataService.getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime(sdf.parse("01/01/2012 00:00"), sdf.parse("01/01/2015 00:00"))
                        .size());
    }

    @Test
    public void getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime_testNoneExistValidDates_returnsEmptyArrayOfObjects() throws Exception {
        assertEquals(0,
                baseDataService.getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime(sdf.parse("01/01/2014 00:00"), sdf.parse("01/01/2015 00:00"))
                        .size());
    }

    @Test
    public void getTop10ImsiBetweenPeriodTime_testExistingDates_returnsArrayOfObjects() throws Exception {
        assertEquals(1, baseDataService.getTop10ImsiBetweenPeriodTime(sdf.parse("01/01/2012 00:00"), sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getTop10ImsiBetweenPeriodTime_testNoneExistValidDates_returnsEmptyArrayOfObjects() throws Exception {
        assertEquals(0, baseDataService.getTop10ImsiBetweenPeriodTime(sdf.parse("01/01/2014 00:00"), sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllImsiByFailureClass_testInvalidFailureId_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0, baseDataService.getAllImsiByFailureClass(-1).size());
    }

    @Test
    public void getAllImsiByFailureClass_testExistFailureId_returnArrayOfObject() throws Exception {
        assertEquals(1, baseDataService.getAllImsiByFailureClass(0).size());
    }

    @Test
    public void getImsiForGivenDate_testExistingDates_returnsArrayOfObjects() throws Exception {
        assertEquals(1, baseDataService.getImsiForGivenDate(sdf.parse("01/01/2012 00:00"), sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getImsiForGivenDate_testNoneExistValidDates_returnsEmptyArrayOfObjects() throws Exception {
        assertEquals(0, baseDataService.getImsiForGivenDate(sdf.parse("01/01/2014 00:00"), sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getTotalNumberOfIMSIForAllFailureClasses_testValidDataAndNonExistingFailure_returnsArrayOfObjects() {
        Collection<FailureClass> failureClasses = new ArrayList<FailureClass>();
        failureClasses.add(failureClass);
        FailureClass invalid_failureClass = new FailureClass(-1);
        invalid_failureClass.setDescription("Invalid");
        failureClasses.add(invalid_failureClass);
        assertEquals(2, baseDataService.getTotalNumberOfIMSIForAllFailureClasses(failureClasses).size());
    }

    @Test
    public void getNumberOfFailureForEachFailureClassByEventCauseAndImsi_testInvalidEventId_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0, baseDataService.getNumberOfFailureForEachFailureClassByEventCauseAndImsi(4098, -1, new BigInteger("344930000000011")).size());
    }

    @Test
    public void getNumberOfFailureForEachFailureClassByEventCauseAndImsi_testInvalidCauseCode_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0, baseDataService.getNumberOfFailureForEachFailureClassByEventCauseAndImsi(-4098, 0, new BigInteger("344930000000011")).size());
    }

    @Test
    public void getNumberOfFailureForEachFailureClassByEventCauseAndImsi_testInvalidImsi_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0, baseDataService.getNumberOfFailureForEachFailureClassByEventCauseAndImsi(4098, 0, new BigInteger("344930000000012")).size());
    }

    @Test
    public void getNumberOfFailureForEachFailureClassByEventCauseAndImsi_testExistEventCauseAndImsi_returnArrayOfObject() throws Exception {
        assertEquals(1, baseDataService.getNumberOfFailureForEachFailureClassByEventCauseAndImsi(4098, 0, new BigInteger("344930000000011")).size());
    }

    @Test
    public void getNumberOfFailureForEachCellIdByEventCauseAndImsi_testInvalidEventId_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0, baseDataService.getNumberOfFailureForEachCellIdByEventCauseAndImsi(4098, -1, new BigInteger("344930000000011")).size());
    }

    @Test
    public void getNumberOfFailureForEachCellIdByEventCauseAndImsi_testInvalidCauseCode_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0, baseDataService.getNumberOfFailureForEachCellIdByEventCauseAndImsi(-4098, 0, new BigInteger("344930000000011")).size());
    }

    @Test
    public void getNumberOfFailureForEachCellIdByEventCauseAndImsi_testInvalidImsi_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0, baseDataService.getNumberOfFailureForEachCellIdByEventCauseAndImsi(4098, 0, new BigInteger("344930000000012")).size());
    }

    @Test
    public void getNumberOfFailureForEachCellIdByEventCauseAndImsi_testExistEventCauseAndImsi_returnArrayOfObject() throws Exception {
        assertEquals(3, baseDataService.getNumberOfFailureForEachCellIdByEventCauseAndImsi(4098, 0, new BigInteger("344930000000011")).size());
    }

    @Test
    public void getAllFailureByEventCauseCellIdAndImsi_testInvalidEventId_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0, baseDataService.getAllFailureByEventCauseCellIdAndImsi(4098, -1, new BigInteger("344930000000011"), 4).size());
    }

    @Test
    public void getAllFailureByEventCauseCellIdAndImsi_testInvalidCauseCode_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0, baseDataService.getAllFailureByEventCauseCellIdAndImsi(-4098, 0, new BigInteger("344930000000011"), 3).size());
    }

    @Test
    public void getAllFailureByEventCauseCellIdAndImsi_testInvalidImsi_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0, baseDataService.getAllFailureByEventCauseCellIdAndImsi(4098, 0, new BigInteger("344930000000012"), 2).size());
    }

    @Test
    public void getAllFailureByEventCauseCellIdAndImsi_testInvalidCellId_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0, baseDataService.getAllFailureByEventCauseCellIdAndImsi(4098, 0, new BigInteger("344930000000011"), -1).size());
    }

    @Test
    public void getAllFailureByEventCauseCellIdAndImsi_testExistEventCauseAndImsi_returnArrayOfObject() throws Exception {
        assertEquals(1, baseDataService.getAllFailureByEventCauseCellIdAndImsi(4098, 0, new BigInteger("344930000000011"), 4).size());
    }

    @Test
    public void getAllFailureByEventCauseFailureClassAndImsi_testInvalidEventId_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0, baseDataService.getAllFailureByEventCauseFailureClassAndImsi(4098, -1, new BigInteger("344930000000011"), "EMERGENCY").size());
    }

    @Test
    public void getAllFailureByEventCauseFailureClassAndImsi_testInvalidCauseCode_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0, baseDataService.getAllFailureByEventCauseFailureClassAndImsi(-4098, 0, new BigInteger("344930000000011"), "EMERGENCY").size());
    }

    @Test
    public void getAllFailureByEventCauseFailureClassAndImsi_testInvalidImsi_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0, baseDataService.getAllFailureByEventCauseFailureClassAndImsi(4098, 0, new BigInteger("344930000000012"), "EMERGENCY").size());
    }

    @Test
    public void getAllFailureByEventCauseFailureClassAndImsi_testInvalidFailureDescription_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0, baseDataService.getAllFailureByEventCauseFailureClassAndImsi(4098, 0, new BigInteger("344930000000011"), "INVALID").size());
    }

    @Test
    public void getAllFailureByEventCauseFailureClassAndImsi_testExistData_returnArrayOfObject() throws Exception {
        assertEquals(3, baseDataService.getAllFailureByEventCauseFailureClassAndImsi(4098, 0, new BigInteger("344930000000011"), "EMERGENCY").size());
    }

    @Test
    public void getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod_testInvalidImsi_returnEmptyArrayOfObject() throws Exception {
        assertEquals(
                0,
                baseDataService.getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod(new BigInteger("344930000000012"),
                        sdf.parse("01/01/2014 00:00"), sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod_testExistImsiAndDates_returnArrayOfObject() throws Exception {
        assertEquals(
                1,
                baseDataService.getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod(new BigInteger("344930000000011"),
                        sdf.parse("01/01/2012 00:00"), sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllFailureByEventCauseImsiBetweenTimePeriod_testInvalidImsi_returnEmptyArrayOfObject() throws Exception {
        assertEquals(
                0,
                baseDataService.getAllFailureByEventCauseImsiBetweenTimePeriod("S1 SIG CONN SETUP-SUCCESS", new BigInteger("344930000000012"),
                        sdf.parse("01/01/2012 00:00"), sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllFailureByEventCauseImsiBetweenTimePeriod_testInvalidEventCauseDescription_returnEmptyArrayOfObject() throws Exception {
        assertEquals(
                0,
                baseDataService.getAllFailureByEventCauseImsiBetweenTimePeriod("INVALID", new BigInteger("344930000000011"),
                        sdf.parse("01/01/2012 00:00"), sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllFailureByEventCauseImsiBetweenTimePeriod_testExistData_returnArrayOfObject() throws Exception {
        assertEquals(
                3,
                baseDataService.getAllFailureByEventCauseImsiBetweenTimePeriod("S1 SIG CONN SETUP-SUCCESS", new BigInteger("344930000000011"),
                        sdf.parse("01/01/2012 00:00"), sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod_testInvalidImsi_returnEmptyArrayOfObject() throws Exception {
        assertEquals(
                0,
                baseDataService.getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod(new BigInteger("344930000000012"),
                        sdf.parse("01/01/2014 00:00"), sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod_testExistImsiAndDates_returnArrayOfObject() throws Exception {
        assertEquals(
                1,
                baseDataService.getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod(new BigInteger("344930000000011"),
                        sdf.parse("01/01/2012 00:00"), sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllFailureByFailureClassImsiBetweenTimePeriod_testInvalidImsi_returnEmptyArrayOfObject() throws Exception {
        assertEquals(
                0,
                baseDataService.getAllFailureByFailureClassImsiBetweenTimePeriod("EMERGENCY", new BigInteger("344930000000012"),
                        sdf.parse("01/01/2012 00:00"), sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllFailureByFailureClassImsiBetweenTimePeriod_testInvalidFailureDescription_returnEmptyArrayOfObject() throws Exception {
        assertEquals(
                0,
                baseDataService.getAllFailureByFailureClassImsiBetweenTimePeriod("INVALID", new BigInteger("344930000000011"),
                        sdf.parse("01/01/2012 00:00"), sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllFailureByFailureClassImsiBetweenTimePeriod_testExistData_returnArrayOfObject() throws Exception {
        assertEquals(
                3,
                baseDataService.getAllFailureByFailureClassImsiBetweenTimePeriod("EMERGENCY", new BigInteger("344930000000011"),
                        sdf.parse("01/01/2012 00:00"), sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod_testInvalidPhoneModel_returnEmptyArrayOfObject() throws Exception {
        assertEquals(
                0,
                baseDataService.getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod("INVALID", sdf.parse("01/01/2014 00:00"),
                        sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod_testExistImsiAndDates_returnArrayOfObject() throws Exception {
        assertEquals(
                1,
                baseDataService.getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod("VEA3", sdf.parse("01/01/2012 00:00"),
                        sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllFailureByEventCausePhoneModelBetweenTimePeriod_testInvalidPhoneModel_returnEmptyArrayOfObject() throws Exception {
        assertEquals(
                0,
                baseDataService.getAllFailureByEventCausePhoneModelBetweenTimePeriod("S1 SIG CONN SETUP-SUCCESS", "INVALID",
                        sdf.parse("01/01/2012 00:00"), sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllFailureByEventCausePhoneModelBetweenTimePeriod_testInvalidEventCauseDescription_returnEmptyArrayOfObject() throws Exception {
        assertEquals(
                0,
                baseDataService.getAllFailureByEventCausePhoneModelBetweenTimePeriod("INVALID", "VEA3", sdf.parse("01/01/2012 00:00"),
                        sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllFailureByEventCausePhoneModelBetweenTimePeriod_testExistData_returnArrayOfObject() throws Exception {
        assertEquals(
                3,
                baseDataService.getAllFailureByEventCausePhoneModelBetweenTimePeriod("S1 SIG CONN SETUP-SUCCESS", "VEA3",
                        sdf.parse("01/01/2012 00:00"), sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod_testInvalidPhoneModel_returnEmptyArrayOfObject() throws Exception {
        assertEquals(
                0,
                baseDataService.getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod("INVALID", sdf.parse("01/01/2014 00:00"),
                        sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod_testExistImsiAndDates_returnArrayOfObject() throws Exception {
        assertEquals(
                1,
                baseDataService.getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod("VEA3", sdf.parse("01/01/2012 00:00"),
                        sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllFailureByFailureClassPhoneModelBetweenTimePeriod_testInvalidPhoneModel_returnEmptyArrayOfObject() throws Exception {
        assertEquals(
                0,
                baseDataService.getAllFailureByFailureClassPhoneModelBetweenTimePeriod("EMERGENCY", "INVALID", sdf.parse("01/01/2012 00:00"),
                        sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllFailureByFailureClassPhoneModelBetweenTimePeriod_testInvalidFailureDescription_returnEmptyArrayOfObject() throws Exception {
        assertEquals(
                0,
                baseDataService.getAllFailureByFailureClassPhoneModelBetweenTimePeriod("INVALID", "VEA3", sdf.parse("01/01/2012 00:00"),
                        sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllFailureByFailureClassPhoneModelBetweenTimePeriod_testExistData_returnArrayOfObject() throws Exception {
        assertEquals(
                3,
                baseDataService.getAllFailureByFailureClassPhoneModelBetweenTimePeriod("EMERGENCY", "VEA3", sdf.parse("01/01/2012 00:00"),
                        sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllFailureByImsiBetweenTimePeriod_testInvalidImsi_returnEmptyArrayOfObject() throws Exception {
        assertEquals(
                0,
                baseDataService.getAllFailureByImsiBetweenTimePeriod(new BigInteger("344930000000012"), sdf.parse("01/01/2014 00:00"),
                        sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllFailureByImsiBetweenTimePeriod_testExistImsiAndDates_returnArrayOfObject() throws Exception {
        assertEquals(
                3,
                baseDataService.getAllFailureByImsiBetweenTimePeriod(new BigInteger("344930000000011"), sdf.parse("01/01/2012 00:00"),
                        sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getAllUniqueIMSIs_returnArrayOfObject() throws Exception {
        assertEquals(1, baseDataService.getAllUniqueIMSIs().size());
    }

    @Test
    public void getFailureDescriptionBetweenPeriod_testInvalidImsi_returnEmptyArrayOfObject() throws Exception {
        assertEquals(
                0,
                baseDataService.getFailureDescriptionBetweenPeriod(new BigInteger("344930000000012"), sdf.parse("01/01/2014 00:00"),
                        sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getFailureDescriptionBetweenPeriod_testExistImsiAndDates_returnArrayOfObject() throws Exception {
        assertEquals(
                3,
                baseDataService.getFailureDescriptionBetweenPeriod(new BigInteger("344930000000011"), sdf.parse("01/01/2012 00:00"),
                        sdf.parse("01/01/2015 00:00")).size());
    }

    @Test
    public void getUniquePhones_returnArrayOfObject() throws Exception {
        assertEquals(1, baseDataService.getUniquePhones().size());
    }

    @Test
    public void getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode_testInvalidImsi_returnEmptyArrayOfObject() throws Exception {
        assertEquals(
                0,
                baseDataService.getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode(new BigInteger("344930000000012"), "EMERGENCY",
                        "S1 SIG CONN SETUP-SUCCESS").size());
    }

    @Test
    public void getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode_testInvalidEventCauseDescription_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0,
                baseDataService.getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode(new BigInteger("344930000000012"), "EMERGENCY", "INVALID")
                        .size());
    }

    @Test
    public void getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode_testInvalidFailureDescription_returnEmptyArrayOfObject() throws Exception {
        assertEquals(
                0,
                baseDataService.getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode(new BigInteger("344930000000012"), "INVALID",
                        "S1 SIG CONN SETUP-SUCCESS").size());
    }

    @Test
    public void getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode_testInvalidData_returnArrayOfObject() throws Exception {
        assertEquals(
                3,
                baseDataService.getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode(new BigInteger("344930000000011"), "EMERGENCY",
                        "S1 SIG CONN SETUP-SUCCESS").size());
    }

    @Test
    public void getTop10UniqueMarketOperatorAndCellIds_returnArrayOfObject() {
        assertEquals(3, baseDataService.getTop10UniqueMarketOperatorAndCellIds().size());
    }

    @Test
    public void getFailuresByPhoneModel_testInvalidPhoneModel_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0, baseDataService.getFailuresByPhoneModel("INVALID", 4098, 0).size());
    }

    @Test
    public void getFailuresByPhoneModel_testInvalidEventId_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0, baseDataService.getFailuresByPhoneModel("VEA3", -4098, 0).size());
    }

    @Test
    public void getFailuresByPhoneModel_testInvalidCauseCode_returnEmptyArrayOfObject() throws Exception {
        assertEquals(0, baseDataService.getFailuresByPhoneModel("VEA3", 4098, -1).size());
    }

    @Test
    public void getFailuresByPhoneModel_testValidData_returnEmptyArrayOfObject() throws Exception {
        assertEquals(3, baseDataService.getFailuresByPhoneModel("VEA3", 4098, 0).size());
    }

    @Test
    public void getFailuresForThisCell_testInvalidCellId() throws Exception {
        assertEquals(0, baseDataService.getFailuresForThisCell(-1, "Antigua and Barbuda"), 0.000001);
    }

    @Test
    public void getFailuresForThisCell_testInvalidMarket() throws Exception {
        assertEquals(0, baseDataService.getFailuresForThisCell(4, "INVALID"), 0.000001);
    }

    @Test
    public void getFailuresForThisCell_testValidData() throws Exception {
        assertEquals(1, baseDataService.getFailuresForThisCell(3, "Antigua and Barbuda"), 0.000001);
    }

    @Test
    public void getNumberOfFailuresForThisMarket_testInvalidMarket() throws Exception {
        assertEquals(0, baseDataService.getNumberOfFailuresForThisMarket("INVALID"), 0.00000000001);
    }

    @Test
    public void getNumberOfFailuresForThisMarket_testValidMarket() throws Exception {
        assertEquals(3, baseDataService.getNumberOfFailuresForThisMarket("Antigua and Barbuda"), 0.0000000001);
    }

    @Test
    public void getFailuresForThisCellCountryOperator_testInvalidCellId() throws Exception {
        assertEquals(0, baseDataService.getFailuresForThisCellCountryOperator(-1, "Antigua and Barbuda", "AT&T Wireless-Antigua AG"), 0.000001);
    }

    @Test
    public void getFailuresForThisCellCountryOperator_testInvalidMarket() throws Exception {
        assertEquals(0, baseDataService.getFailuresForThisCellCountryOperator(4, "INVALID", "AT&T Wireless-Antigua AG"), 0.000001);
    }

    @Test
    public void getFailuresForThisCellCountryOperator_testInvalidCountry() throws Exception {
        assertEquals(0, baseDataService.getFailuresForThisCellCountryOperator(4, "Antigua and Barbuda", "INVALID"), 0.000001);
    }

    // @Test
    // public void getFailuresForThisCellCountryOperator_testValidData() throws Exception{
    // assertEquals(1, baseDataService.getFailuresForThisCellCountryOperator(2, "Antigua and Barbuda", "AT&T Wireless-Antigua AG"), 0.000001);
    // }

}
