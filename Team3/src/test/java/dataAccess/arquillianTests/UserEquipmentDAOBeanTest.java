
package dataAccess.arquillianTests;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import model.BaseData;
import model.UserEquipment;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import restMapping.CellExtractor;
import restMapping.JaxRsActivator;
import serviceLayer.baseData.BaseDataServiceBean;
import serviceLayer.entityCache.EntityCache;
import serviceLayer.userEquipment.UserEquipmentService;
import dataAccess.baseData.BaseDataDAO;
import dataAccess.insertionConflict.InsertionConflictDAOBean;

@RunWith(Arquillian.class)
public class UserEquipmentDAOBeanTest {

    @PersistenceContext
    EntityManager em;
    @Inject
    UserTransaction utx;
    @EJB
    UserEquipmentService userEquipmentService;
    @EJB
    EntityCache cache;

    @Deployment
    public static Archive<?> createTestArchive() {
        PomEquippedResolveStage pom = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies();
        File[] libs = pom.resolve("org.apache.poi:poi").withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(InsertionConflictDAOBean.class.getPackage())
                .addPackage(BaseDataDAO.class.getPackage())
                .addPackage(BaseData.class.getPackage())
                .addPackage(CellExtractor.class.getPackage())
                .addPackage(JaxRsActivator.class.getPackage())
                .addPackage(BaseDataServiceBean.class.getPackage())
                .addPackage(DataFormatter.class.getPackage())
                .addPackage(HSSFRow.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"))
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("testSheet.xls");
        war.addAsLibraries(libs);
        System.out.println(war.toString(true));
        return war;
    }

    @Before
    public void setUp() throws Exception {
        for (Map.Entry<BigInteger, UserEquipment> entry : cache.getUserEquipmentMap().entrySet()) {
            userEquipmentService.removeUserEquipment(entry.getValue());
        }
    }

    @Test
    public void getAllUserEquipment_testWithRecordsPresent_returnsUserEuipment() throws NotSupportedException, SystemException, SecurityException,
            IllegalStateException, RollbackException, HeuristicMixedException, HeuristicRollbackException {
        UserEquipment userEquipment = getUserEquipment();
        utx.begin();
        em.joinTransaction();
        em.persist(userEquipment);
        utx.commit();
        ArrayList<UserEquipment> list = (ArrayList<UserEquipment>) userEquipmentService.getAllUserEquipment();
        assertEquals(userEquipment.hashCode(), list.get(0).hashCode());
        utx.begin();
        em.joinTransaction();
        em.createQuery("DELETE FROM UserEquipment").executeUpdate();
        utx.commit();
    }

    @Test
    public void getAllUserEquipment_testWithNoRecordsPresent_returnsNull() {
        assertEquals(0, userEquipmentService.getAllUserEquipment().size());
    }

    @Test
    public void getUserEquipmentById_testWithRecordsPresent_returnsUserEquipment() throws Exception {
        UserEquipment userEquipment = getUserEquipment();
        userEquipmentService.persistUserEquipment(userEquipment);
        assertEquals(userEquipment.hashCode(), userEquipmentService.getUserEquipmentById(new BigInteger("100100")).hashCode());
    }

    @Test
    public void getUserEquipmentById_testWithNoRecordsPresent_returnsNull() {
        assertEquals(null, userEquipmentService.getUserEquipmentById(new BigInteger("100100")));
    }

    @Test
    public void getUserEquipmentById_testWithNullId_returnsNull() {
        assertEquals(null, userEquipmentService.getUserEquipmentById(null));
    }

    @Test
    public void persistUserEquipment_testValidPersist_persistsToDatabaseAndToCache() {
        userEquipmentService.persistUserEquipment(getUserEquipment());
        ArrayList<UserEquipment> list = (ArrayList<UserEquipment>) em.createQuery("FROM UserEquipment u", UserEquipment.class).getResultList();
        assertEquals(list.get(0).hashCode(), getUserEquipment().hashCode());
        assertEquals(cache.getUserEquipment(new BigInteger("100100")).hashCode(), getUserEquipment().hashCode());
    }

    @Test
    public void persistUserEquipment_testNullValue_returnsNull() {
        userEquipmentService.persistUserEquipment(null);
        assertEquals(cache.getUserEquipmentMap().size(), 0);
    }

    @Test
    public void persistUserEquipments_testValidPersistFromCollection_persistsToDatabaseAndToCache() throws Exception {
        Collection<UserEquipment> list = new ArrayList<>();
        list.add(getUserEquipment());
        userEquipmentService.persistUserEquipments(list);
        ArrayList<UserEquipment> dbList = (ArrayList<UserEquipment>) em.createQuery("FROM UserEquipment u", UserEquipment.class).getResultList();
        assertEquals(cache.getUserEquipment(new BigInteger("100100")).hashCode(), getUserEquipment().hashCode());
        assertEquals(dbList.get(0).getManufacturer(), "Mitsubishi");
    }

    @Test
    public void persistUserEquipments_testNullCollection_doesNothing() throws Exception {
        Collection<UserEquipment> list = new ArrayList<>();
        userEquipmentService.persistUserEquipments(list);
        ArrayList<UserEquipment> dbList = (ArrayList<UserEquipment>) em.createQuery("FROM UserEquipment u", UserEquipment.class).getResultList();
        assertEquals(dbList.size(), 0);
        assertEquals(0, cache.getUserEquipmentMap().size());
    }

    private UserEquipment getUserEquipment() {
        UserEquipment userEquipment = new UserEquipment(new BigInteger("100100"));
        userEquipment.setMarketingName("G410");
        userEquipment.setManufacturer("Mitsubishi");
        userEquipment.setAccessCapability("GSM 1800,GSM 900");
        userEquipment.setModel("G410");
        userEquipment.setVendorName("Mitsubishi");
        userEquipment.setUserEquipmentType(null);
        userEquipment.setOperatingSystem(null);
        userEquipment.setInputMode(null);
        return userEquipment;
    }

}
