
package dataAccess.arquillianTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.EJB;

import model.BaseData;
import model.FailureClass;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import restMapping.CellExtractor;
import restMapping.JaxRsActivator;
import serviceLayer.baseData.BaseDataServiceBean;
import serviceLayer.failureClass.FailureClassService;
import dataAccess.baseData.BaseDataDAO;
import dataAccess.insertionConflict.InsertionConflictDAOBean;

@RunWith(Arquillian.class)
public class FailureClassTest {

    @EJB
    private FailureClassService failureClassService;

    private Collection<FailureClass> failureClassList = new ArrayList<FailureClass>();

    @Deployment
    public static Archive<?> createTestArchive() {
        PomEquippedResolveStage pom = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies();
        File[] libs = pom.resolve("org.apache.poi:poi").withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(InsertionConflictDAOBean.class.getPackage())
                .addPackage(BaseDataDAO.class.getPackage())
                .addPackage(BaseData.class.getPackage())
                .addPackage(CellExtractor.class.getPackage())
                .addPackage(JaxRsActivator.class.getPackage())
                .addPackage(BaseDataServiceBean.class.getPackage())
                .addPackage(DataFormatter.class.getPackage())
                .addPackage(HSSFRow.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"))
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("testSheet.xls");
        war.addAsLibraries(libs);
        System.out.println(war.toString(true));
        return war;
    }

    public FailureClass createFailureData(int failureId, String description) {
        FailureClass failureClass = new FailureClass(failureId);
        failureClass.setDescription(description);
        return failureClass;
    }

    @Before
    public void addFailureClassData() {
        removeFailureClassData();
        failureClassList.add(createFailureData(0, "EMERGENCY"));
        failureClassList.add(createFailureData(1, "HIGH PRIORITY ACCESS"));
        failureClassList.add(createFailureData(2, "MT ACCESS"));
        failureClassService.persistFailureClass(failureClassList);
    }

    @After
    public void removeFailureClassData() {
        failureClassList = failureClassService.getAllFailureClasses();
        for (FailureClass failureClass : failureClassList) {
            failureClassService.removeFailureClass(failureClass);
        }
    }

    @Test
    public void persistDataToFailureClassTest() {
        assertTrue(failureClassService.getAllFailureClasses().contains((FailureClass) failureClassList.toArray()[1]));
    }

    @Test
    public void persistInvalidDataToFailureClassTest() {
        FailureClass failureClass = (createFailureData(0, "Hi"));
        failureClassService.persistFailureClass(failureClass);
        assertEquals("EMERGENCY", failureClassService.getFailureClassById(0).getDescription());
    }

    @Test
    public void persistDuplicateDataToFailureClaa() {
        failureClassService.persistFailureClass((FailureClass) failureClassList.toArray()[2]);
        assertEquals(3, failureClassService.getAllFailureClasses().size());
    }

    @Test
    public void persistNullObject() {
        try {
            failureClassService.persistFailureClass(new FailureClass());
        } catch (Exception e) {
            assertEquals(3, failureClassService.getAllFailureClasses().size());
        }
    }

    @Test
    public void removeValidDataFromFailureClassTest() {
        failureClassService.removeFailureClass((FailureClass) failureClassList.toArray()[0]);
        assertEquals(2, failureClassService.getAllFailureClasses().size());
    }

    @Test
    public void removeInvalidDataFromFailureClass() {
        failureClassService.removeFailureClass(new FailureClass());
        assertEquals(3, failureClassService.getAllFailureClasses().size());
    }

    @Test
    public void mergeUpdateFailureClassTest() {
        FailureClass failureClass = (FailureClass) failureClassList.toArray()[1];
        failureClass.setDescription("test");
        failureClassService.mergeFailureClass(failureClass);
        assertEquals("test", failureClassService.getFailureClassById(failureClass.getFailureId()).getDescription());
    }

    @Test
    public void mergeAddNewFailureClassTest() {
        FailureClass newFailureClass = createFailureData(3, "MO SIGNALLING");
        failureClassService.mergeFailureClass(newFailureClass);
        assertEquals(4, failureClassService.getAllFailureClasses().size());
    }

    @Test
    public void mergeInvalidFailureClassTest() {
        failureClassService.mergeFailureClass(new FailureClass());
        assertEquals(3, failureClassService.getAllFailureClasses().size());
    }
}
