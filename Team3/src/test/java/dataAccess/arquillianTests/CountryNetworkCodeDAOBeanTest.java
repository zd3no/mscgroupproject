
package dataAccess.arquillianTests;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import model.BaseData;
import model.CountryNetworkCode;
import model.CountryNetworkCodePK;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import restMapping.CellExtractor;
import restMapping.JaxRsActivator;
import serviceLayer.baseData.BaseDataServiceBean;
import serviceLayer.countryNetworkCode.CountryNetworkCodeService;
import serviceLayer.entityCache.EntityCache;
import dataAccess.baseData.BaseDataDAO;
import dataAccess.insertionConflict.InsertionConflictDAOBean;

@RunWith(Arquillian.class)
public class CountryNetworkCodeDAOBeanTest {

    @PersistenceContext
    EntityManager em;
    @Inject
    UserTransaction utx;
    @EJB
    CountryNetworkCodeService countryNetworkCodeService;
    @EJB
    EntityCache cache;
    private static Collection<CountryNetworkCode> countryNetworkCodeList = new ArrayList<CountryNetworkCode>();
    private static boolean clearTable = false;

    @Deployment
    public static Archive<?> createTestArchive() {
        PomEquippedResolveStage pom = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies();
        File[] libs = pom.resolve("org.apache.poi:poi").withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(InsertionConflictDAOBean.class.getPackage())
                .addPackage(BaseDataDAO.class.getPackage())
                .addPackage(BaseData.class.getPackage())
                .addPackage(CellExtractor.class.getPackage())
                .addPackage(JaxRsActivator.class.getPackage())
                .addPackage(BaseDataServiceBean.class.getPackage())
                .addPackage(DataFormatter.class.getPackage())
                .addPackage(HSSFRow.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"))
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("testSheet.xls");
        war.addAsLibraries(libs);
        System.out.println(war.toString(true));
        return war;
    }

    @Before
    public void removeDataFromCountryNetworkCode() throws Exception {
        if (clearTable == false) {
            utx.begin();
            em.joinTransaction();
            em.createQuery("DELETE FROM CountryNetworkCode").executeUpdate();
            utx.commit();
            clearTable = true;
        }
        for (CountryNetworkCode countryNetworkCode : countryNetworkCodeList) {
            countryNetworkCodeService.removeCountryNetworkCode(countryNetworkCode);
        }
    }

    @Test
    public void getAllCountryNetworkCode_testWithRecordsPresent_returnsCountryNetworkCode() throws Exception {
        CountryNetworkCode code = createCountryNetworkCode(344, 930, "Belgium", "TDK");
        utx.begin();
        em.joinTransaction();
        em.persist(code);
        utx.commit();
        assertEquals(code.hashCode(), countryNetworkCodeService.getAllCountryNetworkCode().iterator().next().hashCode());
        utx.begin();
        em.joinTransaction();
        em.createQuery("DELETE FROM CountryNetworkCode").executeUpdate();
        utx.commit();
    }

    @Test
    public void getAllCountryNetworkCode_testWithNoRecordsPresent_returnsEmptyCollection() {
        assertEquals(0, countryNetworkCodeService.getAllCountryNetworkCode().size());
    }

    @Test
    public void persistCountryNetworkCode_testValidPersist_persistsToDatabaseAndToCache() {
        countryNetworkCodeService.persistCountryNetworkCode(createCountryNetworkCode(344, 930, "Belgium", "TDK"));
        ArrayList<CountryNetworkCode> list =
                (ArrayList<CountryNetworkCode>) em.createQuery("FROM CountryNetworkCode c", CountryNetworkCode.class).getResultList();
        assertEquals(list.get(0).hashCode(), countryNetworkCodeList.iterator().next().hashCode());
        assertEquals(cache.getCountryNetworkCode(344, 930).hashCode(), countryNetworkCodeList.iterator().next().hashCode());
    }

    @Test
    public void persistCountryNetworkCodes_testValidPersist_persistsToDatabaseAndToCache() throws Exception {
        createCountryNetworkCode(344, 930, "Belgium", "TDK");
        createCountryNetworkCode(500, 500, "Antigua", "ALSA");
        countryNetworkCodeService.persistCountryNetworkCodes(countryNetworkCodeList);
        ArrayList<CountryNetworkCode> dbList =
                (ArrayList<CountryNetworkCode>) em.createQuery("SELECT c FROM CountryNetworkCode c", CountryNetworkCode.class).getResultList();
        assertEquals(cache.getCountryNetworkCode(500, 500).getOperator(), "ALSA");
        assertEquals(dbList.get(1).getCountry(), "Antigua");
    }

    @Test
    public void testPersistValidCountryNetworkCode() {
        createCountryNetworkCode(344, 930, "Belgium", "TDK");
        createCountryNetworkCode(500, 500, "Antigua", "ALSA");
        countryNetworkCodeService.persistCountryNetworkCode(createCountryNetworkCode(344, 930, "Belgium", "TDK"));
        countryNetworkCodeService.persistCountryNetworkCode(createCountryNetworkCode(500, 500, "Antigua", "ALSA"));
        assertEquals(2, countryNetworkCodeService.getAllCountryNetworkCode().size());
    }

    @Test
    public void testPersistInvalidDataToCountryNeworkCode() {
        countryNetworkCodeService.persistCountryNetworkCode(new CountryNetworkCode());
        assertEquals(0, countryNetworkCodeService.getAllCountryNetworkCode().size());
    }

    @Test
    public void persitDuplicateDataToCountryNetworkCode() {
        CountryNetworkCode c1 = createCountryNetworkCode(500, 500, "O", "W");
        CountryNetworkCode c2 = createCountryNetworkCode(500, 500, "O", "W");
        countryNetworkCodeService.persistCountryNetworkCode(c1);
        countryNetworkCodeService.persistCountryNetworkCode(c2);
        assertEquals(1, countryNetworkCodeService.getAllCountryNetworkCode().size());
    }

    @Test
    public void testMergeUpdateCountryNetworkCode() {
        CountryNetworkCode c1 = createCountryNetworkCode(500, 500, "O", "W");
        countryNetworkCodeService.persistCountryNetworkCode(c1);
        c1.setCountry("Sweden");
        countryNetworkCodeService.mergeCountryNetworkCode(c1);
        assertEquals("Sweden", countryNetworkCodeService.getCountryNetworkCodeById(500, 500).getCountry());
    }

    @Test
    public void testMergeNewCountryNetworkCode() {
        CountryNetworkCode cnc = createCountryNetworkCode(340, 1, "Guadeloupe-France", "Orange-Caraibe");
        countryNetworkCodeService.mergeCountryNetworkCode(cnc);
        assertEquals(1, countryNetworkCodeService.getAllCountryNetworkCode().size());
    }

    @Test
    public void testMergeInvalidCountryNetworkCode() {
        countryNetworkCodeService.mergeCountryNetworkCode(new CountryNetworkCode());
        assertEquals(0, countryNetworkCodeService.getAllCountryNetworkCode().size());
    }

    @Test
    public void testMergeDuplicateCountryNetworkCode() {
        CountryNetworkCode c1 = createCountryNetworkCode(500, 500, "O", "W");
        CountryNetworkCode c2 = createCountryNetworkCode(500, 500, "O", "W");
        countryNetworkCodeService.mergeCountryNetworkCode(c1);
        countryNetworkCodeService.mergeCountryNetworkCode(c2);
        assertEquals(1, countryNetworkCodeService.getAllCountryNetworkCode().size());
    }

    @Test
    public void testRemoveValidCountryNetworkCode() {
        CountryNetworkCode c1 = createCountryNetworkCode(500, 500, "O", "W");
        countryNetworkCodeService.persistCountryNetworkCode(c1);
        countryNetworkCodeService.removeCountryNetworkCode(c1);
        assertEquals(0, countryNetworkCodeService.getAllCountryNetworkCode().size());
    }

    @Test
    public void testRemoveInvalidCountryNetworkCode() {
        CountryNetworkCode c1 = createCountryNetworkCode(500, 500, "O", "W");
        countryNetworkCodeService.persistCountryNetworkCode(c1);
        countryNetworkCodeService.removeCountryNetworkCode(new CountryNetworkCode());
        assertEquals(1, countryNetworkCodeService.getAllCountryNetworkCode().size());
    }

    public CountryNetworkCode createCountryNetworkCode(int mobileCountryCode,
            int mobileNetworkCode, String country, String operator) {
        CountryNetworkCodePK countryNetworkCodePk = new CountryNetworkCodePK(
                mobileCountryCode, mobileNetworkCode);
        CountryNetworkCode countryNetworkCode = new CountryNetworkCode(
                countryNetworkCodePk);
        countryNetworkCode.setCountry(country);
        countryNetworkCode.setOperator(operator);
        countryNetworkCodeList.add(countryNetworkCode);
        return countryNetworkCode;
    }

}
