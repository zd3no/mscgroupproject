
package dataAccess.arquillianTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import model.BaseData;
import model.UserEquipment;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import restMapping.CellExtractor;
import restMapping.JaxRsActivator;
import serviceLayer.baseData.BaseDataServiceBean;
import serviceLayer.entityCache.EntityCache;
import serviceLayer.userEquipment.UserEquipmentService;
import dataAccess.baseData.BaseDataDAO;
import dataAccess.insertionConflict.InsertionConflictDAOBean;

@RunWith(Arquillian.class)
public class UserEquipmentTest {

    @Deployment
    public static Archive<?> createTestArchive() {
        PomEquippedResolveStage pom = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies();
        File[] libs = pom.resolve("org.apache.poi:poi").withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(InsertionConflictDAOBean.class.getPackage())
                .addPackage(BaseDataDAO.class.getPackage())
                .addPackage(BaseData.class.getPackage())
                .addPackage(CellExtractor.class.getPackage())
                .addPackage(JaxRsActivator.class.getPackage())
                .addPackage(BaseDataServiceBean.class.getPackage())
                .addPackage(DataFormatter.class.getPackage())
                .addPackage(HSSFRow.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"))
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml");
        war.addAsLibraries(libs);
        System.out.println(war.toString(true));
        return war;
    }

    @EJB
    private UserEquipmentService userEquipmentService;
    @EJB
    EntityCache cache;
    @PersistenceContext
    EntityManager em;
    @Inject
    UserTransaction utx;
    private static boolean clearedTable = false;
    private static Collection<UserEquipment> userEquipmentList = new ArrayList<UserEquipment>();

    public UserEquipment createUserEquipmentAndAddToList(BigInteger typeAllocationCode, String marketingName,
            String manufacturer, String accessCapability, String model, String vendorName,
            String userEquipmentType, String operatingSystem, String inputMode) {
        UserEquipment userEquipment = new UserEquipment(typeAllocationCode);
        userEquipment.setMarketingName(marketingName);
        userEquipment.setManufacturer(manufacturer);
        userEquipment.setAccessCapability(accessCapability);
        userEquipment.setModel(model);
        userEquipment.setVendorName(vendorName);
        userEquipment.setUserEquipmentType(userEquipmentType);
        userEquipment.setOperatingSystem(operatingSystem);
        userEquipment.setInputMode(inputMode);
        userEquipmentList.add(userEquipment);
        return userEquipment;
    }

    public UserEquipment generateAnUEAndAddToList() {
        return createUserEquipmentAndAddToList(new BigInteger("100100"), "G410", "Mitsubishi",
                "GSM 1800, GSM 900", "G410", "Mitsubishi", "HANDHELD", "BLACKBERRY", "QWERTY");
    }

    @Before
    @After
    public void setUp() throws Exception {
        if (clearedTable == false) {
            utx.begin();
            em.joinTransaction();
            em.createQuery("DELETE FROM UserEquipment").executeUpdate();
            utx.commit();
            clearedTable = true;
        }
        System.out.println(em.createQuery("FROM UserEquipment u", UserEquipment.class).getResultList().size());
        for (UserEquipment userEquipment : userEquipmentList) {
            userEquipmentService.removeUserEquipment(userEquipment);
        }
    }

    @Test
    public void getAllUserEquipment_testDatabaseRetrievalWithExistingData_returnsNonNullCollection() throws Exception {
        UserEquipment ue = generateAnUEAndAddToList();
        utx.begin();
        em.joinTransaction();
        em.persist(ue);
        utx.commit();
        assertEquals(ue, userEquipmentService.getAllUserEquipment().iterator().next());
        utx.begin();
        em.joinTransaction();
        em.createQuery("DELETE FROM UserEquipment").executeUpdate();
        utx.commit();

    }

    @Test
    public void getAllUserEquipment_testWithNoRecordsPresent_returnsEmptyCollection() {
        try {
            setUp();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        assertEquals(0, userEquipmentService.getAllUserEquipment().size());
    }

    @Test
    public void persistUserEquipments_testValidPersist_AddsToDBAndCache() {
        userEquipmentService.persistUserEquipment(generateAnUEAndAddToList());
        ArrayList<UserEquipment> dbList = (ArrayList<UserEquipment>) em.createQuery("FROM UserEquipment u", UserEquipment.class).getResultList();
        assertEquals(1, dbList.size());
        assertEquals(new BigInteger("100100"), userEquipmentService.getAllUserEquipment().iterator().next().getTypeAllocationCode());
    }

    @Test
    public void persistUserEquipments_testInvalidPersist_doesntAddToCacheOrDB() {
        userEquipmentService.persistUserEquipment(new UserEquipment());
        ArrayList<UserEquipment> dbList = (ArrayList<UserEquipment>) em.createQuery("FROM UserEquipment u", UserEquipment.class).getResultList();
        assertEquals(0, dbList.size());
        assertEquals(0, userEquipmentService.getAllUserEquipment().size());
    }

    @Test
    public void persistUserEquipments_testNullPersist_doesntAddToCacheOrDB() {
        userEquipmentService.persistUserEquipment(null);
        ArrayList<UserEquipment> dbList = (ArrayList<UserEquipment>) em.createQuery("FROM UserEquipment u", UserEquipment.class).getResultList();
        assertEquals(0, dbList.size());
        assertEquals(0, userEquipmentService.getAllUserEquipment().size());
    }

    @Test
    public void getUserEquipmentById_testValidUE_returnsUserEquipment() {
        UserEquipment ue = generateAnUEAndAddToList();
        userEquipmentService.persistUserEquipment(ue);
        assertEquals(ue, userEquipmentService.getUserEquipmentById(ue.getTypeAllocationCode()));
    }

    @Test
    public void getUserEquipmentById_testEUNotPresentInCache_returnsNull() {
        userEquipmentService.persistUserEquipment(generateAnUEAndAddToList());
        UserEquipment ue = new UserEquipment(new BigInteger("35435646354"));
        assertEquals(null, userEquipmentService.getUserEquipmentById(ue.getTypeAllocationCode()));
    }

    @Test
    public void getUserEquipmentById_testNullTAC_returnsNull() {
        userEquipmentService.persistUserEquipment(generateAnUEAndAddToList());
        assertEquals(null, userEquipmentService.getUserEquipmentById(null));
    }

    @Test
    public void persistUserEquipment_testValidUEList_persistsToDBAndCache() {
        UserEquipment ue = generateAnUEAndAddToList();
        UserEquipment ue1 = new UserEquipment(new BigInteger("56565"));
        userEquipmentList.add(ue1);
        userEquipmentService.persistUserEquipments(userEquipmentList);
        ArrayList<UserEquipment> dbList = (ArrayList<UserEquipment>) em.createQuery("FROM UserEquipment u", UserEquipment.class).getResultList();
        assertEquals(cache.getUserEquipment(ue.getTypeAllocationCode()).getOperatingSystem(), "BLACKBERRY");
        assertEquals(null, dbList.get(1).getOperatingSystem());
    }

    @Test
    public void persistUserEquipment_testEmptyUEList_doesNotPersistAnything() {
        userEquipmentService.persistUserEquipments(new ArrayList<UserEquipment>());
        ArrayList<UserEquipment> dbList = (ArrayList<UserEquipment>) em.createQuery("FROM UserEquipment u", UserEquipment.class).getResultList();
        assertEquals(0, cache.getUserEquipmentMap().size());
        assertEquals(0, dbList.size());
    }

    @Test
    public void persistUserEquipment_testNullPersist_doesNotPersistAnything() {
        try {
            setUp();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        userEquipmentService.persistUserEquipments(null);
        ArrayList<UserEquipment> dbList = (ArrayList<UserEquipment>) em.createQuery("FROM UserEquipment u", UserEquipment.class).getResultList();
        assertEquals(0, cache.getUserEquipmentMap().size());
        assertEquals(0, dbList.size());
    }

    @Test
    public void removeUserEquipment_testValidRemoval_returnsTrue() {
        UserEquipment ue = generateAnUEAndAddToList();
        userEquipmentService.persistUserEquipment(ue);
        assertTrue(userEquipmentService.removeUserEquipment(ue));
        assertEquals(0, userEquipmentService.getAllUserEquipment().size());
    }

    @Test
    public void removeUserEquipment_testNullRemoval_returnsFalse() {
        UserEquipment ue = generateAnUEAndAddToList();
        userEquipmentService.persistUserEquipment(ue);
        assertFalse(userEquipmentService.removeUserEquipment(null));
    }

    @Test
    public void removeUserEquipment_testRemovalOfInexistentUserEquipment_returnsFalse() {
        UserEquipment ue = generateAnUEAndAddToList();
        assertFalse(userEquipmentService.removeUserEquipment(ue));
    }

    @Test
    public void mergeUserEquipment_testValidMerge_returnsTrue() {
        UserEquipment ue = generateAnUEAndAddToList();
        userEquipmentService.persistUserEquipment(ue);
        ue.setManufacturer("Hello");
        assertTrue(userEquipmentService.mergeUserEquipment(ue));
        assertEquals("Hello", cache.getUserEquipment(ue.getTypeAllocationCode()).getManufacturer());
    }

    @Test
    public void mergeUserEquipment_testNonExistentUserEquipmentMerge_returnsTrueAndPersistsInDB() {
        UserEquipment ue = generateAnUEAndAddToList();
        ue.setManufacturer("Hello");
        userEquipmentService.persistUserEquipment(ue);
        UserEquipment ue1 = new UserEquipment(new BigInteger("12"));
        ue1.setManufacturer("World");
        assertTrue(userEquipmentService.mergeUserEquipment(ue1));
        assertEquals("World", cache.getUserEquipment(ue1.getTypeAllocationCode()).getManufacturer());
    }

    @Test
    public void mergeUserEquipment_testNullUEMerge_returnsFalse() {
        assertFalse(userEquipmentService.mergeUserEquipment(null));
    }

    @Test
    public void mergeUserEquipment_testMergingUEWithNullID_returnsFalse() {
        assertFalse(userEquipmentService.mergeUserEquipment(new UserEquipment()));
    }
}
