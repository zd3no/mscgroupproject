
package dataAccess.arquillianTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import model.BaseData;
import model.EventCause;
import model.EventCausePK;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import restMapping.CellExtractor;
import restMapping.JaxRsActivator;
import serviceLayer.baseData.BaseDataServiceBean;
import serviceLayer.eventCause.EventCauseService;
import dataAccess.baseData.BaseDataDAO;
import dataAccess.insertionConflict.InsertionConflictDAOBean;

@RunWith(Arquillian.class)
public class EventCauseDAOBeanTest {

    @Deployment
    public static Archive<?> createTestArchive() {
        PomEquippedResolveStage pom = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies();
        File[] libs = pom.resolve("org.apache.poi:poi").withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(InsertionConflictDAOBean.class.getPackage())
                .addPackage(BaseDataDAO.class.getPackage())
                .addPackage(BaseData.class.getPackage())
                .addPackage(CellExtractor.class.getPackage())
                .addPackage(JaxRsActivator.class.getPackage())
                .addPackage(BaseDataServiceBean.class.getPackage())
                .addPackage(DataFormatter.class.getPackage())
                .addPackage(HSSFRow.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"))
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("testSheet.xls");
        war.addAsLibraries(libs);
        System.out.println(war.toString(true));
        return war;
    }

    @EJB
    private EventCauseService eventCauseService;
    @PersistenceContext
    EntityManager em;
    @Inject
    UserTransaction utx;

    private static Collection<EventCause> collectionList = new ArrayList<EventCause>();

    @Before
    public void setUp() throws Exception {
        for (EventCause eventCause : collectionList) {
            eventCauseService.removeEventCode(eventCause);
        }
        collectionList = new ArrayList<EventCause>();
    }

    @Test
    public void persistEventCode_testInsertingValidEventCause_persistToDBAndCache() throws Exception {
        EventCause ec = createEventCause(50, 50, "Hello");
        eventCauseService.persistEventCode(ec);
        utx.begin();
        em.joinTransaction();
        Collection<EventCause> list = em.createQuery("SELECT e FROM EventCause e", EventCause.class).getResultList();
        utx.commit();
        assertEquals(ec, eventCauseService.getEventCauseById(50, 50));
        assertTrue(list.contains(ec));
    }

    @Test
    public void persistEventCode_testInsertingTwoEventCausesWithSameID_willNotDuplicateExistingEventCauses() throws Exception {
        EventCause ec = createEventCause(50, 50, "Hello");
        EventCause ec1 = createEventCause(50, 50, "World");
        eventCauseService.persistEventCode(ec);
        eventCauseService.persistEventCode(ec1);
        utx.begin();
        em.joinTransaction();
        Collection<EventCause> list = em.createQuery("SELECT e FROM EventCause e", EventCause.class).getResultList();
        utx.commit();
        assertEquals(1, eventCauseService.getAllEventCauses().size());
        assertEquals(1, list.size());
    }

    @Test
    public void persistEventCode_testInsertingNull_doNothing() throws Exception {
        eventCauseService.persistEventCode(null);
        utx.begin();
        em.joinTransaction();
        Collection<EventCause> list = em.createQuery("SELECT e FROM EventCause e", EventCause.class).getResultList();
        utx.commit();
        assertEquals(null, eventCauseService.getEventCauseById(50, 50));
        assertEquals(0, list.size());
    }

    @Test
    public void persistEventCodes_testInsertingValidEventCauseList_persistToDBAndCache() throws Exception {
        EventCause ec = createEventCause(50, 50, "Hello");
        EventCause ec1 = createEventCause(60, 60, "World");
        Collection<EventCause> list = new ArrayList<EventCause>();
        list.add(ec);
        list.add(ec1);
        eventCauseService.persistEventCodes(list);
        utx.begin();
        em.joinTransaction();
        Collection<EventCause> dbList = em.createQuery("SELECT e FROM EventCause e", EventCause.class).getResultList();
        utx.commit();
        assertEquals(2, eventCauseService.getAllEventCauses().size());
        assertEquals(2, dbList.size());
    }

    @Test
    public void persistEventCodes_testPersistingNull_willNotPersistAnything() throws Exception {
        eventCauseService.persistEventCodes(null);
        assertEquals(0, eventCauseService.getAllEventCauses().size());
    }

    @Test
    public void getAllEventCauses_extractExistingDataFromDB_returnListOfEventCause() throws Exception {
        utx.begin();
        em.joinTransaction();
        em.persist(createEventCause(15, 15, "Hello"));
        utx.commit();
        assertEquals("Hello", eventCauseService.getAllEventCauses().iterator().next().getDescription());
        utx.begin();
        em.joinTransaction();
        em.createQuery("DELETE FROM EventCause").executeUpdate();
        utx.commit();
    }

    @Test
    public void getAllEventCauses_extractDataFromEmptyDB_returnEmptyListOfEventCause() {
        assertEquals(0, eventCauseService.getAllEventCauses().size());
    }

    @Test
    public void getEventCauseById_testWithExistingData_returnsEventCause() {
        EventCause ec = createEventCause(50, 50, "Hello");
        eventCauseService.persistEventCode(ec);
        assertEquals("Hello", eventCauseService.getEventCauseById(50, 50).getDescription());
    }

    @Test
    public void getEventCauseById_testNullEventCause_returnsNull() {
        assertEquals(null, eventCauseService.getEventCauseById(null, null));
    }

    @Test
    public void getEventCauseById_testNotExistentEventCause_returnsNull() {
        EventCause ec = createEventCause(50, 50, "Hello");
        eventCauseService.persistEventCode(ec);
        assertEquals(null, eventCauseService.getEventCauseById(4444, 4444));
    }

    @Test
    public void removeEventCode_testSuccessfulRemovalThatRemovesFromBothTheCacheAndDatabase_returnsTrue() throws Exception {
        EventCause ec = createEventCause(50, 50, "Hello");
        eventCauseService.persistEventCode(ec);
        assertTrue(eventCauseService.removeEventCode(ec));
        utx.begin();
        em.joinTransaction();
        Collection<EventCause> list = em.createQuery("SELECT e FROM EventCause e", EventCause.class).getResultList();
        utx.commit();
        assertEquals(0, list.size());
    }

    @Test
    public void removeEventCode_testUnsuccessfulRemoval_returnsFalse() {
        EventCause ec = createEventCause(50, 50, "Hello");
        assertFalse(eventCauseService.removeEventCode(ec));
    }

    @Test
    public void removeEventCode_testRemovalOfNullObject_returnsFalse() {
        assertFalse(eventCauseService.removeEventCode(null));
    }

    @Test
    public void mergeEventCode_testvalidUpdateOfEventCauseDescription_returnsTrue() {
        EventCause ec = createEventCause(50, 50, "Hello");
        eventCauseService.persistEventCode(ec);
        ec.setDescription("World");
        assertTrue(eventCauseService.mergeEventCode(ec) && eventCauseService.getEventCauseById(50, 50).getDescription().equals("World"));
    }

    @Test
    public void mergeEventCode_testMergingNull_returnsFalse() {
        assertFalse(eventCauseService.mergeEventCode(null));
    }

    public EventCause createEventCause(int causeCode, int eventId,
            String description) {
        EventCausePK eventCausePk = new EventCausePK(causeCode, eventId);
        EventCause eventCause = new EventCause(eventCausePk);
        eventCause.setDescription(description);
        collectionList.add(eventCause);
        return eventCause;
    }

}
