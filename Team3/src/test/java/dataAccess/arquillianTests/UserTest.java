
package dataAccess.arquillianTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import model.BaseData;
import model.User;
import model.UserType;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import restMapping.CellExtractor;
import restMapping.JaxRsActivator;
import serviceLayer.baseData.BaseDataServiceBean;
import serviceLayer.user.UserService;
import dataAccess.baseData.BaseDataDAO;
import dataAccess.insertionConflict.InsertionConflictDAOBean;

@RunWith(Arquillian.class)
public class UserTest {

    @Deployment
    public static Archive<?> createTestArchive() {
        PomEquippedResolveStage pom = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies();
        File[] libs = pom.resolve("org.apache.poi:poi").withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(InsertionConflictDAOBean.class.getPackage())
                .addPackage(BaseDataDAO.class.getPackage())
                .addPackage(BaseData.class.getPackage())
                .addPackage(CellExtractor.class.getPackage())
                .addPackage(JaxRsActivator.class.getPackage())
                .addPackage(BaseDataServiceBean.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"))
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("testSheet.xls");
        war.addAsLibraries(libs);
        System.out.println(war.toString(true));
        return war;
    }

    @EJB
    private UserService userService;
    @PersistenceContext
    EntityManager em;
    @Inject
    UserTransaction utx;
    private Collection<User> users = new ArrayList<User>();

    public User createUser(String id, String password, UserType userType, String name) {
        User user = new User(id);
        user.setName(name);
        user.setPassword(password);
        user.setUserType(userType);
        return user;
    }

    @Before
    public void addUsers() throws Exception {
        utx.begin();
        em.joinTransaction();
        em.createQuery("DELETE FROM User u").executeUpdate();
        utx.commit();
        users.add(createUser("user1", "password1", UserType.ADMINISTRATION, "name1"));
        users.add(createUser("user2", "password2", UserType.MANAGEMENT, "name2"));
        users.add(createUser("user3", "password3", UserType.SERVICE, "name3"));
        for (User user : users) {
            userService.persistenceUser(user);
        }
    }

    @After
    public void removeUsers() {
        users = userService.getAllUser();
        for (User user : users) {
            userService.removeUser(user);
        }
    }

    @Test
    public void testPersistValidUser() {
        assertEquals(3, userService.getAllUser().size());
    }

    @Test
    public void testPersistDuplicateUser() {
        User user = (User) users.toArray()[0];
        userService.persistenceUser(user);
        assertEquals(3, userService.getAllUser().size());
    }

    @Test
    public void testPersistInvalidUser() {
        userService.persistenceUser(new User());
        assertEquals(3, userService.getAllUser().size());
    }

    @Test
    public void testRemoveValidUser() {
        User user = userService.getUserById("user2");
        userService.removeUser(user);
        assertTrue(!userService.getAllUser().contains(user));
    }

    @Test
    public void testRemoveInvalidUser() {
        userService.removeUser(new User());
        assertEquals(3, userService.getAllUser().size());
    }

    @Test
    public void testMergeExistUser() {
        User user = userService.getUserById("user1");
        user.setPassword("test");
        userService.mergeUser(user);
        assertEquals("test", userService.getUserById("user1").getPassword());
    }

    @Test
    public void testMergeNewUser() {
        User newUser = createUser("user4", "password4", UserType.SUPPORT, "name4");
        userService.mergeUser(newUser);
        assertEquals(4, userService.getAllUser().size());
    }

    @Test
    public void testMergeInvalidUser() {
        userService.mergeUser(new User());
        assertEquals(3, userService.getAllUser().size());
    }

    @Test
    public void testMergeDuplicateUser() {
        userService.mergeUser((User) users.toArray()[1]);
        assertEquals(3, userService.getAllUser().size());
    }
}
