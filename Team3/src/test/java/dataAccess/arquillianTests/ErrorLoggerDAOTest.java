
package dataAccess.arquillianTests;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import model.BaseData;
import model.InsertionConflict;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import restMapping.CellExtractor;
import restMapping.JaxRsActivator;
import serviceLayer.baseData.BaseDataServiceBean;
import serviceLayer.insertionConflict.InsertionConflictService;
import dataAccess.baseData.BaseDataDAO;
import dataAccess.insertionConflict.InsertionConflictDAOBean;

@RunWith(Arquillian.class)
public class ErrorLoggerDAOTest {

    @PersistenceContext
    EntityManager em;
    @Inject
    UserTransaction utx;
    @EJB
    InsertionConflictService errorLoggerService;

    @Deployment
    public static Archive<?> createTestArchive() {
        PomEquippedResolveStage pom = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies();
        File[] libs = pom.resolve("org.apache.poi:poi").withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(InsertionConflictDAOBean.class.getPackage())
                .addPackage(BaseDataDAO.class.getPackage())
                .addPackage(BaseData.class.getPackage())
                .addPackage(CellExtractor.class.getPackage())
                .addPackage(JaxRsActivator.class.getPackage())
                .addPackage(BaseDataServiceBean.class.getPackage())
                .addPackage(DataFormatter.class.getPackage())
                .addPackage(HSSFRow.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"))
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("testSheet.xls");
        war.addAsLibraries(libs);
        System.out.println(war.toString(true));
        return war;
    }

    @Before
    public void setUp() throws Exception {
        utx.begin();
        em.joinTransaction();
        em.createQuery("DELETE FROM InsertionConflict i").executeUpdate();
        utx.commit();
    }

    @Test
    public void logValidationError_testValidInsert_insertsInDB() {
        InsertionConflict ic = new InsertionConflict();
        ic.setId(1);
        ic.setInputString("Hello");
        ic.setOriginTable("World");
        ic.setErrorDescription("None");
        errorLoggerService.logValidationError(ic);
        assertEquals("Hello", getCollectionsFromDB().iterator().next().getInputString());
    }

    @Test
    public void logValidationError_testInsertWithNoID_insertsInDBWithTheNextAvailableId() {
        InsertionConflict ic = new InsertionConflict();
        ic.setInputString("Hello");
        ic.setOriginTable("World");
        ic.setErrorDescription("None");
        errorLoggerService.logValidationError(ic);
        assertEquals(1, (int) getCollectionsFromDB().iterator().next().getId());
    }

    @Test
    public void logValidationError_testInsertValidCollection_insertsToDBWithCorrectIds() {
        InsertionConflict ic = new InsertionConflict();
        ic.setInputString("Hello");
        ic.setOriginTable("World");
        ic.setErrorDescription("None");
        InsertionConflict ic1 = new InsertionConflict();
        ic1.setInputString("Hello");
        ic1.setOriginTable("World");
        ic1.setErrorDescription("None");
        Collection<InsertionConflict> list = new ArrayList<>();
        list.add(ic);
        list.add(ic1);
        errorLoggerService.logValidationError(list);
        assertEquals(1, (int) getCollectionsFromDB().iterator().next().getId());
        assertEquals(2, (int) getCollectionsFromDB().size());
    }

    @Test
    public void logValidationError_testEmptyListInsert_doesNothing() {
        Collection<InsertionConflict> list = new ArrayList<>();
        errorLoggerService.logValidationError(list);
        assertEquals(0, (int) getCollectionsFromDB().size());
    }

    @Test
    public void getLastIdFromDatabase_testThatAValidNextIdIsReturned_returnsInteger() {
        InsertionConflict ic = new InsertionConflict();
        InsertionConflict ic1 = new InsertionConflict();
        errorLoggerService.logValidationError(ic);
        errorLoggerService.logValidationError(ic1);
        assertEquals(2, (int) ((InsertionConflict) getCollectionsFromDB().toArray()[1]).getId());
    }

    public Collection<InsertionConflict> getCollectionsFromDB() {
        Collection<InsertionConflict> list = null;
        try {
            utx.begin();
            em.joinTransaction();
            list = em.createQuery("FROM InsertionConflict i", InsertionConflict.class).getResultList();
            utx.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

}
