
package dataAccess.arquillianTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.math.BigInteger;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import model.BaseData;
import model.CountryNetworkCode;
import model.CountryNetworkCodePK;
import model.EventCause;
import model.EventCausePK;
import model.FailureClass;
import model.InsertionConflict;
import model.User;
import model.UserEquipment;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import restMapping.CellExtractor;
import restMapping.JaxRsActivator;
import serviceLayer.baseData.BaseDataServiceBean;
import serviceLayer.entityCache.EntityCache;
import dataAccess.baseData.BaseDataDAO;
import dataAccess.insertionConflict.InsertionConflictDAOBean;

@RunWith(Arquillian.class)
public class LookupEntityCacheTest {

    @PersistenceContext
    EntityManager em;
    @Inject
    UserTransaction utx;
    @EJB
    EntityCache cache;

    @Deployment
    public static Archive<?> createTestArchive() {
        PomEquippedResolveStage pom = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies();
        File[] libs = pom.resolve("org.apache.poi:poi").withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(InsertionConflictDAOBean.class.getPackage())
                .addPackage(BaseDataDAO.class.getPackage())
                .addPackage(BaseData.class.getPackage())
                .addPackage(CellExtractor.class.getPackage())
                .addPackage(JaxRsActivator.class.getPackage())
                .addPackage(BaseDataServiceBean.class.getPackage())
                .addPackage(DataFormatter.class.getPackage())
                .addPackage(HSSFRow.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"))
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("testSheet.xls");
        war.addAsLibraries(libs);
        System.out.println(war.toString(true));
        return war;
    }

    @Before
    public void setUp() throws Exception {
        ConcurrentHashMap<BigInteger, UserEquipment> ueMap = cache.getUserEquipmentMap();
        for (UserEquipment ue : ueMap.values()) {
            cache.remove(ue, UserEquipment.class);
        }
        ConcurrentHashMap<Integer, CountryNetworkCode> cncMap = cache.getCountryNetworkCodeMap();
        for (CountryNetworkCode cnc : cncMap.values()) {
            cache.remove(cnc, CountryNetworkCode.class);
        }
        ConcurrentHashMap<Integer, EventCause> ecMap = cache.getEventCauseMap();
        for (EventCause ec : ecMap.values()) {
            cache.remove(ec, EventCause.class);
        }
        ConcurrentHashMap<Integer, FailureClass> fcMap = cache.getFailureClassMap();
        for (FailureClass fc : fcMap.values()) {
            cache.remove(fc, FailureClass.class);
        }
        ConcurrentHashMap<String, User> userMap = cache.getUserMap();
        for (User user : userMap.values()) {
            cache.remove(user, User.class);
        }
    }

    @Test
    public void init_testThatPostConstructIsExecutedAndCachePopulated_populatesTheLookupEntitiesHashMaps() {
        assertEquals(0, cache.getFailureClassMap().size());
    }

    @Test
    public void persist_testPersistingAnInvalidClass_returnsFalse() {
        assertFalse(cache.persist("Hello", String.class));
    }

    @Test
    public void persist_testPersistingNullObject_returnsFalse() {
        assertFalse(cache.persist(null, null));
        assertFalse(cache.persist(null, String.class));
    }

    @Test
    public void persist_testPersistingValidUserEquipment_addsUserEquipmentToCacheAndToDatabase() throws Exception {
        UserEquipment ue = new UserEquipment(new BigInteger("50"));
        cache.persist(ue, UserEquipment.class);
        Collection<UserEquipment> list = null;
        utx.begin();
        em.joinTransaction();
        list = em.createQuery("FROM UserEquipment u", UserEquipment.class).getResultList();
        utx.commit();
        assertEquals(1, cache.getUserEquipmentMap().size());
        assertEquals(1, list.size());
    }

    @Test
    public void persist_testPersistingDuplicateValidUserEquipment_persistsOnlyTheFirstUE() {
        UserEquipment ue = new UserEquipment(new BigInteger("50"));
        ue.setModel("Iphone");
        UserEquipment ue1 = new UserEquipment(new BigInteger("50"));
        ue.setModel("HTC");
        cache.persist(ue, UserEquipment.class);
        cache.persist(ue1, UserEquipment.class);
        assertEquals(1, cache.getUserEquipmentMap().size());
        assertEquals("HTC", cache.getUserEquipment(new BigInteger("50")).getModel());
    }

    @Test
    public void persist_testPersistingNullUserEquipment_returnsFalseAndDoesNotAddAnythingToCache() {
        assertFalse(cache.persist(null, UserEquipment.class));
        assertEquals(0, cache.getUserEquipmentMap().size());
    }

    @Test
    public void persist_testPersistingUserEquipmentWithNoID_returnsFalseAndDoesNotAddAnythingToCache() {
        UserEquipment ue = new UserEquipment();
        assertFalse(cache.persist(ue, UserEquipment.class));
        assertEquals(0, cache.getUserEquipmentMap().size());
    }

    @Test
    public void persist_testPersistingValidCountryNetworkCode_addsCountryNetworkCodeToCacheAndToDatabase() throws Exception {
        CountryNetworkCodePK pk = new CountryNetworkCodePK(50, 50);
        CountryNetworkCode cnc = new CountryNetworkCode(pk);
        cache.persist(cnc, CountryNetworkCode.class);
        Collection<CountryNetworkCode> list = null;
        utx.begin();
        em.joinTransaction();
        list = em.createQuery("FROM CountryNetworkCode u", CountryNetworkCode.class).getResultList();
        utx.commit();
        assertEquals(1, cache.getCountryNetworkCodeMap().size());
        assertEquals(1, list.size());
    }

    @Test
    public void persist_testPersistingDuplicateValidCountryNetworkCode_persistsOnlyTheFirstUE() {
        CountryNetworkCodePK pk = new CountryNetworkCodePK(50, 50);
        CountryNetworkCode cnc = new CountryNetworkCode(pk);
        cnc.setCountry("Ireland");
        CountryNetworkCodePK pk1 = new CountryNetworkCodePK(50, 50);
        CountryNetworkCode cnc1 = new CountryNetworkCode(pk1);
        cnc1.setCountry("Bulgaria");
        cache.persist(cnc, CountryNetworkCode.class);
        cache.persist(cnc1, CountryNetworkCode.class);
        assertEquals(1, cache.getCountryNetworkCodeMap().size());
        assertEquals("Ireland", cache.getCountryNetworkCode(50, 50).getCountry());
    }

    @Test
    public void persist_testPersistingNullCountryNetworkCode_returnsFalseAndDoesNotAddAnythingToCache() {
        assertFalse(cache.persist(null, CountryNetworkCode.class));
        assertEquals(0, cache.getCountryNetworkCodeMap().size());
    }

    @Test
    public void persist_testPersistingCountryNetworkCodeWithNoID_returnsFalseAndDoesNotAddAnythingToCache() {
        CountryNetworkCode cnc = new CountryNetworkCode();
        assertFalse(cache.persist(cnc, CountryNetworkCode.class));
        assertEquals(0, cache.getCountryNetworkCodeMap().size());
    }

    @Test
    public void persist_testPersistingValidEventCause_addsEventCauseToCacheAndToDatabase() throws Exception {
        EventCausePK pk = new EventCausePK(50, 50);
        EventCause ec = new EventCause(pk);
        cache.persist(ec, EventCause.class);
        Collection<EventCause> list = null;
        utx.begin();
        em.joinTransaction();
        list = em.createQuery("FROM EventCause u", EventCause.class).getResultList();
        utx.commit();
        assertEquals(1, cache.getEventCauseMap().size());
        assertEquals(1, list.size());
    }

    @Test
    public void persist_testPersistingDuplicateValidEventCause_persistsOnlyTheFirstUE() {
        EventCausePK pk = new EventCausePK(50, 50);
        EventCause ec = new EventCause(pk);
        ec.setDescription("Peanut");
        EventCausePK pk1 = new EventCausePK(50, 50);
        EventCause ec1 = new EventCause(pk1);
        ec1.setDescription("Wallnut");
        cache.persist(ec, EventCause.class);
        cache.persist(ec1, EventCause.class);
        assertEquals(1, cache.getEventCauseMap().size());
        assertEquals("Peanut", cache.getEventCause(50, 50).getDescription());
    }

    @Test
    public void persist_testPersistingNullEventCause_returnsFalseAndDoesNotAddAnythingToCache() {
        assertFalse(cache.persist(null, EventCause.class));
        assertEquals(0, cache.getEventCauseMap().size());
    }

    @Test
    public void persist_testPersistingEventCauseWithNoID_returnsFalseAndDoesNotAddAnythingToCache() {
        EventCause ec = new EventCause();
        assertFalse(cache.persist(ec, EventCause.class));
        assertEquals(0, cache.getEventCauseMap().size());
    }

    @Test
    public void persist_testPersistingValidFailureClass_addsFailureClassToCacheAndToDatabase() throws Exception {
        FailureClass fc = new FailureClass(50);
        cache.persist(fc, FailureClass.class);
        Collection<FailureClass> list = null;
        utx.begin();
        em.joinTransaction();
        list = em.createQuery("FROM FailureClass u", FailureClass.class).getResultList();
        utx.commit();
        assertEquals(1, cache.getFailureClassMap().size());
        assertEquals(1, list.size());
    }

    @Test
    public void persist_testPersistingDuplicateValidFailureClass_persistsOnlyTheFirstfc() {
        FailureClass fc = new FailureClass(50);
        fc.setDescription("Pancake");
        FailureClass fc1 = new FailureClass(50);
        fc1.setDescription("Creppe");
        cache.persist(fc, FailureClass.class);
        cache.persist(fc1, FailureClass.class);
        assertEquals(1, cache.getFailureClassMap().size());
        assertEquals("Pancake", cache.getFailureClass(50).getDescription());
    }

    @Test
    public void persist_testPersistingNullFailureClass_returnsFalseAndDoesNotAddAnythingToCache() {
        assertFalse(cache.persist(null, FailureClass.class));
        assertEquals(0, cache.getFailureClassMap().size());
    }

    @Test
    public void persist_testPersistingFailureClassWithNoID_returnsFalseAndDoesNotAddAnythingToCache() {
        FailureClass fc = new FailureClass();
        assertFalse(cache.persist(fc, FailureClass.class));
        assertEquals(0, cache.getFailureClassMap().size());
    }

    @Test
    public void persist_testPersistingValidUser_addsUserToCacheAndToDatabase() throws Exception {
        User u = new User("Albert");
        cache.persist(u, User.class);
        Collection<User> list = null;
        utx.begin();
        em.joinTransaction();
        list = em.createQuery("FROM User u", User.class).getResultList();
        utx.commit();
        assertEquals(1, cache.getUserMap().size());
        assertEquals(1, list.size());
    }

    @Test
    public void persist_testPersistingDuplicateValidUser_persistsOnlyTheFirstUE() {
        User u = new User("Albert");
        u.setName("Cox");
        User u1 = new User("Albert");
        u1.setName("Malley");
        ;
        cache.persist(u, User.class);
        cache.persist(u1, User.class);
        assertEquals(1, cache.getUserMap().size());
        assertEquals("Cox", cache.getUser("Albert").getName());
    }

    @Test
    public void persist_testPersistingNullUser_returnsFalseAndDoesNotAddAnythingToCache() {
        assertFalse(cache.persist(null, User.class));
        assertEquals(0, cache.getUserMap().size());
    }

    @Test
    public void persist_testPersistingUserWithNoID_returnsFalseAndDoesNotAddAnythingToCache() {
        User u = new User();
        assertFalse(cache.persist(u, User.class));
        assertEquals(0, cache.getUserMap().size());
    }

    @Test
    public void remove_testRemovingAnInvalidClass_returnsFalse() {
        assertFalse(cache.remove("Hello", String.class));
    }

    @Test
    public void remove_testRemovingNullObject_returnsFalse() {
        assertFalse(cache.remove(null, null));
        assertFalse(cache.remove(null, String.class));
    }

    @Test
    public void remove_testRemovingValidUserEquipment_removesUserEquipmentFromCacheAndDatabase() throws Exception {
        UserEquipment ue = new UserEquipment(new BigInteger("50"));
        cache.persist(ue, UserEquipment.class);
        cache.remove(ue, UserEquipment.class);
        assertEquals(0, cache.getUserEquipmentMap().size());
        Collection<UserEquipment> list = null;
        utx.begin();
        em.joinTransaction();
        list = em.createQuery("FROM UserEquipment u", UserEquipment.class).getResultList();
        utx.commit();
        assertEquals(0, list.size());
    }

    @Test
    public void remove_testRemovalOfAValidUserEquipmentFromEmptyCache_returnsFalse() {
        UserEquipment ue = new UserEquipment(new BigInteger("50"));
        cache.persist(ue, UserEquipment.class);
        cache.remove(ue, UserEquipment.class);
        assertFalse(cache.remove(ue, UserEquipment.class));
    }

    @Test
    public void remove_testRemovalOfAValidUserEquipmentThatIsNotInTheCache_returnsFalse() {
        UserEquipment ue = new UserEquipment(new BigInteger("50"));
        UserEquipment ue1 = new UserEquipment(new BigInteger("60"));
        cache.persist(ue, UserEquipment.class);
        assertFalse(cache.remove(ue1, UserEquipment.class));
    }

    @Test
    public void remove_testRemovalOfNullUserEquipment_returnsFalse() {
        assertFalse(cache.remove(null, UserEquipment.class));
    }

    @Test
    public void remove_testRemovalOfUserEquipmentWithNoID_returnsFalseAndDoesNotRemoveAnythingToCache() {
        UserEquipment ue1 = new UserEquipment(new BigInteger("60"));
        UserEquipment ue = new UserEquipment();
        cache.persist(ue1, UserEquipment.class);
        assertFalse(cache.remove(ue, UserEquipment.class));
        assertTrue(cache.getUserEquipmentMap().contains(ue1));
    }

    @Test
    public void remove_testRemovingValidFailureClass_removesFailureClassFromCacheAndDatabase() throws Exception {
        FailureClass fc = new FailureClass(50);
        cache.persist(fc, FailureClass.class);
        cache.remove(fc, FailureClass.class);
        assertEquals(0, cache.getFailureClassMap().size());
        Collection<FailureClass> list = null;
        utx.begin();
        em.joinTransaction();
        list = em.createQuery("FROM FailureClass u", FailureClass.class).getResultList();
        utx.commit();
        assertEquals(0, list.size());
    }

    @Test
    public void remove_testRemovalOfAValidFailureClassFromEmptyCache_returnsFalse() {
        FailureClass fc = new FailureClass(50);
        cache.persist(fc, FailureClass.class);
        cache.remove(fc, FailureClass.class);
        assertFalse(cache.remove(fc, FailureClass.class));
    }

    @Test
    public void remove_testRemovalOfAValidFailureClassThatIsNotInTheCache_returnsFalse() {
        FailureClass fc = new FailureClass(50);
        FailureClass fc1 = new FailureClass(60);
        cache.persist(fc, FailureClass.class);
        assertFalse(cache.remove(fc1, FailureClass.class));
    }

    @Test
    public void remove_testRemovalOfNullFailureClass_returnsFalse() {
        assertFalse(cache.remove(null, FailureClass.class));
    }

    @Test
    public void remove_testRemovalOfFailureClassWithNoID_returnsFalseAndDoesNotRemoveAnythingToCache() {
        FailureClass fc1 = new FailureClass(60);
        FailureClass fc = new FailureClass();
        cache.persist(fc1, FailureClass.class);
        assertFalse(cache.remove(fc, FailureClass.class));
        assertTrue(cache.getFailureClassMap().contains(fc1));
    }

    @Test
    public void remove_testRemovingValidEventCause_removesEventCauseFromCacheAndDatabase() throws Exception {
        EventCausePK pk = new EventCausePK(50, 50);
        EventCause ec = new EventCause(pk);
        cache.persist(ec, EventCause.class);
        cache.remove(ec, EventCause.class);
        assertEquals(0, cache.getEventCauseMap().size());
        Collection<EventCause> list = null;
        utx.begin();
        em.joinTransaction();
        list = em.createQuery("FROM EventCause u", EventCause.class).getResultList();
        utx.commit();
        assertEquals(0, list.size());
    }

    @Test
    public void remove_testRemovalOfAValidEventCauseFromEmptyCache_returnsFalse() {
        EventCausePK pk = new EventCausePK(50, 50);
        EventCause ec = new EventCause(pk);
        cache.persist(ec, EventCause.class);
        cache.remove(ec, EventCause.class);
        assertFalse(cache.remove(ec, EventCause.class));
    }

    @Test
    public void remove_testRemovalOfAValidEventCauseThatIsNotInTheCache_returnsFalse() {
        EventCausePK pk = new EventCausePK(50, 50);
        EventCause ec = new EventCause(pk);
        EventCausePK pk1 = new EventCausePK(60, 60);
        EventCause ec1 = new EventCause(pk1);
        cache.persist(ec, EventCause.class);
        assertFalse(cache.remove(ec1, EventCause.class));
    }

    @Test
    public void remove_testRemovalOfNullEventCause_returnsFalse() {
        assertFalse(cache.remove(null, EventCause.class));
    }

    @Test
    public void remove_testRemovalOfEventCauseWithNoID_returnsFalseAndDoesNotRemoveAnythingToCache() {
        EventCausePK pk1 = new EventCausePK(50, 50);
        EventCause ec1 = new EventCause(pk1);
        EventCause ec = new EventCause();
        cache.persist(ec1, EventCause.class);
        assertFalse(cache.remove(ec, EventCause.class));
        assertTrue(cache.getEventCauseMap().contains(ec1));
    }

    @Test
    public void remove_testRemovingValidCountryNetworkCode_removesCountryNetworkCodeFromCacheAndDatabase() throws Exception {
        CountryNetworkCodePK pk = new CountryNetworkCodePK(50, 50);
        CountryNetworkCode cnc = new CountryNetworkCode(pk);
        cache.persist(cnc, CountryNetworkCode.class);
        cache.remove(cnc, CountryNetworkCode.class);
        assertEquals(0, cache.getCountryNetworkCodeMap().size());
        Collection<CountryNetworkCode> list = null;
        utx.begin();
        em.joinTransaction();
        list = em.createQuery("FROM CountryNetworkCode u", CountryNetworkCode.class).getResultList();
        utx.commit();
        assertEquals(0, list.size());
    }

    @Test
    public void remove_testRemovalOfAValidCountryNetworkCodeFromEmptyCache_returnsFalse() {
        CountryNetworkCodePK pk = new CountryNetworkCodePK(50, 50);
        CountryNetworkCode cnc = new CountryNetworkCode(pk);
        cache.persist(cnc, CountryNetworkCode.class);
        cache.remove(cnc, CountryNetworkCode.class);
        assertFalse(cache.remove(cnc, CountryNetworkCode.class));
    }

    @Test
    public void remove_testRemovalOfAValidCountryNetworkCodeThatIsNotInTheCache_returnsFalse() {
        CountryNetworkCodePK pk = new CountryNetworkCodePK(50, 50);
        CountryNetworkCode cnc = new CountryNetworkCode(pk);
        CountryNetworkCodePK pk1 = new CountryNetworkCodePK(60, 60);
        CountryNetworkCode cnc1 = new CountryNetworkCode(pk1);
        cache.persist(cnc, CountryNetworkCode.class);
        assertFalse(cache.remove(cnc1, CountryNetworkCode.class));
    }

    @Test
    public void remove_testRemovalOfNullCountryNetworkCode_returnsFalse() {
        assertFalse(cache.remove(null, CountryNetworkCode.class));
    }

    @Test
    public void remove_testRemovalOfCountryNetworkCodeWithNoID_returnsFalseAndDoesNotRemoveAnythingToCache() {
        CountryNetworkCodePK pk1 = new CountryNetworkCodePK(50, 50);
        CountryNetworkCode cnc1 = new CountryNetworkCode(pk1);
        CountryNetworkCode cnc = new CountryNetworkCode();
        cache.persist(cnc1, CountryNetworkCode.class);
        assertFalse(cache.remove(cnc, CountryNetworkCode.class));
        assertTrue(cache.getCountryNetworkCodeMap().contains(cnc1));
    }

    @Test
    public void remove_testRemovingValidUser_removesUserFromCacheAndDatabase() throws Exception {
        User user = new User("Ut30");
        cache.persist(user, User.class);
        cache.remove(user, User.class);
        assertEquals(0, cache.getUserMap().size());
        Collection<User> list = null;
        utx.begin();
        em.joinTransaction();
        list = em.createQuery("FROM User u", User.class).getResultList();
        utx.commit();
        assertEquals(0, list.size());
    }

    @Test
    public void remove_testRemovalOfAValidUserFromEmptyCache_returnsFalse() {
        User user = new User("Ut30");
        cache.persist(user, User.class);
        cache.remove(user, User.class);
        assertFalse(cache.remove(user, User.class));
    }

    @Test
    public void remove_testRemovalOfAValidUserThatIsNotInTheCache_returnsFalse() {
        User user = new User("Ut30");
        User user1 = new User("Ut50");
        cache.persist(user, User.class);
        assertFalse(cache.remove(user1, User.class));
    }

    @Test
    public void remove_testRemovalOfNullUser_returnsFalse() {
        assertFalse(cache.remove(null, User.class));
    }

    @Test
    public void remove_testRemovalOfUserWithNoID_returnsFalseAndDoesNotRemoveAnythingToCache() {
        User user1 = new User("Ut30");
        User user = new User();
        cache.persist(user1, User.class);
        assertFalse(cache.remove(user, User.class));
        assertTrue(cache.getUserMap().contains(user1));
    }

    @Test
    public void merge_testMergingAnInvalidClass_returnsFalse() {
        assertFalse(cache.merge("Hello", String.class));
    }

    @Test
    public void merge_testMergingNullObject_returnsFalse() {
        assertFalse(cache.merge(null, null));
        assertFalse(cache.merge(null, String.class));
    }

    @Test
    public void merge_testMergingValidUserEquipment_updatesUserEquipmentInCacheAndInDatabase() throws Exception {
        UserEquipment ue = new UserEquipment(new BigInteger("50"));
        ue.setModel("White");
        cache.persist(ue, UserEquipment.class);
        ue.setModel("Blue");
        cache.merge(ue, UserEquipment.class);
        assertEquals("Blue", cache.getUserEquipment(new BigInteger("50")).getModel());
        Collection<UserEquipment> list = null;
        utx.begin();
        em.joinTransaction();
        list = em.createQuery("FROM UserEquipment u", UserEquipment.class).getResultList();
        utx.commit();
        assertEquals("Blue", list.iterator().next().getModel());
    }

    @Test
    public void merge_testMergingAValidUserEquipmentToEmptyCache_returnsTrueAndPersistsToDB() {
        UserEquipment ue = new UserEquipment(new BigInteger("50"));
        assertTrue(cache.merge(ue, UserEquipment.class));
        assertEquals(1, cache.getUserEquipmentMap().size());
    }

    @Test
    public void merge_testMergingAValidUserEquipmentThatIsNotInTheCache_returnsTrueAndPersistsToDB() {
        UserEquipment ue = new UserEquipment(new BigInteger("50"));
        UserEquipment ue1 = new UserEquipment(new BigInteger("60"));
        cache.persist(ue, UserEquipment.class);
        assertTrue(cache.merge(ue1, UserEquipment.class));
        assertEquals(2, cache.getUserEquipmentMap().size());
    }

    @Test
    public void merge_testMergeNullUserEquipment_returnsFalse() {
        assertFalse(cache.merge(null, UserEquipment.class));
    }

    @Test
    public void merge_testMergeUserEquipmentWithNoID_returnsFalseAndDoesNotUpdateAnythingToCache() {
        UserEquipment ue1 = new UserEquipment(new BigInteger("60"));
        UserEquipment ue = new UserEquipment();
        cache.persist(ue1, UserEquipment.class);
        assertFalse(cache.merge(ue, UserEquipment.class));
        assertTrue(cache.getUserEquipmentMap().contains(ue1));
    }

    @Test
    public void merge_testMergingValidFailureClass_updatesFailureClassInCacheAndInDatabase() throws Exception {
        FailureClass fc = new FailureClass(50);
        fc.setDescription("White");
        cache.persist(fc, FailureClass.class);
        fc.setDescription("Blue");
        cache.merge(fc, FailureClass.class);
        assertEquals("Blue", cache.getFailureClass(50).getDescription());
        Collection<FailureClass> list = null;
        utx.begin();
        em.joinTransaction();
        list = em.createQuery("FROM FailureClass u", FailureClass.class).getResultList();
        utx.commit();
        assertEquals("Blue", list.iterator().next().getDescription());
    }

    @Test
    public void merge_testMergingAValidFailureClassToEmptyCache_returnsTrueAndPersistsToDB() {
        FailureClass fc = new FailureClass(50);
        assertTrue(cache.merge(fc, FailureClass.class));
        assertEquals(1, cache.getFailureClassMap().size());
    }

    @Test
    public void merge_testMergingAValidFailureClassThatIsNotInTheCache_returnsTrueAndPersistsToDB() {
        FailureClass fc = new FailureClass(50);
        FailureClass fc1 = new FailureClass(60);
        cache.persist(fc, FailureClass.class);
        assertTrue(cache.merge(fc1, FailureClass.class));
        assertEquals(2, cache.getFailureClassMap().size());
    }

    @Test
    public void merge_testMergeNullFailureClass_returnsFalse() {
        assertFalse(cache.merge(null, FailureClass.class));
    }

    @Test
    public void merge_testMergeFailureClassWithNoID_returnsFalseAndDoesNotUpdateAnythingToCache() {
        FailureClass fc1 = new FailureClass(50);
        FailureClass fc = new FailureClass();
        cache.persist(fc1, FailureClass.class);
        assertFalse(cache.merge(fc, FailureClass.class));
        assertTrue(cache.getFailureClassMap().contains(fc1));
    }

    @Test
    public void merge_testMergingValidUser_updatesUserInCacheAndInDatabase() throws Exception {
        User u = new User("gh45");
        u.setName("White");
        cache.persist(u, User.class);
        u.setName("Blue");
        cache.merge(u, User.class);
        assertEquals("Blue", cache.getUser("gh45").getName());
        Collection<User> list = null;
        utx.begin();
        em.joinTransaction();
        list = em.createQuery("FROM User u", User.class).getResultList();
        utx.commit();
        assertEquals("Blue", list.iterator().next().getName());
    }

    @Test
    public void merge_testMergingAValidUserToEmptyCache_returnsTrueAndPersistsToDB() {
        User u = new User("gh45");
        assertTrue(cache.merge(u, User.class));
        assertEquals(1, cache.getUserMap().size());
    }

    @Test
    public void merge_testMergingAValidUserThatIsNotInTheCache_returnsTrueAndPersistsToDB() {
        User u = new User("gh45");
        User u1 = new User("gh455");
        cache.persist(u, User.class);
        assertTrue(cache.merge(u1, User.class));
        assertEquals(2, cache.getUserMap().size());
    }

    @Test
    public void merge_testMergeNullUser_returnsFalse() {
        assertFalse(cache.merge(null, User.class));
    }

    @Test
    public void merge_testMergeUserWithNoID_returnsFalseAndDoesNotUpdateAnythingToCache() {
        User u1 = new User("gh45");
        User u = new User();
        cache.persist(u1, User.class);
        assertFalse(cache.merge(u, User.class));
        assertTrue(cache.getUserMap().contains(u1));
    }

    @Test
    public void merge_testMergingValidCountryNetworkCode_updatesCountryNetworkCodeInCacheAndInDatabase() throws Exception {
        CountryNetworkCodePK pk = new CountryNetworkCodePK(50, 50);
        CountryNetworkCode cnc = new CountryNetworkCode(pk);
        cnc.setCountry("Egypt");
        cache.persist(cnc, CountryNetworkCode.class);
        cnc.setCountry("Croatia");
        cache.merge(cnc, CountryNetworkCode.class);
        assertEquals("Croatia", cache.getCountryNetworkCode(50, 50).getCountry());
        Collection<CountryNetworkCode> list = null;
        utx.begin();
        em.joinTransaction();
        list = em.createQuery("FROM CountryNetworkCode u", CountryNetworkCode.class).getResultList();
        utx.commit();
        assertEquals("Croatia", list.iterator().next().getCountry());
    }

    @Test
    public void merge_testMergingAValidCountryNetworkCodeToEmptyCache_returnsTrueAndPersistsToDB() {
        CountryNetworkCodePK pk = new CountryNetworkCodePK(50, 50);
        CountryNetworkCode cnc = new CountryNetworkCode(pk);
        assertTrue(cache.merge(cnc, CountryNetworkCode.class));
        assertEquals(1, cache.getCountryNetworkCodeMap().size());
    }

    @Test
    public void merge_testMergingAValidCountryNetworkCodeThatIsNotInTheCache_returnsTrueAndPersistsToDB() {
        CountryNetworkCodePK pk = new CountryNetworkCodePK(50, 50);
        CountryNetworkCode cnc = new CountryNetworkCode(pk);
        CountryNetworkCodePK pk1 = new CountryNetworkCodePK(60, 60);
        CountryNetworkCode cnc1 = new CountryNetworkCode(pk1);
        cache.persist(cnc, CountryNetworkCode.class);
        assertTrue(cache.merge(cnc1, CountryNetworkCode.class));
        assertEquals(2, cache.getCountryNetworkCodeMap().size());
    }

    @Test
    public void merge_testMergeNullCountryNetworkCode_returnsFalse() {
        assertFalse(cache.merge(null, CountryNetworkCode.class));
    }

    @Test
    public void merge_testMergeCountryNetworkCodeWithNoID_returnsFalseAndDoesNotUpdateAnythingToCache() {
        CountryNetworkCodePK pk = new CountryNetworkCodePK(50, 50);
        CountryNetworkCode cnc1 = new CountryNetworkCode(pk);
        CountryNetworkCode cnc = new CountryNetworkCode();
        cache.persist(cnc1, CountryNetworkCode.class);
        assertFalse(cache.merge(cnc, CountryNetworkCode.class));
        assertTrue(cache.getCountryNetworkCodeMap().contains(cnc1));
    }

    @Test
    public void merge_testMergingValidEventCause_updatesEventCauseInCacheAndInDatabase() throws Exception {
        EventCausePK pk = new EventCausePK(50, 50);
        EventCause ec = new EventCause(pk);
        ec.setDescription("White");
        cache.persist(ec, EventCause.class);
        ec.setDescription("Black");
        cache.merge(ec, EventCause.class);
        assertEquals("Black", cache.getEventCause(50, 50).getDescription());
        Collection<EventCause> list = null;
        utx.begin();
        em.joinTransaction();
        list = em.createQuery("FROM EventCause u", EventCause.class).getResultList();
        utx.commit();
        assertEquals("Black", list.iterator().next().getDescription());
    }

    @Test
    public void merge_testMergingAValidEventCauseToEmptyCache_returnsTrueAndPersistsToDB() {
        EventCausePK pk = new EventCausePK(50, 50);
        EventCause ec = new EventCause(pk);
        assertTrue(cache.merge(ec, EventCause.class));
        assertEquals(1, cache.getEventCauseMap().size());
    }

    @Test
    public void merge_testMergingAValidEventCauseThatIsNotInTheCache_returnsTrueAndPersistsToDB() {
        EventCausePK pk = new EventCausePK(50, 50);
        EventCause ec = new EventCause(pk);
        EventCausePK pk1 = new EventCausePK(60, 60);
        EventCause ec1 = new EventCause(pk1);
        cache.persist(ec, EventCause.class);
        assertTrue(cache.merge(ec1, EventCause.class));
        assertEquals(2, cache.getEventCauseMap().size());
    }

    @Test
    public void merge_testMergeNullEventCause_returnsFalse() {
        assertFalse(cache.merge(null, EventCause.class));
    }

    @Test
    public void merge_testMergeEventCauseWithNoID_returnsFalseAndDoesNotUpdateAnythingToCache() {
        EventCausePK pk = new EventCausePK(50, 50);
        EventCause ec1 = new EventCause(pk);
        EventCause ec = new EventCause();
        cache.persist(ec1, EventCause.class);
        assertFalse(cache.merge(ec, EventCause.class));
        assertTrue(cache.getEventCauseMap().contains(ec1));
    }

    @Test
    public void resetInsertionConflicts_testThatInsertionConflictsAreResetToEmtyList_performsResetCorrectly() {
        InsertionConflict insertioNConflict = new InsertionConflict();
        insertioNConflict.setId(50);
        insertioNConflict.setErrorDescription("hello");
        cache.addInsertionConflict(insertioNConflict);
        cache.resetInsertionConflicts();
        assertEquals(0, cache.getInsertionConflicts().size());
    }

    @Test
    public void addInsertionConflict_testValidInsetionConflict_addsToList() {
        InsertionConflict insertioNConflict = new InsertionConflict();
        insertioNConflict.setId(50);
        insertioNConflict.setErrorDescription("hello");
        cache.addInsertionConflict(insertioNConflict);
        assertEquals(1, cache.getInsertionConflicts().size());
        cache.resetInsertionConflicts();
    }

}
