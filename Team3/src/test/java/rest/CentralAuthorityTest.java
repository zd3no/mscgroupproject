
package rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.HttpHeaders;

import model.BaseData;
import model.User;
import model.UserType;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.resteasy.util.Base64;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import restMapping.CellExtractor;
import restMapping.CentralAuthority;
import restMapping.JaxRsActivator;
import serviceLayer.baseData.BaseDataServiceBean;
import serviceLayer.entityCache.EntityCache;
import serviceLayer.user.UserService;
import dataAccess.baseData.BaseDataDAO;
import dataAccess.insertionConflict.InsertionConflictDAOBean;

@RunWith(Arquillian.class)
public class CentralAuthorityTest {

    @EJB
    CentralAuthority centralAuthority;
    @EJB
    UserService userService;
    @PersistenceContext
    EntityManager em;
    @Inject
    UserTransaction utx;
    @EJB
    EntityCache cache;

    @Deployment
    public static Archive<?> createTestArchive() {
        PomEquippedResolveStage pom = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies();
        File[] libs = pom.resolve("org.apache.poi:poi").withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(InsertionConflictDAOBean.class.getPackage())
                .addPackage(BaseDataDAO.class.getPackage())
                .addPackage(BaseData.class.getPackage())
                .addPackage(CellExtractor.class.getPackage())
                .addPackage(JaxRsActivator.class.getPackage())
                .addPackage(BaseDataServiceBean.class.getPackage())
                .addPackage(DataFormatter.class.getPackage())
                .addPackage(HSSFRow.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"))
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("testSheet.xls");
        war.addAsLibraries(libs);
        System.out.println(war.toString(true));
        return war;
    }

    @Before
    public void setUp() throws Exception {
        ConcurrentHashMap<String, User> userMap = cache.getUserMap();
        for (User user : userMap.values()) {
            cache.remove(user, User.class);
        }
        User user = new User("admin");
        user.setPassword("Y46UXStRp9HhuL0eu3Ws8JDVm3Y=");
        user.setToken("rageagainstthevirtualmachines");
        user.setUserType(UserType.ADMINISTRATION);
        userService.persistenceUser(user);
    }

    @After
    public void tearDown() throws Exception {
        ConcurrentHashMap<String, User> userMap = cache.getUserMap();
        for (User user : userMap.values()) {
            cache.remove(user, User.class);
        }
    }

    @Test
    public void makeUserWithPassword_testValidEncryption_returnsUser() {
        String garbage = new String(Base64.encodeBytes("userid:secretPassword".getBytes()));
        byte[] salt = String.valueOf(
                "AbCdEfGhIjKlMnOpRsTuVxQwZ0123456789").getBytes();
        int iteration = 1000;
        assertEquals("userid", centralAuthority.makeUserWithPassword(garbage).getId());
        assertEquals(centralAuthority.makeUserWithPassword(garbage).getPassword(), centralAuthority.getHash(iteration, "secretPassword", salt));
    }

    @Test
    public void makeUserWithPassword_testEncryptionWithMissingParts_returnsNull() {
        String garbage = new String(Base64.encodeBytes(":missing".getBytes()));
        assertEquals(null, centralAuthority.makeUserWithPassword(garbage));
    }

    @Test
    public void makeUserWithPassword_testInvalidEncryption_returnsNull() {
        String garbage = new String(Base64.encodeBytes("useridsecretPassword".getBytes()));
        assertEquals(null, centralAuthority.makeUserWithPassword(garbage));
    }

    @Test
    public void makeUserWithPassword_testValidEncryptionWithExistingUser_returnsNull() {
        String garbage = new String(Base64.encodeBytes("admin:Y46UXStRp9HhuL0eu3Ws8JDVm3Y=".getBytes()));
        assertEquals(null, centralAuthority.makeUserWithPassword(garbage));
    }

    @Test
    public void generateToken_testValidToken_returnsTokenString() {
        assertTrue(centralAuthority.generateToken("hello", "world").length() > 0);
    }

    @Test
    public void generateToken_testInvalidToken_returnsNull() {
        assertEquals(null, centralAuthority.generateToken("", ""));
    }

    @Test
    public void isAllowed_testIfAdminIsAllowedForLowerPermission_Allow() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        assertTrue(centralAuthority.isAllowed(headers, UserType.MANAGEMENT));
    }

    @Test
    public void isAllowed_testIfSupportEngineerIsAllowedForHigherPermission_Deny() {
        User user = new User("some");
        user.setPassword("user");
        user.setToken("token");
        user.setUserType(UserType.SUPPORT);
        userService.persistenceUser(user);
        HttpHeaders headers = new Headers("some", "token");
        assertFalse(centralAuthority.isAllowed(headers, UserType.ADMINISTRATION));
        userService.removeUser(user);
    }

    @Test
    public void isAllowed_testWithNullHeaders_Deny() {
        User user = new User("some");
        user.setPassword("user");
        user.setToken("token");
        user.setUserType(UserType.SUPPORT);
        userService.persistenceUser(user);
        assertFalse(centralAuthority.isAllowed(null, UserType.ADMINISTRATION));
        userService.removeUser(user);
    }

    @Test
    public void isAllowed_testWithNullHeaders_Densy() {
        User user = new User("some");
        user.setPassword("user");
        user.setToken("token");
        user.setUserType(UserType.SUPPORT);
        userService.persistenceUser(user);
        HttpHeaders headers = new Headers(null, null);
        assertFalse(centralAuthority.isAllowed(headers, UserType.ADMINISTRATION));
        userService.removeUser(user);
    }

    @Test
    public void isSuchUserInDBWithAccessRights_testValidUserWithIncorrectRights_returnFalse() {
        User user = new User("some");
        user.setPassword("user");
        user.setToken("token");
        user.setUserType(UserType.SUPPORT);
        userService.persistenceUser(user);
        assertFalse(centralAuthority.isSuchUserInDBWithAccessRights("some", "token", UserType.ADMINISTRATION));
        userService.removeUser(user);
    }

    @Test
    public void isSuchUserInDBWithAccessRights_testValidUserWithCorrectRights_returnTrue() {
        User user = new User("some");
        user.setPassword("user");
        user.setToken("token");
        user.setUserType(UserType.MANAGEMENT);
        userService.persistenceUser(user);
        assertTrue(centralAuthority.isSuchUserInDBWithAccessRights("some", "token", UserType.SUPPORT));
        userService.removeUser(user);
    }

    @Test
    public void isSuchUserInDBWithAccessRights_testInValidUser_returnFalse() {
        assertFalse(centralAuthority.isSuchUserInDBWithAccessRights("other", "one", UserType.SUPPORT));
    }

    @Test
    public void decryptUserSecret_testValidSecret_returnMap() {
        String garbage = new String(Base64.encodeBytes("asd:asd".getBytes()));
        assertEquals("asd", centralAuthority.decryptUserSecret(garbage).get("password"));
        assertEquals("asd", centralAuthority.decryptUserSecret(garbage).get("username"));
    }

    @Test
    public void decryptUserSecret_testInvalidString_returnNull() {
        String garbage = new String(Base64.encodeBytes("specialone".getBytes()));
        assertEquals(null, centralAuthority.decryptUserSecret(garbage));
    }

    @Test
    public void decryptUserSecret_testInvalidString1_returnNull() {
        String garbage = new String(Base64.encodeBytes(":".getBytes()));
        assertEquals(null, centralAuthority.decryptUserSecret(garbage));
    }

    @Test
    public void getUserWithSecret_testValidUser_returnsUser() {
        String encryptedCredentials = new String(Base64.encodeBytes("some:user".getBytes()));
        User user = new User("some");
        user.setPassword(centralAuthority.makeUserWithPassword(encryptedCredentials).getPassword());
        userService.persistenceUser(user);
        assertEquals(user, centralAuthority.getUserWithSecret(encryptedCredentials));
        userService.removeUser(user);
    }

    @Test
    public void getuserWithSecret_testInvalidUser_returnsNull() {
        String encryptedCredentials = new String(Base64.encodeBytes("some:user".getBytes()));
        assertEquals(null, centralAuthority.getUserWithSecret(encryptedCredentials));
    }

    @Test
    public void getuserWithSecret_testInvalidSecret_returnsNull() {
        String encryptedCredentials = new String(Base64.encodeBytes(":".getBytes()));
        assertEquals(null, centralAuthority.getUserWithSecret(encryptedCredentials));
    }

    @Test
    public void authenticateUser_testValidUser_returnToken() {
        String encryptedCredentials = new String(Base64.encodeBytes("admin:admin".getBytes()));
        User user = centralAuthority.authenticateUser(encryptedCredentials);
        assertTrue(user.getToken().length() > 0);
        assertEquals(user.getToken(), userService.getUserById("admin").getToken());

    }

    @Test
    public void authenticateUser_testInvalidUser_returnNull() {
        String encryptedCredentials = new String(Base64.encodeBytes("invaliduser".getBytes()));
        assertEquals(null, centralAuthority.authenticateUser(encryptedCredentials));
    }
}
