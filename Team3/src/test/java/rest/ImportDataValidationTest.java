
package rest;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.math.BigInteger;
import java.util.Collection;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import model.BaseData;
import model.CountryNetworkCode;
import model.CountryNetworkCodePK;
import model.EventCause;
import model.EventCausePK;
import model.FailureClass;
import model.InsertionConflict;
import model.UserEquipment;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import restMapping.CellExtractor;
import restMapping.ImportDataValidation;
import restMapping.JaxRsActivator;
import serviceLayer.baseData.BaseDataServiceBean;
import serviceLayer.entityCache.EntityCache;
import dataAccess.baseData.BaseDataDAO;
import dataAccess.insertionConflict.InsertionConflictDAOBean;

@RunWith(Arquillian.class)
public class ImportDataValidationTest {

    private HSSFSheet baseDataSheet;
    private HSSFSheet eventCauseSheet;
    private HSSFSheet testSheet;
    private HSSFSheet failureClassSheet;
    private HSSFSheet countryNetworkSheet;
    private HSSFWorkbook workbook;
    private HSSFSheet userEquipmentSheet;
    private CellExtractor cellextract = new CellExtractor();

    @Inject
    ImportDataValidation importDataValidation;
    @EJB
    EntityCache cache;
    @PersistenceContext
    EntityManager em;

    @Inject
    UserTransaction utx;

    @Deployment
    public static Archive<?> createTestArchive() {
        PomEquippedResolveStage pom = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies();
        File[] libs = pom.resolve("org.apache.poi:poi").withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(InsertionConflictDAOBean.class.getPackage())
                .addPackage(BaseDataDAO.class.getPackage())
                .addPackage(BaseData.class.getPackage())
                .addPackage(CellExtractor.class.getPackage())
                .addPackage(JaxRsActivator.class.getPackage())
                .addPackage(BaseDataServiceBean.class.getPackage())
                .addPackage(DataFormatter.class.getPackage())
                .addPackage(HSSFRow.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"))
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("testSheet.xls");
        war.addAsLibraries(libs);
        System.out.println(war.toString(true));
        return war;
    }

    @Before
    public void setUp() throws Exception {
        utx.begin();
        em.joinTransaction();
        em.createQuery("DELETE FROM InsertionConflict").executeUpdate();
        utx.commit();
        loadTestSheet();
    }

    @Test
    public void validateBaseDataRow_testValidRow_returnBaseData() {
        BaseData baseData = new BaseData();
        baseData.setTimeStamp(cellextract.getDateFromCell(baseDataSheet.getRow(1).getCell(0)));
        baseData.setCellId(4);
        baseData.setNetworkElementVersion("11B");
        baseData.setImsi(new BigInteger("344930000000011"));
        baseData.setHier3Id(new BigInteger("4809532081614990336"));
        baseData.setHier32Id(new BigInteger("8226896360947470336"));
        baseData.setHier321Id(new BigInteger("1150444940909479936"));
        baseData.setDuration(1000);
        baseData.setEventCause(null);
        baseData.setFailureClass(getFailureClass());
        baseData.setCountryNetworkCode(getCountryNetworkCode());
        baseData.setUserEquipment(getUE());
        cache.persist(getEventCause(), EventCause.class);
        cache.persist(getCountryNetworkCode(), CountryNetworkCode.class);
        cache.persist(getUE(), UserEquipment.class);
        cache.persist(getFailureClass(), FailureClass.class);
        assertEquals(baseData, importDataValidation.validateBaseDataRow(baseDataSheet.getRow(1)));
    }

    @Test
    public void validateBaseDataRow_testNullRow_returnsNull() {
        assertEquals(null, importDataValidation.validateBaseDataRow(null));
    }

    @Test
    public void validateBaseDataRow_testInvalidRow_returnsNullAndLogsToDatabase() {
        cache.resetInsertionConflicts();
        importDataValidation.validateBaseDataRow(testSheet.getRow(0));
        Collection<InsertionConflict> count = cache.getInsertionConflicts();
        assertEquals(1, count.size());
    }

    @Test
    public void validateUserEquipmentRow_testValidRow_returnUserEquipment() {
        UserEquipment ue = getUE();
        assertEquals(ue, importDataValidation.validateUserEquipmentRow(userEquipmentSheet.getRow(70)));
    }

    @Test
    public void validateUserEquipmentRow_testNullRow_returnsNull() {
        assertEquals(null, importDataValidation.validateUserEquipmentRow(null));
    }

    @Test
    public void validateUserEquipmentRow_testInvalidRow_returnsNull() {
        assertEquals(null, importDataValidation.validateUserEquipmentRow(testSheet.getRow(0)));
    }

    @Test
    public void validateUserEquipmentRow_testInvalidRow_logsError() {
        importDataValidation.validateUserEquipmentRow(testSheet.getRow(0));
        List<InsertionConflict> count = em.createQuery("SELECT i FROM InsertionConflict i", InsertionConflict.class).getResultList();
        assertEquals(1, count.size());
    }

    @Test
    public void validateEventCauseRow_testValidRow_returnEventCause() {
        EventCause ec = getEventCause();
        assertEquals(ec.hashCode(), importDataValidation.validateEventCauseRow(eventCauseSheet.getRow(18)).hashCode());
    }

    @Test
    public void validateEventCauseRow_testNullRow_returnsNull() {
        assertEquals(null, importDataValidation.validateEventCauseRow(null));
    }

    @Test
    public void validateEventCauseRow_testInvalidRow_returnsNullAndLogsToDatabase() {
        importDataValidation.validateEventCauseRow(testSheet.getRow(0));
        List<InsertionConflict> count = em.createQuery("SELECT i FROM InsertionConflict i", InsertionConflict.class).getResultList();
        assertEquals(1, count.size());
    }

    @Test
    public void validateFailureClassRow_testValidRow_returnFailureClass() {
        FailureClass fc = getFailureClass();
        assertEquals(fc.hashCode(), importDataValidation.validateFailureClassRow(failureClassSheet.getRow(2)).hashCode());
    }

    @Test
    public void validateFailureClassRow_testNullRow_returnsNull() {
        assertEquals(null, importDataValidation.validateFailureClassRow(null));
    }

    @Test
    public void validateFailureClassRow_testInvalidRow_returnsNullAndLogsToDatabase() {
        importDataValidation.validateFailureClassRow(testSheet.getRow(0));
        List<InsertionConflict> count = em.createQuery("SELECT i FROM InsertionConflict i", InsertionConflict.class).getResultList();
        assertEquals(1, count.size());
    }

    @Test
    public void validateCountryNetworkCodeRow_testValidRow_returnCountryNetworkCode() {
        CountryNetworkCode c = getCountryNetworkCode();
        assertEquals(c.hashCode(), importDataValidation.validateCountryNetworkCodeRow(countryNetworkSheet.getRow(27)).hashCode());
    }

    @Test
    public void validateCountryNetworkCodeRow_testNullRow_returnsNull() {
        assertEquals(null, importDataValidation.validateCountryNetworkCodeRow(null));
    }

    @Test
    public void validateCountryNetworkCodeRow_testInvalidRow_returnsNullAndLogsToDatabase() {
        importDataValidation.validateCountryNetworkCodeRow(testSheet.getRow(0));
        List<InsertionConflict> count = em.createQuery("SELECT i FROM InsertionConflict i", InsertionConflict.class).getResultList();
        assertEquals(1, count.size());
    }

    @Test
    public void generateInputString_testFullString_returnString() {
        String output = importDataValidation.generateInputString(null, 15, 15, new BigInteger("15"), 15, 15, 15, 15, 15, "ee",
                new BigInteger("15"), new BigInteger("15"), new BigInteger("15"), new BigInteger("15"));
        String compare = "DateTime: null Event Id: " + 15 +
                " Failure Class: " + 15 + " TAC: " + 15 +
                " MCC: " + 15 + " MNC: " + 15 + " Cell ID: " + 15 +
                " Duration: " + 15 + " Cause Code: " + 15 + " NE Version: ee IMSI: " + 15 + " HIER3_ID: " + 15 + " HIER32_ID :" + 15 +
                " HIER321_ID: " + 15;
        assertEquals(compare, output);
    }

    @Test
    public void generateInputString_testPartialString_returnString() {
        String output = importDataValidation.generateInputString(null, 15, 15, null, 15, 15, 15, 15, 15, "ee", null, null, null, null);
        String compare = "DateTime: null Event Id: " + 15 +
                " Failure Class: " + 15 + " TAC: null MCC: " + 15 + " MNC: " + 15 + " Cell ID: " + 15 +
                " Duration: " + 15 + " Cause Code: " + 15 + " NE Version: ee IMSI: null HIER3_ID: null HIER32_ID :null HIER321_ID: null";
        assertEquals(compare, output);
    }

    private CountryNetworkCode getCountryNetworkCode() {
        CountryNetworkCodePK pk = new CountryNetworkCodePK(344, 930);
        CountryNetworkCode c = new CountryNetworkCode(pk);
        c.setCountry("Antigua and Barbuda");
        c.setOperator("AT&T Wireless-Antigua AG ");
        return c;
    }

    private FailureClass getFailureClass() {
        FailureClass fc = new FailureClass(1);
        fc.setDescription("HIGH PRIORITY ACCESS");
        return fc;
    }

    private EventCause getEventCause() {
        EventCausePK pk = new EventCausePK(0, 4098);
        EventCause ec = new EventCause(pk);
        ec.setDescription("S1 SIG CONN SETUP-SUCCESS");
        return ec;
    }

    private UserEquipment getUE() {
        UserEquipment ue = new UserEquipment(new BigInteger("21060800"));
        ue.setMarketingName("VEA3");
        ue.setManufacturer("S.A.R.L. B  & B International");
        ue.setAccessCapability("GSM 1800, GSM 900");
        ue.setModel("VEA3");
        ue.setVendorName("S.A.R.L. B  & B International");
        return ue;
    }

    /**
     * Get the testSheet from resources folder to be used in this test.
     */
    private void loadTestSheet() {
        try {
            POIFSFileSystem fileStream = new POIFSFileSystem(this.getClass().getResourceAsStream("/testSheet.xls"));
            workbook = new HSSFWorkbook(fileStream);
            baseDataSheet = workbook.getSheet("Base Data");
            eventCauseSheet = workbook.getSheet("Event-Cause Table");
            failureClassSheet = workbook.getSheet("Failure Class Table");
            userEquipmentSheet = workbook.getSheet("UE Table");
            countryNetworkSheet = workbook.getSheet("MCC - MNC Table");
            testSheet = workbook.getSheetAt(5);
            workbook.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
