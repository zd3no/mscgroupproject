
package rest;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.math.BigInteger;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import model.BaseData;
import model.User;
import model.UserType;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import restMapping.CellExtractor;
import restMapping.CentralAuthority;
import restMapping.JaxRsActivator;
import restMapping.SupplementalInfo;
import serviceLayer.baseData.BaseDataServiceBean;
import serviceLayer.entityCache.EntityCache;
import serviceLayer.user.UserService;
import dataAccess.baseData.BaseDataDAO;
import dataAccess.insertionConflict.InsertionConflictDAOBean;

@RunWith(Arquillian.class)
public class SupplementalInfoTest {

    @EJB
    CentralAuthority centralAuthority;
    @EJB
    UserService userService;
    @PersistenceContext
    EntityManager em;
    @Inject
    UserTransaction utx;
    @Inject
    SupplementalInfo supplementalInfo;
    @EJB
    EntityCache cache;

    @Deployment
    public static Archive<?> createTestArchive() {
        PomEquippedResolveStage pom = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies();
        File[] libs = pom.resolve("org.apache.poi:poi").withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(InsertionConflictDAOBean.class.getPackage())
                .addPackage(BaseDataDAO.class.getPackage())
                .addPackage(BaseData.class.getPackage())
                .addPackage(CellExtractor.class.getPackage())
                .addPackage(JaxRsActivator.class.getPackage())
                .addPackage(BaseDataServiceBean.class.getPackage())
                .addPackage(DataFormatter.class.getPackage())
                .addPackage(HSSFRow.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"))
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("testSheet.xls");
        war.addAsLibraries(libs);
        System.out.println(war.toString(true));
        return war;
    }

    @Before
    public void setUp() throws Exception {
        ConcurrentHashMap<String, User> userMap = cache.getUserMap();
        for (User user : userMap.values()) {
            cache.remove(user, User.class);
        }
        User user = new User("admin");
        user.setPassword("admin");
        user.setToken("rageagainstthevirtualmachines");
        user.setUserType(UserType.ADMINISTRATION);
        userService.persistenceUser(user);
    }

    @After
    public void tearDown() throws Exception {
        ConcurrentHashMap<String, User> userMap = cache.getUserMap();
        for (User user : userMap.values()) {
            cache.remove(user, User.class);
        }
    }

    @Test
    public void getTotalNumberOfIMSIForAllFailureClasses_testValidHeader_returnsOkStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(pass.getStatus(), supplementalInfo.getTotalNumberOfIMSIForAllFailureClasses(headers).getStatus());
    }

    @Test
    public void getTotalNumberOfIMSIForAllFailureClasses_testInValidToken_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "invalid");
        Response fail = Response.ok().status(401).entity("FALSE").build();
        assertEquals(fail.getStatus(), supplementalInfo.getTotalNumberOfIMSIForAllFailureClasses(headers).getStatus());
    }

    @Test
    public void getTotalNumberOfIMSIForAllFailureClasses_testInvalidUser_returnsFalseStatus() {
        HttpHeaders headers = new Headers("network", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(401).entity("FALSE").build();
        assertEquals(fail.getStatus(), supplementalInfo.getTotalNumberOfIMSIForAllFailureClasses(headers).getStatus());
    }

    @Test
    public void getNumberOfFailureForEachFailureClassByEventCauseAndImsi_testValidHeaderAndData_returnsOkStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(pass.getStatus(),
                supplementalInfo.getNumberOfFailureForEachFailureClassByEventCauseAndImsi(headers, new BigInteger("344930000000011"), 4098, 0)
                        .getStatus());
    }

    @Test
    public void getNumberOfFailureForEachFailureClassByEventCauseAndImsi_testInvalidUser_returnsFalseStatus() {
        HttpHeaders headers = new Headers("network", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(401).entity("FALSE").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getNumberOfFailureForEachFailureClassByEventCauseAndImsi(headers, new BigInteger("344930000000011"), 4098, 0)
                        .getStatus());
    }

    @Test
    public void getNumberOfFailureForEachFailureClassByEventCauseAndImsi_testInvalidImsi_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("FALSE").build();
        assertEquals(fail.getStatus(), supplementalInfo.getNumberOfFailureForEachFailureClassByEventCauseAndImsi(headers, null, 4098, 0).getStatus());
    }

    @Test
    public void getNumberOfFailureForEachFailureClassByEventCauseAndImsi_testInvalidEventId_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("FALSE").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getNumberOfFailureForEachFailureClassByEventCauseAndImsi(headers, new BigInteger("344930000000011"), null, 0)
                        .getStatus());
    }

    @Test
    public void getNumberOfFailureForEachFailureClassByEventCauseAndImsi_testInvalidCauseCode_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("FALSE").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getNumberOfFailureForEachFailureClassByEventCauseAndImsi(headers, new BigInteger("344930000000011"), 4098, null)
                        .getStatus());
    }

    @Test
    public void getAllFailureByEventCauseFailureClassAndImsi_testValidHeaderAndData_returnsOkStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(pass.getStatus(),
                supplementalInfo.getAllFailureByEventCauseFailureClassAndImsi(headers, new BigInteger("344930000000011"), 4098, 0, "description")
                        .getStatus());
    }

    @Test
    public void getAllFailureByEventCauseFailureClassAndImsi_testInvalidUser_returnsFalseStatus() {
        HttpHeaders headers = new Headers("network", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(401).entity("FALSE").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getAllFailureByEventCauseFailureClassAndImsi(headers, new BigInteger("344930000000011"), 4098, 0, "description")
                        .getStatus());
    }

    @Test
    public void getAllFailureByEventCauseFailureClassAndImsi_testInvalidImsi_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("FALSE").build();
        assertEquals(fail.getStatus(), supplementalInfo.getAllFailureByEventCauseFailureClassAndImsi(headers, null, 4098, 0, "description")
                .getStatus());
    }

    @Test
    public void getAllFailureByEventCauseFailureClassAndImsi_testInvalidEventId_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("FALSE").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getAllFailureByEventCauseFailureClassAndImsi(headers, new BigInteger("344930000000011"), null, 0, "description")
                        .getStatus());
    }

    @Test
    public void ggetAllFailureByEventCauseFailureClassAndImsi_testInvalidCauseCode_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("FALSE").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getAllFailureByEventCauseFailureClassAndImsi(headers, new BigInteger("344930000000011"), 4098, null, "description")
                        .getStatus());
    }

    @Test
    public void ggetAllFailureByEventCauseFailureClassAndImsi_testInvalidFailureDescription_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("FALSE").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getAllFailureByEventCauseFailureClassAndImsi(headers, new BigInteger("344930000000011"), 4098, null, null)
                        .getStatus());
    }

    @Test
    public void getNumberOfFailureForEachCellIdByEventCauseAndImsi_testValidHeaderAndData_returnsOkStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(pass.getStatus(),
                supplementalInfo.getNumberOfFailureForEachCellIdByEventCauseAndImsi(headers, new BigInteger("344930000000011"), 4098, 0).getStatus());
    }

    @Test
    public void getNumberOfFailureForEachCellIdByEventCauseAndImsi_testInvalidUser_returnsFalseStatus() {
        HttpHeaders headers = new Headers("network", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(401).entity("FALSE").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getNumberOfFailureForEachCellIdByEventCauseAndImsi(headers, new BigInteger("344930000000011"), 4098, 0).getStatus());
    }

    @Test
    public void getNumberOfFailureForEachCellIdByEventCauseAndImsi_testInvalidImsi_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("FALSE").build();
        assertEquals(fail.getStatus(), supplementalInfo.getNumberOfFailureForEachCellIdByEventCauseAndImsi(headers, null, 4098, 0).getStatus());
    }

    @Test
    public void getNumberOfFailureForEachCellIdByEventCauseAndImsi_testInvalidEventId_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("FALSE").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getNumberOfFailureForEachCellIdByEventCauseAndImsi(headers, new BigInteger("344930000000011"), null, 0).getStatus());
    }

    @Test
    public void getNumberOfFailureForEachCellIdByEventCauseAndImsi_testInvalidCauseCode_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("FALSE").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getNumberOfFailureForEachCellIdByEventCauseAndImsi(headers, new BigInteger("344930000000011"), 4098, null)
                        .getStatus());
    }

    @Test
    public void getAllFailureByEventCauseCellIdAndImsi_testValidHeaderAndData_returnsOkStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(pass.getStatus(), supplementalInfo
                .getAllFailureByEventCauseCellIdAndImsi(headers, new BigInteger("344930000000011"), 4098, 0, 3).getStatus());
    }

    @Test
    public void getAllFailureByEventCauseCellIdAndImsi_testInvalidUser_returnsFalseStatus() {
        HttpHeaders headers = new Headers("network", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(401).entity("FALSE").build();
        assertEquals(fail.getStatus(), supplementalInfo
                .getAllFailureByEventCauseCellIdAndImsi(headers, new BigInteger("344930000000011"), 4098, 0, 3).getStatus());
    }

    @Test
    public void getAllFailureByEventCauseCellIdAndImsi_testInvalidImsi_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("FALSE").build();
        assertEquals(fail.getStatus(), supplementalInfo.getAllFailureByEventCauseCellIdAndImsi(headers, null, 4098, 0, 3).getStatus());
    }

    @Test
    public void getAllFailureByEventCauseCellIdAndImsi_testInvalidEventId_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("FALSE").build();
        assertEquals(fail.getStatus(), supplementalInfo
                .getAllFailureByEventCauseCellIdAndImsi(headers, new BigInteger("344930000000011"), null, 0, 3).getStatus());
    }

    @Test
    public void getAllFailureByEventCauseCellIdAndImsi_testInvalidCauseCode_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("FALSE").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getAllFailureByEventCauseCellIdAndImsi(headers, new BigInteger("344930000000011"), 4098, null, 3).getStatus());
    }

    @Test
    public void getAllFailureByEventCauseCellIdAndImsi_testInvalidCellIdreturnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("FALSE").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getAllFailureByEventCauseCellIdAndImsi(headers, new BigInteger("344930000000011"), 4098, 0, null).getStatus());
    }

    @Test
    public void getAllUniqueIMSI_testValidHeader_returnsOkStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(pass.getStatus(), supplementalInfo.getAllUniqueIMSI(headers).getStatus());
    }

    @Test
    public void getAllUniqueIMSI_testInValidToken_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "invalid");
        Response fail = Response.ok().status(401).entity("FALSE").build();
        assertEquals(fail.getStatus(), supplementalInfo.getAllUniqueIMSI(headers).getStatus());
    }

    @Test
    public void getAllUniqueIMSI_testInvalidUser_returnsFalseStatus() {
        HttpHeaders headers = new Headers("network", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(401).entity("FALSE").build();
        assertEquals(fail.getStatus(), supplementalInfo.getAllUniqueIMSI(headers).getStatus());
    }

    @Test
    public void getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode_testValidHeaderAndData_returnOkStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(
                pass.getStatus(),
                supplementalInfo.getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode(headers, new BigInteger("344930000000011"),
                        "failure description", "event cause description").getStatus());
    }

    @Test
    public void getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode_testInvalidHeader_returnsFalseStatus() {
        HttpHeaders headers = new Headers("invalid", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(401).entity("FALSE").build();
        assertEquals(
                pass.getStatus(),
                supplementalInfo.getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode(headers, new BigInteger("344930000000011"),
                        "failure description", "event cause description").getStatus());
    }

    @Test
    public void getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode_testInvalidImsi_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(404).entity("Passed").build();
        assertEquals(
                pass.getStatus(),
                supplementalInfo.getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode(headers, null, "failure description",
                        "event cause description").getStatus());
    }

    @Test
    public void getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode_testInvalidFailureDescription_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(404).entity("Passed").build();
        assertEquals(
                pass.getStatus(),
                supplementalInfo.getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode(headers, new BigInteger("344930000000011"), null,
                        "event cause description").getStatus());
    }

    @Test
    public void getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode_testInvalidEventCauseDescription_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(404).entity("Passed").build();
        assertEquals(
                pass.getStatus(),
                supplementalInfo.getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode(headers, new BigInteger("344930000000011"),
                        "failure description", null).getStatus());
    }

    @Test
    public void getFailureDescriptionBetweenPeriod_testValidData_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(
                pass.getStatus(),
                supplementalInfo.getFailureDescriptionBetweenPeriod(headers, new BigInteger("344930000000011"), "04/03/2015 16:31",
                        "04/03/2015 16:31").getStatus());
    }

    @Test
    public void getFailureDescriptionBetweenPeriod_testStringPassedAsDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getFailureDescriptionBetweenPeriod(headers, new BigInteger("344930000000011"), "sdfgdfg", "dfgfdg").getStatus());
    }

    @Test
    public void getFailureDescriptionBetweenPeriod_testInvalidUser_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getFailureDescriptionBetweenPeriod(headers, new BigInteger("344930000000011"), "22/09/1987 00:00",
                        "25/02/1977 00:00").getStatus());
    }

    @Test
    public void getFailureDescriptionBetweenPeriod_testInvalidDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getFailureDescriptionBetweenPeriod(headers, new BigInteger("344930000000011"), "50/03/2015 00:00",
                        "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getFailureDescriptionBetweenPeriod_testInvalidDatesWithWrongFormat_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getFailureDescriptionBetweenPeriod(headers, new BigInteger("344930000000011"), "20-03-2015 00:00",
                        "15-03-2015 00:00").getStatus());
    }

    @Test
    public void getFailureDescriptionBetweenPeriod_testInvalidImsi_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid data").build();
        assertEquals(fail.getStatus(), supplementalInfo.getFailureDescriptionBetweenPeriod(headers, null, "50/03/2015 00:00", "15/03/2015 00:00")
                .getStatus());
    }

    @Test
    public void getUniquePhones_testValidHeader_returnsOkStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(pass.getStatus(), supplementalInfo.getUniquePhones(headers).getStatus());
    }

    @Test
    public void getUniquePhones_testInValidToken_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "invalid");
        Response fail = Response.ok().status(401).entity("FALSE").build();
        assertEquals(fail.getStatus(), supplementalInfo.getUniquePhones(headers).getStatus());
    }

    @Test
    public void getUniquePhones_testInvalidUser_returnsFalseStatus() {
        HttpHeaders headers = new Headers("network", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(401).entity("FALSE").build();
        assertEquals(fail.getStatus(), supplementalInfo.getUniquePhones(headers).getStatus());
    }

    @Test
    public void getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod_testValidData_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(
                pass.getStatus(),
                supplementalInfo.getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"),
                        "04/03/2015 16:31", "04/03/2015 16:31").getStatus());
    }

    @Test
    public void getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod_testStringPassedAsDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), "sdfgdfg",
                        "dfgfdg").getStatus());
    }

    @Test
    public void getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod_testInvalidUser_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"),
                        "22/09/1987 00:00", "25/02/1977 00:00").getStatus());
    }

    @Test
    public void getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod_testInvalidDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"),
                        "50/03/2015 00:00", "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod_testInvalidDatesWithWrongFormat_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"),
                        "20-03-2015 00:00", "15-03-2015 00:00").getStatus());
    }

    @Test
    public void getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod_testInvalidImsi_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid data").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod(headers, null, "50/03/2015 00:00", "15/03/2015 00:00")
                        .getStatus());
    }

    @Test
    public void getAllFailureByEventCauseImsiBetweenTimePeriod_testValidData_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(
                pass.getStatus(),
                supplementalInfo.getAllFailureByEventCauseImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), "description",
                        "04/03/2015 16:31", "04/03/2015 16:31").getStatus());
    }

    @Test
    public void getAllFailureByEventCauseImsiBetweenTimePeriod_testStringPassedAsDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByEventCauseImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), "description", "sdfgdfg",
                        "dfgfdg").getStatus());
    }

    @Test
    public void getAllFailureByEventCauseImsiBetweenTimePeriod_testInvalidUser_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByEventCauseImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), "description",
                        "22/09/1987 00:00", "25/02/1977 00:00").getStatus());
    }

    @Test
    public void getAllFailureByEventCauseImsiBetweenTimePeriod_testInvalidDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByEventCauseImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), "description",
                        "50/03/2015 00:00", "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureByEventCauseImsiBetweenTimePeriod_testInvalidDatesWithWrongFormat_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByEventCauseImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), "description",
                        "20-03-2015 00:00", "15-03-2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureByEventCauseImsiBetweenTimePeriod_testInvalidImsi_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid data").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getAllFailureByEventCauseImsiBetweenTimePeriod(headers, null, "description", "50/03/2015 00:00", "15/03/2015 00:00")
                        .getStatus());
    }

    @Test
    public void getAllFailureByEventCauseImsiBetweenTimePeriod_testInvalidEventCauseDescription_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid data").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByEventCauseImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), null, "50/03/2015 00:00",
                        "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod_testValidData_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(
                pass.getStatus(),
                supplementalInfo.getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"),
                        "04/03/2015 16:31", "04/03/2015 16:31").getStatus());
    }

    @Test
    public void getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod_testStringPassedAsDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), "sdfgdfg",
                        "dfgfdg").getStatus());
    }

    @Test
    public void getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod_testInvalidUser_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"),
                        "22/09/1987 00:00", "25/02/1977 00:00").getStatus());
    }

    @Test
    public void getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod_testInvalidDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"),
                        "50/03/2015 00:00", "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod_testInvalidDatesWithWrongFormat_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"),
                        "20-03-2015 00:00", "15-03-2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod_testInvalidImsi_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid data").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod(headers, null, "50/03/2015 00:00", "15/03/2015 00:00")
                        .getStatus());
    }

    @Test
    public void getAllFailureByFailureClassImsiBetweenTimePeriod_testValidData_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(
                pass.getStatus(),
                supplementalInfo.getAllFailureByFailureClassImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), "description",
                        "04/03/2015 16:31", "04/03/2015 16:31").getStatus());
    }

    @Test
    public void getAllFailureByFailureClassImsiBetweenTimePeriod_testStringPassedAsDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByFailureClassImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), "description",
                        "sdfgdfg", "dfgfdg").getStatus());
    }

    @Test
    public void getAllFailureByFailureClassImsiBetweenTimePeriod_testInvalidUser_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByFailureClassImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), "description",
                        "22/09/1987 00:00", "25/02/1977 00:00").getStatus());
    }

    @Test
    public void getAllFailureByFailureClassImsiBetweenTimePeriod_testInvalidDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByFailureClassImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), "description",
                        "50/03/2015 00:00", "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureByFailureClassImsiBetweenTimePeriod_testInvalidDatesWithWrongFormat_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByFailureClassImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), "description",
                        "20-03-2015 00:00", "15-03-2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureByFailureClassImsiBetweenTimePeriod_testInvalidImsi_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid data").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByFailureClassImsiBetweenTimePeriod(headers, null, "description", "50/03/2015 00:00",
                        "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureByFailureClassImsiBetweenTimePeriod_testInvalidFailureDescription_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid data").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByFailureClassImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), null,
                        "50/03/2015 00:00", "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod_testValidData_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(
                pass.getStatus(),
                supplementalInfo.getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod(headers, "phone model", "04/03/2015 16:31",
                        "04/03/2015 16:31").getStatus());
    }

    @Test
    public void getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod_testStringPassedAsDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod(headers, "phone model", "sdfgdfg", "dfgfdg")
                        .getStatus());
    }

    @Test
    public void getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod_testInvalidUser_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod(headers, "phone model", "22/09/1987 00:00",
                        "25/02/1977 00:00").getStatus());
    }

    @Test
    public void getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod_testInvalidDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod(headers, "phone model", "50/03/2015 00:00",
                        "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod_testInvalidDatesWithWrongFormat_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod(headers, "phone model", "20-03-2015 00:00",
                        "15-03-2015 00:00").getStatus());
    }

    @Test
    public void getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod_testInvalidPhoneModel_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid data").build();
        assertEquals(fail.getStatus(),
                supplementalInfo
                        .getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod(headers, null, "50/03/2015 00:00", "15/03/2015 00:00")
                        .getStatus());
    }

    @Test
    public void getAllFailureByEventCausePhoneModelBetweenTimePeriod_testValidData_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(
                pass.getStatus(),
                supplementalInfo.getAllFailureByEventCausePhoneModelBetweenTimePeriod(headers, "phone model", "description", "04/03/2015 16:31",
                        "04/03/2015 16:31").getStatus());
    }

    @Test
    public void getAllFailureByEventCausePhoneModelBetweenTimePeriod_testStringPassedAsDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getAllFailureByEventCausePhoneModelBetweenTimePeriod(headers, "phone model", "description", "sdfgdfg", "dfgfdg")
                        .getStatus());
    }

    @Test
    public void getAllFailureByEventCausePhoneModelBetweenTimePeriod_testInvalidUser_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByEventCausePhoneModelBetweenTimePeriod(headers, "phone model", "description", "22/09/1987 00:00",
                        "25/02/1977 00:00").getStatus());
    }

    @Test
    public void getAllFailureByEventCausePhoneModelBetweenTimePeriod_testInvalidDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByEventCausePhoneModelBetweenTimePeriod(headers, "phone model", "description", "50/03/2015 00:00",
                        "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureByEventCausePhoneModelBetweenTimePeriod_testInvalidDatesWithWrongFormat_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByEventCausePhoneModelBetweenTimePeriod(headers, "phone model", "description", "20-03-2015 00:00",
                        "15-03-2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureByEventCausePhoneModelBetweenTimePeriod_testInvalidPhoneModel_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid data").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByEventCausePhoneModelBetweenTimePeriod(headers, null, "description", "50/03/2015 00:00",
                        "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureByEventCausePhoneModelBetweenTimePeriod_testInvalidEventCauseDescription_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid data").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByEventCausePhoneModelBetweenTimePeriod(headers, "phone model", null, "50/03/2015 00:00",
                        "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod_testValidData_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(
                pass.getStatus(),
                supplementalInfo.getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod(headers, "phone model", "04/03/2015 16:31",
                        "04/03/2015 16:31").getStatus());
    }

    @Test
    public void getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod_testStringPassedAsDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod(headers, "phone model", "sdfgdfg", "dfgfdg")
                        .getStatus());
    }

    @Test
    public void getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod_testInvalidUser_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod(headers, "phone model", "22/09/1987 00:00",
                        "25/02/1977 00:00").getStatus());
    }

    @Test
    public void getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod_testInvalidDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod(headers, "phone model", "50/03/2015 00:00",
                        "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod_testInvalidDatesWithWrongFormat_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod(headers, "phone model", "20-03-2015 00:00",
                        "15-03-2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod_testInvalidPhoneModel_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid data").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod(headers, null, "50/03/2015 00:00", "15/03/2015 00:00")
                        .getStatus());
    }

    @Test
    public void getAllFailureByFailureClassPhoneModelBetweenTimePeriod_testValidData_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(
                pass.getStatus(),
                supplementalInfo.getAllFailureByFailureClassPhoneModelBetweenTimePeriod(headers, "phone model", "description", "04/03/2015 16:31",
                        "04/03/2015 16:31").getStatus());
    }

    @Test
    public void getAllFailureByFailureClassPhoneModelBetweenTimePeriod_testStringPassedAsDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getAllFailureByFailureClassPhoneModelBetweenTimePeriod(headers, "phone model", "description", "sdfgdfg", "dfgfdg")
                        .getStatus());
    }

    @Test
    public void getAllFailureByFailureClassPhoneModelBetweenTimePeriod_testInvalidUser_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByFailureClassPhoneModelBetweenTimePeriod(headers, "phone model", "description", "22/09/1987 00:00",
                        "25/02/1977 00:00").getStatus());
    }

    @Test
    public void getAllFailureByFailureClassPhoneModelBetweenTimePeriod_testInvalidDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByFailureClassPhoneModelBetweenTimePeriod(headers, "phone model", "description", "50/03/2015 00:00",
                        "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureByFailureClassPhoneModelBetweenTimePeriod_testInvalidDatesWithWrongFormat_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByFailureClassPhoneModelBetweenTimePeriod(headers, "phone model", "description", "20-03-2015 00:00",
                        "15-03-2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureByFailureClassPhoneModelBetweenTimePeriod_testInvalidPhoneModel_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid data").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByFailureClassPhoneModelBetweenTimePeriod(headers, null, "description", "50/03/2015 00:00",
                        "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureByFailureClassPhoneModelBetweenTimePeriod_testInvalidFailureDescription_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid data").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByFailureClassPhoneModelBetweenTimePeriod(headers, "phone model", null, "50/03/2015 00:00",
                        "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getFailuresByPhoneModel_testValidHeaderAndData_returnsOkStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(pass.getStatus(), supplementalInfo.getFailuresByPhoneModel(headers, "phone model", 4098, 0).getStatus());
    }

    @Test
    public void getFailuresByPhoneModel_testInvalidUser_returnsFalseStatus() {
        HttpHeaders headers = new Headers("network", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(401).entity("FALSE").build();
        assertEquals(fail.getStatus(), supplementalInfo.getFailuresByPhoneModel(headers, "phone model", 4098, 0).getStatus());
    }

    @Test
    public void getFailuresByPhoneModel_testInvalidPhoneModel_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("FALSE").build();
        assertEquals(fail.getStatus(), supplementalInfo.getFailuresByPhoneModel(headers, null, 4098, 0).getStatus());
    }

    @Test
    public void getFailuresByPhoneModel_testInvalidEventId_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("FALSE").build();
        assertEquals(fail.getStatus(), supplementalInfo.getFailuresByPhoneModel(headers, "phone model", null, 0).getStatus());
    }

    @Test
    public void getFailuresByPhoneModel_testInvalidCauseCode_returnsFalseStatus() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("FALSE").build();
        assertEquals(fail.getStatus(), supplementalInfo.getFailuresByPhoneModel(headers, "phone model", 4098, null).getStatus());
    }

    @Test
    public void getAllFailureByImsiBetweenTimePeriod_testValidData_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(
                pass.getStatus(),
                supplementalInfo.getAllFailureByImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), "04/03/2015 16:31",
                        "04/03/2015 16:31").getStatus());
    }

    @Test
    public void getAllFailureByImsiBetweenTimePeriod_testStringPassedAsDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(fail.getStatus(),
                supplementalInfo.getAllFailureByImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), "sdfgdfg", "dfgfdg").getStatus());
    }

    @Test
    public void getAllFailureByImsiBetweenTimePeriod_testInvalidUser_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), "22/09/1987 00:00",
                        "25/02/1977 00:00").getStatus());
    }

    @Test
    public void getAllFailureByImsiBetweenTimePeriod_testInvalidDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), "50/03/2015 00:00",
                        "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureByImsiBetweenTimePeriod_testInvalidDatesWithWrongFormat_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(
                fail.getStatus(),
                supplementalInfo.getAllFailureByImsiBetweenTimePeriod(headers, new BigInteger("344930000000011"), "20-03-2015 00:00",
                        "15-03-2015 00:00").getStatus());
    }

    @Test
    public void getAllFailureByImsiBetweenTimePeriod_testInvalidImsi_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid data").build();
        assertEquals(fail.getStatus(), supplementalInfo.getAllFailureByImsiBetweenTimePeriod(headers, null, "50/03/2015 00:00", "15/03/2015 00:00")
                .getStatus());
    }

    @Test
    public void getNumberOfFailuresForThisMarketCellOperator_testValidData_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(200).entity("Passed").build();
        assertEquals(pass.getStatus(), supplementalInfo.getNumberOfFailuresForThisMarketCellOperator(headers, "market", 3, "operator").getStatus());
    }

    @Test
    public void getNumberOfFailuresForThisMarketCellOperator_testInvalidUser_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(fail.getStatus(), supplementalInfo.getNumberOfFailuresForThisMarketCellOperator(headers, "market", 3, "operator").getStatus());
    }

    @Test
    public void getNumberOfFailuresForThisMarketCellOperator_testInvalidMarket_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(404).entity("Passed").build();
        assertEquals(pass.getStatus(), supplementalInfo.getNumberOfFailuresForThisMarketCellOperator(headers, null, 3, "operator").getStatus());
    }

    @Test
    public void getNumberOfFailuresForThisMarketCellOperator_testInvalidCellId_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(404).entity("Passed").build();
        assertEquals(pass.getStatus(), supplementalInfo.getNumberOfFailuresForThisMarketCellOperator(headers, "market", null, "operator").getStatus());
    }

    @Test
    public void getNumberOfFailuresForThisMarketCellOperator_testInvalidOperator_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response pass = Response.ok().status(404).entity("Passed").build();
        assertEquals(pass.getStatus(), supplementalInfo.getNumberOfFailuresForThisMarketCellOperator(headers, "market", 3, null).getStatus());
    }

}
