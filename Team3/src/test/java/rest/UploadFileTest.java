
package rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import model.BaseData;
import model.CountryNetworkCode;
import model.EventCause;
import model.FailureClass;
import model.InsertionConflict;
import model.UserEquipment;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import restMapping.CellExtractor;
import restMapping.ImportDataValidation;
import restMapping.JaxRsActivator;
import restMapping.UploadFile;
import serviceLayer.baseData.BaseDataService;
import serviceLayer.baseData.BaseDataServiceBean;
import serviceLayer.countryNetworkCode.CountryNetworkCodeService;
import serviceLayer.entityCache.EntityCache;
import serviceLayer.eventCause.EventCauseService;
import serviceLayer.failureClass.FailureClassService;
import serviceLayer.userEquipment.UserEquipmentService;
import dataAccess.baseData.BaseDataDAO;
import dataAccess.insertionConflict.InsertionConflictDAOBean;

@RunWith(Arquillian.class)
public class UploadFileTest {

    private HSSFSheet baseDataSheet;
    private HSSFSheet eventCauseSheet;
    private HSSFSheet failureClassSheet;
    private HSSFSheet countryNetworkSheet;
    private HSSFWorkbook workbook;
    private HSSFSheet userEquipmentSheet;
    @Inject
    ImportDataValidation importDataValidation;
    @Inject
    UploadFile uploadFile;
    @EJB
    EntityCache cache;
    @PersistenceContext
    EntityManager em;
    @Inject
    UserTransaction utx;
    @EJB
    private EventCauseService eventCauseService;
    @EJB
    private UserEquipmentService userEquipmentService;
    @EJB
    private FailureClassService failureClassService;
    @EJB
    private BaseDataService baseDataService;
    @EJB
    private CountryNetworkCodeService countryNetworkCodeService;

    @Deployment
    public static Archive<?> createTestArchive() {
        PomEquippedResolveStage pom = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies();
        File[] libs = pom.resolve("org.apache.poi:poi").withTransitivity().asFile();
        File[] libs1 = pom.resolve("org.apache.httpcomponents:httpmime:4.4").withTransitivity().asFile();
        File[] libs2 = pom.resolve("org.apache.httpcomponents:httpclient:4.4").withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(InsertionConflictDAOBean.class.getPackage())
                .addPackage(BaseDataDAO.class.getPackage())
                .addPackage(BaseData.class.getPackage())
                .addPackage(CellExtractor.class.getPackage())
                .addPackage(JaxRsActivator.class.getPackage())
                .addPackage(BaseDataServiceBean.class.getPackage())
                .addPackage(DataFormatter.class.getPackage())
                .addPackage(HSSFRow.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"))
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("testSheet.xls");
        war.addAsLibraries(libs);
        war.addAsLibraries(libs1);
        war.addAsLibraries(libs2);
        System.out.println(war.toString(true));
        return war;
    }

    @Before
    public void setUp() throws Exception {
        loadTestSheet();
    }

    @After
    public void clearTrace() throws Exception {
        for (Map.Entry<Integer, EventCause> entry : cache.getEventCauseMap().entrySet()) {
            eventCauseService.removeEventCode(entry.getValue());
        }
        for (Entry<BigInteger, UserEquipment> entry : cache.getUserEquipmentMap().entrySet()) {
            userEquipmentService.removeUserEquipment(entry.getValue());
        }
        for (Entry<Integer, FailureClass> entry : cache.getFailureClassMap().entrySet()) {
            failureClassService.removeFailureClass(entry.getValue());
        }
        for (Entry<Integer, CountryNetworkCode> entry : cache.getCountryNetworkCodeMap().entrySet()) {
            countryNetworkCodeService.removeCountryNetworkCode(entry.getValue());
        }
    }

    @Test
    public void extractWorkbookFromFile_testWithValidInputStream_returnsHSSFWorkbook() {
        HSSFWorkbook wb = uploadFile.extractWorkbookFromFile(loadTestSheetAsInputStream());
        assertTrue(wb.getBytes().length > 0);
    }

    @Test
    public void extractWorkbookFromFile_testWithInvalidInputStream_returnsNull() {
        HSSFWorkbook wb = uploadFile.extractWorkbookFromFile(null);
        assertEquals(null, wb);
    }

    @Test
    public void generateReport_testIfReturnsString() {
        assertTrue(uploadFile.generateReport(586544L).length() > 0);
    }

    @Test(expected = Exception.class)
    public void extractSheetsFromWorkbook_testIfExceptionIsThrown_throwsException() throws Exception {
        uploadFile.extractSheetsFromWorkbook(null);
    }

    @Test
    public void extractSheetsFromWorkbook_testValidHSSFWorkbook_populatesSheets() throws Exception {
        HSSFWorkbook wb = uploadFile.extractWorkbookFromFile(loadTestSheetAsInputStream());
        uploadFile.extractSheetsFromWorkbook(wb);
    }

    @Test
    public void extractSheetsFromWorkbook_testInvalidHSSFWorkbook_throwsException() {
        try {
            uploadFile.extractSheetsFromWorkbook(new HSSFWorkbook());
        } catch (Exception e) {
            assertEquals(e.getMessage(), "Failed to extract the valid sheets. Please make sure your file contains 5 sheets named:" +
                    "Base Data, Event-Cause table, Failure Class Table, UE Table and MCC - MNC Table in this order.");
        }
    }

    @Test
    public void exportDataToEventCauseTable_testValidSheet_persistsDataToEventCauseTable() throws Exception {
        uploadFile.exportDataToEventCauseTable(eventCauseSheet);
        int cacheSize = cache.getEventCauseMap().size();
        assertTrue(cacheSize > 0);
        utx.begin();
        em.joinTransaction();
        int size = em.createQuery("Select e FROM EventCause e", EventCause.class).getResultList().size();
        utx.commit();
        assertEquals(cacheSize, size);
    }

    @Test
    public void exportDataToFailureClassTable_testValidSheet_persistsDataToFailureClassTable() throws Exception {
        uploadFile.exportDataToFailureClassTable(failureClassSheet);
        int cacheSize = cache.getFailureClassMap().size();
        assertTrue(cacheSize > 0);
        utx.begin();
        em.joinTransaction();
        int size = em.createQuery("Select e FROM FailureClass e", FailureClass.class).getResultList().size();
        utx.commit();
        assertEquals(cacheSize, size);
    }

    @Test
    public void exportDataToMCCMNCTablee_testValidSheet_persistsDataToCountrynetworkCodeTable() throws Exception {
        uploadFile.exportDataToMCCMNCTable(countryNetworkSheet);
        int cacheSize = cache.getCountryNetworkCodeMap().size();
        assertTrue(cacheSize > 0);
        utx.begin();
        em.joinTransaction();
        int size = em.createQuery("Select e FROM CountryNetworkCode e", CountryNetworkCode.class).getResultList().size();
        utx.commit();
        assertEquals(cacheSize, size);
    }

    @Test
    public void exportDataToUserEquipmentTable_testValidSheet_persistsDataTouserEquipmentTable() throws Exception {
        uploadFile.exportDataToUserEquipmentTable(userEquipmentSheet);
        int cacheSize = cache.getUserEquipmentMap().size();
        assertTrue(cacheSize > 0);
        utx.begin();
        em.joinTransaction();
        int size = em.createQuery("Select e FROM UserEquipment e", UserEquipment.class).getResultList().size();
        utx.commit();
        assertEquals(cacheSize, size);
    }

    @Test
    public void exportDataToBaseData_testValidSheet_persistsDataToBaseDataTable() throws Exception {
        uploadFile.exportDataToBaseDataTable(baseDataSheet);
        utx.begin();
        em.joinTransaction();
        int size = em.createQuery("Select e FROM BaseData e", BaseData.class).getResultList().size();
        int conflictSize = em.createQuery("Select e FROM InsertionConflict e", InsertionConflict.class).getResultList().size();
        utx.commit();
        assertTrue(size == 0);
        assertTrue(conflictSize > 0);
        utx.begin();
        em.joinTransaction();
        em.createQuery("DELETE FROM BaseData u").executeUpdate();
        utx.commit();
    }

    private InputStream loadTestSheetAsInputStream() {
        try {
            InputStream is = this.getClass().getResourceAsStream("/testSheet.xls");
            return is;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get the testSheet from resources folder to be used in this test.
     */
    private void loadTestSheet() {
        try {
            POIFSFileSystem fileStream = new POIFSFileSystem(this.getClass().getResourceAsStream("/testSheet.xls"));
            workbook = new HSSFWorkbook(fileStream);
            baseDataSheet = workbook.getSheet("Base Data");
            eventCauseSheet = workbook.getSheet("Event-Cause Table");
            failureClassSheet = workbook.getSheet("Failure Class Table");
            userEquipmentSheet = workbook.getSheet("UE Table");
            countryNetworkSheet = workbook.getSheet("MCC - MNC Table");
            workbook.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
