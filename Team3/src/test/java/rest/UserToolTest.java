
package rest;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.HttpHeaders;

import model.BaseData;
import model.User;
import model.UserType;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.resteasy.util.Base64;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import restMapping.CellExtractor;
import restMapping.CentralAuthority;
import restMapping.JaxRsActivator;
import restMapping.UserTool;
import serviceLayer.baseData.BaseDataServiceBean;
import serviceLayer.entityCache.EntityCache;
import serviceLayer.user.UserService;
import dataAccess.baseData.BaseDataDAO;
import dataAccess.insertionConflict.InsertionConflictDAOBean;

@RunWith(Arquillian.class)
public class UserToolTest {

    @EJB
    CentralAuthority centralAuthority;
    @Inject
    UserTool userTool;
    @EJB
    UserService userService;
    @PersistenceContext
    EntityManager em;
    @Inject
    UserTransaction utx;
    @EJB
    EntityCache cache;

    @Deployment
    public static Archive<?> createTestArchive() {
        PomEquippedResolveStage pom = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies();
        File[] libs = pom.resolve("org.apache.poi:poi").withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(InsertionConflictDAOBean.class.getPackage())
                .addPackage(BaseDataDAO.class.getPackage())
                .addPackage(BaseData.class.getPackage())
                .addPackage(CellExtractor.class.getPackage())
                .addPackage(JaxRsActivator.class.getPackage())
                .addPackage(BaseDataServiceBean.class.getPackage())
                .addPackage(DataFormatter.class.getPackage())
                .addPackage(HSSFRow.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"))
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("testSheet.xls");
        war.addAsLibraries(libs);
        System.out.println(war.toString(true));
        return war;
    }

    @Before
    public void setUp() throws Exception {
        for (Map.Entry<String, User> entry : cache.getUserMap().entrySet()) {
            userService.removeUser(entry.getValue());
        }
        User user = new User("admin");
        user.setPassword("Y46UXStRp9HhuL0eu3Ws8JDVm3Y=");
        user.setToken("rageagainstthevirtualmachines");
        user.setUserType(UserType.ADMINISTRATION);
        userService.persistenceUser(user);
    }

    @After
    public void tearDown() throws Exception {
        User user = new User("admin");
        userService.removeUser(user);
    }

    @Test
    public void loginUser_testNullUsernameAndPasswordEncrypted_AccessDenied() {
        assertEquals("Access denied!", userTool.loginUser(null).getEntity().toString());
    }

    @Test
    public void loginUser_testInvalidUsernameAndPasswordEncrypted_AccessDenied() {
        assertEquals("Access denied!", userTool.loginUser("me:me").getEntity().toString());
    }

    @Test
    public void loginUser_testValidLogin_Status200() {
        String user = "admin:admin";
        assertEquals(200, userTool.loginUser(Base64.encodeBytes(user.getBytes())).getStatus());
    }

    @Test
    public void registerUser_testInvalidUserAccess_AccessDenied() {
        HttpHeaders headers = new Headers("asd", "rageagainstthevirtualmachines");
        String userSecret = "me:me";
        assertEquals("Access denied!", userTool.registerUser(headers, "ADMINISTRATION", "Aladin", Base64.encodeBytes(userSecret.getBytes()))
                .getEntity().toString());
    }

    @Test
    public void registerUser_testValidAccessWithInvalidNewUser_UserAlreadyExists() {
        User user = userService.getUserById("admin");
        System.out.println(user);
        HttpHeaders headers = new Headers(user.getId(), user.getToken());
        String userSecret = "admin:admin";
        assertEquals("That user already exists!", userTool
                .registerUser(headers, "ADMINISTRATION", "Admin", Base64.encodeBytes(userSecret.getBytes())).getEntity().toString());
    }

    @Test
    public void registerUser_testValidRegistration_Status200() {
        User user = userService.getUserById("admin");
        HttpHeaders headers = new Headers(user.getId(), user.getToken());
        String userSecret = "aladin:baba";
        assertEquals("OK", userTool.registerUser(headers, "ADMINISTRATION", "Admin", Base64.encodeBytes(userSecret.getBytes())).getEntity()
                .toString());
    }

}
