package rest;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import model.FailureClass;
import model.UserEquipment;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;

import restMapping.CellExtractor;
import restMapping.UploadFile;

@SuppressWarnings("unused")
public class CellExtractorTest {

	private static HSSFSheet baseDataSheet;
	private static HSSFSheet testSheet;
	private static HSSFWorkbook workbook;
	private HSSFRow row;
	private static DataFormatter formatter;
	private static CellExtractor cellExtractor;


	@BeforeClass
	public static void beforeClass() {
		formatter = new DataFormatter();
		loadTestSheet();
		cellExtractor = new CellExtractor();
	}

	@Test
	public void getIntegerFromCell_testingIntegerValueGreaterThanIntegerMax_nullReturned()
			throws Exception {
		HSSFCell cell = testSheet.getRow(0).getCell(4);
		assertEquals(null,cellExtractor.getIntegerFromCell(cell));
	}
	
	@Test
	public void getIntegerFromCell_testingStringValue_nullReturned()
			throws Exception {
		HSSFCell cell = testSheet.getRow(0).getCell(2);
		assertEquals(null,cellExtractor.getIntegerFromCell(cell));
	}
	
	@Test
	public void getIntegerFromCell_testingDecimalValue_ReturnWholeValue()
			throws Exception {
		HSSFCell cell = testSheet.getRow(0).getCell(6);
		assertEquals(54,cellExtractor.getIntegerFromCell(cell),0);
	}
	
	@Test
	public void getIntegerFromCell_testingNullValue_nullReturned()
			throws Exception {
		assertEquals(null,cellExtractor.getIntegerFromCell(null));
	}

	@Test
	public void getIntegerFromCell_testingEmptyCell_nullReturned()
			throws Exception {
		HSSFCell cell = testSheet.getRow(0).getCell(1);
		assertEquals(null,cellExtractor.getIntegerFromCell(cell));
	}

	@Test
	public void getIntegerFromCell_testingValidValue_returnInteger()
			throws Exception {
		HSSFCell cell = testSheet.getRow(0).getCell(3);
		assertEquals(12,cellExtractor.getIntegerFromCell(cell),0.00);
	}
	
	@Test
	public void getBigIntegerFromCell_testingInvalidStringValue_nullReturned()
			throws Exception {
		HSSFCell cell = testSheet.getRow(0).getCell(2);
		assertEquals(null,cellExtractor.getBigIntegerFromCell(cell));
	}

	@Test
	public void getBigIntegerFromCell_testingNullCell_nullReturned()
			throws Exception {
		assertEquals(null,cellExtractor.getBigIntegerFromCell(null));
	}

	@Test
	public void getBigIntegerFromCell_testingEmptyCell_nullReturned()
			throws Exception {
		HSSFCell cell = testSheet.getRow(0).getCell(1);
		assertEquals(null,cellExtractor.getBigIntegerFromCell(cell));
	}
	
	@Test
	public void getBigIntegerFromCell_testingDecimalValue_nullReturned()
			throws Exception {
		HSSFCell cell = testSheet.getRow(0).getCell(6);
		assertEquals(null,cellExtractor.getBigIntegerFromCell(cell));
	}

	@Test
	public void getBigIntegerFromCell_testingValidValue_returnBigInteger()
			throws Exception {
		HSSFCell cell = testSheet.getRow(0).getCell(7);
		assertEquals(0,cellExtractor.getBigIntegerFromCell(cell).compareTo(
						new BigInteger("100100")));
	}
	
	@Test
	public void getStringFromCell_testingNullCell_nullReturned()
			throws Exception {
		assertEquals(null,cellExtractor.getStringFromCell(null));
	}

	@Test
	public void getStringFromCell_testingNullStringCell_nullReturned()
			throws Exception {
		HSSFCell cell = testSheet.getRow(0).getCell(1);
		assertEquals(null, cellExtractor.getStringFromCell(cell));
	}
	@Test
	public void getStringFromCell_testingEmptyCell_nullReturned()
			throws Exception {
		HSSFCell cell = testSheet.getRow(0).getCell(0);
		assertEquals(null, cellExtractor.getStringFromCell(cell));
	}

	@Test
	public void getStringFromCell_testingNonStrictlyStringNonNullCell_returnsString()
			throws Exception {
		HSSFCell cell = testSheet.getRow(0).getCell(5);
		assertTrue(cellExtractor.getStringFromCell(cell).equals(
				"6546546546465.54"));
	}

	@Test
	public void getStringFromCell_testingStrictlyStringCell_returnsString()
			throws Exception {
		HSSFCell cell = testSheet.getRow(0).getCell(2);
		assertTrue(cellExtractor.getStringFromCell(cell).equals("string"));
	}
	
	@Test
	public void getDateFromCell_testingValidDate_returnDate()
			throws Exception {
		HSSFCell cell = baseDataSheet.getRow(1).getCell(0);
		Date date = cell.getDateCellValue();
		assertEquals(cellExtractor.getDateFromCell(cell), date);
	}

	@Test
	public void getDateFromCell_testingInvalidDate_returnNull()
			throws Exception {
		HSSFCell cell = baseDataSheet.getRow(1).getCell(1);
		cellExtractor.getDateFromCell(cell);
	}
	
	/**
	 * Get the testSheet from resources folder to be used in this test.
	 */
	private static void loadTestSheet() {
		try {
			ClassLoader classLoader = UploadFile.class.getClassLoader();
			String filePath = classLoader.getResource("testSheet.xls")
					.getFile();
			File workSheet = new File(filePath);
			FileInputStream inputStream = new FileInputStream(workSheet);
			POIFSFileSystem fileStream = new POIFSFileSystem(inputStream);
			workbook = new HSSFWorkbook(fileStream);
			baseDataSheet = workbook.getSheet("Base Data");
			testSheet = workbook.getSheetAt(5);
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
