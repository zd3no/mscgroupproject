package rest;

import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

public class Headers implements HttpHeaders{
	private ArrayList<String> identity = new ArrayList<>();
	private ArrayList<String> token = new ArrayList<>();
	
	public Headers(String username, String strtoken){
		identity.add(username);
		token.add(strtoken);
	}

	@Override
	public List<String> getRequestHeader(String name) {
		if (name.equals("identity")) {
			return identity;
		}else if (name.equals("token")){
			return token;
		}
		return null;
	}

	@Override
	public String getHeaderString(String name) {
		return null;
	}

	@Override
	public MultivaluedMap<String, String> getRequestHeaders() {
		return null;
	}

	@Override
	public List<MediaType> getAcceptableMediaTypes() {
		return null;
	}

	@Override
	public List<Locale> getAcceptableLanguages() {
		return null;
	}

	@Override
	public MediaType getMediaType() {
		return null;
	}

	@Override
	public Locale getLanguage() {
		return null;
	}

	@Override
	public Map<String, Cookie> getCookies() {
		return null;
	}

	@Override
	public Date getDate() {
		return null;
	}

	@Override
	public int getLength() {
		return 0;
	}
	
}
