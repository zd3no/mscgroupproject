
package rest;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import model.BaseData;
import model.User;
import model.UserType;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import restMapping.CellExtractor;
import restMapping.CentralAuthority;
import restMapping.DisplayData;
import restMapping.JaxRsActivator;
import serviceLayer.baseData.BaseDataServiceBean;
import serviceLayer.entityCache.EntityCache;
import serviceLayer.user.UserService;
import dataAccess.baseData.BaseDataDAO;
import dataAccess.insertionConflict.InsertionConflictDAOBean;

@RunWith(Arquillian.class)
public class DisplayDataTest {

    @EJB
    CentralAuthority centralAuthority;
    @EJB
    UserService userService;
    @PersistenceContext
    EntityManager em;
    @Inject
    UserTransaction utx;
    @Inject
    DisplayData displayData;
    @EJB
    EntityCache cache;

    @Deployment
    public static Archive<?> createTestArchive() {
        PomEquippedResolveStage pom = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies();
        File[] libs = pom.resolve("org.apache.poi:poi").withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(InsertionConflictDAOBean.class.getPackage())
                .addPackage(BaseDataDAO.class.getPackage())
                .addPackage(BaseData.class.getPackage())
                .addPackage(CellExtractor.class.getPackage())
                .addPackage(JaxRsActivator.class.getPackage())
                .addPackage(BaseDataServiceBean.class.getPackage())
                .addPackage(DataFormatter.class.getPackage())
                .addPackage(HSSFRow.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"))
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("testSheet.xls");
        war.addAsLibraries(libs);
        System.out.println(war.toString(true));
        return war;
    }

    @Before
    public void setUp() throws Exception {
        ConcurrentHashMap<String, User> userMap = cache.getUserMap();
        for (User user : userMap.values()) {
            cache.remove(user, User.class);
        }
        User user = new User("admin");
        user.setPassword("admin");
        user.setToken("rageagainstthevirtualmachines");
        user.setUserType(UserType.ADMINISTRATION);
        userService.persistenceUser(user);
    }

    @After
    public void tearDown() throws Exception {
        ConcurrentHashMap<String, User> userMap = cache.getUserMap();
        for (User user : userMap.values()) {
            cache.remove(user, User.class);
        }
    }

    @Test
    public void getImsiWithFailuresForGivenDate_testInvalidDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Not Valid Dates!").build();
        assertEquals(fail.getStatus(), displayData.getImsiForGivenDate(headers, "50/03/2015 00:00", "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getImsiWithFailuresForGivenDate_testInvalidDatesWithWrongFormat_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Not Valid Dates!").build();
        assertEquals(fail.getStatus(), displayData.getImsiForGivenDate(headers, "50-03-2015 00:00", "15-03-2015 00:00").getStatus());
    }

    @Test
    public void getImsiWithFailuresForGivenDate_testStringPassedAsDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Not Valid Dates!").build();
        assertEquals(fail.getStatus(), displayData.getImsiForGivenDate(headers, "sdfgdfg", "dfgfdg").getStatus());
    }

    @Test
    public void getImsiWithFailuresForGivenDate_testInvalidAccess_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(fail.getStatus(), displayData.getImsiForGivenDate(headers, "22/09/1987 00:00", "25/02/1977 00:00").getStatus());
    }

    @Test
    public void getImsiWithFailuresForGivenDate_testValidData_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(200).entity("Passed").build();
        assertEquals(fail.getStatus(), displayData.getImsiForGivenDate(headers, "04/03/2015 16:31", "04/03/2015 16:31").getStatus());
    }

    @Test
    public void getAllImsiByFailureClass_testInvalidFailureID_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        assertEquals("Invalid Failure ID", displayData.getAllImsiByFailureClass(headers, "df").getEntity().toString());
    }

    @Test
    public void getAllImsiByFailureClass_testNegativeFailureID_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        assertEquals("Invalid Failure ID", displayData.getAllImsiByFailureClass(headers, "-1").getEntity().toString());
    }

    @Test
    public void getAllImsiByFailureClass_testEmptyFailureID_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        assertEquals("Invalid Failure ID", displayData.getAllImsiByFailureClass(headers, "").getEntity().toString());
    }

    @Test
    public void getAllImsiByFailureClass_testInvalidRequest_returnsErrorMessage() {
        HttpHeaders headers = new Headers("toor", "rageagainstthevirtualmachines");
        assertEquals("Not Allowed", displayData.getAllImsiByFailureClass(headers, "0").getEntity().toString());
    }

    @Test
    public void getAllImsiByFailureClass_testFailureIdAsBigInteger_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        assertEquals("Invalid Failure ID", displayData.getAllImsiByFailureClass(headers, "1234567891012033").getEntity().toString());
    }

    @Test
    public void getAllImsiByFailureClass_testValidRequest_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        assertEquals(200, displayData.getAllImsiByFailureClass(headers, "1").getStatus());
    }

    @Test
    public void getNumberOfFailuresByImsiBetweenPeriodTime_testInvalidIMSIAsString_returnError() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        assertEquals("Invalid IMSI", displayData.getNumberOfFailuresByImsiBetweenPeriodTime(headers, "sdf", "04/03/2015 16:31", "04/03/2015 16:31")
                .getEntity().toString());
    }

    @Test
    public void getNumberOfFailuresByImsiBetweenPeriodTime_testInvalidIMSIAsEmpty_returnError() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        assertEquals("Invalid IMSI", displayData.getNumberOfFailuresByImsiBetweenPeriodTime(headers, "", "04/03/2015 16:31", "04/03/2015 16:31")
                .getEntity().toString());
    }

    @Test
    public void getNumberOfFailuresByImsiBetweenPeriodTime_testInvalidNegativeIMSI_returnError() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        assertEquals("Invalid IMSI",
                displayData.getNumberOfFailuresByImsiBetweenPeriodTime(headers, "-554154", "04/03/2015 16:31", "04/03/2015 16:31").getEntity()
                        .toString());
    }

    @Test
    public void getNumberOfFailuresByImsiBetweenPeriodTime_testInvalidInvalidDate_returnError() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        assertEquals("Invalid date",
                displayData.getNumberOfFailuresByImsiBetweenPeriodTime(headers, "515464545", "04-03-2015 16:31", "04/03/2015 16:31").getEntity()
                        .toString());
    }

    @Test
    public void getNumberOfFailuresByImsiBetweenPeriodTime_testValidInput_returnsResponse() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        assertEquals(200, displayData.getNumberOfFailuresByImsiBetweenPeriodTime(headers, "515464545", "04/03/2015 16:31", "04/03/2015 16:31")
                .getStatus());
    }

    @Test
    public void getUniqureFailureIdEventCauseAndNumberOccurrencesByPhoneModel_testEmptyModel_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Not Valid Model!").build();
        assertEquals(fail.getStatus(), displayData.getUniqueEventCauseAndNumberOccurrencesByPhoneModel(headers, "").getStatus());
    }

    @Test
    public void getUniqureFailureIdEventCauseAndNumberOccurrencesByPhoneModel_testInvalidAccess_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(fail.getStatus(), displayData.getUniqueEventCauseAndNumberOccurrencesByPhoneModel(headers, "9109 PA").getStatus());
    }

    @Test
    public void getUniqureFailureIdEventCauseAndNumberOccurrencesByPhoneModel_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(200).entity("Passed").build();
        assertEquals(fail.getStatus(), displayData.getUniqueEventCauseAndNumberOccurrencesByPhoneModel(headers, "9109 PA").getStatus());
    }

    @Test
    public void getAllEventCauseByImsi_testInvalidImsi_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Not Valid IMSI!").build();
        assertEquals(fail.getStatus(), displayData.getAllEventCauseByImsi(headers, "").getStatus());
    }

    @Test
    public void getAllEventCauseByImsi_testNegativeImsi_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid IMSI").build();
        assertEquals(fail.getStatus(), displayData.getAllEventCauseByImsi(headers, "-344930000000011").getStatus());
    }

    @Test
    public void getAllEventCauseByImsi_testInvalidImsiWithWrongFormat_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid IMSI").build();
        assertEquals(fail.getStatus(), displayData.getAllEventCauseByImsi(headers, "1a2se233").getStatus());
    }

    @Test
    public void getAllEventCauseByImsi_testInvalidAccess_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(fail.getStatus(), displayData.getAllEventCauseByImsi(headers, "344930000000011").getStatus());
    }

    @Test
    public void getAllEventCauseByImsi_testValidData_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(200).entity("Passed").build();
        assertEquals(fail.getStatus(), displayData.getAllEventCauseByImsi(headers, "344930000000011").getStatus());
    }

    //
    @Test
    public void getNumberOfFailuresByModelBetweenPeriodTime_testInvalidDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Not Valid Dates!").build();
        assertEquals(fail.getStatus(),
                displayData.getNumberOfFailuresByModelBetweenPeriodTime(headers, "9109 PA", "50/03/2015 00:00", "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getNumberOfFailuresByModelBetweenPeriodTime_testEmptyModel_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Not Valid Model!").build();
        assertEquals(fail.getStatus(), displayData.getNumberOfFailuresByModelBetweenPeriodTime(headers, "", "50-03-2015 00:00", "15-03-2015 00:00")
                .getStatus());
    }

    @Test
    public void getNumberOfFailuresByModelBetweenPeriodTime_testInvalidDatesWithWrongFormat_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Not Valid Dates!").build();
        assertEquals(fail.getStatus(),
                displayData.getNumberOfFailuresByModelBetweenPeriodTime(headers, "9109 PA", "50-03-2015 00:00", "15-03-2015 00:00").getStatus());
    }

    @Test
    public void getNumberOfFailuresByModelBetweenPeriodTime_testStringPassedAsDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Not Valid Dates!").build();
        assertEquals(fail.getStatus(), displayData.getNumberOfFailuresByModelBetweenPeriodTime(headers, "9109 PA", "sdfgdfg", "dfgfdg").getStatus());
    }

    @Test
    public void getNumberOfFailuresByModelBetweenPeriodTime_testInvalidAccess_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(fail.getStatus(),
                displayData.getNumberOfFailuresByModelBetweenPeriodTime(headers, "9109 PA", "22/09/1987 00:00", "25/02/1977 00:00").getStatus());
    }

    @Test
    public void getNumberOfFailuresByModelBetweenPeriodTime_testValidData_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(200).entity("Passed").build();
        assertEquals(fail.getStatus(),
                displayData.getNumberOfFailuresByModelBetweenPeriodTime(headers, "9109 PA", "04/03/2015 16:31", "04/03/2015 16:31").getStatus());
    }

    //
    @Test
    public void getTop10ImsiBetweenPeriodTime_testInvalidDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(fail.getStatus(), displayData.getTop10ImsiBetweenPeriodTime(headers, "50/03/2015 00:00", "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getTop10ImsiBetweenPeriodTime_testInvalidDatesWithWrongFormat_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(fail.getStatus(), displayData.getTop10ImsiBetweenPeriodTime(headers, "50-03-2015 00:00", "15-03-2015 00:00").getStatus());
    }

    @Test
    public void getTop10ImsiBetweenPeriodTime_testStringPassedAsDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(fail.getStatus(), displayData.getTop10ImsiBetweenPeriodTime(headers, "sdfgdfg", "dfgfdg").getStatus());
    }

    @Test
    public void getTop10ImsiBetweenPeriodTime_testInvalidAccess_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(fail.getStatus(), displayData.getTop10ImsiBetweenPeriodTime(headers, "22/09/1987 00:00", "25/02/1977 00:00").getStatus());
    }

    @Test
    public void getTop10ImsiBetweenPeriodTime_testValidData_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(200).entity("Passed").build();
        assertEquals(fail.getStatus(), displayData.getTop10ImsiBetweenPeriodTime(headers, "04/03/2015 16:31", "04/03/2015 16:31").getStatus());
    }

    //
    @Test
    public void getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime_testInvalidDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(fail.getStatus(),
                displayData.getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime(headers, "50/03/2015 00:00", "55/03/2015 00:00").getStatus());
    }

    @Test
    public void getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime_testInvalidDatesWithWrongFormat_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(fail.getStatus(),
                displayData.getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime(headers, "50-03-2015 00:00", "15-03-2015 00:00").getStatus());
    }

    @Test
    public void getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime_testStringPassedAsDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(fail.getStatus(), displayData.getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime(headers, "sdfgdfg", "dfgfdg").getStatus());
    }

    @Test
    public void getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime_testInvalidAccess_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(fail.getStatus(),
                displayData.getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime(headers, "22/09/1987 00:00", "25/02/1977 00:00").getStatus());
    }

    @Test
    public void getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime_testValidData_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(200).entity("Passed").build();
        assertEquals(fail.getStatus(),
                displayData.getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime(headers, "04/03/2015 16:31", "04/03/2015 16:31").getStatus());
    }

    //
    @Test
    public void getNumberOfFailuresAndDurationForEachImsiBetweenPeriodTime_testInvalidDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(fail.getStatus(),
                displayData.getNumberOfFailuresAndDurationForEachImsiBetweenPeriod(headers, "50/03/2015 00:00", "15/03/2015 00:00").getStatus());
    }

    @Test
    public void getNumberOfFailuresAndDurationForEachImsiBetweenPeriodTime_testInvalidDatesWithWrongFormat_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(fail.getStatus(),
                displayData.getNumberOfFailuresAndDurationForEachImsiBetweenPeriod(headers, "50-03-2015 00:00", "15-03-2015 00:00").getStatus());
    }

    @Test
    public void getNumberOfFailuresAndDurationForEachImsiBetweenPeriodTime_testStringPassedAsDates_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid date").build();
        assertEquals(fail.getStatus(), displayData.getNumberOfFailuresAndDurationForEachImsiBetweenPeriod(headers, "sdfgdfg", "dfgfdg").getStatus());
    }

    @Test
    public void getNumberOfFailuresAndDurationForEachImsiBetweenPeriodTime_testInvalidAccess_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(fail.getStatus(),
                displayData.getNumberOfFailuresAndDurationForEachImsiBetweenPeriod(headers, "22/09/1987 00:00", "25/02/1977 00:00").getStatus());
    }

    @Test
    public void getNumberOfFailuresAndDurationForEachImsiBetweenPeriodTime_testValidData_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(200).entity("Passed").build();
        assertEquals(fail.getStatus(),
                displayData.getNumberOfFailuresAndDurationForEachImsiBetweenPeriod(headers, "04/03/2015 16:31", "04/03/2015 16:31").getStatus());
    }

    @Test
    public void getAllUniqueCauseCodeAndFailureClassByImsi_testNegativeImsi_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid IMSI").build();
        assertEquals(fail.getStatus(), displayData.getAllUniqueCauseCodeAndFailureClassByImsi(headers, "-344930000000011").getStatus());
    }

    @Test
    public void getAllUniqueCauseCodeAndFailureClassByImsi_testInvalidImsiWithWrongFormat_returnsErrorMessage() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(404).entity("Invalid IMSI").build();
        assertEquals(fail.getStatus(), displayData.getAllUniqueCauseCodeAndFailureClassByImsi(headers, "1a2se233").getStatus());
    }

    @Test
    public void getAllUniqueCauseCodeAndFailureClassByImsi_testInvalidAccess_returnsErrorMessage() {
        HttpHeaders headers = new Headers("world", "hello");
        Response fail = Response.ok().status(401).entity("Not Allowed").build();
        assertEquals(fail.getStatus(), displayData.getAllUniqueCauseCodeAndFailureClassByImsi(headers, "344930000000011").getStatus());
    }

    @Test
    public void getAllUniqueCauseCodeAndFailureClassByImsi_testValidData_returnsCollectionOfObjectArray() {
        HttpHeaders headers = new Headers("admin", "rageagainstthevirtualmachines");
        Response fail = Response.ok().status(200).entity("Passed").build();
        assertEquals(fail.getStatus(), displayData.getAllUniqueCauseCodeAndFailureClassByImsi(headers, "344930000000011").getStatus());
    }
}
