package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-02-20T15:17:13.568+0000")
@StaticMetamodel(EventCausePK.class)
public class EventCausePK_ {
	public static volatile SingularAttribute<EventCausePK, Integer> causeCode;
	public static volatile SingularAttribute<EventCausePK, Integer> eventId;
}
