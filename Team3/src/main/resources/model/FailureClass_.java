package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-02-20T15:17:13.569+0000")
@StaticMetamodel(FailureClass.class)
public class FailureClass_ {
	public static volatile SingularAttribute<FailureClass, Integer> failureId;
	public static volatile SingularAttribute<FailureClass, String> description;
}
