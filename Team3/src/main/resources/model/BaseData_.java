package model;

import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-02-20T15:17:13.565+0000")
@StaticMetamodel(BaseData.class)
public class BaseData_ {
	public static volatile SingularAttribute<BaseData, Integer> id;
	public static volatile SingularAttribute<BaseData, Integer> duration;
	public static volatile SingularAttribute<BaseData, Date> timeStamp;
	public static volatile SingularAttribute<BaseData, String> networkElementVersion;
	public static volatile SingularAttribute<BaseData, BigInteger> imsi;
	public static volatile SingularAttribute<BaseData, Integer> cellId;
	public static volatile SingularAttribute<BaseData, BigInteger> hier3Id;
	public static volatile SingularAttribute<BaseData, BigInteger> hier32Id;
	public static volatile SingularAttribute<BaseData, BigInteger> hier321Id;
	public static volatile SingularAttribute<BaseData, UserEquipment> userEquipment;
	public static volatile SingularAttribute<BaseData, EventCause> eventCause;
	public static volatile SingularAttribute<BaseData, FailureClass> failureClass;
	public static volatile SingularAttribute<BaseData, CountryNetworkCode> countryNetworkCode;
}
