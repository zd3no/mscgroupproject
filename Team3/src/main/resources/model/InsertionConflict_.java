package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-02-20T15:17:13.569+0000")
@StaticMetamodel(InsertionConflict.class)
public class InsertionConflict_ {
	public static volatile SingularAttribute<InsertionConflict, Integer> id;
	public static volatile SingularAttribute<InsertionConflict, String> originTable;
	public static volatile SingularAttribute<InsertionConflict, String> inputString;
	public static volatile SingularAttribute<InsertionConflict, String> errorDescription;
}
