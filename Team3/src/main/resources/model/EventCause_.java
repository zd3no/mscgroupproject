package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-02-20T15:17:13.568+0000")
@StaticMetamodel(EventCause.class)
public class EventCause_ {
	public static volatile SingularAttribute<EventCause, EventCausePK> id;
	public static volatile SingularAttribute<EventCause, String> description;
}
