package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-02-20T15:17:13.566+0000")
@StaticMetamodel(CountryNetworkCode.class)
public class CountryNetworkCode_ {
	public static volatile SingularAttribute<CountryNetworkCode, CountryNetworkCodePK> id;
	public static volatile SingularAttribute<CountryNetworkCode, String> country;
	public static volatile SingularAttribute<CountryNetworkCode, String> operator;
}
