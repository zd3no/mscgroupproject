package model;

import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-02-20T15:17:13.571+0000")
@StaticMetamodel(UserEquipment.class)
public class UserEquipment_ {
	public static volatile SingularAttribute<UserEquipment, BigInteger> typeAllocationCode;
	public static volatile SingularAttribute<UserEquipment, String> marketingName;
	public static volatile SingularAttribute<UserEquipment, String> manufacturer;
	public static volatile SingularAttribute<UserEquipment, String> accessCapability;
	public static volatile SingularAttribute<UserEquipment, String> model;
	public static volatile SingularAttribute<UserEquipment, String> vendorName;
	public static volatile SingularAttribute<UserEquipment, String> userEquipmentType;
	public static volatile SingularAttribute<UserEquipment, String> operatingSystem;
	public static volatile SingularAttribute<UserEquipment, String> inputMode;
}
