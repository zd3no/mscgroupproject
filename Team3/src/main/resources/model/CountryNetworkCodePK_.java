package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-02-20T15:17:13.567+0000")
@StaticMetamodel(CountryNetworkCodePK.class)
public class CountryNetworkCodePK_ {
	public static volatile SingularAttribute<CountryNetworkCodePK, Integer> mobileCountryCode;
	public static volatile SingularAttribute<CountryNetworkCodePK, Integer> mobileNetworkCode;
}
