/**
 * This method is used to log in the user in the system. It extracts the values passed by the form,
 * validates them against the server and redirects to the proper userType page.
 */
function login(){
	var username = document.getElementById("username");
	var password = document.getElementById("password");
	var loginButton = document.getElementById("btn_login");
	var strUsername = username.value;
	var strPassword = password.value;
	
	//validation
	username.setCustomValidity("");
	password.setCustomValidity("");
	document.getElementById("error").style.visibility = "hidden";
	if (strUsername.length ==0) {
		username.setCustomValidity("Username/ID must not be null");
		return;
	}else if (strPassword.length == 0){
		password.setCustomValidity("Password cannot be null");
		return;
	}
	
	//process call
	var authData = makeSecret(strUsername,strPassword);
	console.log(authData);
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4){
			if(xmlhttp.status == 200) {
				var response = xmlhttp.responseText;
				var decodedResponse = window.atob(response);
				var jsonUser = JSON.parse(decodedResponse);
				setCookie("user",JSON.stringify(jsonUser),1);
				window.location.href="user.html";
			}else{
				document.getElementById("error").style.visibility = "visible";
				document.getElementById("error").innerHTML = "*Invalid login!"
				return;
			}
		}
	};
	xmlhttp.open("POST", "rest/user-service/loginUser", true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send("secret=" + authData);
}

function registerNewUser() {
	document.getElementById("error").style.visibility = "hidden";
	//passwordConfirm.setCustomValidity("");
	//userName.setCustomValidity("");
	var userName = document.getElementById("userName");
	var userId = document.getElementById("userId");
	var password = document.getElementById("userPassword");
	var passwordConfirm = document.getElementById("userPasswordConfirm");
	var userType = document.getElementById("userType");
	var strUserType = userType.options[userType.selectedIndex].value;
	var strUserName = userName.value;
	var strUserId = userId.value;
	var strPassword = password.value;
	//Validation
	if (userName.value == "" || userId.value=="" || password.value=="" || passwordConfirm.value=="") {
		document.getElementById("error").style.visibility = "visible";
		document.getElementById("error").innerHTML = "*Please fill the field"
		return;
	}else if(userName.value.match("^[a-zA-Z\\']+&")){
		document.getElementById("error").style.visibility = "visible";
		document.getElementById("error").innerHTML = "*Invalid user name";
		userName.setCustomValidity("Invalid user name");
		return;
	}else if (password.value != passwordConfirm.value) {
		passwordConfirm.setCustomValidity("Passwords do not match");
		document.getElementById("error").style.visibility = "visible";
		document.getElementById("error").innerHTML = "*Passwords do not match!"
		return;
	}
	
	var authData = makeSecret(strUserId,strPassword);
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.open("POST", "rest/user-service/registerUser", true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("identity",JSON.parse(getCookie("user")).id);
	xmlhttp.setRequestHeader("token",JSON.parse(getCookie("user")).token);
	xmlhttp.send("secret=" + authData + "&userType=" + strUserType + "&name=" + strUserName);
	
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4){ 
			if(xmlhttp.status == 200) {
				var response = xmlhttp.responseText;
				if (response == "OK") {
					document.getElementById("error").style.visibility = "visible";
					document.getElementById("error").style.color="green";
					document.getElementById("error").innerHTML = "You've registered this user successfuly";
				}else{
					document.getElementById("error").style.visibility = "visible";
					document.getElementById("error").innerHTML = xmlhttp.responseText;
				}
			}else{
				document.getElementById("error").style.visibility = "visible";
				document.getElementById("error").innerHTML = xmlhttp.responseText;
			}
		}
	};
}

/**
 * Generates a Base64 encoded string from username and password
 * @param userId
 * @param password
 * @returns
 */
function makeSecret(userId, password){
	var string = userId+':'+password;
	var encodedString = window.btoa(string);
	return encodedString;
}

/**
 * Store the cookie.
 * @param cname - cookie name
 * @param cvalue - cookie value
 * @param exdays - amount of days set for cookie to expire
 */
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

/**
 * Returns the cookie value by name
 * @param cname
 * @returns
 */
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}
