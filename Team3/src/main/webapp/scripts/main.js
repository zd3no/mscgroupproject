//GLOBAL VARIABLES//
var paginationCache = null;

/**
 * onload functionality to get the user from cookie, verify if the current location is available to the user
 * and add handler for the logout functionality. Lastly, load the page for user and refine available queries.
 */
window.onload=function(){
	var cookie = getCookie("user");
	var mLocation = window.location.pathname;
	var mPage = mLocation.substring(mLocation.lastIndexOf('/') + 1);
	if(cookie == "" && mPage != "index.html"){
		alert("You must be logged in to access this page!");
		window.location.href="index.html";
		return;
	}else if(cookie != ""){
		document.getElementById("user_type").innerHTML = JSON.parse(getCookie("user")).userType;
		document.getElementById("user_type").addEventListener("click", user_type_onclick);
		document.getElementById("logOut").addEventListener("mouseout", logout_onmouseout);
		document.getElementById("logOut").addEventListener("click", logout_onclick);
		loadPageForUser();
		refineAvailableQueriesForUser();
		display.appendChild(queryForm);
		//set default headers for ajaxRequest. These headers will always be sent when ajaxRequest is created
		$.ajaxSetup({
			headers: { 'Content-type': 'application/x-www-form-urlencoded', 
				"identity": JSON.parse(getCookie("user")).id, 
				"token": JSON.parse(getCookie("user")).token }
		});
		
		$(document).mouseup(function (e){
		    var container = $("#popup");
		    if (!container.is(e.target)&& container.has(e.target).length === 0)
		        container.hide();
		});
	}
};

jQuery(function($) {
    mousePosition = { x: -1, y: -1 };
    $(document).mousemove(function(event) {
    	mousePosition.x = event.pageX;
    	mousePosition.y = event.pageY;
    });
});

/**
 * Show the Logout button
 */
user_type_onclick = function() {
	var logout = document.getElementById("logOut");
	logout.parentNode.style.width = "100%";
	logout.parentNode.style.visibility = "visible";
}

/**
 * onMouseOut handler for LOGOUT. Hides the logout button.
 */
logout_onmouseout = function() {
	var logout = document.getElementById("logOut");
	logout.parentNode.style.width = "100%";
	logout.parentNode.style.visibility = "hidden";
}

/**
 * OnClick handler for LOGOUT. Deletes the cookie and redirects to Index.
 */
logout_onclick = function() {
	setCookie("user",null,-1);
	window.location.href="index.html";
}

/**
 * Depending on the userType from the cookie, load the user's content.
 * If user type is invalid, display error message and redirect to Index.html
 */
function loadPageForUser() {
	var user = JSON.parse(getCookie("user"));
	switch(user.userType) {
    case "ADMINISTRATION":
    	loadAdminPage();
        break;
    case "MANAGEMENT":
        loadManagementPage();
        break;
    case "SERVICE":
    	loadServicePage();
    	break;
    case "SUPPORT":
    	loadSupport();
    	break;
    default:
    	alert("You have no valid user type, please contact Administration office.")
        window.location.href="index.html";
	} 
	document.getElementById("chartContainer").style.display="none";
}

/**
 * Load the content for admin page.
 */
function loadAdminPage(){
	document.getElementById("nav_left_side").appendChild(upload_file);
	document.getElementById("nav_left_side").appendChild(register_user);
	document.getElementById("nav_left_side").appendChild(show_data);
	document.getElementById("nav_left_side").appendChild(display_queries);
}

/**
 * Load the content for network management user page.
 */
function loadManagementPage(){
	document.getElementById("nav_left_side").appendChild(display_queries);
}

/**
 * Load the content for customer service page.
 */
function loadServicePage(){
	document.getElementById("nav_left_side").appendChild(display_queries);
}

/**
 * Load the content for support engineer page.
 */
function loadSupport(){
	document.getElementById("nav_left_side").appendChild(display_queries);
}

/*LEFT NAV BUTTONS*/
upload_file = document.createElement("button");
upload_file.id="upload_file";
upload_file.innerHTML="Upload Data Files";
upload_file.addEventListener("click", changeView);
upload_file.addEventListener("click", selectButton);
upload_file.view = 'uploadForm';

register_user = document.createElement("button");
register_user.id="register_user";
register_user.innerHTML = "Register User";
register_user.addEventListener("click", changeView);
register_user.addEventListener("click", selectButton);
register_user.view = 'register_form';

show_data = document.createElement("button");
show_data.id="show_data";
show_data.innerHTML = "Display All Data";
show_data.addEventListener("click", changeView);
show_data.addEventListener("click", selectButton);
show_data.view = 'display_data';

display_queries = document.createElement("button");
display_queries.id="displayQueries";
display_queries.innerHTML = "Filters";
display_queries.addEventListener("click", changeView);
display_queries.addEventListener("click", selectButton);
display_queries.view = 'display_queries';

/*FORMS*/
upload_form = document.createElement("div");
upload_form.id="upload_form";
upload_form.setAttribute("class","upload_form");
upload_form.innerHTML = 
	"<h2>Upload files</h2>"
		+"<div id='uploadFile'>"
		+"<form action='rest/uploadFile/excel' method='post'"
		+"enctype='multipart/form-data' id='upload_file'"
		+"onsubmit='uploadFile(); return false;'>"
		+"<input id='selectedFile' type='file' name='uploadedFile' size='50' /><br>"
		+"<input type='submit' class='button' value='Upload' id='btn_uploadFile' /> <br>"
		+"<p style='display: none' id='upload_file_status'></p>"
		+"</form>"
		+"</div>";

register_form = document.createElement("div");
register_form.id = "register_form";
register_form.setAttribute("class","register_form");
register_form.innerHTML = 
	"<form id='registerForm' onsubmit='return false'>"
	+"<h2>Register Form</h2>"
	+"<input type='text' placeholder='user name' required='required'"
	+"id='userName'></input> <br> <input type='text'"
	+"placeholder='user id' required='required' id='userId'></input> <br>"
	+"<input type='password' placeholder='password' required='required'"
	+"id='userPassword'></input><br> <input type='password'"
	+"placeholder='confirm password' required='required'"
	+"id='userPasswordConfirm'></input><br> <select id='userType'>"
	+"<option value='ADMINISTRATION'>Administrator</option>"
	+"<option value='MANAGEMENT'>Network Manager</option>"
	+"<option value='SUPPORT'>Support Engineer</option>"
	+"<option value='SERVICE'>Customer Service Rep</option>"
	+"</select><br>"
	+"<button id='btn_register' class='button' onClick='registerNewUser()'>Register</button>"
	+"<p id='error'></p>"
	+"</form>";

display_data = document.createElement("div");
display_data.id="displayData";
display_data.setAttribute("class","displayData");
display_data.innerHTML = 
	"<div id='heading'><h2>Display Data</h2></div>"
	+"<div><button class='button' onclick='getBaseData(\"rest/display/getAllBaseData\")'>Diplay "
	+"Base Data</button><button class='button' onclick='getFailureClass(\"rest/display/getAllFailureClass\")'>Diplay "
	+"Failure Class</button><button class='button' onclick='getUserEquipment(\"rest/display/getAllUserEquipment\")'>Diplay"
	+"User Equipment</button><button class='button' onclick='getCountryNetworkCode(\"rest/display/getAllCountryNetworkCode\")'>Diplay"
	+"MCC MNC</button><button class='button' onclick='getEventCause(\"rest/display/getAllEventCause\")'>Diplay"
	+"Event Cause</button>";

queryForm = document.createElement("div");
queryForm.id = "queryForm";
queryForm.innerHTML = "<h2>Select your query:</h2>"
selectQuery = document.createElement("select");
selectQuery.id="queries";

selectQuery.addEventListener("change",onSelectQueryChangeListener);
selectQuery.innerHTML = 
		"   <option value='0'> </option>"
		+"	<option value='1'>Display IMSIs by Failure Class</option>"
		+"	<option value='2'>Top 10 IMSI failures in selected time period</option>"
		+"	<option value='3'>Failure description for selected IMSI</option>"
		+"	<option value='4'>Failure description, Failure Class for a particular IMSI</option>"
		+"	<option value='5'>No. of Call Failures for an IMSI in selected time period</option>"
		+"	<option value='6'>All IMSIs affected in selected time period</option>"
		+"	<option value='7'>No. of Call Failures by phone model in selected time period</option>"
		+"	<option value='8'>Top 10 most frequent Cell ID failures</option>"
		+"	<option value='9'>No. of all Failures and Duration for each IMSI in selected time period</option>"
		+"	<option value='10'>No. of Failures and Failure description for selected Phone Model</option>";
	/*	+"	<option value='1'>IMSIs for a particular Failure Class</option>"
		+"	<option value='2'>Top 10 IMSI between time period</option>"
		+"	<option value='3'>Event ID and Cause Code for particular IMSI</option>"
		+"	<option value='4'>Unique Cause Code and Failure Class for a particular IMSI</option>"
		+"	<option value='5'>No. of Call Failures for a particular IMSI between time period</option>"
		+"	<option value='6'>IMSIs with Call Failures between a time period</option>"
		+"	<option value='7'>No. of Call Failures by phone model between time period</option>"
		+"	<option value='8'>Top 10 unique Markets, Operators and Cell IDs between time period</option>"
		+"	<option value='9'>No. of Call Failures and duration for each IMSI between time period</option>"
		+"	<option value='10'>Unique Event Id, Cause Code and No. of occurrences for a particular phone model</option>";*/
queryForm.appendChild(selectQuery); 

var one = selectQuery.options[1];
var two = selectQuery.options[2];
var three = selectQuery.options[3];
var four = selectQuery.options[4];
var five = selectQuery.options[5];
var six = selectQuery.options[6];
var seven = selectQuery.options[7];
var eight = selectQuery.options[8];
var nine = selectQuery.options[9];
var ten = selectQuery.options[10];
/**
 * Remove the queries from a form based on accessibility allowed to a userType
 */
function refineAvailableQueriesForUser(){
	userType=JSON.parse(getCookie("user")).userType;
	switch (userType){
	case "ADMINISTRATION":
		break;
	case "MANAGEMENT":
		break;
	case "SERVICE":
		selectQuery.removeChild(one);
		selectQuery.removeChild(two);
		selectQuery.removeChild(six);
		selectQuery.removeChild(seven);
		selectQuery.removeChild(eight);
		selectQuery.removeChild(nine);
		selectQuery.removeChild(ten);
		break;
	case "SUPPORT":
		selectQuery.removeChild(two);
		selectQuery.removeChild(eight);
		selectQuery.removeChild(nine);
		selectQuery.removeChild(ten);
		break;
	}
	
}
/**
 * Switch statement to create input elements and or submit buttons for each of the queries individual parameters
 */

function onSelectQueryChangeListener(){
	document.getElementById("chartContainer").style.display="none";
	document.getElementById("querySelection").style.display="block";
	var selectedQuery = document.getElementById('queries');
	var query = selectedQuery.options[selectedQuery.selectedIndex].value;
	switch(query){
	case "1":
		hideDates();
		createTable(failureTable()+submitButton("getAllImsiByFailureClass()"));
		break;
	case "2": 
		showDates();
		createTable(submitButton("getTop10ImsiBetweenPeriodTime()"));
		break;	
	case "3":
		hideDates();
		createTable(imsiTable()+submitButton("getAllEventCauseByImsi()"));
		autocompleteIMSI();
		break;
	case "6":
		showDates();
		createTable(submitButton("getAllImsiBetweenPeriodTime()"));
		break;
	case "5":
		showDates();
		createTable(imsiTable()+submitButton("getNumberOfFailuresByImsiBetweenPeriodTime()"));
		autocompleteIMSI();
		break;
	case "4":
		hideDates();
		createTable(imsiTable()+submitButton("getAllUniqueCauseCodeAndFailureClassByImsi()"));
		autocompleteIMSI();
		break;
	case "7":
		showDates();
		createTable(modelTable()+submitButton("getNumberOfFailuresByModelBetweenPeriodTime()"));
		autocompletePhoneModel();
		break;
	case "8":
		hideDates();
		document.getElementById("tablespace").innerHTML ="";
		var noTimeButtonQuery = "<input id='refineData' type='button' class='button' value='Refine' onclick='switchRefineData();'/>";
		var buttons = "<input id='submitQuery' type='button' class='button' value='Submit'/>";
		document.getElementById("querySelection").innerHTML = buttons+noTimeButtonQuery;
		document.getElementById("submitQuery").addEventListener("click",getTop10UniqueMarketOperatorAndCellIds,false);
		break;
	case "9":
		showDates();
		createTable(submitButton("getNumberOfFailuresAndDurationForEachImsiBetweenPeriodTime()"));
		break;
	case "10":
		hideDates();
		createTable(modelTable()+submitButton("getUniqueEventCauseAndNumberOccurrencesByPhoneModel()"));
		autocompletePhoneModel();
		break;
	}
}

function switchRefineData(){
	if (document.getElementById("dates").style.display=="block") {
		document.getElementById("submitQuery").removeEventListener("click",getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime,false);
		document.getElementById("submitQuery").addEventListener("click",getTop10UniqueMarketOperatorAndCellIds,false);
		hideDates();
		$('#refineData').attr("value","Refine");
	}else{
		document.getElementById("submitQuery").removeEventListener("click",getTop10UniqueMarketOperatorAndCellIds,false);	
		document.getElementById("submitQuery").addEventListener("click",getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime,false);
		showDates();
		$('#refineData').attr("value","All");
	}
	
}

function autocompleteIMSI(){
	$.ajax({
	    type: "GET", 
	    url: "rest/get/getUniqueIMSIs",
	    dataType: 'json'
	}).done(function(data) {
		for (var i = 0; i < data.length; i++) {
			data[i] = String(data[i]);
		}
		$("#imsiNumber").autocomplete({source: data});
	});
}

function autocompletePhoneModel(){
	$.ajax({
	    type: "GET", 
	    url: "rest/get/getUniquePhones",
	    dataType: 'json'
	}).done(function(data) {
		$("#phoneModel").autocomplete({source: data});
	});
}

/**
 * changeView function removes any nodes from display and tablespace divs and adds new nodes based on env param
 * @param env
 */
function changeView(env) {
	var toShow = env.target.view;
	var display = document.getElementById("display");
	while (display.hasChildNodes()) {
		display.removeChild(display.lastChild);
	}
	var tablespace = document.getElementById("tablespace");
	while (tablespace.hasChildNodes()) {
		tablespace.removeChild(tablespace.lastChild);
	}
	document.getElementById("querySelection").style.display="none";
	hideDates();
	
	switch (toShow){
	case "uploadForm":
		display.appendChild(upload_form);
		break;
	case "register_form":
		display.appendChild(register_form);
		break;
	case "display_data":
		display.appendChild(display_data);
		break;
	case "display_queries":
		display.appendChild(queryForm);
		break;
	}
}

/**
 * This method uploads the selected file to database.
 */
function uploadFile() {
	var fileSelect = document.getElementById('selectedFile');
	var uploadButton = document.getElementById('btn_uploadFile');
	var error = document.getElementById("upload_file_status");
	error.style.display = "none";
	var file = fileSelect.files[0];
	try {
		var formData = new FormData();
		if (!file.type.match('application/vnd.ms-excel')) {
			error.style.display = "inline-block";
			error.innerHTML = "Invalid File Selected!";
			return;
		}
	} catch (error) {
		return;
	}
	formData.append('uploadedFile', file, file.name);
	var xhr = new XMLHttpRequest();
	xhr.open('POST', 'rest/uploadFile/excel', true);
	xhr.send(formData);
	uploadButton.value = "Uploading...";
	showSpinner();
	xhr.onload = function() {
		if (xhr.status === 200) {
			hideSpinner();
			uploadButton.value = "Upload";
			uploadButton.disabled = false;
			error.style.display = "inline-block";
			error.innerHTML = xhr.responseText;
			fileSelect.value = null;
		} else {
			hideSpinner();
			uploadButton.value = "Upload";
			uploadButton.disabled = false;
			error.style.display = "inline-block";
			error.innerHTML = xhr.responseText;
		}
	};
}
/**
 * showSpinner displays a spinner to indicate that records are still in the process of being retrieved
 */
function showSpinner(){
	var spinner = document.createElement("div");
	spinner.id="circularG";
	spinner.innerHTML = "<div id='circularG_1' class='circularG'>" +
			"</div><div id='circularG_2' class='circularG'></div>" +
			"<div id='circularG_3' class='circularG'></div><div id='circularG_4' class='circularG'>" +
			"</div><div id='circularG_5' class='circularG'></div><div id='circularG_6' class='circularG'>" +
			"</div><div id='circularG_7' class='circularG'></div><div id='circularG_8' class='circularG'>" +
			"</div>";
	document.getElementById("display").appendChild(spinner);
}

function hideSpinner(){
	if (document.getElementById("circularG") != null) {
		document.getElementById("display").removeChild(document.getElementById("circularG"));
	}
}
/**
 * getBaseData retrieves values from BaseData table, using an XMLHttpRequest and parses the resulting xmlhttp response using jquery
 * @param url
 */
function getBaseData(url) {
	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
	showSpinner();
	var now = Date.now();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var baseData = JSON.parse(xmlhttp.responseText);
				var then = Date.now();
				console.log("time taken to load and parse: "+(now-then));
				$('#tablespace').html( '<table cellspacing="0" id="queryResults"></table>' );
				$('#queryResults').dataTable( {
			        data: baseData,
			        bAutoWidth: false,
			        bDeferRender: true,
			        search: false,
			        sort:false,
			        columns: [
			            { data : "id", title : "ID"},
			            { data : "timeStamp", title : "DATE", "render": ParseDateColumn},
			            { data : "eventCause.id.eventId", title : "EVENT ID"},
			            { data : "failureClass.failureId", title : "FAILURE CLASS"},
			            { data : "userEquipment.typeAllocationCode", title : "UE Type"},
			            { data : "countryNetworkCode.id.mobileNetworkCode", title : "MNC"},
			            { data : "countryNetworkCode.id.mobileCountryCode", title : "MCC"},
			            { data : "cellId", title : "CELL ID"},
			            { data : "duration", title : "DURATION"},
			            { data : "eventCause.id.causeCode", title : "CAUSE CODE"},
			            { data : "networkElementVersion", title : "NE VERSION"},
			            { data : "imsi", title : "IMSI"},
			            { data : "hier3Id", title : "HIER3ID"},
			            { data : "hier32Id", title : "HIER32ID"},
			            { data : "hier321Id", title : "HIER321ID"}],
					aoColumnDefs: [{ "sDefaultContent": '', "aTargets": [ '_all' ]}]
				} );    
				hideSpinner();
			} else {
				document.getElementById("tablespace").innerHTML = xmlhttp.responseText;
				hideSpinner();
			}
		}
	};
}

function ParseDateColumn(data, type, row) {
	date = new Date(data);
	return date.toLocaleDateString()+" "+ date.toLocaleTimeString();
}
/**
 * getFailureClass retrieves values from FailureClass table, using an XMLHttpRequest and parses the resulting xmlhttp response using jquery
 * @param url
 */
function getFailureClass(url) {
	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", url, true);
	xmlhttp.send(); showSpinner();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var failureClass = JSON.parse(xmlhttp.responseText);
				$('#tablespace').html( '<table cellspacing="0" id="queryResults"></table>' );
				$('#queryResults').dataTable( {
			        data: failureClass,
			        bAutoWidth: false,
			        bPaginate : false,
			        searching:false,
			        columns: [
			            { data : "failureId", title : "FAILURE CLASS"},
			            { data : "description", title : "DESCRIPTION"}],
					aoColumnDefs: [{ "sDefaultContent": '', "aTargets": [ '_all' ]}]
				} );    
				hideSpinner();
			} else {
				document.getElementById("tablespace").innerHTML = xmlhttp.responseText;
				hideSpinner();
			}
		}
	};
}
/**
 *  getEventCause retrieves values from EventCause table, using an XMLHttpRequest and parses the resulting xmlhttp response using jquery
 * @param url
 */
function getEventCause(url) {

	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", url, true);
	xmlhttp.send(); showSpinner();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var eventCause = JSON.parse(xmlhttp.responseText);
				$('#tablespace').html( '<table cellspacing="0" id="queryResults"></table>' );
				$('#queryResults').dataTable( {
			        data: eventCause,
			        bAutoWidth: false,
			        bPaginate : false,
			        sScrollY: $(window).height()-$(".nav_top_side").outerHeight()-$("#display").outerHeight()-80+"px",
			        columns: [
			            { data : "id.causeCode", title : "CAUSE CODE"},
			            { data : "id.eventId", title : "EVENT ID"},
			            { data : "description", title : "DESCRIPTION"}],
					aoColumnDefs: [{ "sDefaultContent": '', "aTargets": [ '_all' ]}]
				} );    
				hideSpinner();
			} else {
				hideSpinner();
				document.getElementById("tablespace").innerHTML = xmlhttp.responseText;
				hideSpinner();
			}
		}
	};
}

function getUserEquipment(url) {

	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", url, true);
	xmlhttp.send(); showSpinner();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var userEquipment = JSON.parse(xmlhttp.responseText);
				$('#tablespace').html( '<table cellspacing="0" id="queryResults"></table>' );
				$('#queryResults').dataTable( {
			        data: userEquipment,
			        bAutoWidth: false,
			        bPaginate : false,
			        sScrollY: $(window).height()-$(".nav_top_side").outerHeight()-$("#display").outerHeight()-80+"px",
			        columns: [
			            { data : "typeAllocationCode", title : "TAC"},
			            { data : "marketingName", title : "MARKETING NAME"},
			            { data : "manufacturer", title : "MANUFACTURER"},
			            { data : "accessCapability", title : "ACCESS CAPABILITY"},
			            { data : "model", title : "MODEL"},
			            { data : "vendorName", title : "VENDOR NAME"},
			            { data : "userEquipmentType", title : "UE TYPE"},
			            { data : "operatingSystem", title : "OS"},
			            { data : "inputMode", title : "INPUT MODE"}],
				aoColumnDefs: [
				        { "sDefaultContent": '', "aTargets": [ '_all' ]}]
			    } );    
				hideSpinner();
			} else {
				document.getElementById("tablespace").innerHTML = xmlhttp.responseText;
				hideSpinner();
			}
		}
	};
}
/**
 * getCountryNetworkCode retrieves values from CountryNetworkCode table, using an XMLHttpRequest and parses the resulting xmlhttp response using jquery
 */
function getCountryNetworkCode(url) {
	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", url, true);
	xmlhttp.send(); showSpinner();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var countryNetworkCodes = JSON.parse(xmlhttp.responseText);
				var myTable = "<table cellspacing='0'>";
				myTable += "<tr> <th>MCC</th> <th>MNC</th> <th>COUNTRY</th> <th>OPERATOR</th></tr>";
				for (var int = 0; int < countryNetworkCodes.length; int++) {
					myTable += "<tr> <td align='center'>" + countryNetworkCodes[int].id.mobileCountryCode
							+ "</td> <td align='center'>" + countryNetworkCodes[int].id.mobileNetworkCode
							+ "</td> <td align='center'>" + countryNetworkCodes[int].country
							+ "</td> <td align='center'>" + countryNetworkCodes[int].operator + "</td> </tr>";
				}
				myTable += "</table>";

				document.getElementById("tablespace").innerHTML = myTable;
				$('#tablespace').html( '<table cellspacing="0" id="queryResults"></table>' );
				$('#queryResults').dataTable( {
			        data: countryNetworkCodes,
			        bAutoWidth: false,
			        bPaginate : false,
			        sScrollY: $(window).height()-$(".nav_top_side").outerHeight()-$("#display").outerHeight()-80+"px",
			        columns: [
			            { data : "id.mobileCountryCode", title : "MCC"},
			            { data : "id.mobileNetworkCode", title : "MNC"},
			            { data : "country", title : "COUNTRY"},
			            { data : "operator", title : "OPERATOR"}],
					aoColumnDefs: [{ "sDefaultContent": '', "aTargets": [ '_all' ]}]
				} );    
				hideSpinner();
			} else {
				document.getElementById("tablespace").innerHTML = xmlhttp.responseText;
				hideSpinner();
			}
		}
	};
}

/**
 * getAllErrorLogger retrieves values from InsertionConflct table, using an XMLHttpRequest and parses the resulting xmlhttp response using jquery
 * @param url
 */
function getAllErrorLogger(url) {

	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", url, true);
	xmlhttp.send(); showSpinner();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var insertConflict = JSON.parse(xmlhttp.responseText);
				var myTable = "<table cellspacing='0'>";
				myTable += "<tr> <th>ID</th> <th>Data/Time</th> <th>Origin Table</th> <th>Input</th> <th>Error Description</th></tr>";
		
				for ( var i in insertConflict) {
					myTable += "<tr> <td align='center'>" + insertConflict[i].id
							+ "</td> <td align='center'>" + insertConflict[i].timeStamp
							+ "</td> <td align='center'>" + insertConflict[i].originTable
							+ "</td> <td align='center'>" + insertConflict[i].inputString
							+ "</td> <td align='center'>" + insertConflict[i].errorDescription
							+ "</td> </tr>";
				}
				myTable += "</table>";
				document.getElementById("tablespace").innerHTML = myTable;
				hideSpinner();
			} else {
				document.getElementById("tablespace").innerHTML = xmlhttp.responseText;
				hideSpinner();
			}
		}
	};
}

/**
 * Select the button on the left nav Side. This method displays the necessary css for a selected button.
 * @param button - The button to select
 */
function selectButton() {
	document.getElementById("chartContainer").style.display="none";
	var parent = this.parentNode;
	var buttons = parent.getElementsByTagName("BUTTON");
	for (var int = 0; int < buttons.length; int++) {
		buttons[int].setAttribute("class", "");
	}
	this.setAttribute("class", "selected");
}

/**
 * Verify if dateFrom and dateTo are valid
 * @returns {Boolean}
 */
function areValidDates(dateFrom, dateTo){
	var dateFromMillis = new Date(dateFrom).getTime();
	var dateToMillis = new Date(dateTo).getTime();
	var thisDateFrom = document.getElementById("dateFrom");
	var thisDateTo = document.getElementById("dateTo");
	thisDateFrom.setCustomValidity("");
	thisDateTo.setCustomValidity("");
	
	if(isNaN(dateFromMillis)){
		thisDateFrom.setCustomValidity("Invalid Date");
		document.getElementById("tablespace").innerHTML = "Invalid Date From";
		return false;
	}else if(isNaN(dateToMillis)) {
		thisDateTo.setCustomValidity("Invalid Date");
		document.getElementById("tablespace").innerHTML = "Invalid Date To";
		return false;
	}else if(dateFromMillis >= Date.now()){
		thisDateFrom.setCustomValidity("'Date From' must be in the past");
		document.getElementById("tablespace").innerHTML = "'Date From' must be in the past";
		return false;
	}if(dateToMillis <= dateFromMillis){
		thisDateTo.setCustomValidity("'Date from' must be earlier than 'Date to'");
		document.getElementById("tablespace").innerHTML = "'Date from' must be earlier than 'Date to'";
		return false;
	}
	return true;
}

/**
 * Get a valid XMLHTTP Object
 * @returns {xmlhttp}
 */
function getXmlhttpObject(){
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xmlhttp;
	
}

/**
 * Query to return all IMSIs between selected dates.
 * Displays records in table by 50 increment (7)
 */
function getAllImsiBetweenPeriodTime() {
	var dateFrom = document.getElementById("dateFrom").value;
	var dateTo = document.getElementById("dateTo").value;
	if(!areValidDates(dateFrom, dateTo)){
		return;
	}

	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET","rest/display/getImsiForGivenDate?dateFrom=" + dateFrom
					+ "&dateTo=" + dateTo, true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
	xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
	xmlhttp.send(); showSpinner();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var txt = xmlhttp.responseText;
				var x = new Array(txt);
				paginationCache = JSON.parse(txt);
				if (paginationCache.length == 0){
					document.getElementById("tablespace").innerHTML = "No faults to display";
					hideSpinner();
					return;
				}
				$('#tablespace').html( '<table cellspacing="0" id="queryResults"></table>' );
				myTable = $('#queryResults').dataTable( {
			        data: paginationCache,
			        bAutoWidth: false,
			        bPaginate : true,
			        deferRender: true,
			        fnDrawCallback: function(){
			        	$("#queryResults tbody tr").on("click", function(event){
			        		clearHighlightsOnTables();
			        		this.style.color = '#B67500';
							var selectedRowObject = myTable.fnGetData(myTable.fnGetPosition(this));
							var IMSI = selectedRowObject[0];
							var dateFrom = document.getElementById("dateFrom").value;
							var dateTo = document.getElementById("dateTo").value;
							if(!areValidDates(dateFrom, dateTo)){
								return;
							}
							$.ajax({
			        		    type: "GET", 
			        		    url: "rest/get/getFailureDescriptionBetweenPeriod?"
			        		    	+"imsi="+IMSI+"&from="+dateFrom+"&to="+dateTo,
			        		    dataType: 'json',
			        		}).done(function(data) {
			        			var popup = $('#popup');
								document.getElementById("popoverTitle").innerHTML="Selected IMSI failures:";
								contentDiv = document.getElementById("popContent");
								var tableText = "<table><tr><th>Date</th><th>Failure Class</th><th>Event Cause</th><th>Cell ID</th><th>NE version</th></tr>";
								for (var i = 0; i < data.length; i++) {
									var date = new Date(data[i][0]);
									var dateString = date.toLocaleDateString()+" "+ date.toLocaleTimeString().replace("PM","").replace("AM","");
									tableText+="<tr><td>"+dateString+"</td><td>"+data[i][1]+"</td><td>"+data[i][2]+"</td><td>"+data[i][3]+"</td><td>"+data[i][4]+"</td></tr>";
								}
								tableText+="</table>";
								contentDiv.innerHTML=tableText;
								popup.show();
								alignPopup();
			        		});
			        	});
			        },
			        columns: [
			            { title : "IMSI"},
			            { title : "ACCESS"},
			            { title : "MANUFACTURER"},
			            { title : "MODEL"},
			            { title : "COUNTRY"},
			            { title : "OPERATOR"}],
					aoColumnDefs: [{ "sDefaultContent": '', "aTargets": [ '_all' ]}]
				} );    
				hideSpinner();
				myTable.css("cursor","pointer");
			} else {
				document.getElementById("tablespace").innerHTML = xmlhttp.responseText;
				hideSpinner();
			}
		}
	};
}

function displayPager(){
	var pager = document.getElementById("pager");
	xmlhttp.open("GET", "rest/display/getFailureIds", true);
}
/**
 * createTable sets the tablespace div in user.html to " " and create the necessary input elements for different queries
 */
function createTable(innerHTML_tableData){
	document.getElementById("tablespace").innerHTML ="";
	document.getElementById("querySelection").innerHTML = innerHTML_tableData;
}
/**
 * showDates sets the date div in user.html to be visible
 */
function showDates(){
	document.getElementById("dates").style.display="block";
}
/**
 * hideDates sets the date div in user.html to be invisible
 */
function hideDates(){
	document.getElementById("dates").style.display="none";
}
/**
 * imsiTable returns a string, which creates input box for an imsiNumber
 * @returns {String}
 */
function imsiTable(){
	return "IMSI: <input type='number' id= 'imsiNumber'/>";
}
/**
 * failureTable returns a string, which creates input box for a failureClassId 
 * @returns {String}
 */
function failureTable(){
	jQuery.ajax({
        url:    'rest/display/getFailureClassIds',
        success: function(result) {
        			//resutl = JSON.parse(result);
                    var options;
     				for(var i=0; i<result.length; i++){
     					options+="<option value='"+result[i].failureId+"'>"+result[i].description+"</option>";
     				}
     				document.getElementById("failureClassId").innerHTML = options;
                 }
   });  
	return "Failure Class: <select id='failureClassId'></select>";
}
/**
 * modelTable returns a string, which creates an input box for a phoneModel
 * @returns {String}
 */
function modelTable(){
	return "Phone Model: <input type='text' id='phoneModel'/>"
}

function submitButton(string){
	return "<input id='submitQuery' type='button' class='button' value='Submit' onclick='"+string+"'/>";
}

/**
 * As a Network Management Engineer I want to count for each IMSI, the number of call failures and their total duration for a given time period (9)
 */
function getNumberOfFailuresAndDurationForEachImsiBetweenPeriodTime() {	
	var dateFrom = document.getElementById("dateFrom").value;
	var dateTo = document.getElementById("dateTo").value;
	
	if(!areValidDates(dateFrom, dateTo)){
		return;
	}
		var xmlhttp = getXmlhttpObject();
		xmlhttp.open("GET", "rest/display/getNumberOfFailuresAndDurationForEachImsiBetweenPeriodTime?dateFrom="+ dateFrom + "&dateTo=" + dateTo, true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
		xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
		xmlhttp.send(); showSpinner();
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				if (xmlhttp.status == 200) {			
					var paginationCache = JSON.parse(xmlhttp.responseText);
					if (paginationCache.length == 0){
						document.getElementById("tablespace").innerHTML = "No faults to display";
						hideSpinner();
					}else{
						$('#tablespace').html( '<table cellspacing="0" id="queryResults"></table>' );
						var myTable = $('#queryResults').dataTable( {
					        data: paginationCache,
					        bAutoWidth: false,
					        bPaginate : true,
					        deferRender: true,
					        fnDrawCallback: function(){
					        	$("#queryResults tbody tr").on("click", function(event){
					        		clearHighlightsOnTables();
					        		this.style.color = '#B67500';
									var selectedRowObject = myTable.fnGetData(myTable.fnGetPosition(this));
									getAllFailureByImsiBetweenTimePeriod(selectedRowObject[2], dateFrom, dateTo);
					        	});
					        },
					        columns: [
					            { title : "# FAILURES"},
					            { title : "DURATION"},
					            { title : "IMSI"}],
							aoColumnDefs: [{ "sDefaultContent": '', "aTargets": [ '_all' ]}]
						} );    
						hideSpinner();
						myTable.css("cursor","pointer");
					}
				} else {
					document.getElementById("tablespace").innerHTML = xmlhttp.responseText;
					hideSpinner();
				}
			}
		};
	}


function getAllFailureByImsiBetweenTimePeriod(imsi, dateFrom, dateTo){
	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", "rest/get/getAllFailureByImsiBetweenTimePeriod?imsi=" + imsi
			+"&dateFrom=" + dateFrom + "&dateTo=" +dateTo, true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
	xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
	xmlhttp.send();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var paginationCache = JSON.parse(xmlhttp.responseText);
				if (paginationCache.length == 0){
					document.getElementById("tablespace").innerHTML = "No faults to display";
				}
				else{
					var popup = $('#popup');
					var element = document.getElementById("popContent");
					var tableText = "<table><tr><th>Date</th><th>Cell ID</th><th>Duration</th>"
								  + "<th>Cause Description</th><th>Failure Description</th>"
								  + "<th>Country</th><th>Operator</th><th>Access</th></tr>";
					for(var i in paginationCache){
						var date = new Date(paginationCache[i][0]);
						tableText += "<tr><td>" + date.toLocaleDateString()+" "+ date.toLocaleTimeString().replace("PM","").replace("AM","")
								   + "</td><td>" + paginationCache[i][1]
								   + "</td><td>" + paginationCache[i][2]
								   + "</td><td>" + paginationCache[i][3]
								   + "</td><td>" + paginationCache[i][4]
								   + "</td><td>" + paginationCache[i][5]
								   + "</td><td>" + paginationCache[i][6]
								   + "</td><td>" + paginationCache[i][7] + "</td></tr>";
					}
					element.innerHTML=tableText;
					popup.show();
					var left = event.pageX;
					var top = event.pageY;
					var theHeight = $('#popup').height();
					popup.css('left', (left+50) + 'px');
					popup.css('top', (top-(theHeight/2)-10) + 'px');
				}	
			}
		}
	};
}


/**
 * As a customer service rep I want to see all the unique cause codes assoiciated with its call failures
 * for a given imsi (6)
 */
		
function getAllUniqueCauseCodeAndFailureClassByImsi(){
	var imsi = document.getElementById("imsiNumber").value;
	if(imsi == null || imsi == ""){
		document.getElementById("tablespace").innerHTML = "Please fill imsi number";
		return;
	}
		var xmlhttp = getXmlhttpObject();
		xmlhttp.open("GET", "rest/display/getAllUniqueCauseCodeAndFailureClassByImsi?imsi="
				+ imsi, true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
		xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
		xmlhttp.send(); showSpinner();

		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				if (xmlhttp.status == 200) {
					var paginationCache = JSON.parse(xmlhttp.responseText);
					if (paginationCache.length == 0){
						document.getElementById("tablespace").innerHTML = "No faults to display";
						hideSpinner();
					}else{
						$('#tablespace').html( '<table cellspacing="0" id="queryResults"></table>' );
						myTable = $('#queryResults').dataTable( {
					        data: paginationCache,
					        bAutoWidth: false,
					        bPaginate : true,
					        deferRender: true,
					        fnDrawCallback: function(){
					        	$("#queryResults tbody tr").on("click", function(event){
					        		clearHighlightsOnTables();
					        		this.style.color = '#B67500';
									var selectedRowObject = myTable.fnGetData(myTable.fnGetPosition(this));
									var IMSI = document.getElementById("imsiNumber").value;
									$.ajax({
					        		    type: "GET", 
					        		    url: "rest/get/getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode?"
					        		    	+"imsi="+IMSI+"&failureClassDescription="+selectedRowObject[0]+"&eventCauseDescription="+selectedRowObject[1],
					        		    dataType: 'json',
					        		}).done(function(data) {
					        			var popup = $('#popup');
										document.getElementById("popoverTitle").innerHTML="Most recent 10 failures:";
										contentDiv = document.getElementById("popContent");
										var tableText = "<table><tr><th>Date</th><th>CellID</th><th>Access</th><th>Manufacturer</th><th>Model</th><th>Country</th><th>Operator</th></tr>";
										for (var i = 0; i < data.length; i++) {
											var date = new Date(data[i][0]);
											var dateString = date.toLocaleDateString()+" "+ date.toLocaleTimeString().replace("PM","").replace("AM","");
											tableText+="<tr><td>"+dateString+"</td><td>"+data[i][1]+"</td><td>"+data[i][2]+"</td><td>"+data[i][3]+"</td><td>"+data[i][4]+"</td><td>"+data[i][5]+"</td><td>"+data[i][6]+"</td></tr>";
										}
										tableText+="</table>";
										contentDiv.innerHTML=tableText;
										popup.show();
										alignPopup();
					        		});
					        	});
					        },
					        columns: [
					            { title : "FAILURE CLASS"},
					            { title : "EVENT CAUSE"},
					            { title : "# OF OCCURENCES"}],
							aoColumnDefs: [{ "sDefaultContent": '', "aTargets": [ '_all' ]}]
						} );    
						hideSpinner();
						myTable.css("cursor","pointer");
					}
				} else {
					document.getElementById("tablespace").innerHTML = xmlhttp.responseText;
					hideSpinner();
				}
			}
		};
	}	

function displayPopup(){
	var popup = $('#popup');
	var theHeight = $('#popup').height();
	var theWidth = $('#popup').width();
	var viewport = {width:$(window).width(), height:$(window).height()};
	popup.css('left', mousePosition.x + "px");
	popup.css('top', mousePosition.y + "px");
	if(viewport.width < mousePosition.x + theWidth)
		popup.css('left',(viewport.width - theWidth) + "px");
	if(viewport.height <  mousePosition.x + theHeight)
		popup.css('top', (viewport.height - theHeight + 95) + "px");
}


function alignPopup(){
	var popup = $('#popup');
	var theHeight = $('#popup').height();
	var theWidth = $('#popup').width();
	var viewport = {width:$(window).width(), height:$(window).height()};
	if (mousePosition.x > viewport.width-theWidth) {
		popup.css('left', viewport.width-theWidth + 'px');
	}else
		popup.css('left', (mousePosition.x+10) + 'px');
	if (viewport.height+$(document).scrollTop() - mousePosition.y < viewport.height-theHeight){
		popup.css('top', $(document).scrollTop()+viewport.height-theHeight-50 + 'px');
	}else if (viewport.height+$(document).scrollTop() - mousePosition.y > viewport.height-theHeight){
		popup.css('top', $(document).scrollTop()+viewport.height-theHeight-50 + 'px');
	}else{
		popup.css('top', (mousePosition.y-(theHeight/2)-10) + 'px');
	}
}

/**
 * As a Support Engineer I want to display, for a given failure Cause Class, the IMSIs that were affected.(14)
 */
function getAllImsiByFailureClass(){
	getTotalNumberOfIMSIForAllFailureClasses();
	var failureId = document.getElementById("failureClassId").value;
	if (failureId == null || failureId == "" || isNaN(failureId) || failureId < 0 || failureId.length > 9) {
		document.getElementById("tablespace").innerHTML = "Invalid failure class";
		return;
	}
		var xmlhttp = getXmlhttpObject();
		xmlhttp.open("GET", "rest/display/getAllImsiByFailureClass?failureId="+failureId, true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
		xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
		xmlhttp.send(); showSpinner();

		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				if (xmlhttp.status == 200) {
					var paginationCache = JSON.parse(xmlhttp.responseText);
					if (paginationCache.length == 0){
						document.getElementById("tablespace").innerHTML = "No faults to display";
						hideSpinner();
					}else{
						$('#tablespace').html( '<table cellspacing="0" id="queryResults"></table>' );
						$('#queryResults').dataTable( {
					        data: paginationCache,
					        bAutoWidth: false,
					        bPaginate : true,
					        deferRender: true,
					        columns: [
					            { title : "IMSI"},
					            { title : "ACCESS"},
					            { title : "MANUFACTURER"},
					            { title : "MODEL"},
					            { title : "COUNTRY"},
					            { title : "OPERATOR"}],
							aoColumnDefs: [{ "sDefaultContent": '', "aTargets": [ '_all' ]}]
						});    
						hideSpinner();
					}
				} else {
					document.getElementById("tablespace").innerHTML = xmlhttp.responseText;
					hideSpinner();
				}
			}
		};
	}

/**
 * Get number of unique IMSI for all failure classes
 */
function getTotalNumberOfIMSIForAllFailureClasses(){
	$.ajax({
	    type: "GET", 
	    url: "rest/get/getTotalNumberOfIMSIForAllFailureClasses",
	    dataType: 'json'
	}).done(function(data) {
		var dps = [];
		var colours = ['#ff0000','#ff9900','#660033','#ffff00','#669900','#00ffcc','#0099ff','#0000ff','#cc3300','#333300']
		for (var i = 0; i < data.length; i++) {
			dps.push({name: data[i].failureClass.split(":")[1],y: data[i].number, color: colours[i], exploded: false});
		}
		document.getElementById("chartContainer").style.display="block";
		chart = new CanvasJS.Chart("chartContainer", {
			theme: "theme2",
			animationEnabled: true, 
			backgroundColor: "#FFFAF0",
			title:{
				text: "Distinct IMSI per Failure Class",
				fontSize: 18,
				fontColor: "black",
			},
			legend: {
				fontSize: 12
			},
			axisX:{
		        labelFontSize: 12,
		        labelFontColor: "grey"
		    },
		    axisY:{
		        labelFontSize: 12,
		        labelFontColor: "grey"
		    },
			data: [              
			{
				indexLabel: "{name} #percent%",
				indexLabelFontSize: 12,
				indexLabelFontColor: "grey",
				percentFormatString: "#0.##",
				toolTipContent: "{name} [{y}] (#percent%)",
	        	showInLegend: true, 
				type: "pie",
				dataPoints: dps,
			}
			]
		});
		var failClass = document.getElementById("failureClassId");
		var strText = failClass.options[failClass.selectedIndex].text;
		for (var i = 0; i < chart.options.data[0].dataPoints.length; i++) {
			if (chart.options.data[0].dataPoints[i].name.toLowerCase() == strText.toLowerCase()) {
				chart.options.data[0].dataPoints[i].exploded = true;
			}
		}
		chart.render();
	}).fail(function() {
	    alert("Sorry. Server unavailable. ");
	});
}

/**
 *  As a customer service rep I want to display for a given imsi, the event ID and cause code for any/all failures (4)
 */
function getAllEventCauseByImsi() {
	var imsi = document.getElementById("imsiNumber").value;
	if(imsi == null || imsi == ""){
		document.getElementById("tablespace").innerHTML = "Please fill imsi number";
		document.getElementById("chartContainer").style.display = "none";
		return;
	}
		var xmlhttp = getXmlhttpObject();
		xmlhttp.open("GET", "rest/display/getAllEventCauseByImsi?imsi="+imsi, true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
		xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
		xmlhttp.send(); showSpinner();

		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				if (xmlhttp.status == 200) {
					var paginationCache = JSON.parse(xmlhttp.responseText);
					if (paginationCache.length == 0){
						document.getElementById("tablespace").innerHTML = "No faults to display";
						document.getElementById("chartContainer").style.display = "none";
						hideSpinner();
					}else{
						$('#tablespace').html( '<table cellspacing="0" id="queryResults"></table>' );
						var mytable = $('#queryResults').dataTable( {
					        data: paginationCache,
					        bAutoWidth: false,
					        bPaginate : true,
					        deferRender: true,
					        fnDrawCallback : function(){$('#queryResults tbody tr').on("click", function(){
					        	clearHighlightsOnTables();
					        	this.style.color = '#B67500';
								var rowSelected = mytable.fnGetPosition(this);
								document.getElementById("chartContainer").style.display="inline";
								createTwoChart();
								document.getElementById("chart1").style.marginRight ="50%";
								document.getElementById("chart2").style.marginLeft ="50%";
								getNumberOfFailureForEachFailureClassByEventCauseAndImsi(mytable.fnGetData(rowSelected), imsi);
								getNumberOfFailureForEachCellIdByEventCauseAndImsi(mytable.fnGetData(rowSelected), imsi);
							});},
					        columns: [
					            { data: "id.causeCode", title : "CAUSE CODE"},
					            { data: "id.eventId", title : "EVENT ID"},
					            { data: "description", title : "DESCRIPTION"}],
							aoColumnDefs: [{ "sDefaultContent": '', "aTargets": [ '_all' ]}]
						} );
						hideSpinner();
						mytable.css("cursor","pointer");
					}
				} else {
					document.getElementById("tablespace").innerHTML = xmlhttp.responseText;
					hideSpinner();
				}
			}
		};
	}	


function getNumberOfFailureForEachCellIdByEventCauseAndImsi(eventCause, imsi){
	if (eventCause==null) {
		document.getElementById("chartContainer").style.display = "none";
		return;
	}
	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", "rest/get/getNumberOfFailureForEachCellIdByEventCauseAndImsi?imsi=" + imsi 
			+"&eventId=" + eventCause.id.eventId + "&causeCode=" + eventCause.id.causeCode, true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
	xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
	xmlhttp.send();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var paginationCache = JSON.parse(xmlhttp.responseText);
				if (paginationCache.length == 0){
					document.getElementById("chartContainer").style.display = "none";
				}else{
					var dps = [];
					for (var i in paginationCache){
						dps.push({name : "Cell ID:"+String(paginationCache[i][0]), y : paginationCache[i][1], click:function(e){
							getAllFailureByEventCauseCellIdAndImsi(eventCause, imsi,e);
							}}
						);
					}
					chart = new CanvasJS.Chart("chart2", {
						theme: "theme2",//theme1
						title:{
							text: "Failure",
							fontSize: 28
						},
						title:{
							text: "# failures per cell",
							fontSize: 18,
							fontColor: "black",
						},
						legend: {
							fontSize: 12
						},
						axisX:{
					        labelFontSize: 12,
					        labelFontColor: "grey"
					    },
					    axisY:{
					        labelFontSize: 12,
					        labelFontColor: "grey"
					    },
						animationEnabled: true, 
						backgroundColor: "#FFFAF0",
						height: 500,
						data: [{
							indexLabel: "#percent%",
							percentFormatString: "#0.##",
							toolTipContent: "{name} Occured:[{y}] (#percent%)",
							showInLegend: true, 
							type: "pie",
							dataPoints: dps
						}]
					});
					chart.render();
				}
			}
		}
	};
}


function getAllFailureByEventCauseCellIdAndImsi(eventCause, imsi,e){
	if (eventCause == null) {
		return;
	}
	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", "rest/get/getAllFailureByEventCauseCellIdAndImsi?imsi=" + imsi
			+"&eventId=" + eventCause.id.eventId + "&causeCode=" + eventCause.id.causeCode + "&cellId=" + e.dataPoint.name.split(":")[1], true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
	xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
	xmlhttp.send();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var paginationCache = JSON.parse(xmlhttp.responseText);
				if (paginationCache.length == 0){
					document.getElementById("tablespace").innerHTML = "No faults to display";
				}
				else{
					var popup = $('#popup');
					popup.attr('style', 'display:none');
					var element = document.getElementById("popContent");
					var tableText = "<table><tr><th>Date</th><th>Cell ID</th><th>Country</th><th>Operator</th>"
								  +	"<th>Failure Description</th><th>Access</th><th>Model</th></tr>";
					for(var i in paginationCache){
						var date = new Date(paginationCache[i][0]);
						tableText += "<tr><td>" + date.toLocaleDateString()+" "+ date.toLocaleTimeString().replace("PM","").replace("AM","")
								   + "</td><td>" + paginationCache[i][1]
								   + "</td><td>" + paginationCache[i][2]
								   + "</td><td>" + paginationCache[i][3]
								   + "</td><td>" + paginationCache[i][4]
								   + "</td><td>" + paginationCache[i][5]
								   + "</td><td>" + paginationCache[i][6] + "</td></tr>";
					}
					element.innerHTML=tableText;
					displayPopup();
					popup.show();
				}	
			}
		}
	};
}






function getNumberOfFailureForEachFailureClassByEventCauseAndImsi(eventCause, imsi){
	if (eventCause==null) {
		document.getElementById("chartContainer").style.display = "none";
		return;
	}
	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", "rest/get/getNumberOfFailureForEachFailureClassByEventCauseAndImsi?imsi=" + imsi 
			+"&eventId=" + eventCause.id.eventId + "&causeCode=" + eventCause.id.causeCode, true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
	xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
	xmlhttp.send();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var paginationCache = JSON.parse(xmlhttp.responseText);
				if (paginationCache.length == 0){
					document.getElementById("chartContainer").style.display = "none";
				}else{
					var dps = [];
					for (var i in paginationCache){
						dps.push({name : paginationCache[i][0], y : paginationCache[i][1], click:function(e){
							getAllFailureByEventCauseFailureClassAndImsi(eventCause, imsi, e);
							}}
						);
					}
					chart = new CanvasJS.Chart("chart1", {
						theme: "theme2",//theme1
						title:{
							text: "# of failures per Failure Class",
							fontSize: 18,
							fontColor: "black",
						},
						legend: {
							fontSize: 12
						},
						axisX:{
							labelFontSize: 12,
							labelFontColor: "grey"
						},
						axisY:{
							labelFontSize: 12,
							labelFontColor: "grey"
						},
						animationEnabled: true,   
						backgroundColor: "#FFFAF0",
						height: 500,
						data: [{
							indexLabel: "#percent%",
							percentFormatString: "#0.##",
							toolTipContent: "{name} [{y}] (#percent%)",
							showInLegend: true, 
							type: "pie",
							dataPoints: dps
						}]
					});
					chart.render();
				}
			}
		}
	};
}

function getAllFailureByEventCauseFailureClassAndImsi(eventCause, imsi, e){
	if (eventCause == null) {
		return;
	}
	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", "rest/get/getAllFailureByEventCauseFailureClassAndImsi?imsi=" + imsi
			+"&eventId=" + eventCause.id.eventId + "&causeCode=" + eventCause.id.causeCode + "&failureDescription=" +  e.dataPoint.name, true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
	xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
	xmlhttp.send();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var paginationCache = JSON.parse(xmlhttp.responseText);
				if (paginationCache.length == 0){
					document.getElementById("tablespace").innerHTML = "No faults to display";
				}
				else{
					var popup = $('#popup');
					var element = document.getElementById("popContent");
					var tableText = "<table><tr><th>Date</th><th>Cell ID</th><th>Country</th><th>Operator</th>"
								  +	"<th>Failure Description</th><th>Access</th><th>Model</th></tr>";
					for(var i in paginationCache){
						var date = new Date(paginationCache[i][0]);
						tableText += "<tr><td>" + date.toLocaleDateString()+" "+ date.toLocaleTimeString().replace("PM","").replace("AM","")
								   + "</td><td>" + paginationCache[i][1]
								   + "</td><td>" + paginationCache[i][2]
								   + "</td><td>" + paginationCache[i][3]
								   + "</td><td>" + paginationCache[i][4]
								   + "</td><td>" + paginationCache[i][5]
								   + "</td><td>" + paginationCache[i][6] + "</td></tr>";
					}
					element.innerHTML=tableText;
					displayPopup();
					popup.show();
				}	
			}
		}
	};
}
/**
 * As a Support Engineer I want to count for a given model of phone, the number of call failures during a given time period (8)
 */
function getNumberOfFailuresByModelBetweenPeriodTime() {
	var model = document.getElementById("phoneModel").value;
	var dateFrom = document.getElementById("dateFrom").value;
	var dateTo = document.getElementById("dateTo").value;
	if(!areValidDates(dateFrom, dateTo)){
		return;
	}
	if (model == null ||model == "") {
		document.getElementById("tablespace").innerHTML = "Please fill model";
		return;
	}
		var xmlhttp = getXmlhttpObject();
		xmlhttp.open("GET", "rest/display/getNumberOfFailuresByModelBetweenPeriodTime?model=" + model + "&dateFrom="
				+ dateFrom + "&dateTo=" + dateTo, true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
		xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
		xmlhttp.send(); showSpinner();

		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				if (xmlhttp.status == 200) {
					var paginationCache = JSON.parse(xmlhttp.responseText);
					paginationCache = JSON.parse("[[\""+model+"\","+paginationCache+"]]");
					if (paginationCache.length == 0){
						document.getElementById("tablespace").innerHTML = "No faults to display";
						hideSpinner();
					}else{
						$('#tablespace').html( '<table cellspacing="0" id="queryResults"></table>' );
						myTable = $('#queryResults').dataTable( {
					        data: paginationCache,
					        bAutoWidth: false,
					        bPaginate : true,
					        deferRender: true,
					        columns: [
					            { title : " MODEL"},
					            { title : "# OF FAILURES"}],
							aoColumnDefs: [{ "sDefaultContent": '', "aTargets": [ '_all' ]}]
						} );    
						hideSpinner();
						createTwoChart();
						getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod(dateFrom, dateTo, model);
						getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod(dateFrom, dateTo, model);
					}
				} else {
					document.getElementById("tablespace").innerHTML = xmlhttp.responseText;
					hideSpinner();
				}
			}
		};
	}	



function getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod(dateFrom, dateTo, model){
	if(!areValidDates(dateFrom, dateTo) || model == null || model == ""){
		document.getElementById("chartContainer").style.display = "none";
		return;
	}
	document.getElementById("chart2").style.marginTop="420px";
	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", "rest/get/getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod?model=" + model 
			+"&dateFrom=" + dateFrom + "&dateTo=" + dateTo, true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
	xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
	xmlhttp.send();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var paginationCache = JSON.parse(xmlhttp.responseText);
				if (paginationCache.length == 0){
					document.getElementById("chartContainer").style.display = "none";
				}else{
					var dps = [];
					for (var i in paginationCache){
						dps.push({label:"ID: "+paginationCache[i][0]+" Code: "+paginationCache[i][1], name : paginationCache[i][2], y : paginationCache[i][3], click:function(e){
							getAllFailureByEventCausePhoneModelBetweenTimePeriod(dateFrom, dateTo, model, e);
							}}
						);
					}
					document.getElementById("chartContainer").style.display = "block";
					chart = new CanvasJS.Chart("chart2", {
						theme: "theme2",
						title:{
							text: "# failures per Event Cause",
							fontSize: 18,
							fontColor: "black",
						},
						legend: {
							fontSize: 12
						},
						axisX:{
							labelFontSize: 12,
							labelFontColor: "grey",
							interval:1
						},
						axisY:{
							labelFontSize: 12,
							labelFontColor: "grey"
						},
						animationEnabled: true,
						backgroundColor: "#FFFAF0",
						height: 500,
						data: [{
							indexLabel: "{y}",
							indexLabelPlacement: "inside",
							indexLabelFontSize:12,
							indexLabelFontColor: "black",
							toolTipContent: "{name} [{y}]",
							type: "bar",
							dataPoints: dps
						}]
					});
					chart.render();
				}
			}
		}
	};
}

function getAllFailureByEventCausePhoneModelBetweenTimePeriod(dateFrom, dateTo, model, e){
	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", "rest/get/getAllFailureByEventCausePhoneModelBetweenTimePeriod?model=" + model
			+ "&description=" + e.dataPoint.name + "&dateFrom=" + dateFrom + "&dateTo=" + dateTo, true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
	xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
	xmlhttp.send();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var paginationCache = JSON.parse(xmlhttp.responseText);
				if (paginationCache.length == 0){
					document.getElementById("tablespace").innerHTML = "No faults to display";
				}
				else{
					var popup = $('#popup');
					var element = document.getElementById("popContent");
					var tableText = "<table><tr><th>Date</th><th>IMSI</th><th>Cell ID</th><th>Country</th>"
								  +	"<th>Operator</th><th>Failure Description</th><th>NE Version</th></tr>";
					for(var i in paginationCache){
						var date = new Date(paginationCache[i][0]);
						tableText += "<tr><td>" + date.toLocaleDateString()+" "+ date.toLocaleTimeString().replace("PM","").replace("AM","")
								   + "</td><td>" + paginationCache[i][1]
								   + "</td><td>" + paginationCache[i][2]
								   + "</td><td>" + paginationCache[i][3]
								   + "</td><td>" + paginationCache[i][4]
								   + "</td><td>" + paginationCache[i][5]
								   + "</td><td>" + paginationCache[i][6] + "</td></tr>";
					}
					element.innerHTML=tableText;
					displayPopup();
					popup.show();
				}	
			}
		}
	};
}

function getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod(dateFrom, dateTo, model){
	if(!areValidDates(dateFrom, dateTo) || model == null || model == ""){
		document.getElementById("chartContainer").style.display = "none";
		return;
	}
	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", "rest/get/getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod?model=" + model 
			+"&dateFrom=" + dateFrom + "&dateTo=" + dateTo, true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
	xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
	xmlhttp.send();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var paginationCache = JSON.parse(xmlhttp.responseText);
				if (paginationCache.length == 0){
					document.getElementById("chartContainer").style.display = "none";
				}else{
					var dps = [];
					var colours = ['#ff0000','#ff9900','#660033','#ffff00','#669900','#00ffcc','#0099ff','#0000ff','#cc3300','#333300']
					for (var i in paginationCache){
						dps.push({name : paginationCache[i][0], y : paginationCache[i][1], color: colours[i], click:function(e){
							getAllFailureByFailureClassPhoneModelBetweenTimePeriod(dateFrom, dateTo, model, e);
							}}
						);
					}
					document.getElementById("chartContainer").style.display = "block";
					chart = new CanvasJS.Chart("chart1", {
						theme: "theme2",
						title:{
							text: "# failures per Failure Class",
							fontSize: 18,
							fontColor: "black",
						},
						legend: {
							fontSize: 12
						},
						axisX:{
							labelFontSize: 12,
							labelFontColor: "grey"
						},
						axisY:{
							labelFontSize: 12,
							labelFontColor: "grey"
						},
						animationEnabled: true,   
						backgroundColor: "#FFFAF0",
						height: 400,
						data: [{
							indexLabel: "{name} #percent%",
							indexLabelFontSize: 12,
							indexLabelFontColor: "grey",
							percentFormatString: "#0.##",
							toolTipContent: "{name} [{y}] (#percent%)",
							showInLegend:true,
							type: "doughnut",
							dataPoints: dps
						}]
					});
					chart.render();
				}
			}
		}
	};
}


function getAllFailureByFailureClassPhoneModelBetweenTimePeriod(dateFrom, dateTo, model, e){
	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", "rest/get/getAllFailureByFailureClassPhoneModelBetweenTimePeriod?model=" + model
			+ "&description=" + e.dataPoint.name + "&dateFrom=" + dateFrom + "&dateTo=" + dateTo, true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
	xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
	xmlhttp.send();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var paginationCache = JSON.parse(xmlhttp.responseText);
				if (paginationCache.length == 0){
					document.getElementById("tablespace").innerHTML = "No faults to display";
				}
				else{
					var popup = $('#popup');
					var element = document.getElementById("popContent");
					var tableText = "<table><tr><th>Date</th><th>IMSI</th><th>Cell ID</th><th>Country</th>"
								  +	"<th>Operator</th><th>Failure Description</th><th>NE Version</th></tr>";
					for(var i in paginationCache){
						var date = new Date(paginationCache[i][0]);
						tableText += "<tr><td>" + date.toLocaleDateString()+" "+ date.toLocaleTimeString().replace("PM","").replace("AM","")
								   + "</td><td>" + paginationCache[i][1]
								   + "</td><td>" + paginationCache[i][2]
								   + "</td><td>" + paginationCache[i][3]
								   + "</td><td>" + paginationCache[i][4]
								   + "</td><td>" + paginationCache[i][5]
								   + "</td><td>" + paginationCache[i][6] + "</td></tr>";
					}
					element.innerHTML=tableText;
					displayPopup();
					popup.show();
				}	
			}
		}
	};
}



/**
 * As a Network Management Engineer I want to see, for a given model of phone, all the unique failure Event Id and cause code combinations
 * they have exhibited and the number of occurrences (10)
 */
function getUniqueEventCauseAndNumberOccurrencesByPhoneModel() {
	model = document.getElementById("phoneModel").value;
	if(model == null || model == ""){
		document.getElementById("tablespace").innerHTML = "Please fill phone model";
		return;
	}
		var xmlhttp = getXmlhttpObject();
		xmlhttp.open("GET", "rest/display/getUniqueEventCauseAndNumberOccurrencesByPhoneModel?model=" 
				+ model, true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
		xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
		xmlhttp.send(); showSpinner();

		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				if (xmlhttp.status == 200) {
					paginationCache = JSON.parse(xmlhttp.responseText);
					if (paginationCache.length == 0){
						document.getElementById("tablespace").innerHTML = "No faults to display";
						hideSpinner();
					}else{
						$('#tablespace').html( '<table cellspacing="0" id="queryResults"></table>' );
						myTable = $('#queryResults').dataTable( {
					        data: paginationCache,
					        bAutoWidth: false,
					        bPaginate : true,
					        deferRender: true,
					        columns: [
					            { title : "EVENT ID"},
					            { title : "CAUSE CODE"},
					            { title : "DESCRIPTION"},
					            { title : "# OF OCCURENCES"}],
							aoColumnDefs: [{ "sDefaultContent": '', "aTargets": [ '_all' ]}],
							fnDrawCallback: function(){
					        	$("#queryResults tbody tr").on("click", function(event){
					        		clearHighlightsOnTables();
					        		this.style.color = '#B67500';
									var selectedRowObject = myTable.fnGetData(myTable.fnGetPosition(this));
									getUniqueEventCauseAndNumberOccurrencesByPhoneModelClick(model,selectedRowObject[0],selectedRowObject[1]);
					        	});
					        },
						} );    
						hideSpinner();
						myTable.css("cursor","pointer");
						getUniqueEventCauseAndNumberOccurrencesByPhoneModelChart();
					}
				} else {
					document.getElementById("tablespace").innerHTML = xmlhttp.responseText;
					hideSpinner();
				}
			}
		};
	}	

function getUniqueEventCauseAndNumberOccurrencesByPhoneModelClick(model,eventId,causeCode){
	var popup = $('#popup');
	document.getElementById("popoverTitle").innerHTML="Failure details:";
	contentDiv = document.getElementById("popContent");
	$.ajax({
	    type: "GET", 
	    url: "rest/get/getFailuresByPhoneModel?"
	    	+"phone="+model+"&eventId="+eventId+"&causeCode="+causeCode,
	    dataType: 'json',
	}).done(function(data) {
		var popup = $('#popup');
		document.getElementById("popoverTitle").innerHTML="Selected Event failures:";
		contentDiv = document.getElementById("popContent");
		var tableText = "<table><tr><th>Date</th><th>Failure Class</th><th>Cell ID</th><th>NE version</th><th>IMSI</th><th>Network</th><th>Country</th><th>UE Type</th><th>UE OS</th><th>Access</th></tr>";
		for (var i = 0; i < data.length; i++) {
			var date = new Date(data[i][0]);
			var dateString = date.toLocaleDateString()+" "+ date.toLocaleTimeString().replace("PM","").replace("AM","");
			tableText+="<tr><td>"+dateString+"</td><td>"+data[i][1]+"</td><td>"+data[i][2]+"</td><td>"+data[i][3]+"</td><td>"+data[i][4]+"</td><td>"+data[i][5]+"</td><td>"+data[i][6]+"</td><td>"+data[i][7]+"</td><td>"+data[i][8]+"</td><td>"+data[i][9]+"</td></tr>";
		}
		tableText+="</table>";
		contentDiv.innerHTML=tableText;
		popup.show();
		alignPopup();
	});
}

function getUniqueEventCauseAndNumberOccurrencesByPhoneModelChart(){
	var dps = [];
	for (var i = 0; i < paginationCache.length; i++) {
		dps.push({label: paginationCache[i][0]+" "+paginationCache[i][1], name: paginationCache[i][2], y:paginationCache[i][3], click:function(e){getUniqueEventCauseAndNumberOccurrencesByPhoneModelClick(model,e.dataPoint.label.split(" ")[0],e.dataPoint.label.split(" ")[1])}});
	}
	document.getElementById("chartContainer").style.display="inline";
	chart = new CanvasJS.Chart("chartContainer", {
		theme: "theme2",
		animationEnabled: true,
		backgroundColor: "#FFFAF0",
		width: 800,
		height: 500,
		axisX:{labelAngle: 90,labelFontSize: 12,interval:1},
		data: [              
		{
			toolTipContent: "{name} [{y}]",
			type: "column",
			dataPoints: dps
		}
		]
	});
	chart.render();
}

/**
 * As a Network Management Engineer I want to see the top 10 Market/Operator/Cell Id combinations that had call
 * failures during a given time period (11)
 */
function getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime() {
	var dateFrom = document.getElementById("dateFrom").value;
	var dateTo = document.getElementById("dateTo").value;
	if(!areValidDates(dateFrom, dateTo)){
		return;
	} 
		var xmlhttp = getXmlhttpObject();
		xmlhttp.open("GET", "rest/display/getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime?dateFrom="
				+ dateFrom + "&dateTo=" + dateTo, true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
		xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
		xmlhttp.send(); showSpinner();
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				if (xmlhttp.status == 200) {
					paginationCache = JSON.parse(xmlhttp.responseText);
					if (paginationCache.length == 0){
						document.getElementById("tablespace").innerHTML = "No faults to display";
						hideSpinner();
						document.getElementById("chartContainer").style.display="none";
					}else{
						$('#tablespace').html( '<table cellspacing="0" id="queryResults"></table>' );
						$('#queryResults').dataTable( {
					        data: paginationCache,
					        bAutoWidth: false,
					        bPaginate : true,
					        deferRender: true,
					        columns: [
					            
					            { title : "MARKET"},
					            { title : "OPERATOR"},
					            { title : "CELL ID"},
					            { title : "# OF OCCURENCES"}],
							aoColumnDefs: [{ "sDefaultContent": '', "aTargets": [ '_all' ]}]
						} );    
						hideSpinner();
						drawgraph();
					}
					
				} else {
					document.getElementById("tablespace").innerHTML = xmlhttp.responseText;
					hideSpinner();
				}
			}
		};
	}

function getTop10UniqueMarketOperatorAndCellIds(){
	$.ajax({
	    type: "GET", 
	    url: "rest/display/getTop10UniqueMarketOperatorAndCellIds",
	    dataType: 'json'
	}).done(function(response) {
		paginateCache = response;
		if (paginateCache.length == 0){
			document.getElementById("tablespace").innerHTML = "No faults to display";
			hideSpinner();
		}else{
			$('#tablespace').html( '<table cellspacing="0" id="queryResults"></table>' );
			myTable = $('#queryResults').dataTable( {
		        data: paginateCache,
		        bAutoWidth: false,
		        bPaginate : true,
		        deferRender: true,
		        columns: [
		            { title : "MARKET"},
		            { title : "OPERATOR"},
		            { title : "CELL ID"},
		            { title : "# OF OCCURENCES"}],
				aoColumnDefs: [{ "sDefaultContent": '', "aTargets": [ '_all' ]}],
				fnDrawCallback: function(){
		        	$("#queryResults tbody tr").on("click", function(event){
		        		clearHighlightsOnTables();
		        		this.style.color = '#B67500';
						var selectedRowObject = myTable.fnGetData(myTable.fnGetPosition(this));
						createTwoChart();
						document.getElementById("chart1").style.marginRight ="50%";
						document.getElementById("chart2").style.marginLeft ="50%";
						drawGraphForThisCell(paginateCache,selectedRowObject);
						drawGraphForAllCells(selectedRowObject);
					});
		        }
			} );    
			hideSpinner();
			myTable.css("cursor","pointer");
		}
	});
}

function drawGraphForThisCell(paginateCache,row){
	var dps = [];
	var colours = ['#ff0000','#ff9900','#660033','#ffff00','#669900','#00ffcc','#0099ff','#0000ff','#cc3300','#333300'];
	for (var i = 0; i < paginateCache.length; i++) {
		if (paginateCache[i][0] == row[0] && paginateCache[i][1] == row[1] && paginateCache[i][2] == row[2] && paginateCache[i][3] == row[3]) {
			dps.push({name:paginateCache[i][0]+" "+paginateCache[i][1]+" cell "+paginateCache[i][2], y:paginateCache[i][3], exploded:true, color: colours[i], indexLabel:"(#percent%)"});
		}else
			dps.push({name:paginateCache[i][0]+" "+paginateCache[i][1]+" cell "+paginateCache[i][2], y:paginateCache[i][3], exploded:false, color: colours[i], indexLabel:"(#percent%)"});
	}
	chart = new CanvasJS.Chart("chart1", {
		theme: "theme2",
		title:{
			text: "Top 10 distribution",
			fontSize: 18,
			fontColor: "black",
		},
		legend: {
			fontSize: 12
		},
		animationEnabled: true,
		backgroundColor: "#FFFAF0",
		height: 400,
		width:400,
		data: [{
			indexLabelPlacement: "outside",
			indexLabelFontSize: 10,
			indexLabelFontColor: "black",
			percentFormatString: "#0.##",
			toolTipContent: "{name} [{y}] (#percent%)",
			showInLegend: true, 
			type: "doughnut",
			dataPoints: dps
		}]
	});
	chart.render();
}

function drawGraphForAllCells(row){
	$.ajax({
	    type: "GET", 
	    url: "rest/get/getNumberOfFailuresForThisMarketCellOperator?market="+row[0]+"&cell="+row[2]+"&operator="+row[1],
	    dataType: 'json'
	}).done(function(data) {
		var dps = [];
		dps.push({label:row[1]+" cell "+row[2], y:row[3]});
		dps.push({label:"Cell "+row[2], y:data.cell});
		dps.push({label:"Country: "+row[0], y:data.market});
		chart = new CanvasJS.Chart("chart2", {
			theme: "theme2",
			title:{
				text: "Operator, Cell, Country comparisson",
				fontSize: 18,
				fontColor: "black",
			},
			legend: {
				fontSize: 12
			},
			axisX:{
		        labelFontSize: 12,
		        labelFontColor: "grey"
		    },
		    axisY:{
		        labelFontSize: 12,
		        labelFontColor: "grey"
		    },
			animationEnabled: true,
			backgroundColor: "#FFFAF0",
			height: 400,
			width:400,
			data: [{
				indexLabel: "{y}",
		        indexLabelPlacement: "outside",  
		        indexLabelOrientation: "horizontal",
		        indexLabelFontSize: 12,
				indexLabelFontColor: "black",
				toolTipContent: "{label} [{y}]",
				type: "column",
				dataPoints: dps
			}]
		});
		chart.render();
	});
}

function drawgraph() {
	var chartData = [];
	var colours = ['#ff0000','#ff9900','#660033','#ffff00','#669900','#00ffcc','#0099ff','#0000ff','#cc3300','#333300'];
	for (var i = 0; i < paginationCache.length; i++) {
		chartData.push({showInLegend: true,
	        name:paginationCache[i][0]+ ' '+ paginationCache[i][1]+' '+ paginationCache[i][2],
	        toolTipContent: "{name} [{y}]",
	        markerType: "triangle", legendMarkerColor:  colours[i],
	        dataPoints: [
	        {y: paginationCache[i][3], color: colours[i]}
	        ]
		});
		chartData.push({showInLegend: false,
	        dataPoints: [
	        {y: 1}
	        ]
		})
	}
	document.getElementById("chartContainer").style.display="inline";
	chart = new CanvasJS.Chart("chartContainer", {
		theme: "theme2",
		animationEnabled: true,
		 scaleShowLabels : false,
		backgroundColor: "#FFFAF0",
		width: 800,
		height: 500,
		axisX:{labelAngle: 90,labelFontSize:0, labelColor:"#FFFAF0", interval: 1},
		data: chartData
	});
	chart.render();
}
 /**
 * As a Network Management Engineer I want to see the top 10 IMSIs that had call
 * failures during a given time period (12)
 */
 
function getTop10ImsiBetweenPeriodTime() {
	var dateFrom = document.getElementById("dateFrom").value;
	var dateTo = document.getElementById("dateTo").value;
	if(!areValidDates(dateFrom, dateTo)){
		document.getElementById("chartContainer").style.display = "none";
		return;
	} 
		var xmlhttp = getXmlhttpObject();
		xmlhttp.open("GET", "rest/display/getTop10ImsiBetweenPeriodTime?dateFrom="+ dateFrom + "&dateTo=" + dateTo, true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
		xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
		xmlhttp.send(); showSpinner();
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				if (xmlhttp.status == 200) {
					paginationCache = JSON.parse(xmlhttp.responseText);
					if (paginationCache.length == 0){
						document.getElementById("tablespace").innerHTML = "No faults to display";
						document.getElementById("chartContainer").style.display = "none";
						hideSpinner();
					}else{
						$('#tablespace').html( '<table cellspacing="0" id="queryResults"></table>' );
						myTable = $('#queryResults').dataTable( {
					        data: paginationCache,
					        bAutoWidth: false,
					        bPaginate : true,
					        deferRender: true,
					        fnDrawCallback: function(){
					        	$("#queryResults tbody tr td").on("click", function(event){
					        		clearHighlightsOnTables();
					        		this.parentNode.style.color = '#B67500';
									var selectedRowObject = myTable.fnGetData(myTable.fnGetPosition(this)[0]);
									var popup = $('#popup');
									document.getElementById("popoverTitle").innerHTML="Details:";
									contentDiv = document.getElementById("popContent");
									var tableText = "<table>";
									tableText+="<tr><td>IMSI</td><td>"+selectedRowObject[0]+"</td></tr>";
									tableText+="<tr><td># OF OCCURENCES</td><td>"+selectedRowObject[1]+"</td></tr>";
									tableText+="<tr><td>ACCESSIBILITY</td><td>"+selectedRowObject[2]+"</td></tr>";
									tableText+="<tr><td>MANUFACTURER</td><td>"+selectedRowObject[3]+"</td></tr>";
									tableText+="<tr><td>MODEL</td><td>"+selectedRowObject[4]+"</td></tr>";
									tableText+="<tr><td>COUNTRY</td><td>"+selectedRowObject[5]+"</td></tr>";
									tableText+="<tr><td>OPERATOR</td><td>"+selectedRowObject[6]+"</td></tr></table>";
									contentDiv.innerHTML=tableText;
									popup.show();
									alignPopup();
								    });
					        },
					        columns: [
					            { title : "IMSI"},
					            { title : "# OF OCCURENCES"},
					            { title : "ACCESSIBILITY", visible:false},
					            { title : "MANUFACTURER", visible:false},
					            { title : "MODEL", visible:false},
					            { title : "COUNTRY", visible:false},
					            { title : "OPERATOR", visible:false}],
							aoColumnDefs: [{ "sDefaultContent": '', "aTargets": [ '_all' ]}]
						} );    
						hideSpinner();
						myTable.css("cursor","pointer");
						displayBarChartForTop10IMSIBetweenTimePeriod();
					}
				} else {
					document.getElementById("tablespace").innerHTML = xmlhttp.responseText;
					hideSpinner();
				}
			}
		};
	}

function displayBarChartForTop10IMSIBetweenTimePeriod(){
		var dps = [];
		var colours = ['#ff0000','#ff9900','#660033','#ffff00','#669900','#00ffcc','#0099ff','#0000ff','#cc3300','#333300']
		for (var i = 0; i < paginationCache.length; i++) {
			dps.push({label: paginationCache[i][0], y:paginationCache[i][1], color: colours[i], click: function(e){displayBarChartForTop10IMSIBetweenTimePeriodClick(e);}});
		}
		document.getElementById("chartContainer").style.display="inline";
		chart = new CanvasJS.Chart("chartContainer", {
			theme: "theme2",
			animationEnabled: true,
			backgroundColor: "#FFFAF0",
			width: 800,
			height: 500,
			title:{
				text: "Top 10 IMSI with failures",
				fontSize: 18,
				fontColor: "black",
			},
			legend: {
				fontSize: 12
			},
			axisX:{
				labelFontSize: 12,
				labelFontColor: "grey",
				labelAngle: 60,
				labelFontSize: 14, 
				labelMaxWidth: 50,
				labelWrap: true
			},
			axisY:{
				labelFontSize: 12,
				labelFontColor: "grey"
			},
			data: [              
			{
				indexLabel: "{y}",
				indexLabelFontSize: 12,
				indexLabelFontColor: "black",
				toolTipContent: "{label} [{y}]",
				type: "column",
				dataPoints: dps
			}
			]
		});
		chart.render();
}

function displayBarChartForTop10IMSIBetweenTimePeriodClick(e){
	var imsi = e.dataPoint.label;
	var record = null;
	if (paginationCache == null)
		return;
	//find record with this imsi
	for (var i =0; i<paginationCache.length; i++){
		if (paginationCache[i][0]==imsi){
			record = paginationCache[i];
			break;
		}
	}
	var popup = $('#popup');
	contentDiv = document.getElementById("popContent");
	var tableText = "<table>";
	tableText+="<tr><td>IMSI</td><td>"+record[0]+"</td></tr>";
	tableText+="<tr><td># OF OCCURENCES</td><td>"+record[1]+"</td></tr>";
	tableText+="<tr><td>ACCESSIBILITY</td><td>"+record[2]+"</td></tr>";
	tableText+="<tr><td>MANUFACTURER</td><td>"+record[3]+"</td></tr>";
	tableText+="<tr><td>MODEL</td><td>"+record[4]+"</td></tr>";
	tableText+="<tr><td>COUNTRY</td><td>"+record[5]+"</td></tr>";
	tableText+="<tr><td>OPERATOR</td><td>"+record[6]+"</td></tr></table>";
	contentDiv.innerHTML=tableText;
	popup.show();
	alignPopup();
}

function displayBarChartForTop10IMSIBetweenTimePeriodClick(e){
	var imsi = e.dataPoint.label;
	var record = null;
	if (paginationCache == null)
		return;
	//find record with this imsi
	for (var i =0; i<paginationCache.length; i++){
		if (paginationCache[i][0]==imsi){
			record = paginationCache[i];
			break;
		}
	}
	var popup = $('#popup');
	contentDiv = document.getElementById("popContent");
	var tableText = "<table>";
	tableText+="<tr><td>IMSI</td><td>"+record[0]+"</td></tr>";
	tableText+="<tr><td># OF OCCURENCES</td><td>"+record[1]+"</td></tr>";
	tableText+="<tr><td>ACCESSIBILITY</td><td>"+record[2]+"</td></tr>";
	tableText+="<tr><td>MANUFACTURER</td><td>"+record[3]+"</td></tr>";
	tableText+="<tr><td>MODEL</td><td>"+record[4]+"</td></tr>";
	tableText+="<tr><td>COUNTRY</td><td>"+record[5]+"</td></tr>";
	tableText+="<tr><td>OPERATOR</td><td>"+record[6]+"</td></tr></table>";
	contentDiv.innerHTML=tableText;
	popup.show();
	alignPopup();
}

/**
 * As a Customer Service Rep, I want to count, for a given IMSI, the number of failures they have 
 * had during a given time period (5).
 */
function getNumberOfFailuresByImsiBetweenPeriodTime() {
	var imsi = document.getElementById("imsiNumber").value;
	var dateFrom = document.getElementById("dateFrom").value;
	var dateTo = document.getElementById("dateTo").value;

	if(!areValidDates(dateFrom, dateTo)){
		return;
	}
	if (imsi == null || imsi == "") {
		document.getElementById("tablespace").innerHTML = "Invalid imsi";
		document.getElementById("imsiNumber").setCustomValidity("Invalid IMSI");
		return;
	}
		var xmlhttp = getXmlhttpObject();
		xmlhttp.open("GET", "rest/display/getNumberOfFailuresByImsiBetweenPeriodTime?imsi="+imsi+"&dateFrom="+dateFrom+"&dateTo="+dateTo, true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
		xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
		xmlhttp.send(); showSpinner();

		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				if (xmlhttp.status == 200) {
					createTwoChart();
					document.getElementById("chart2").style.marginTop = "420px";
					getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod(dateFrom, dateTo, imsi);
					getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod(dateFrom, dateTo, imsi);
					var paginationCache = JSON.parse(xmlhttp.responseText);
					if (paginationCache.length == 0){
						document.getElementById("tablespace").innerHTML = "No failures to display";
						hideSpinner();
					}else{
						$('#tablespace').html( '<table cellspacing="0" id="queryResults"></table>' );
						var myTable = $('#queryResults').dataTable( {
					        data: paginationCache,
					        bAutoWidth: false,
					        bPaginate : true,
					        deferRender: true,
					        fnDrawCallback: function(){
					        	$("#queryResults tbody tr td").on("click", function(event){
					        		clearHighlightsOnTables();
					        		this.parentNode.style.color = '#B67500';
					        			var selectedRowObject = myTable.fnGetData(myTable.fnGetPosition(this)[0]);
					        			var popup = $('#popup');
					        			document.getElementById("popoverTitle").innerHTML="Details:";
					        			contentDiv = document.getElementById("popContent");
					        			var tableText = "<table>";
					        			tableText+="<tr><td>IMSI</td><td>"+selectedRowObject[0]+"</td></tr>";
					        			tableText+="<tr><td># OF OCCURENCES</td><td>"+selectedRowObject[1]+"</td></tr>";
					        			tableText+="<tr><td>ACCESSIBILITY</td><td>"+selectedRowObject[2]+"</td></tr>";
					        			tableText+="<tr><td>MANUFACTURER</td><td>"+selectedRowObject[3]+"</td></tr>";
					        			tableText+="<tr><td>MODEL</td><td>"+selectedRowObject[4]+"</td></tr>";
					        			tableText+="<tr><td>COUNTRY</td><td>"+selectedRowObject[5]+"</td></tr>";
					        			tableText+="<tr><td>OPERATOR</td><td>"+selectedRowObject[6]+"</td></tr></table>";
					        			contentDiv.innerHTML=tableText;
					        			popup.show();
					        			alignPopup();
					        		    });
					         },
					        columns: [
					            { title : "IMSI"},
					            { title : "# OF OCCURENCES"},
					            { title : "ACCESSIBILITY", visible:false},
					            { title : "MANUFACTURER", visible:false},
					            { title : "MODEL", visible:false},
					            { title : "COUNTRY", visible:false},
					            { title : "OPERATOR", visible:false}],
							aoColumnDefs: [{ "sDefaultContent": '', "aTargets": [ '_all' ]}]
						} );    
						hideSpinner();
						myTable.css("cursor","pointer");
					}
				} else {
					document.getElementById("tablespace").innerHTML = xmlhttp.responseText;
					hideSpinner();
				}
			}
		};
	}	



function getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod(dateFrom, dateTo, imsi){
	if(!areValidDates(dateFrom, dateTo) || imsi == null || imsi == ""){
		document.getElementById("chartContainer").style.display = "none";
		return;
	}
	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", "rest/get/getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod?imsi=" + imsi 
			+"&dateFrom=" + dateFrom + "&dateTo=" + dateTo, true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
	xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
	xmlhttp.send();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var paginationCache = JSON.parse(xmlhttp.responseText);
				if (paginationCache.length == 0){
					document.getElementById("chartContainer").style.display = "none";
				}else{
					var dps = [];
					for (var i in paginationCache){
						dps.push({label : "Id:"+paginationCache[i][0]+", Code:"+paginationCache[i][1], name: paginationCache[i][2], y : paginationCache[i][3], click:function(e){
							getAllFailureByEventCauseImsiBetweenTimePeriod(dateFrom, dateTo, imsi, e);
							}}
						);
					}
					document.getElementById("chartContainer").style.display = "block";
					chart = new CanvasJS.Chart("chart2", {
						theme: "theme2",
						title:{
							text: "# failures per Event Cause",
							fontSize: 18,
							fontColor: "black",
						},
						legend: {
							fontSize: 12
						},
						axisX:{
							labelFontSize: 12,
							labelFontColor: "grey",
							interval:1
						},
						axisY:{
							labelFontSize: 12,
							labelFontColor: "grey"
						},
						animationEnabled: true,
						backgroundColor: "#FFFAF0",
						height: 400,
						data: [{
							indexLabel: "{y}",
							indexLabelFontSize:10,
							indexLabelFontColor: "black",
							toolTipContent: "{name} [{y}]",
							type: "bar",
							dataPoints: dps
						}]
					});
					chart.render();
				}
			}
		}
	};
}


function getAllFailureByEventCauseImsiBetweenTimePeriod(dateFrom, dateTo, imsi, e){
	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", "rest/get/getAllFailureByEventCauseImsiBetweenTimePeriod?imsi=" + imsi
			+ "&description=" + e.dataPoint.name + "&dateFrom=" + dateFrom + "&dateTo=" + dateTo, true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
	xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
	xmlhttp.send();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var paginationCache = JSON.parse(xmlhttp.responseText);
				if (paginationCache.length == 0){
					document.getElementById("tablespace").innerHTML = "No faults to display";
				}
				else{
					var popup = $('#popup');
					var element = document.getElementById("popContent");
					var tableText = "<table><tr><th>Date</th><th>Cell ID</th><th>Country</th><th>Operator</th>"
								  +	"<th>Failure Description</th><th>Access</th><th>Model</th></tr>";
					for(var i in paginationCache){
						var date = new Date(paginationCache[i][0]);
						tableText += "<tr><td>" + date.toLocaleDateString()+" "+ date.toLocaleTimeString().replace("PM","").replace("AM","")
								   + "</td><td>" + paginationCache[i][1]
								   + "</td><td>" + paginationCache[i][2]
								   + "</td><td>" + paginationCache[i][3]
								   + "</td><td>" + paginationCache[i][4]
								   + "</td><td>" + paginationCache[i][5]
								   + "</td><td>" + paginationCache[i][6] + "</td></tr>";
					}
					element.innerHTML=tableText;
					displayPopup();
					popup.show();
				}	
			}
		}
	};
}



function getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod(dateFrom, dateTo, imsi){
	if(!areValidDates(dateFrom, dateTo) || imsi == null || imsi == ""){
		document.getElementById("chartContainer").style.display = "none";
		return;
	}
	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", "rest/get/getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod?imsi=" + imsi 
			+"&dateFrom=" + dateFrom + "&dateTo=" + dateTo, true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
	xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
	xmlhttp.send();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var paginationCache = JSON.parse(xmlhttp.responseText);
				if (paginationCache.length == 0){
					document.getElementById("chartContainer").style.display = "none";
				}else{
					var dps = [];
					for (var i in paginationCache){
						dps.push({name : paginationCache[i][0], y : paginationCache[i][1], click:function(e){
							getAllFailureByFailureClassImsiBetweenTimePeriod(dateFrom, dateTo, imsi, e);
							}}
						);
					}
					chart = new CanvasJS.Chart("chart1", {
						theme: "theme2",
						title:{
							text: "# failures per Failure Class",
							fontSize: 18,
							fontColor: "black",
						},
						legend: {
							fontSize: 12
						},
						axisX:{
							labelFontSize: 12,
							labelFontColor: "grey"
						},
						axisY:{
							labelFontSize: 12,
							labelFontColor: "grey"
						},
						animationEnabled: true,   
						backgroundColor: "#FFFAF0",
						height: 400,
						data: [{
							indexLabel: "#percent%",
							indexLabelFontSize: 12,
							indexLabelFontColor: "grey",
							percentFormatString: "#0.##",
							toolTipContent: "{name} [{y}] (#percent%)",
							showInLegend:true,
							type: "doughnut",
							dataPoints: dps
						}]
					});
					chart.render();
				}
			}
		}
	};
}


function getAllFailureByFailureClassImsiBetweenTimePeriod(dateFrom, dateTo, imsi, e){
	var xmlhttp = getXmlhttpObject();
	xmlhttp.open("GET", "rest/get/getAllFailureByFailureClassImsiBetweenTimePeriod?imsi=" + imsi
			+ "&failureDescription=" +  e.dataPoint.name +"&dateFrom=" + dateFrom + "&dateTo=" + dateTo, true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("identity", JSON.parse(getCookie("user")).id);
	xmlhttp.setRequestHeader("token", JSON.parse(getCookie("user")).token);
	xmlhttp.send();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				var paginationCache = JSON.parse(xmlhttp.responseText);
				if (paginationCache.length == 0){
					document.getElementById("tablespace").innerHTML = "No faults to display";
				}
				else{
					var popup = $('#popup');
					var element = document.getElementById("popContent");
					var tableText = "<table><tr><th>Date</th><th>Cell ID</th><th>Country</th><th>Operator</th>"
								  +	"<th>Failure Description</th><th>Access</th><th>Model</th></tr>";
					for(var i in paginationCache){
						var date = new Date(paginationCache[i][0]);
						tableText += "<tr><td>" + date.toLocaleDateString()+" "+ date.toLocaleTimeString().replace("PM","").replace("AM","")
								   + "</td><td>" + paginationCache[i][1]
								   + "</td><td>" + paginationCache[i][2]
								   + "</td><td>" + paginationCache[i][3]
								   + "</td><td>" + paginationCache[i][4]
								   + "</td><td>" + paginationCache[i][5]
								   + "</td><td>" + paginationCache[i][6] + "</td></tr>";
					}
					element.innerHTML=tableText;
					displayPopup();
					popup.show();
				}	
			}
		}
	};
}

function createTwoChart(){
	var element = document.getElementById("chartContainer");
	element.innerHTML= "<div id='chart1' ></div><div id='chart2'></div>";
	element.style.display = "block";
}

function removeZeroInFrontOfNumber(imsi){
	var str = imsi.toString();
	while(str.charAt(0) == '0'){
		str = str.substr(1);
	}
	return str;
}
function clearHighlightsOnTables(){
	var list = document.getElementsByTagName("TR");
	for (var i = 0; i < list.length; i++) {
		list[i].style.color="#666";
	}
}
