
package serviceLayer.baseData;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;

import model.BaseData;
import model.EventCause;
import model.FailureClass;
import dataAccess.baseData.BaseDataDAO;

@Local
@Stateless
public class BaseDataServiceBean implements BaseDataService {

    @EJB
    BaseDataDAO baseDataDAO;

    @Override
    public Collection<BaseData> getAllBaseData() {
        return baseDataDAO.getAllBaseData();
    }

    @Override
    public BaseData getBaseDataById(Integer id) {
        return baseDataDAO.getBaseDataById(id);
    }

    @Override
    public void persistBaseData(BaseData baseData) {
        baseDataDAO.persistBaseData(baseData);
    }

    @Override
    public void persistBaseData(Collection<BaseData> baseData) {
        baseDataDAO.persistBaseData(baseData);
    }

    @Override
    public boolean removeBaseData(BaseData baseData) {
        return baseDataDAO.removeBaseData(baseData);
    }

    @Override
    public boolean mergeBaseData(BaseData baseData) {
        return baseDataDAO.mergeBaseData(baseData);
    }

    @Override
    public Collection<Object[]> getAllUniqueCauseCodeAndFailureClassByImsi(
            BigInteger imsi) {
        return baseDataDAO.getAllUniqueCauseCodeAndFailureClassByImsi(imsi);
    }

    @Override
    public Collection<Object[]> getAllImsiByFailureClass(int failureId) {
        return baseDataDAO.getAllImsiByFailureClass(failureId);
    }

    @Override
    public Collection<Object[]> getAllImsiBetweenPeriodTime(Date dateFrom,
            Date dateTo) {
        return baseDataDAO.getAllImsiBetweenPeriodTime(dateFrom, dateTo);
    }

    @Override
    public Collection<Object[]> getNumberOfFailuresAndDurationForEachImsiBetweenPeriodTime(
            Date dateFrom, Date dateTo) {
        return baseDataDAO
                .getNumberOfFailuresAndDurationForEachImsiBetweenPeriodTime(
                        dateFrom, dateTo);
    }

    @Override
    public Collection<EventCause> getAllEventCauseByImsi(BigInteger imsi) {
        return baseDataDAO.getAllEventCauseByImsi(imsi);
    }

    @Override
    public Long getNumberOfFailuresByModelBetweenPeriodTime(String model,
            Date dateFrom, Date dateTo) {
        return baseDataDAO.getNumberOfFailuresByModelBetweenPeriodTime(model,
                dateFrom, dateTo);
    }

    @Override
    public Collection<Object[]> getUniqueFailureIdEventCauseAndNumberOccurrencesByPhoneModel(
            String model) {
        return baseDataDAO
                .getUniqueEventCauseAndNumberOccurrencesByPhoneModel(model);
    }

    @Override
    public Collection<Object[]> getNumberOfFailuresByImsiBetweenPeriodTime(BigInteger imsi,
            Date dateFrom, Date dateTo) {
        return baseDataDAO.getNumberOfFailuresByImsiBetweenPeriodTime(imsi,
                dateFrom, dateTo);
    }

    @Override
    public Collection<Object[]> getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime(
            Date dateFrom, Date dateTo) {
        return baseDataDAO
                .getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime(
                        dateFrom, dateTo);
    }

    @Override
    public Collection<Object[]> getTop10ImsiBetweenPeriodTime(Date dateFrom,
            Date dateTo) {
        return baseDataDAO.getTop10ImsiBetweenPeriodTime(dateFrom, dateTo);
    }

    @Override
    public Collection<Object[]> getImsiForGivenDate(Date dateFrom, Date dateTo) {

        return baseDataDAO.getImsiWithFailuresForGivenDate(dateFrom, dateTo);

    }

    @Override
    public Map<FailureClass, Integer> getTotalNumberOfIMSIForAllFailureClasses(
            Collection<FailureClass> failureClasses) {
        return baseDataDAO
                .getTotalNumberOfIMSIForAllFailureClasses(failureClasses);
    }

    @Override
    public Collection<Object[]> getNumberOfFailureForEachFailureClassByEventCauseAndImsi(
            Integer eventId, Integer causeCode, BigInteger imsi) {
        return baseDataDAO
                .getNumberOfFailureForEachFailureClassByEventCauseAndImsi(
                        eventId, causeCode, imsi);
    }

    @Override
    public Collection<Object[]> getAllFailureByEventCauseFailureClassAndImsi(
            Integer eventId, Integer causeCode, BigInteger imsi,
            String failureDescription) {
        return baseDataDAO.getAllFailureByEventCauseFailureClassAndImsi(
                eventId, causeCode, imsi, failureDescription);
    }

    @Override
    public Collection<BigInteger> getAllUniqueIMSIs() {
        return baseDataDAO.getAllUniqueIMSIs();
    }

    @Override
    public Collection<Object[]> getNumberOfFailureForEachCellIdByEventCauseAndImsi(
            Integer eventId, Integer causeCode, BigInteger imsi) {
        return baseDataDAO.getNumberOfFailureForEachCellIdByEventCauseAndImsi(
                eventId, causeCode, imsi);
    }

    @Override
    public Collection<Object[]> getAllFailureByEventCauseCellIdAndImsi(
            Integer eventId, Integer causeCode, BigInteger imsi, Integer cellId) {
        return baseDataDAO.getAllFailureByEventCauseCellIdAndImsi(eventId,
                causeCode, imsi, cellId);
    }

    @Override
    public Collection<Object[]> getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode(
            BigInteger imsi, String failureClassDescription,
            String eventCauseDescription) {
        return baseDataDAO
                .getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode(imsi,
                        failureClassDescription, eventCauseDescription);
    }

    @Override
    public Collection<Object[]> getFailureDescriptionBetweenPeriod(
            BigInteger imsi, Date from, Date to) {
        return baseDataDAO.getFailureDescriptionBetweenPeriod(imsi, from, to);
    }

    @Override
    public Collection<String> getUniquePhones() {
        return baseDataDAO.getUniquePhones();
    }

    @Override
    public Collection<Object[]> getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod(
            BigInteger imsi, Date DateFrom, Date DateTo) {
        return baseDataDAO.getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod(imsi, DateFrom, DateTo);
    }

    @Override
    public Collection<Object[]> getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod(
            BigInteger imsi, Date dateFrom, Date dateTo) {
        return baseDataDAO.getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod(imsi, dateFrom, dateTo);
    }

    @Override
    public Collection<Object[]> getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod(
            String model, Date dateFrom, Date dateTo) {
        return baseDataDAO.getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod(model, dateFrom, dateTo);
    }

    @Override
    public Collection<Object[]> getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod(
            String model, Date dateFrom, Date dateTo) {
        return baseDataDAO.getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod(model, dateFrom, dateTo);
    }

    @Override
    public Collection<Object[]> getAllFailureByEventCauseImsiBetweenTimePeriod(
            String description, BigInteger imsi, Date dateFrom, Date dateTo) {
        return baseDataDAO.getAllFailureByEventCauseImsiBetweenTimePeriod(description, imsi, dateFrom, dateTo);
    }

    @Override
    public Collection<Object[]> getAllFailureByFailureClassImsiBetweenTimePeriod(
            String failureDescription, BigInteger imsi, Date dateFrom,
            Date dateTo) {
        return baseDataDAO.getAllFailureByFailureClassImsiBetweenTimePeriod(failureDescription, imsi, dateFrom, dateTo);
    }

    @Override
    public Collection<Object[]> getAllFailureByEventCausePhoneModelBetweenTimePeriod(
            String description, String model, Date dateFrom, Date dateTo) {
        return baseDataDAO.getAllFailureByEventCausePhoneModelBetweenTimePeriod(description, model, dateFrom, dateTo);
    }

    @Override
    public Collection<Object[]> getAllFailureByFailureClassPhoneModelBetweenTimePeriod(
            String failureDescription, String model, Date dateFrom, Date dateTo) {
        return baseDataDAO.getAllFailureByFailureClassPhoneModelBetweenTimePeriod(failureDescription, model, dateFrom, dateTo);
    }

    @Override
    public Collection<Object[]> getFailuresByPhoneModel(String phone, Integer eventId, Integer causeCode) {
        return baseDataDAO.getFailuresByPhoneModel(phone, eventId, causeCode);
    }

    @Override
    public Collection<Object[]> getAllFailureByImsiBetweenTimePeriod(
            BigInteger imsi, Date dateFrom, Date dateTo) {
        return baseDataDAO.getAllFailureByImsiBetweenTimePeriod(imsi, dateFrom, dateTo);
    }

    @Override
    public Collection<Object[]> getTop10UniqueMarketOperatorAndCellIds() {
        return baseDataDAO.getTop10UniqueMarketOperatorAndCellIds();
    }

    @Override
    public Long getFailuresForThisCell(Integer cellId, String market) {
        return baseDataDAO.getFailuresForThisCell(cellId, market);
    }

    @Override
    public Long getNumberOfFailuresForThisMarket(String market) {
        return baseDataDAO.getNumberOfFailuresForThisMarket(market);
    }

    @Override
    public Long getFailuresForThisCellCountryOperator(Integer cellId,
            String market, String operator) {
        return baseDataDAO.getFailuresForThisCellCountryOperator(cellId, market, operator);
    }

}
