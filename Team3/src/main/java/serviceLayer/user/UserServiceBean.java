package serviceLayer.user;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Startup;
import javax.ejb.Stateless;

import dataAccess.user.UserDAO;
import model.User;

@Local
@Stateless
public class UserServiceBean implements UserService {

	@EJB
	private UserDAO userDao;
	
	@Override
	public Collection<User> getAllUser() {
		return userDao.getAllUser();
	}

	@Override
	public User getUserById(String id) {
		return userDao.getUserById(id);
	}

	@Override
	public void persistenceUser(User user) {
		userDao.persistenceUser(user);
	}

	@Override
	public boolean removeUser(User user) {
		return userDao.removeUser(user);
	}

	@Override
	public boolean mergeUser(User user) {
		return userDao.mergeUser(user);
	}

	@Override
	public User getUserByUsernameAndPassword(String username, String password) {
		return userDao.getUserByUsernameAndPassword(username, password);
	}
	
}
