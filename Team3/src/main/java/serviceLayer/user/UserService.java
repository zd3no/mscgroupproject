package serviceLayer.user;

import java.util.Collection;

import javax.ejb.Local;

import model.User;

/**
 * UserDAO provides methods to retrieve a collection of User objects, individual User
 * objects based on the id, add User objects to a repository, remove User object from
 * a repository
 *
 */
@Local
public interface UserService {

	/**
	 * Retrieve all existing User objects.
	 * @return Collection of User objects or empty Collection if DB contains no entries.
	 * <b>NOTE:</b>I an intermediary cache is present, this method bypases it.
	 */
	Collection<User> getAllUser();
	
	/**
	 * Retrieve a User object from the database/cache based on the id 
	 * @param id
	 * @return User object or null if not found in DB
	 */
	User getUserById(String id);
	
	/**
	 * Adds a User object to the LookUpEntityCache.userMap (a ConcurrentHashMap), only if it is absent from the map,
	 * and subsequently adds the User object to the DB. 
	 * @param user
	 */
	void persistenceUser(User user);
	
	/**
	 * Removes a User object from the LookUpEntityCache.userMap (a ConcurrentHashMap), only if the key
	 * maps to that object) and subsequently removes the User object from the DB.  
	 * @param user
	 * @return true if successful
	 */
	boolean removeUser(User user);
	
	/**
	 * Merges User entity to LookUpEntityCache.userMap (a ConcurrentHashMap) and subsequently to DB
	 * if there exist such id in the cache. If a new User entity is merged, it will be added to cache and database.
	 * @param user The User entity to merge
	 * @return True if the entity has been updated/persisted successfully, False if user is null or the id is null
	 */
	boolean mergeUser(User user);

	/**
	 * This method returns the user from the database with matching username and password
	 * @param username
	 * @param password
	 * @return User or null if not such user has been found in database
	 */
	User getUserByUsernameAndPassword(String username, String password);
	
}
