package serviceLayer.failureClass;

import java.util.Collection;

import javax.ejb.Local;

import model.FailureClass;
/**
 * FailureClassService provides methods to retrieve a collection of FailureClass objects, individual FailureClass
 * objects based on the failureId, add FailureClass objects to a repository, remove FailureClass object from
 * a repository
 * @author dee
 *
 */
@Local
public interface FailureClassService {
	/**
	 * Retrieve all existing FailureClass objects.
	 * @return Collection of FailureClass objects or empty Collection if DB contains no entries.
	 * <b>NOTE:</b>I an intermediary cache is present, this method bypases it.
	 */
	Collection<FailureClass> getAllFailureClasses();
	
	/**
	 * Retrieve a FailureClass object from the Collection failureClasses, based on the failureId 
	 * @param failureId
	 * @return FailureClass object or null if not found in DB
	 */
	FailureClass getFailureClassById(Integer failureId);
	
	/**
	 * Adds a FailureClass object to the LookUpEntityCache.failureClassMap (a ConcurrentHashMap), only if it is absent from the map,
	 * and subsequently adds the FailureClass object to the DB. 
	 * @param failureClass
	 */
	void persistFailureClass(FailureClass failureClass);
	
	/**
	 * Adds FailureClass objects from the collection failureClasses to the LookUpEntityCache.failureClassMap (a ConcurrentHashMap), only if they are absent from the map,
	 * and subsequently add the FailureClass objects to the DB. 
	 * @param failureClasses
	 */
	void persistFailureClass(Collection<FailureClass> failureClasses);
	
	/**
	 * Removes a FailureClass object from the LookUpEntityCache.failureClassMap (a ConcurrentHashMap), only if the key
	 * maps to that object) and subsequently removes the FailureClass object from the DB.  
	 * @param failureClass
	 * @return true if successful
	 */
	boolean removeFailureClass(FailureClass failureClass);
	
	/**
	 * Merges FailureClass entity to LookUpEntityCache.failureClassMap (a ConcurrentHashMap) and subsequently to DB
	 * if there exist such id in the cache. If a new FailureClass entity is merged, it will be added to cache and database.
	 * @param failureClass The FailureClass entity to merge
	 * @return True if the entity has been updated/persisted successfully, False if failureClass is null or the id is null
	 */
	boolean mergeFailureClass(FailureClass failureClass);
	
	
}
