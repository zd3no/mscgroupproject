package serviceLayer.failureClass;

import java.util.Collection;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;

import dataAccess.failureClass.FailureClassDAO;
import model.FailureClass;

@Local
@Stateless
public class FailureClassServiceBean implements FailureClassService {
	
	@EJB
	private FailureClassDAO failureClassDAO;

	public FailureClassServiceBean() {}

	@Override
	public Collection<FailureClass> getAllFailureClasses() {
		return failureClassDAO.getAllFailureClasses();
	}

	@Override
	public FailureClass getFailureClassById(Integer failureId) {
		return failureClassDAO.getFailureClassById(failureId);
	}

	@Override
	public void persistFailureClass(FailureClass failureClass){
		failureClassDAO.persistFailureClass(failureClass);
	}

	@Override
	public void persistFailureClass(Collection<FailureClass> failureClasses){
		failureClassDAO.persistFailureClass(failureClasses);
	}

	@Override
	public boolean removeFailureClass(FailureClass failureClass){
		return failureClassDAO.removeFailureClass(failureClass);
	}

	@Override
	public boolean mergeFailureClass(FailureClass failureClass){
		return failureClassDAO.mergeFailureClass(failureClass);
	}

}
