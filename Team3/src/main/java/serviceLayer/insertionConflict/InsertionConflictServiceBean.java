package serviceLayer.insertionConflict;

import java.util.Collection;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;

import dataAccess.insertionConflict.InsertionConflictDAO;
import model.InsertionConflict;

@Local
@Stateless
public class InsertionConflictServiceBean implements InsertionConflictService {
	
	@EJB
	InsertionConflictDAO errorLogger;

	@Override
	public void logValidationError(InsertionConflict insertionConflict) {
		errorLogger.logValidationError(insertionConflict);
	}

	@Override
	public void logPersistenceError(InsertionConflict insertionConflict) {
		errorLogger.logPersistenceError(insertionConflict);
	}

	@Override
	public void logValidationError(
			Collection<InsertionConflict> insertionConflicts) {
		errorLogger.logValidationError(insertionConflicts);
	}

	/*@Override
	public Collection<InsertionConflict> getAllErrorLogger() {
		return errorLogger.getAllInsertionConflict();
	}*/

}
