
package serviceLayer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import model.User;
import model.UserType;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.jboss.resteasy.util.Base64;

import restMapping.CentralAuthority;
import restMapping.UploadFile;
import serviceLayer.user.UserService;

@Startup
@Singleton
public class UploadFileMonitor {

    public final String FOLDER = "C:/Users/NhatHa/Desktop/Test1";
    public static Logger log = Logger.getLogger(UploadFileMonitor.class
            .getName());
    @EJB
    UploadFile uploadFile;
    @EJB
    UserService userService;
    @EJB
    CentralAuthority centralAuthority;

    @PostConstruct
    public void init() throws Exception {
        createAdminIfNotExist();
        final long pollingInterval = 5 * 1000;
        File folder = new File(FOLDER);

        if (!folder.exists()) {
            throw new RuntimeException("Directory not found: " + FOLDER);
        }

        FileAlterationObserver observer = new FileAlterationObserver(folder);
        FileAlterationMonitor monitor = new FileAlterationMonitor(
                pollingInterval);
        FileAlterationListener listener = new FileAlterationListener() {

            @Override
            public void onFileCreate(File file) {
                String fileName = file.getName();
                if (fileName.contains(".upload"))
                    return;
                try {
                    Path filePath = file.getCanonicalFile().toPath();
                    fileName += ".uploading";
                    Files.move(filePath, filePath.resolveSibling(fileName));
                    File newFile = new File(filePath.toString() + ".uploading");
                    InputStream is = new FileInputStream(newFile);
                    String report = uploadFile.uploadFile(is);
                    is.close();
                    if (report != null) {
                        fileName = fileName.replace(".uploading", "");
                        FileWriter writer = new FileWriter(new File(FOLDER
                                + "/FileLogger.txt"), true);
                        writer.write(System.lineSeparator());
                        writer.write(new Date().toString() + " :: "
                                + fileName + " = " + report);
                        writer.close();
                        log.info("Uploaded File: " + fileName);
                        fileName += ".uploaded";
                        filePath = newFile.getCanonicalFile().toPath();
                        Files.move(filePath, filePath.resolveSibling(fileName));
                    } else {
                        fileName = fileName.replace(".uploading", "");
                        Files.move(newFile.getCanonicalFile().toPath(), newFile.getCanonicalFile().toPath().resolveSibling(fileName));
                    }
                } catch (IOException e) {
                    e.printStackTrace(System.err);
                }
            }

            @Override
            public void onFileDelete(File file) {
                try {
                    System.out.println("File removed: "
                            + file.getCanonicalPath());
                } catch (IOException e) {
                    e.printStackTrace(System.err);
                }
            }

            @Override
            public void onDirectoryChange(File arg0) {}

            @Override
            public void onDirectoryCreate(File arg0) {}

            @Override
            public void onDirectoryDelete(File arg0) {}

            @Override
            public void onFileChange(File arg0) {}

            @Override
            public void onStart(FileAlterationObserver arg0) {}

            @Override
            public void onStop(FileAlterationObserver arg0) {}
        };

        observer.addListener(listener);
        monitor.addObserver(observer);
        monitor.start();
    }

    private void createAdminIfNotExist() {
        if (userService.getAllUser().size() == 0) {
            User user = centralAuthority.makeUserWithPassword(Base64.encodeBytes(new String("admin:admin").getBytes()));
            user.setName("Admin");
            user.setUserType(UserType.ADMINISTRATION);
            userService.persistenceUser(user);
        }
    }

}
