package serviceLayer.entityCache;

import java.math.BigInteger;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;

import model.CountryNetworkCode;
import model.EventCause;
import model.FailureClass;
import model.InsertionConflict;
import model.User;
import model.UserEquipment;
import dataAccess.LookupEntityCache;

@Stateless @Local
public class EntityCacheServiceBean implements EntityCache {

	public EntityCacheServiceBean() {
	}
	
	@Inject LookupEntityCache lookupCache;

	@Override
	public boolean persist(Object entity, Class<?> entityClass) {
		return lookupCache.persist(entity, entityClass);
	}

	@Override
	public boolean merge(Object entity, Class<?> entityClass) {
		return lookupCache.merge(entity, entityClass);
	}

	@Override
	public boolean remove(Object entity, Class<?> entityClass) {
		return lookupCache.remove(entity, entityClass);
	}

	@Override
	public final ConcurrentHashMap<Integer, EventCause> getEventCauseMap() {
		return lookupCache.getEventCauseMap();
	}

	@Override
	public final ConcurrentHashMap<Integer, FailureClass> getFailureClassMap() {
		return lookupCache.getFailureClassMap();
	}

	@Override
	public final ConcurrentHashMap<Integer, CountryNetworkCode> getCountryNetworkCodeMap() {
		return lookupCache.getCountryNetworkCodeMap();
	}

	@Override
	public final ConcurrentHashMap<BigInteger, UserEquipment> getUserEquipmentMap() {
		return lookupCache.getUserEquipmentMap();
	}

	@Override
	public final EventCause getEventCause(Integer eventId, Integer causeCode) {
		return lookupCache.getEventCause(eventId, causeCode);
	}

	@Override
	public final CountryNetworkCode getCountryNetworkCode(Integer mobileCountryCode,
			Integer mobileNetworkCode) {
		return lookupCache.getCountryNetworkCode(mobileCountryCode, mobileNetworkCode);
	}

	@Override
	public final UserEquipment getUserEquipment(BigInteger typeAllocationCode) {
		return lookupCache.getUserEquipment(typeAllocationCode);
	}

	@Override
	public final FailureClass getFailureClass(Integer failureId) {
		return lookupCache.getFailureClass(failureId);
	}

	@Override
	public Collection<InsertionConflict> getInsertionConflicts() {
		return lookupCache.getInsertionConflicts();
	}

	@Override
	public void addInsertionConflict(InsertionConflict conflict) {
		lookupCache.addInsertionConflict(conflict);
	}

	@Override
	public void resetInsertionConflicts() {
		lookupCache.resetInsertionConflicts();
	}

	@Override
	public ConcurrentHashMap<String, User> getUserMap() {
		return lookupCache.getUserMap();
	}

	@Override
	public User getUser(String id) {
		return lookupCache.getUser(id);
	}

}
