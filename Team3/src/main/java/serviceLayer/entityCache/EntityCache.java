package serviceLayer.entityCache;

import java.math.BigInteger;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.Local;

import model.CountryNetworkCode;
import model.EventCause;
import model.FailureClass;
import model.InsertionConflict;
import model.User;
import model.UserEquipment;

/**
 * LookUpEntityCache provides concurrent hash maps for the objects created from the Failure Class, Event Cause,
 * User Equipment and CountryNetwork codes data.  Provides methods for each of the class of objects to add them to the
 * relevant hash map (if not already present) and subsequently add them to the DB.  Similar methods are provided to remove
 * and merge the objects from the relevant hash map and the database.  Generic persist, remove and merge methods are provided 
 * which identify the class of an entity and call the relevant method for that class.
 *
 */
@Local
public interface EntityCache {

	/**
	 * Method that persists the desired entity to the cache and database.
	 * @param entity Non-null entity with a valid id (primary key)
	 * @param entityClass The class of the entity to persist. Supports: EventCause, FailureClass, UserEquipment, CoutnryNetworkCode and User
	 * @return True if persist is successful, False otherwise.
	 */
	public abstract boolean persist(Object entity, Class<?> entityClass);

	/**
	 * Method that merges the desired entity to the cache and database. If such entity is already in the cache, it will be replaced, otherwise it will be added and
	 * subsequently persisted to database.
	 * @param entity Non-null entity with a valid id (primary key)
	 * @param entityClass The class of the entity to persist. Supports: EventCause, FailureClass, UserEquipment, CoutnryNetworkCode and User
	 * @return True if update is successful, False otherwise.
	 */
	public abstract boolean merge(Object entity, Class<?> entityClass);

	/**
	 * Method that removes the desired entity to the cache and database.
	 * @param entity Non-null entity with a valid id (primary key)
	 * @param entityClass The class of the entity to persist. Supports: EventCause, FailureClass, UserEquipment, CoutnryNetworkCode and User
	 * @return True if removal is successful, False otherwise.
	 */
	public abstract boolean remove(Object entity, Class<?> entityClass);

	/**
	 * Get the eventCauseMap from cache. <p><b>DO NOT ADD OR REMOVE ELEMENTS DIRECTLY FROM THIS MAP!!!</b>
	 * @return eventCauseMap ConcurrentHashMap
	 */
	public abstract ConcurrentHashMap<Integer, EventCause> getEventCauseMap();

	/**
	 * Get the failureClassMap from cache. <p><b>DO NOT ADD OR REMOVE ELEMENTS DIRECTLY FROM THIS MAP!!!</b>
	 * @return failureClassMap ConcurrentHashMap
	 */
	public abstract ConcurrentHashMap<Integer, FailureClass> getFailureClassMap();

	/**
	 * Get the countryNetworkCodeMap from cache. <p><b>DO NOT ADD OR REMOVE ELEMENTS DIRECTLY FROM THIS MAP!!!</b>
	 * @return countryNetworkCodeMap ConcurrentHashMap
	 */
	public abstract ConcurrentHashMap<Integer, CountryNetworkCode> getCountryNetworkCodeMap();

	/**
	 * Get the userEquipmentMap from cache. <p><b>DO NOT ADD OR REMOVE ELEMENTS DIRECTLY FROM THIS MAP!!!</b>
	 * @return userEquipmentMap ConcurrentHashMap
	 */
	public abstract ConcurrentHashMap<BigInteger, UserEquipment> getUserEquipmentMap();
	
	/**
	 * Get the userMap from cache. <p><b>DO NOT ADD OR REMOVE ELEMENTS DIRECTLY FROM THIS MAP!!!</b>
	 * @return userMap ConcurrentHashMap
	 */ 
	public abstract ConcurrentHashMap<String, User> getUserMap();

	/**
	 * Gets the EventCause entity from the cache based on the provided eventId and causeCode
	 * <p><b>NOTE:</b> Editing properties on this object without merging afterwards will leave the cache - database unsynchronized.
	 * <p>ALWAYS merge entity if you make changes to keep the cache-db synchronized.
	 * @param eventId
	 * @param causeCode
	 * @return EventCause if such entity exists in the cache, NULL otherwise
	 */
	public abstract EventCause getEventCause(Integer eventId, Integer causeCode);

	/**
	 * Gets the CountryNetworkCode entity from the cache based on the provided mobileCountryCode and mobileNetworkCode
	 * <p><b>NOTE:</b> Editing properties on this object without merging afterwards will leave the cache - database unsynchronized.
	 * <p>ALWAYS merge entity if you make changes to keep the cache-db synchronized.
	 * @param mobileCountryCode
	 * @param mobileNetworkCode
	 * @return CountryNetworkCode if such entity exists in the cache, NULL otherwise
	 */
	public abstract CountryNetworkCode getCountryNetworkCode(
			Integer mobileCountryCode, Integer mobileNetworkCode);

	/**
	 * Gets the UserEquipment entity from the cache based on the provided typeAllocationCode
	 * <p><b>NOTE:</b> Editing properties on this object without merging afterwards will leave the cache - database unsynchronized.
	 * <p>ALWAYS merge entity if you make changes to keep the cache-db synchronized.
	 * @param typeAllocationCode
	 * @return UserEquipment if such entity exists in the cache, NULL otherwise
	 */
	public abstract UserEquipment getUserEquipment(BigInteger typeAllocationCode);

	/**
	 * Gets the FailureClass entity from the cache based on the provided failureId
	 * <p><b>NOTE:</b> Editing properties on this object without merging afterwards will leave the cache - database unsynchronized.
	 * <p>ALWAYS merge entity if you make changes to keep the cache-db synchronized.
	 * @param failureId
	 * @return FailureClass if such entity exists in the cache, NULL otherwise
	 */
	public abstract FailureClass getFailureClass(Integer failureId);

	/**
	 * Gets the User entity from the cache based on the provided id
	 * <p><b>NOTE:</b> Editing properties on this object without merging afterwards will leave the cache - database unsynchronized.
	 * <p>ALWAYS merge entity if you make changes to keep the cache-db synchronized.
	 * @param id
	 * @return User if such entity exists in the cache, NULL otherwise
	 */
	public abstract User getUser(String id);
		
	/**
	 * Gets the InsertionConflict collection that currently resides in cache. Please note that this collection is not persisted in the database.
	 * @return Collection of InsertionConflict
	 */
	public abstract Collection<InsertionConflict> getInsertionConflicts();

	/**
	 * Adds InsertionConflict to the collection of InsertionConflict that is temporarilu stored in cache. This action does not add the InsertionConflict to
	 * Databse and must be persisted separately.
	 * @param conflict
	 */
	public abstract void addInsertionConflict(InsertionConflict conflict);

	/**
	 * Deletes the current Collection of InsertionConflict from cache. This method should be called immediately after you have persisted InsertionConflicts to Database.
	 */
	public abstract void resetInsertionConflicts();

}