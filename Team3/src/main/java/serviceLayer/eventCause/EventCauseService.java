package serviceLayer.eventCause;

import java.util.Collection;

import javax.ejb.Local;

import model.EventCause;
/**
 * EventCauseService provides methods to retrieve a collection of CountryNetworkCode objects, individual CountryNetworkCode
 * objects based on a composite PK, add EventCause objects to a repository and remove EventCause objects from
 * a repository.
 * @author dee
 *
 */
@Local
public interface EventCauseService {
	/**
	 * Retrieve all existing EventCause objects from database.
	 * @return Collection of EventCause objects / empty Collection if DB contains no entries.
	 * Please note that this method queries the Database directly, bypassing any existent cache.
	 */
	Collection<EventCause> getAllEventCauses();
	
	/**
	 * Retrieves a EventCause object from the database/cache based on the eventId
	 * and causeCode 
	 * @param eventId
	 * @param causeCode
	 * @return EventCause object or null if not found in DB
	 */
	EventCause getEventCauseById(Integer eventId, Integer causeCode);
	
	/**
	 * Adds an EventCause object to the LookUpEntityCache.eventCauseMap (a ConcurrentHashMap), only if it is absent from the map,
	 * and subsequently adds the EventCause object to the DB. 
	 * @param eventCause
	 */
	void persistEventCode(EventCause eventCause);
	
	/**
	 * Adds  EventCause objects from a collection of EventCause objects (eventCauses) to the LookUpEntityCache.eventCauseMap (a ConcurrentHashMap), 
	 * only if it is absent from the map, and subsequently adds the EventCause object to the DB. 
	 * @param eventCauses
	 */
	void persistEventCodes(Collection<EventCause> eventCauses);
	
	/**
	 * Removes an EventCause object from the LookUpEntityCache.eventCauseMap (a ConcurrentHashMap), only if the key
	 * maps to that object) and subsequently removes the EventCause object from the DB.  
	 * @param eventCause
	 * @return true if successful
	 */
	boolean removeEventCode(EventCause eventCause);
	
	/**
	 * Merges EventCause entity to LookUpEntityCache.eventCauseMap (a ConcurrentHashMap) and subsequently to DB
	 * if there exist such id in the cache. If a new EventCause entity is merged, it will be added to cache and database.
	 * @param eventCause The EventCause entity to merge
	 * @return True if the entity has been updated/persisted successfully, False if eventCause is null or the id is null
	 */
	boolean mergeEventCode(EventCause eventCause);
	
	
}
