package serviceLayer.eventCause;

import java.util.Collection;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;

import dataAccess.eventCause.EventCauseDAO;
import model.EventCause;

@Local
@Stateless
public class EventCauseServiceBean implements EventCauseService {
	
	@EJB
	private EventCauseDAO eventCauseDAO;
	
	public EventCauseServiceBean() {
	}

	@Override
	public Collection<EventCause> getAllEventCauses() {
		return eventCauseDAO.getAllEventCauses();
	}

	@Override
	public EventCause getEventCauseById(Integer eventId, Integer causeCode) {
		return eventCauseDAO.getEventCauseById(eventId, causeCode);
	}

	@Override
	public void persistEventCode(EventCause eventCause) {
		eventCauseDAO.persistEventCode(eventCause);
	}

	@Override
	public boolean removeEventCode(EventCause eventCause) {
		return eventCauseDAO.removeEventCode(eventCause);
	}

	@Override
	public boolean mergeEventCode(EventCause eventCause){
		return eventCauseDAO.mergeEventCode(eventCause);
	}

	@Override
	public void persistEventCodes(Collection<EventCause> eventCauses) {
		eventCauseDAO.persistEventCodes(eventCauses);
	}

}
