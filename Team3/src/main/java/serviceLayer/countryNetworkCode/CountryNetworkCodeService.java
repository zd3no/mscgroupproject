package serviceLayer.countryNetworkCode;

import java.util.Collection;

import javax.ejb.Local;

import model.CountryNetworkCode;
/**
 * CountryNetworkCodeService provides methods to retrieve a collection of CountryNetworkCode objects, individual CountryNetworkCode
 * objects based on a composite PK, add CountryNetworkCode objects to a repository and remove CountryNetworkCode objects from
 * a repository.
 * @author dee
 *
 */
@Local
public interface CountryNetworkCodeService {
	/**
	 *Retrieve all existing CountryNetworkCode objects 
	 * @return Collection of CountryNetworkCode objects or empty Collection 
	 */
	Collection<CountryNetworkCode> getAllCountryNetworkCode();
	
	/**
	 * Retrieves a CountryNetworkCode object from the database/cache based on the mobileCountryCode
	 * and the mobileNetworkCode
	 * @param mobileCountryCode
	 * @param mobileNetworkCode
	 * @return CountryNetworkCode object or null if not found 
	 */
	CountryNetworkCode getCountryNetworkCodeById(Integer mobileCountryCode, Integer mobileNetworkCode);
	
	/**
	 * Adds a CountryNetworkCode object to the LookUpEntityCache.countryNetworkCodeMap (a ConcurrentHashMap), only if it is absent from the map,
	 * and subsequently adds the CountryNetworkCode object to the DB. 
	 * @param countryNetworkCode must be non-null valid CountryNetworkCode object
	 */
	void persistCountryNetworkCode(CountryNetworkCode countryNetworkCode);
	
	/**
	 * Adds a collection of CountryNetworkCode objects to the LookUpEntityCache.countryNetworkCodeMap (a ConcurrentHashMap), only if object is absent from the map,
	 * and subsequently adds the CountryNetworkCode object to the DB. 
	 * @param countryNetworkCode must have a non-null Id
	 */
	void persistCountryNetworkCodes(Collection<CountryNetworkCode> countryNetworkCodes);
	
	/**
	 * Removes a CountryNetworkCode object from the LookUpEntityCache.countryNetworkCodeMap (a ConcurrentHashMap), only if the key
	 * maps to that object) and subsequently removes the CountryNetworkCode object from the DB.  
	 * @param countryNetworkCode The CountryNetworkCode entity to remove (Must have a non-null id)
	 * @return True if the entity has been removed successfully, false otherwise.
	 */
	boolean removeCountryNetworkCode(CountryNetworkCode countryNetworkCode);
	
	/**
	 * Merges CountryNetworkCode entity to LookUpEntityCache.countryNetworkCodeMap (a ConcurrentHashMap) and subsequently to DB
	 * if there exist such id in the cache. If a new CountryNetworkCode entity is merged, it will be added to cache and database.
	 * @param countryNetworkCode The CountryNetworkCode entity to merge
	 * @return true if the entity has been updated/persisted successfully, false if countryNetworkCode is null or the id is null
	 */
	boolean mergeCountryNetworkCode(CountryNetworkCode countryNetworkCode);
}
