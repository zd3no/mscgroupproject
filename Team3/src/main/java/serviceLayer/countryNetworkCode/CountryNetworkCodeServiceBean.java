package serviceLayer.countryNetworkCode;

import java.util.Collection;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;

import dataAccess.countryNetworkCode.CountryNetworkCodeDAO;
import model.CountryNetworkCode;

@Local
@Stateless
public class CountryNetworkCodeServiceBean implements CountryNetworkCodeService {
	
	@EJB
	CountryNetworkCodeDAO countryNetworkCodeDAO;

	public CountryNetworkCodeServiceBean() {}

	@Override
	public Collection<CountryNetworkCode> getAllCountryNetworkCode() {
		return countryNetworkCodeDAO.getAllCountryNetworkCode();
	}

	@Override
	public CountryNetworkCode getCountryNetworkCodeById(
			Integer mobileCountryCode, Integer mobileNetworkCode) {
		return countryNetworkCodeDAO.getCountryNetworkCodeById(mobileCountryCode, mobileNetworkCode);
	}

	@Override
	public void persistCountryNetworkCode(CountryNetworkCode countryNetworkCode){
		countryNetworkCodeDAO.persistCountryNetworkCode(countryNetworkCode);
	}

	@Override
	public void persistCountryNetworkCodes(Collection<CountryNetworkCode> countryNetworkCodes){
		countryNetworkCodeDAO.persistCountryNetworkCodes(countryNetworkCodes);
	}

	@Override
	public boolean removeCountryNetworkCode(CountryNetworkCode countryNetworkCode){
		return countryNetworkCodeDAO.removeCountryNetworkCode(countryNetworkCode);
	}

	@Override
	public boolean mergeCountryNetworkCode(CountryNetworkCode countryNetworkCode){
		return countryNetworkCodeDAO.mergeCountryNetworkCode(countryNetworkCode);
	}

}
