package serviceLayer.userEquipment;

import java.math.BigInteger;
import java.util.Collection;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;

import dataAccess.userEquipment.UserEquipmentDAO;
import model.UserEquipment;

@Local
@Stateless
public class UserEquipmentServiceBean implements UserEquipmentService {
	
	@EJB
	private UserEquipmentDAO userEquipmentDAO;

	public UserEquipmentServiceBean() {}

	@Override
	public Collection<UserEquipment> getAllUserEquipment() {
		return userEquipmentDAO.getAllUserEquipment();
	}

	@Override
	public UserEquipment getUserEquipmentById(BigInteger typeAllocationCode) {
		return userEquipmentDAO.getUserEquipmentById(typeAllocationCode);
	}

	@Override
	public void persistUserEquipment(UserEquipment userEquipment){
		userEquipmentDAO.persistUserEquipment(userEquipment);
	}

	@Override
	public void persistUserEquipments(Collection<UserEquipment> userEquipments){
		userEquipmentDAO.persistUserEquipments(userEquipments);
	}

	@Override
	public boolean removeUserEquipment(UserEquipment userEquipment){
		return userEquipmentDAO.removeUserEquipment(userEquipment);
	}

	@Override
	public boolean mergeUserEquipment(UserEquipment userEquipment){
		return userEquipmentDAO.mergeUserEquipment(userEquipment);
	}

}
