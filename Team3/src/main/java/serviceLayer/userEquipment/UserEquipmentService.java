package serviceLayer.userEquipment;

import java.math.BigInteger;
import java.util.Collection;

import javax.ejb.Local;

import model.UserEquipment;
/**
* UserEquipmentService provides methods to retrieve a collection of UserEquipment objects, individual UserEquipment
* objects based on the failureId, add UserEquipment objects to a repository, remove a UserEquipment object from
* a repository
* @author dee
*
*/
@Local
public interface UserEquipmentService {

	/**
	 * Retrieve all existing UserEquipment objects from database. 
	 * @return Collection of UserEquipment objects or an empty Collection if DB contains no entries.
	 * <b>NOTE:</b>I an intermediary cache is present, this method bypases it.
	 */
	Collection<UserEquipment> getAllUserEquipment();
	
	/**
	 * Retrieve a UserEquipment object from the database/cache based on the typeAllocationCode (primary key)  
	 * @param typeAllocationCode
	 * @return UserEquipment object or null if not found in DB
	 */
	UserEquipment getUserEquipmentById(BigInteger typeAllocationCode);
	
	/**
	 * Adds a UserEquipment object to the LookUpEntityCache.userEquipmentMap (a ConcurrentHashMap), only if it is absent from the map,
	 * and subsequently adds the UserEquipment object to the DB. 
	 * @param userEquipment
	 */
	void persistUserEquipment(UserEquipment userEquipment);
	
	/**
	 * Adds UserEquipment objects from the collection userEquipments to the LookUpEntityCache.userEquipmentMap (a ConcurrentHashMap), only if they are absent from the map,
	 * and subsequently add the UserEquipment objects to the DB. 
	 * @param userEquipment
	 */
	void persistUserEquipments(Collection<UserEquipment> userEquipments);
	
	/**
	 * Removes a UserEquipment object from the LookUpEntityCache.userEquipmentMap (a ConcurrentHashMap), only if the key
	 * maps to that object) and subsequently removes the UserEquipment object from the DB.  
	 * @param userEquipment
	 * @return true if successful
	 */
	boolean removeUserEquipment(UserEquipment userEquipment);
	
	/**
	 * Merges UserEquipment entity to LookUpEntityCache.userEquipmentMap (a ConcurrentHashMap) and subsequently to DB
	 * if there exist such id in the cache. If a new UserEquipment entity is merged, it will be added to cache and database.
	 * @param userEquipment The UserEquipment entity to merge
	 * @return True if the entity has been updated/persisted successfully, False if userEquipment is null or the id is null
	 */
	boolean mergeUserEquipment(UserEquipment userEquipment);
}
