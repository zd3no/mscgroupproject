
package restMapping;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import model.FailureClass;
import model.UserType;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import serviceLayer.baseData.BaseDataService;
import serviceLayer.countryNetworkCode.CountryNetworkCodeService;
import serviceLayer.eventCause.EventCauseService;
import serviceLayer.failureClass.FailureClassService;
import serviceLayer.userEquipment.UserEquipmentService;

@Path("/get")
public class SupplementalInfo {

    @EJB
    private EventCauseService eventCauseService;
    @EJB
    private FailureClassService failureClassService;
    @EJB
    private UserEquipmentService userEquipmentService;
    @EJB
    private CountryNetworkCodeService countryNetworkCodeService;
    @EJB
    private BaseDataService baseDataService;
    @EJB
    private CentralAuthority centralAuthority;
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    @SuppressWarnings("unchecked")
    @GET
    @Path("/getTotalNumberOfIMSIForAllFailureClasses")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTotalNumberOfIMSIForAllFailureClasses(@Context HttpHeaders headers) {
        if (centralAuthority.isAllowed(headers, UserType.SERVICE)) {
            Collection<FailureClass> failureClasses = failureClassService.getAllFailureClasses();
            Map<FailureClass, Integer> toReturn = baseDataService.getTotalNumberOfIMSIForAllFailureClasses(failureClasses);
            JSONArray jsonArray = new JSONArray();
            for (Entry<FailureClass, Integer> entry : toReturn.entrySet()) {
                JSONObject obj = new JSONObject();
                obj.put("failureClass", entry.getKey().getFailureId() + ":" + entry.getKey().getDescription());
                obj.put("number", entry.getValue());
                jsonArray.add(obj);
            }
            return Response.status(200).entity(jsonArray).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getNumberOfFailureForEachFailureClassByEventCauseAndImsi")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNumberOfFailureForEachFailureClassByEventCauseAndImsi(@Context HttpHeaders headers,
            @QueryParam("imsi") BigInteger imsi, @QueryParam("eventId") Integer eventId, @QueryParam("causeCode") Integer causeCode) {
        if (centralAuthority.isAllowed(headers, UserType.SUPPORT)) {
            if (imsi != null && eventId != null && causeCode != null) {
                return Response.status(200)
                        .entity(baseDataService.getNumberOfFailureForEachFailureClassByEventCauseAndImsi(eventId, causeCode, imsi)).build();
            }
            else
                return Response.status(404).entity("Invalid Data").build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getAllFailureByEventCauseFailureClassAndImsi")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllFailureByEventCauseFailureClassAndImsi(@Context HttpHeaders headers,
            @QueryParam("imsi") BigInteger imsi, @QueryParam("eventId") Integer eventId, @QueryParam("causeCode") Integer causeCode,
            @QueryParam("failureDescription") String failureDescription) {
        if (centralAuthority.isAllowed(headers, UserType.SUPPORT)) {
            if (imsi != null && eventId != null && causeCode != null && failureDescription != null) {
                return Response.status(200)
                        .entity(baseDataService.getAllFailureByEventCauseFailureClassAndImsi(eventId, causeCode, imsi, failureDescription)).build();
            }
            else
                return Response.status(404).entity("Invalid Data").build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getNumberOfFailureForEachCellIdByEventCauseAndImsi")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNumberOfFailureForEachCellIdByEventCauseAndImsi(@Context HttpHeaders headers,
            @QueryParam("imsi") BigInteger imsi, @QueryParam("eventId") Integer eventId, @QueryParam("causeCode") Integer causeCode) {
        if (centralAuthority.isAllowed(headers, UserType.SUPPORT)) {
            if (imsi != null && eventId != null && causeCode != null) {
                return Response.status(200).entity(baseDataService.getNumberOfFailureForEachCellIdByEventCauseAndImsi(eventId, causeCode, imsi))
                        .build();
            }
            else
                return Response.status(404).entity("Invalid Data").build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getAllFailureByEventCauseCellIdAndImsi")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllFailureByEventCauseCellIdAndImsi(@Context HttpHeaders headers,
            @QueryParam("imsi") BigInteger imsi, @QueryParam("eventId") Integer eventId, @QueryParam("causeCode") Integer causeCode,
            @QueryParam("cellId") Integer cellId) {
        if (centralAuthority.isAllowed(headers, UserType.SUPPORT)) {
            if (imsi != null && eventId != null && causeCode != null && cellId != null) {
                return Response.status(200).entity(baseDataService.getAllFailureByEventCauseCellIdAndImsi(eventId, causeCode, imsi, cellId)).build();
            }
            else
                return Response.status(404).entity("Invalid Data").build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getUniqueIMSIs")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllUniqueIMSI(@Context HttpHeaders headers) {
        if (centralAuthority.isAllowed(headers, UserType.SUPPORT)) {
            return Response.status(200).entity(baseDataService.getAllUniqueIMSIs()).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode(@Context HttpHeaders headers,
            @QueryParam("imsi") BigInteger imsi, @QueryParam("failureClassDescription") String failureClassDescription,
            @QueryParam("eventCauseDescription") String eventCauseDescription) {
        if (centralAuthority.isAllowed(headers, UserType.SUPPORT)) {
            if (imsi == null || failureClassDescription == null || eventCauseDescription == null)
                return Response.ok().status(404).entity("Invalid Data").build();
            return Response
                    .status(200)
                    .entity(baseDataService.getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode(imsi, failureClassDescription,
                            eventCauseDescription)).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getFailureDescriptionBetweenPeriod")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFailureDescriptionBetweenPeriod(@Context HttpHeaders headers, @QueryParam("imsi") BigInteger imsi,
            @QueryParam("from") String dateFrom, @QueryParam("to") String dateTo) {
        if (centralAuthority.isAllowed(headers, UserType.SUPPORT)) {
            Date from;
            Date to;
            simpleDateFormat.setLenient(false);
            try {
                from = simpleDateFormat.parse(dateFrom);
                to = simpleDateFormat.parse(dateTo);
            } catch (ParseException e) {
                return Response.ok().status(404).entity("Not Valid Dates!").build();
            }
            if (imsi == null)
                return Response.ok().status(404).entity("Not Valid IMSI!").build();
            return Response.ok().entity(baseDataService.getFailureDescriptionBetweenPeriod(imsi, from, to)).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getUniquePhones")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUniquePhones(@Context HttpHeaders headers) {
        if (centralAuthority.isAllowed(headers, UserType.SUPPORT)) {
            return Response.status(200).entity(baseDataService.getUniquePhones()).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod(@Context HttpHeaders headers,
            @QueryParam("imsi") BigInteger imsi, @QueryParam("dateFrom") String dateFrom, @QueryParam("dateTo") String dateTo) {
        if (centralAuthority.isAllowed(headers, UserType.SUPPORT)) {
            Date from;
            Date to;
            simpleDateFormat.setLenient(false);
            try {
                from = simpleDateFormat.parse(dateFrom);
                to = simpleDateFormat.parse(dateTo);
            } catch (ParseException e) {
                return Response.ok().status(404).entity("Not Valid Dates!").build();
            }
            if (imsi == null)
                return Response.ok().status(404).entity("Not Valid IMSI!").build();
            return Response.ok().entity(baseDataService.getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod(imsi, from, to)).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getAllFailureByEventCauseImsiBetweenTimePeriod")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllFailureByEventCauseImsiBetweenTimePeriod(@Context HttpHeaders headers, @QueryParam("imsi") BigInteger imsi,
            @QueryParam("description") String description, @QueryParam("dateFrom") String dateFrom,
            @QueryParam("dateTo") String dateTo) {
        if (centralAuthority.isAllowed(headers, UserType.SUPPORT)) {
            Date from;
            Date to;
            simpleDateFormat.setLenient(false);
            try {
                from = simpleDateFormat.parse(dateFrom);
                to = simpleDateFormat.parse(dateTo);
            } catch (ParseException e) {
                return Response.ok().status(404).entity("Not Valid Dates!").build();
            }
            if (imsi == null || description == null)
                return Response.ok().status(404).entity("Not Valid Data!").build();
            return Response.ok().entity(baseDataService.getAllFailureByEventCauseImsiBetweenTimePeriod(description, imsi, from, to)).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod(@Context HttpHeaders headers,
            @QueryParam("imsi") BigInteger imsi, @QueryParam("dateFrom") String dateFrom, @QueryParam("dateTo") String dateTo) {
        if (centralAuthority.isAllowed(headers, UserType.SUPPORT)) {
            Date from;
            Date to;
            simpleDateFormat.setLenient(false);
            try {
                from = simpleDateFormat.parse(dateFrom);
                to = simpleDateFormat.parse(dateTo);
            } catch (ParseException e) {
                return Response.ok().status(404).entity("Not Valid Dates!").build();
            }
            if (imsi == null)
                return Response.ok().status(404).entity("Not Valid IMSI!").build();
            return Response.ok().entity(baseDataService.getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod(imsi, from, to)).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getAllFailureByFailureClassImsiBetweenTimePeriod")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllFailureByFailureClassImsiBetweenTimePeriod(@Context HttpHeaders headers,
            @QueryParam("imsi") BigInteger imsi, @QueryParam("failureDescription") String failureDescription,
            @QueryParam("dateFrom") String dateFrom, @QueryParam("dateTo") String dateTo) {
        if (centralAuthority.isAllowed(headers, UserType.SUPPORT)) {
            Date from;
            Date to;
            simpleDateFormat.setLenient(false);
            try {
                from = simpleDateFormat.parse(dateFrom);
                to = simpleDateFormat.parse(dateTo);
            } catch (ParseException e) {
                return Response.ok().status(404).entity("Not Valid Dates!").build();
            }
            if (imsi == null || failureDescription == null)
                return Response.ok().status(404).entity("Not Valid Data!").build();
            return Response.ok().entity(baseDataService.getAllFailureByFailureClassImsiBetweenTimePeriod(failureDescription, imsi, from, to)).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod(@Context HttpHeaders headers,
            @QueryParam("model") String model, @QueryParam("dateFrom") String dateFrom, @QueryParam("dateTo") String dateTo) {
        if (centralAuthority.isAllowed(headers, UserType.SERVICE)) {
            Date from;
            Date to;
            simpleDateFormat.setLenient(false);
            try {
                from = simpleDateFormat.parse(dateFrom);
                to = simpleDateFormat.parse(dateTo);
            } catch (ParseException e) {
                return Response.ok().status(404).entity("Not Valid Dates!").build();
            }
            if (model == null || model == "")
                return Response.ok().status(404).entity("Not Valid Phone Model!").build();
            return Response.ok().entity(baseDataService.getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod(model, from, to)).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getAllFailureByEventCausePhoneModelBetweenTimePeriod")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllFailureByEventCausePhoneModelBetweenTimePeriod(@Context HttpHeaders headers,
            @QueryParam("model") String model, @QueryParam("description") String description,
            @QueryParam("dateFrom") String dateFrom, @QueryParam("dateTo") String dateTo) {
        if (centralAuthority.isAllowed(headers, UserType.SERVICE)) {
            Date from;
            Date to;
            simpleDateFormat.setLenient(false);
            try {
                from = simpleDateFormat.parse(dateFrom);
                to = simpleDateFormat.parse(dateTo);
            } catch (ParseException e) {
                return Response.ok().status(404).entity("Not Valid Dates!").build();
            }
            return Response.ok().entity(baseDataService.getAllFailureByEventCausePhoneModelBetweenTimePeriod(description, model, from, to)).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod(@Context HttpHeaders headers,
            @QueryParam("model") String model, @QueryParam("dateFrom") String dateFrom, @QueryParam("dateTo") String dateTo) {
        if (centralAuthority.isAllowed(headers, UserType.SERVICE)) {
            Date from;
            Date to;
            simpleDateFormat.setLenient(false);
            try {
                from = simpleDateFormat.parse(dateFrom);
                to = simpleDateFormat.parse(dateTo);
            } catch (ParseException e) {
                return Response.ok().status(404).entity("Not Valid Dates!").build();
            }
            return Response.ok().entity(baseDataService.getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod(model, from, to)).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getAllFailureByFailureClassPhoneModelBetweenTimePeriod")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllFailureByFailureClassPhoneModelBetweenTimePeriod(@Context HttpHeaders headers,
            @QueryParam("model") String model, @QueryParam("description") String description,
            @QueryParam("dateFrom") String dateFrom, @QueryParam("dateTo") String dateTo) {
        if (centralAuthority.isAllowed(headers, UserType.SERVICE)) {
            Date from;
            Date to;
            simpleDateFormat.setLenient(false);
            try {
                from = simpleDateFormat.parse(dateFrom);
                to = simpleDateFormat.parse(dateTo);
            } catch (ParseException e) {
                return Response.ok().status(404).entity("Not Valid Dates!").build();
            }
            if (model == null || description == null)
                return Response.ok().status(404).entity("Not Valid Data!").build();
            return Response.ok().entity(baseDataService.getAllFailureByFailureClassPhoneModelBetweenTimePeriod(description, model, from, to)).build();
        }
        return Response.ok().status(401).entity("Not Allowed").build();
    }

    @GET
    @Path("/getFailuresByPhoneModel")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFailuresByPhoneModel(@Context HttpHeaders headers, @QueryParam("phone") String phone,
            @QueryParam("eventId") Integer eventId, @QueryParam("causeCode") Integer causeCode) {
        if (centralAuthority.isAllowed(headers, UserType.MANAGEMENT)) {
            if (phone == null || eventId == null || causeCode == null) {
                return Response.status(404).entity("Invalid Phone Model").build();
            }
            return Response.status(200).entity(baseDataService.getFailuresByPhoneModel(phone, eventId, causeCode)).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getAllFailureByImsiBetweenTimePeriod")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllFailureByImsiBetweenTimePeriod(@Context HttpHeaders headers,
            @QueryParam("imsi") BigInteger imsi, @QueryParam("dateFrom") String dateFrom, @QueryParam("dateTo") String dateTo) {
        if (centralAuthority.isAllowed(headers, UserType.SUPPORT)) {
            Date from;
            Date to;
            simpleDateFormat.setLenient(false);
            try {
                from = simpleDateFormat.parse(dateFrom);
                to = simpleDateFormat.parse(dateTo);
            } catch (ParseException e) {
                return Response.ok().status(404).entity("Not Valid Dates!").build();
            }
            if (imsi == null)
                return Response.ok().status(404).entity("Not Valid IMSI!").build();
            return Response.ok().entity(baseDataService.getAllFailureByImsiBetweenTimePeriod(imsi, from, to)).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @SuppressWarnings("unchecked")
    @GET
    @Path("/getNumberOfFailuresForThisMarketCellOperator")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNumberOfFailuresForThisMarketCellOperator(@Context HttpHeaders headers,
            @QueryParam("market") String market, @QueryParam("cell") Integer cellId,
            @QueryParam("operator") String operator) {
        if (centralAuthority.isAllowed(headers, UserType.MANAGEMENT)) {
            if (market == null || cellId == null || operator == null) {
                return Response.status(404).entity("Invalid Market/Operator/Cell ID").build();
            }
            JSONObject toReturn = new JSONObject();
            toReturn.put("market", baseDataService.getNumberOfFailuresForThisMarket(market));
            toReturn.put("cell", baseDataService.getFailuresForThisCell(cellId, market));
            toReturn.put("cellMarketOperator", baseDataService.getFailuresForThisCellCountryOperator(cellId, market, operator));
            return Response.status(200).entity(toReturn).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }
}
