
package restMapping;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import model.BaseData;
import model.CountryNetworkCode;
import model.EventCause;
import model.FailureClass;
import model.UserEquipment;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import dataAccess.insertionConflict.InsertionConflictDAO;
import serviceLayer.baseData.BaseDataService;
import serviceLayer.countryNetworkCode.CountryNetworkCodeService;
import serviceLayer.entityCache.EntityCache;
import serviceLayer.eventCause.EventCauseService;
import serviceLayer.failureClass.FailureClassService;
import serviceLayer.userEquipment.UserEquipmentService;

@Path("/uploadFile")
@Stateless
public class UploadFile {

    @EJB
    EventCauseService eventCauseService;
    @EJB
    FailureClassService failureClassService;
    @EJB
    UserEquipmentService userEquipmentService;
    @EJB
    CountryNetworkCodeService countryNetworkCodeService;
    @EJB
    BaseDataService baseDataService;
    @Inject
    ImportDataValidation importDataValidator;
    @EJB
    InsertionConflictDAO errorLogger;
    @Inject
    EntityCache lookupEntityCache;

    private HSSFWorkbook workBook;
    private HSSFSheet baseDataSheet;
    private HSSFSheet eventCauseSheet;
    private HSSFSheet failureClassSheet;
    private HSSFSheet uESheet;
    private HSSFSheet mCCMNCScheet;
    private int rowsToRead = 0;
    private int rowsRejected = 0;
    public static Logger log = Logger.getLogger(UploadFile.class.getName());

    public UploadFile() {}

    /**
     * Method that loads the excel file from the client side and uploads the
     * contents to database.
     * 
     * @param input
     *            MultipartFormDataInput is the input stream of the uploaded
     *            file.
     * @return Response with either a summary of imported records (ok()) or
     *         error message (serverError())
     */
    @POST
    @Path("/excel")
    @Consumes("multipart/form-data")
    public Response uploadFile(MultipartFormDataInput input) {
        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
        List<InputPart> inputParts = uploadForm.get("uploadedFile");
        long startTime = System.currentTimeMillis();
        for (InputPart inputPart : inputParts) {
            try {
                InputStream inputStream = inputPart.getBody(InputStream.class,
                        null);

                workBook = extractWorkbookFromFile(inputStream);
                if (workBook == null) {
                    return Response.serverError()
                            .entity("Server error: Invalid File").build();
                }
                extractSheetsFromWorkbook(workBook);
                workBook.close();

                exportDataToEventCauseTable(eventCauseSheet);
                exportDataToMCCMNCTable(mCCMNCScheet);
                exportDataToFailureClassTable(failureClassSheet);
                exportDataToUserEquipmentTable(uESheet);
                exportDataToBaseDataTable(baseDataSheet);

            } catch (Exception e) {
                return Response.serverError()
                        .entity("Server error: " + e.getMessage()).build();
            }
        }
        long endTime = System.currentTimeMillis();
        String report = generateReport(endTime - startTime);
        rowsToRead = 0;
        rowsRejected = 0;
        return Response.ok().entity(report).build();
    }

    /**
     * Method that Loads the excel file from the folder specified in the FileSystemMonitorInitializer
     * 
     * @param inputStream
     *            - File to process as input stream
     * @return - Summary String if upload is successful, null otherwise
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public String uploadFile(InputStream inputStream) {
        try {
            long startTime = System.currentTimeMillis();
            workBook = extractWorkbookFromFile(inputStream);
            if (workBook == null) {
                log.warning("File is not a Excel file");
                return null;
            }
            extractSheetsFromWorkbook(workBook);
            workBook.close();

            exportDataToEventCauseTable(eventCauseSheet);
            exportDataToMCCMNCTable(mCCMNCScheet);
            exportDataToFailureClassTable(failureClassSheet);
            exportDataToUserEquipmentTable(uESheet);
            exportDataToBaseDataTable(baseDataSheet);
            long endTime = System.currentTimeMillis();
            log.info("Uploaded data to server");
            String toReturn = generateReport(endTime - startTime);
            rowsToRead = 0;
            rowsRejected = 0;
            return toReturn;
        } catch (Exception e) {
            log.warning("The Excel file does not contain valid data");
            return null;
        }
    }

    /**
     * Creates a HSSFWorkbook from the input stream.
     * 
     * @param inputStream
     * @return HSSFWorkbook or null if not a valid input stream was provided.
     */
    public HSSFWorkbook extractWorkbookFromFile(InputStream inputStream) {
        try {
            POIFSFileSystem fileStream = new POIFSFileSystem(inputStream);
            return new HSSFWorkbook(fileStream);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Generates the summary report as string from imported rows to Database.
     * 
     * @param timeTaken
     *            The Difference of time between start time and end time of
     *            database import
     * @return String summary of how many rows were inserted and how many rows
     *         were rejected.
     */
    public String generateReport(long timeTaken) {
        String reportString = (rowsRejected + rowsToRead)
                + " Records were analyzed and imported to Database in "
                + (timeTaken / 1000) + " seconds.";
        if (rowsRejected > 0) {
            reportString += "<br>" + rowsRejected
                    + " invalid records were rejected";
        }
        return reportString;
    }

    /**
     * Method that extracts the following sheets from workBook: </p>Base Data,
     * Event-Cause table, Failure Class Table, UE Table, MCC - MNC Table
     * 
     * @param workBook
     *            Workbook containing the above sheets
     * @throws Exception
     *             thrown if a workbook is supplied that does not have these
     *             sheets
     */
    public void extractSheetsFromWorkbook(HSSFWorkbook workBook)
            throws Exception {
        baseDataSheet = workBook.getSheet("Base Data");
        eventCauseSheet = workBook.getSheet("Event-Cause table");
        failureClassSheet = workBook.getSheet("Failure Class Table");
        uESheet = workBook.getSheet("UE Table");
        mCCMNCScheet = workBook.getSheet("MCC - MNC Table");
        if (baseDataSheet == null || eventCauseSheet == null
                || failureClassSheet == null || uESheet == null
                || mCCMNCScheet == null) {
            throw new Exception(
                    "Failed to extract the valid sheets. Please make sure your file contains 5 sheets named:"
                            + "Base Data, Event-Cause table, Failure Class Table, UE Table and MCC - MNC Table in this order.");
        }
    }

    /**
     * Method to import data from provided excel sheet ("Event-Cause Table"
     * sheet) to Database.
     * 
     * @param sheet
     *            The sheet to import
     */
    public void exportDataToEventCauseTable(HSSFSheet sheet) {
        Collection<EventCause> eventCauses = new ArrayList<EventCause>();
        HSSFRow row;
        for (int i = 1; i <= sheet.getLastRowNum(); i++) {
            row = sheet.getRow(i);
            EventCause eventCause = importDataValidator
                    .validateEventCauseRow(row);
            if (eventCause != null) {
                rowsToRead++;
                eventCauses.add(eventCause);
            } else {
                rowsRejected++;
            }
        }
        eventCauseService.persistEventCodes(eventCauses);
    }

    /**
     * Method to import data from provided excel sheet ("Failure Class table"
     * sheet) to Database.
     * 
     * @param sheet
     *            The sheet to import
     */
    public void exportDataToFailureClassTable(HSSFSheet sheet) {
        Collection<FailureClass> failureClasses = new ArrayList<FailureClass>();
        HSSFRow row;
        for (int i = 1; i <= sheet.getLastRowNum(); i++) {
            row = sheet.getRow(i);
            FailureClass failureClass = importDataValidator
                    .validateFailureClassRow(row);
            if (failureClass != null) {
                rowsToRead++;
                failureClasses.add(failureClass);
            } else {
                rowsRejected++;
            }
        }
        failureClassService.persistFailureClass(failureClasses);
    }

    /**
     * Method to import data from provided excel sheet ("UE Table" sheet) to
     * Database.
     * 
     * @param sheet
     *            The sheet to import
     */
    public void exportDataToUserEquipmentTable(HSSFSheet sheet) {
        Collection<UserEquipment> userEquipments = new ArrayList<UserEquipment>();
        HSSFRow row;
        for (int i = 1; i <= sheet.getLastRowNum(); i++) {
            row = sheet.getRow(i);
            UserEquipment userEquipment = importDataValidator
                    .validateUserEquipmentRow(row);
            if (userEquipment != null) {
                rowsToRead++;
                userEquipments.add(userEquipment);
            } else {
                rowsRejected++;
            }
        }
        userEquipmentService.persistUserEquipments(userEquipments);
    }

    /**
     * Method to import data from provided excel sheet ("MCC - MNC Table" sheet)
     * to Database.
     * 
     * @param sheet
     *            The sheet to import
     */
    public void exportDataToMCCMNCTable(HSSFSheet sheet) {
        Collection<CountryNetworkCode> countryNetworkCodes = new ArrayList<CountryNetworkCode>();
        HSSFRow row;
        for (int i = 1; i <= sheet.getLastRowNum(); i++) {
            row = sheet.getRow(i);
            CountryNetworkCode countryNetworkCode = importDataValidator
                    .validateCountryNetworkCodeRow(row);
            if (countryNetworkCode != null) {
                rowsToRead++;
                countryNetworkCodes.add(countryNetworkCode);
            } else {
                rowsRejected++;
            }
        }
        countryNetworkCodeService
                .persistCountryNetworkCodes(countryNetworkCodes);
    }

    /**
     * Method to import data from provided excel sheet ("Base Data" sheet) to
     * Database.
     * 
     * @param sheet
     *            The sheet to import
     */
    public void exportDataToBaseDataTable(HSSFSheet sheet) {
        HSSFRow row;
        Collection<BaseData> baseData = new ArrayList<BaseData>();
        for (int i = 1; i <= sheet.getLastRowNum(); i++) {
            row = sheet.getRow(i);
            BaseData data = importDataValidator.validateBaseDataRow(row);
            if (data != null) {
                rowsToRead++;
                baseData.add(data);
            } else {
                rowsRejected++;
            }
        }
        persistErroneousReads();
        baseDataService.persistBaseData(baseData);
    }

    /**
     * Log the rows which contained errors to database as a batch insert
     */
    public void persistErroneousReads() {
        errorLogger.logValidationError(lookupEntityCache
                .getInsertionConflicts());
        // reseting the Cache of errors for future batch imports
        lookupEntityCache.resetInsertionConflicts();
    }

}
