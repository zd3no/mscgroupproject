
package restMapping;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.EJB;
import javax.inject.Inject;

import model.BaseData;
import model.CountryNetworkCode;
import model.CountryNetworkCodePK;
import model.EventCause;
import model.EventCausePK;
import model.FailureClass;
import model.InsertionConflict;
import model.UserEquipment;

import org.apache.poi.hssf.usermodel.HSSFRow;

import serviceLayer.entityCache.EntityCache;
import dataAccess.insertionConflict.InsertionConflictDAO;

public class ImportDataValidation {

    @EJB
    InsertionConflictDAO errorLogger;
    @Inject
    CellExtractor cellExtractor;
    @Inject
    EntityCache lookupEntityCache;

    private static SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy hh:mm");

    /**
     * Method to validate a base data row from the BaseData sheet, creates an insertion conflict object if non valid data is identified.
     * 
     * @param row
     *            : row from BaseData sheet to import into database
     * @return An instance of BaseData containing the validated row or null if if non valid data is identified.
     */
    public BaseData validateBaseDataRow(HSSFRow row) {
        if (row == null) {
            return null;
        }
        Date dateTime = cellExtractor.getDateFromCell(row.getCell(0));
        Integer eventId = cellExtractor.getIntegerFromCell(row.getCell(1));
        Integer failureID = cellExtractor.getIntegerFromCell(row.getCell(2));
        BigInteger typeAllocationCode = cellExtractor.getBigIntegerFromCell(row.getCell(3));
        Integer mobileCountryCode = cellExtractor.getIntegerFromCell(row.getCell(4));
        Integer mobileNetworkCode = cellExtractor.getIntegerFromCell(row.getCell(5));
        Integer cellId = cellExtractor.getIntegerFromCell(row.getCell(6));
        Integer duration = cellExtractor.getIntegerFromCell(row.getCell(7));
        Integer causeCode = cellExtractor.getIntegerFromCell(row.getCell(8));
        String networkElementVersion = cellExtractor.getStringFromCell(row.getCell(9));
        BigInteger imsi = cellExtractor.getBigIntegerFromCell(row.getCell(10));
        BigInteger hier3_id = cellExtractor.getBigIntegerFromCell(row.getCell(11));
        BigInteger hier32_id = cellExtractor.getBigIntegerFromCell(row.getCell(12));
        BigInteger hier321_id = cellExtractor.getBigIntegerFromCell(row.getCell(13));

        FailureClass failureClass = lookupEntityCache.getFailureClass(failureID);
        UserEquipment userEquipment = lookupEntityCache.getUserEquipment(typeAllocationCode);
        CountryNetworkCode countryNetworkCode = lookupEntityCache.getCountryNetworkCode(mobileCountryCode, mobileNetworkCode);
        EventCause eventCause = lookupEntityCache.getEventCause(eventId, causeCode);
        if (dateTime == null || cellId == null || duration == null || imsi == null ||
                hier321_id == null || hier32_id == null || hier3_id == null || failureClass == null ||
                userEquipment == null || eventCause == null || countryNetworkCode == null) {
            InsertionConflict ic = new InsertionConflict();
            ic.setErrorDescription("Couldn't parse cell values or foreign key dependency failed");
            ic.setOriginTable("Base Data");
            ic.setInputString(generateInputString(dateTime, eventId, failureID, typeAllocationCode, mobileCountryCode,
                    mobileNetworkCode, cellId, duration, causeCode, networkElementVersion, imsi, hier3_id, hier32_id, hier321_id));
            lookupEntityCache.addInsertionConflict(ic);
            return null;
        } else {
            BaseData data = new BaseData();
            data.setFailureClass(failureClass);
            data.setUserEquipment(userEquipment);
            data.setEventCause(eventCause);
            data.setCountryNetworkCode(countryNetworkCode);
            data.setTimeStamp(dateTime);
            data.setDuration(duration);
            data.setImsi(imsi);
            data.setNetworkElementVersion(networkElementVersion);
            data.setCellId(cellId);
            data.setHier3Id(hier3_id);
            data.setHier32Id(hier32_id);
            data.setHier321Id(hier321_id);
            return data;
        }
    }

    /**
     * Method to validate a UserEquipment row from the UserEquipment sheet creates an insertion conflict object if non valid data is identified.
     * 
     * @param row
     *            : row from UserEquipment sheet to import into database
     * @return An instance of UserEquipment containing the validated row or null if if non valid data is identified.
     */
    public UserEquipment validateUserEquipmentRow(HSSFRow row) {
        if (row == null) {
            return null;
        }
        BigInteger tac = cellExtractor.getBigIntegerFromCell(row.getCell(0));
        String marketingName = cellExtractor.getStringFromCell(row.getCell(1));
        String manufacturer = cellExtractor.getStringFromCell(row.getCell(2));
        String accessCapability = cellExtractor.getStringFromCell(row.getCell(3));
        String model = cellExtractor.getStringFromCell(row.getCell(4));
        String vendorName = cellExtractor.getStringFromCell(row.getCell(5));
        String ueType = cellExtractor.getStringFromCell(row.getCell(6));
        String os = cellExtractor.getStringFromCell(row.getCell(7));
        String inputMode = cellExtractor.getStringFromCell(row.getCell(8));

        if (tac != null) {
            UserEquipment userEquipment = new UserEquipment(tac);
            userEquipment.setMarketingName(marketingName);
            userEquipment.setManufacturer(manufacturer);
            userEquipment.setAccessCapability(accessCapability);
            userEquipment.setModel(model);
            userEquipment.setVendorName(vendorName);
            userEquipment.setUserEquipmentType(ueType);
            userEquipment.setOperatingSystem(os);
            userEquipment.setInputMode(inputMode);
            return userEquipment;
        } else {
            InsertionConflict ic = new InsertionConflict();
            ic.setErrorDescription("Couldn't parse cell values. Invalid entries in row.");
            ic.setOriginTable("User Equipment");
            ic.setInputString("Type allocation code: " + tac + " Marketing name: " + marketingName +
                    " Manufacturer: " + manufacturer + " Access capability: " + accessCapability +
                    " Model: " + model + " Vendor name: " + vendorName + " UE type: " + ueType +
                    " OS: " + os + " Input mode: " + inputMode);
            errorLogger.logValidationError(ic);
            return null;
        }

    }

    /**
     * Method to validate an EventCause row from the EventCause sheet creates an insertion conflict object if non valid data is identified.
     * 
     * @param row
     *            : row from EventCause sheet to import into database
     * @return An instance of EventCause containing the validated row or null if if non valid data is identified.
     */
    public EventCause validateEventCauseRow(HSSFRow row) {
        if (row == null) {
            return null;
        }
        Integer causeCode = cellExtractor.getIntegerFromCell(row.getCell(0));
        Integer eventId = cellExtractor.getIntegerFromCell(row.getCell(1));
        String description = cellExtractor.getStringFromCell(row.getCell(2));
        if (causeCode != null && eventId != null) {
            EventCausePK eventCauseId = new EventCausePK(causeCode, eventId);
            EventCause eventCause = new EventCause(eventCauseId);
            eventCause.setDescription(description);
            return eventCause;
        } else {
            InsertionConflict ic = new InsertionConflict();
            ic.setErrorDescription("Couldn't parse cell values. Invalid entries in row.");
            ic.setOriginTable("Event Cause");
            ic.setInputString("Cause code: " + causeCode + " Event ID: " + eventId + " Description: " + description);
            errorLogger.logValidationError(ic);
            return null;
        }
    }

    /**
     * Method to validate a ValidateFailureClass row from the ValidateFailureClass sheet creates an insertion conflict object if non valid data is
     * identified.
     * 
     * @param row
     *            : row from ValidateFailureClass sheet to import into database
     * @return An instance of ValidateFailureClass containing the validated row or null if if non valid data is identified.
     */
    public FailureClass validateFailureClassRow(HSSFRow row) {
        if (row == null) {
            return null;
        }
        Integer failureId = cellExtractor.getIntegerFromCell(row.getCell(0));
        String description = cellExtractor.getStringFromCell(row.getCell(1));

        if (failureId != null) {
            FailureClass failureClass = new FailureClass(failureId);
            failureClass.setDescription(description);
            return failureClass;
        } else {
            InsertionConflict ic = new InsertionConflict();
            ic.setErrorDescription("Couldn't parse cell values. Invalid entries in row.");
            ic.setOriginTable("Failure Class");
            ic.setInputString("Failure Id: " + failureId + " Description: " + description);
            errorLogger.logValidationError(ic);
            return null;
        }
    }

    /**
     * Method to validate a CountryNetworkCode row from the CountryNetworkCode sheet creates an insertion conflict object if non valid data is
     * identified.
     * 
     * @param row
     *            : row from CountryNetworkCode sheet to import into database
     * @return An instance of CountryNetworkCode containing the validated row or null if if non valid data is identified.
     */
    public CountryNetworkCode validateCountryNetworkCodeRow(HSSFRow row) {
        if (row == null) {
            return null;
        }
        Integer mcc = cellExtractor.getIntegerFromCell(row.getCell(0));
        Integer mnc = cellExtractor.getIntegerFromCell(row.getCell(1));
        String country = cellExtractor.getStringFromCell(row.getCell(2));
        String operator = cellExtractor.getStringFromCell(row.getCell(3));

        if (mcc != null && mnc != null) {
            CountryNetworkCodePK countryNetworkCodeId = new CountryNetworkCodePK(mcc, mnc);
            CountryNetworkCode countryNetworkCode = new CountryNetworkCode(countryNetworkCodeId);
            countryNetworkCode.setCountry(country);
            countryNetworkCode.setOperator(operator);
            return countryNetworkCode;
        } else {
            InsertionConflict ic = new InsertionConflict();
            ic.setErrorDescription("Couldn't parse cell values. Invalid entries in row.");
            ic.setOriginTable("Country Network Code");
            ic.setInputString("MCC: " + mcc + " MNC: " + mnc + " Country: " + country + " Operator: " + operator);
            errorLogger.logValidationError(ic);
            return null;
        }
    }

    /**
     * Method to concatenate data from all columns in row into one input string.
     * 
     * @param dateTime
     * @param eventId
     * @param failureID
     * @param typeAllocationCode
     * @param mobileCountryCode
     * @param mobileNetworkCode
     * @param cellId
     * @param duration
     * @param causeCode
     * @param networkElementVersion
     * @param imsi
     * @param hier3_id
     * @param hier32_id
     * @param hier321_id
     * @return String containing information for all columns in current row
     */
    public String generateInputString(Date dateTime, Integer eventId, Integer failureID,
            BigInteger typeAllocationCode, Integer mobileCountryCode,
            Integer mobileNetworkCode, Integer cellId, Integer duration, Integer causeCode,
            String networkElementVersion, BigInteger imsi, BigInteger hier3_id, BigInteger hier32_id,
            BigInteger hier321_id) {
        String formattedDate = "null";
        if (dateTime != null) {
            formattedDate = SDF.format(dateTime);
        }
        return "DateTime: " + formattedDate + " Event Id: " + eventId +
                " Failure Class: " + failureID + " TAC: " + typeAllocationCode +
                " MCC: " + mobileCountryCode + " MNC: " + mobileNetworkCode + " Cell ID: " + cellId +
                " Duration: " + duration + " Cause Code: " + causeCode + " NE Version: " + networkElementVersion +
                " IMSI: " + imsi + " HIER3_ID: " + hier3_id + " HIER32_ID :" + hier32_id +
                " HIER321_ID: " + hier321_id;
    }
}
