
package restMapping;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import model.UserType;
import serviceLayer.baseData.BaseDataService;
import serviceLayer.countryNetworkCode.CountryNetworkCodeService;
import serviceLayer.eventCause.EventCauseService;
import serviceLayer.failureClass.FailureClassService;
import serviceLayer.userEquipment.UserEquipmentService;

@Path("/display")
public class DisplayData {

    @EJB
    EventCauseService eventCauseService;
    @EJB
    FailureClassService failureClassService;
    @EJB
    UserEquipmentService userEquipmentService;
    @EJB
    CountryNetworkCodeService countryNetworkCodeService;
    @EJB
    BaseDataService baseDataService;
    @EJB
    CentralAuthority centralAuthority;

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    @GET
    @Path("/getAllBaseData")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBaseData() {
        return Response.status(200).entity(baseDataService.getAllBaseData()).build();
    }

    @GET
    @Path("/getAllEventCause")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEventCause() {
        return Response.status(200).entity(eventCauseService.getAllEventCauses()).build();
    }

    @GET
    @Path("/getAllFailureClass")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFailureClass() {
        return Response.status(200).entity(failureClassService.getAllFailureClasses()).build();
    }

    @GET
    @Path("/getAllUserEquipment")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserEquipment() {
        return Response.status(200).entity(userEquipmentService.getAllUserEquipment()).build();
    }

    @GET
    @Path("/getAllCountryNetworkCode")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCountryNetworkCode() {
        return Response.status(200).entity(countryNetworkCodeService.getAllCountryNetworkCode()).build();
    }

    @GET
    @Path("/getAllUniqueCauseCodeAndFailureClassByImsi")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllUniqueCauseCodeAndFailureClassByImsi(@Context HttpHeaders headers, @QueryParam("imsi") String imsi) {
        if (centralAuthority.isAllowed(headers, UserType.SUPPORT)) {
            if (!imsi.matches("^[0-9]\\d*$")) {
                return Response.ok().status(404).entity("Invalid IMSI").build();
            }
            return Response.status(200).entity(baseDataService.getAllUniqueCauseCodeAndFailureClassByImsi(new BigInteger(imsi))).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getNumberOfFailuresAndDurationForEachImsiBetweenPeriodTime")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNumberOfFailuresAndDurationForEachImsiBetweenPeriod(@Context HttpHeaders headers,
            @QueryParam("dateFrom") String dateFrom, @QueryParam("dateTo") String dateTo) {
        if (centralAuthority.isAllowed(headers, UserType.MANAGEMENT)) {
            try {
                Date from = simpleDateFormat.parse(dateFrom);
                Date to = simpleDateFormat.parse(dateTo);
                simpleDateFormat.setLenient(false);
                Collection<Object[]> objs = baseDataService.getNumberOfFailuresAndDurationForEachImsiBetweenPeriodTime(from, to);
                return Response.status(200).entity(objs).build();
            } catch (ParseException e) {
                return Response.status(404).entity("Invalid date").build();
            }
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getAllImsiByFailureClass")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllImsiByFailureClass(@Context HttpHeaders headers, @QueryParam("failureId") String failureId) {
        if (!failureId.matches("^[0-9]\\d*$") || failureId.length() > String.valueOf(failureClassService.getAllFailureClasses().size()).length())
            return Response.ok().status(404).entity("Invalid Failure ID").build();
        if (centralAuthority.isAllowed(headers, UserType.SERVICE)) {
            return Response.status(200).entity(baseDataService.getAllImsiByFailureClass(Integer.parseInt(failureId))).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getAllEventCauseByImsi")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllEventCauseByImsi(@Context HttpHeaders headers, @QueryParam("imsi") String imsi) {
        if (centralAuthority.isAllowed(headers, UserType.SUPPORT)) {
            if (!imsi.matches("^[0-9]\\d*$")) {
                return Response.ok().status(404).entity("Invalid IMSI").build();
            }
            return Response.status(200).entity(baseDataService.getAllEventCauseByImsi(new BigInteger(imsi))).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getNumberOfFailuresByModelBetweenPeriodTime")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNumberOfFailuresByModelBetweenPeriodTime(@Context HttpHeaders headers, @QueryParam("model") String model,
            @QueryParam("dateFrom") String dateFrom, @QueryParam("dateTo") String dateTo) {
        if (centralAuthority.isAllowed(headers, UserType.SERVICE)) {
            if (model.isEmpty() || model == null) {
                return Response.status(404).entity("Not Valid Model!").build();
            }
            try {
                Date from = simpleDateFormat.parse(dateFrom);
                Date to = simpleDateFormat.parse(dateTo);
                simpleDateFormat.setLenient(false);
                Long numberFailure = baseDataService
                        .getNumberOfFailuresByModelBetweenPeriodTime(model, from,
                                to);
                return Response.status(200).entity(numberFailure).build();
            } catch (ParseException e) {
                return Response.status(404).entity("Invalid date").build();
            }
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    /**
     * Returns the unique Event Causes from database and their number of occurences based
     * on the model of the phone provided.
     * 
     * @param headers
     *            - Expected "identity" and "token"
     * @param model
     *            - The model of the phone
     * @return Collection of Objects containing Failure ID, Event Cause and number of occurences
     */
    @GET
    @Path("/getUniqueEventCauseAndNumberOccurrencesByPhoneModel")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUniqueEventCauseAndNumberOccurrencesByPhoneModel(
            @Context HttpHeaders headers, @QueryParam("model") String model) {
        if (model.isEmpty() || model == null) {
            return Response.ok().status(404).entity("Not Valid Model").build();
        }
        if (centralAuthority.isAllowed(headers, UserType.MANAGEMENT)) {
            return Response.status(200).entity(baseDataService
                    .getUniqueFailureIdEventCauseAndNumberOccurrencesByPhoneModel(model)).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    /**
     * Method returns the number of failures for a particular IMSI between 2 dates from the database.
     * It is presumed that dates are in the correct order, everything else is re-checked for validity.
     * 
     * @param headers
     *            - Expected identity and token headers
     * @param imsi
     *            - The IMSI to search for
     * @param dateFrom
     *            - Date From, should be earlier than Now
     * @param dateTo
     *            - Date To, should be after dateFrom
     * @return - Number of failures as long or String as exception in a non-200 status response
     */
    @GET
    @Path("/getNumberOfFailuresByImsiBetweenPeriodTime")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNumberOfFailuresByImsiBetweenPeriodTime(@Context HttpHeaders headers, @QueryParam("imsi") String imsi,
            @QueryParam("dateFrom") String dateFrom, @QueryParam("dateTo") String dateTo) {
        if (centralAuthority.isAllowed(headers, UserType.SUPPORT)) {
            if (!imsi.matches("^[0-9]\\d*$")) {
                return Response.ok().status(404).entity("Invalid IMSI").build();
            }
            try {
                Date from = simpleDateFormat.parse(dateFrom);
                Date to = simpleDateFormat.parse(dateTo);
                simpleDateFormat.setLenient(false);
                Collection<Object[]> numberFailure = baseDataService
                        .getNumberOfFailuresByImsiBetweenPeriodTime(new BigInteger(
                                imsi), from, to);
                return Response.status(200).entity(numberFailure).build();
            } catch (ParseException e) {
                return Response.status(404).entity("Invalid date").build();
            }
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime(
            @Context HttpHeaders headers, @QueryParam("dateFrom") String dateFrom, @QueryParam("dateTo") String dateTo) {
        if (centralAuthority.isAllowed(headers, UserType.MANAGEMENT)) {
            try {
                Date from = simpleDateFormat.parse(dateFrom);
                Date to = simpleDateFormat.parse(dateTo);
                simpleDateFormat.setLenient(false);
                Collection<Object[]> objs = baseDataService
                        .getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime(from, to);
                return Response.status(200).entity(objs).build();
            } catch (ParseException e) {
                return Response.status(404).entity("Invalid date").build();
            }
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getTop10UniqueMarketOperatorAndCellIds")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTop10UniqueMarketOperatorAndCellIds(@Context HttpHeaders headers) {
        if (centralAuthority.isAllowed(headers, UserType.MANAGEMENT)) {
            return Response.ok().entity(baseDataService.getTop10UniqueMarketOperatorAndCellIds()).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    @GET
    @Path("/getTop10ImsiBetweenPeriodTime")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTop10ImsiBetweenPeriodTime(@Context HttpHeaders headers, @QueryParam("dateFrom") String dateFrom,
            @QueryParam("dateTo") String dateTo) {
        if (centralAuthority.isAllowed(headers, UserType.MANAGEMENT)) {
            try {
                Date from = simpleDateFormat.parse(dateFrom);
                Date to = simpleDateFormat.parse(dateTo);
                simpleDateFormat.setLenient(false);
                Collection<Object[]> objs = baseDataService
                        .getTop10ImsiBetweenPeriodTime(from, to);
                return Response.status(200).entity(objs).build();
            } catch (ParseException e) {
                return Response.status(404).entity("Invalid date").build();
            }
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    /**
     * This method will get all the IMSIs from database for a given time period
     * 
     * @param headers
     *            - should contain at least 2 headers: "identity" and "token"
     * @param dateFrom
     * @param dateTo
     * @return
     */
    @GET
    @Path("/getImsiForGivenDate")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response getImsiForGivenDate(@Context HttpHeaders headers, @QueryParam("dateFrom") String dateFrom, @QueryParam("dateTo") String dateTo) {
        if (centralAuthority.isAllowed(headers, UserType.SERVICE)) {
            Date from;
            Date to;
            simpleDateFormat.setLenient(false);
            try {
                from = simpleDateFormat.parse(dateFrom);
                to = simpleDateFormat.parse(dateTo);
            } catch (ParseException e) {
                return Response.ok().status(404).entity("Not Valid Dates!").build();
            }
            return Response.ok().entity(baseDataService.getImsiForGivenDate(from, to)).build();
        } else {
            return Response.ok().status(401).entity("Not Allowed").build();
        }
    }

    /**
     * This method returns a collection of Failure Class ID's
     * 
     * @return
     */
    @GET
    @Path("/getFailureClassIds")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFailureClassIds() {
        return Response.ok().entity(failureClassService.getAllFailureClasses()).build();
    }
}
