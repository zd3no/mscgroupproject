package restMapping;

import java.io.IOException;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import model.User;
import model.UserType;

import org.jboss.resteasy.util.Base64;
import org.json.simple.JSONObject;

import serviceLayer.user.UserService;

@Path("/user-service")
public class UserTool {

	@EJB
	UserService userService;
	@EJB
	CentralAuthority centralAuthority;

	/**
	 * This method is used by the client to authenticate a user at login.
	 * 
	 * @param usernameAndPasswordEncrypted
	 *            - the Base64 encrypted username:password string
	 * @return Returns User as stringified and Base64 encoded String. Client is
	 *         expected to decode the result and Json parse it to extract
	 *         relevant information, particularly userType and token.
	 */
	@SuppressWarnings("unchecked")
	@POST
	@Path("/loginUser")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response loginUser(
			@FormParam("secret") String usernameAndPasswordEncrypted) {
		if (usernameAndPasswordEncrypted == null) {
			return Response.ok().status(401).entity("Access denied!").build();
		}
		User user = centralAuthority.authenticateUser(usernameAndPasswordEncrypted);
		if (user != null) {
			JSONObject jsonUser = new JSONObject();
			jsonUser.put("name", user.getName());
			jsonUser.put("id", user.getId());
			jsonUser.put("userType", user.getUserType().toString());
			jsonUser.put("token", user.getToken());
			String stringifiedUser = jsonUser.toJSONString();
			try {
				return Response
						.ok()
						.entity(Base64.encodeBytes(stringifiedUser.getBytes(),
								Base64.URL_SAFE)).build();
			} catch (IOException e) {
				return Response.ok().status(401).entity("Access denied!")
						.build();
			}
		} else {
			return Response.ok().status(401).entity("Access denied!").build();
		}
	}

	/**
	 * This method is used to register users in the system.
	 * 
	 * @param headers
	 *            - Expected 2 headers: "identity" and "token" where identity
	 *            should contain the user's ID and token must contain the user's
	 *            token.
	 * @param type
	 *            - registering user's type
	 * @param name
	 *            - registering user's name
	 * @param usernameAndPasswordEncrypted
	 *            - registering user's Base64 encrypted ":" separated userID and
	 *            password
	 * @return String literal "OK" or 401 "Access Denied" if user with same
	 *         userId already exists, or any of the headers are invalid /
	 *         missing
	 */
	@POST
	@Path("/registerUser")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response registerUser(@Context HttpHeaders headers,
			@FormParam("userType") String type, @FormParam("name") String name,
			@FormParam("secret") String usernameAndPasswordEncrypted) {
		if (centralAuthority.isAllowed(headers, UserType.ADMINISTRATION)) {
			User newUser = centralAuthority.makeUserWithPassword(usernameAndPasswordEncrypted);
			if (newUser != null) {
				newUser.setName(name);
				newUser.setUserType(UserType.valueOf(type));
				userService.persistenceUser(newUser);
				return Response.ok().status(200).entity("OK").build();
			}else{
				return Response.ok().status(404).entity("That user already exists!").build();
			}
		}
		return Response.ok().status(401).entity("Access denied!").build();
	}

}
