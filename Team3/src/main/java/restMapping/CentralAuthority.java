package restMapping;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.HttpHeaders;

import model.User;
import model.UserType;

import org.jboss.resteasy.util.Base64;

import serviceLayer.user.UserService;

/**
 * This EJB class is used centrally by all REST calls to authenticate and
 * authorize access to server's resources.
 */
@Stateless
public class CentralAuthority {

	@EJB
	UserService userService;

	/**
	 * This method decrypts the usernameAndPasswordEncrypted string into two
	 * parts: username and password. A User entity is returned with username,
	 * generated token and hashed password.
	 * 
	 * @param usernameAndPasswordEncrypted
	 * @return User or null if a user already exists with provided username
	 */
	public User makeUserWithPassword(String usernameAndPasswordEncrypted) {
		Map<String, String> userIdPasswordPair = decryptUserSecret(usernameAndPasswordEncrypted);
		if (userIdPasswordPair == null) {
			return null;
		}
		String username = userIdPasswordPair.get("username");
		String password = userIdPasswordPair.get("password");
		User u = userService.getUserById(username);
		if (u != null) {
			return null;
		}
		User newUser = new User(username);
		newUser.setPassword(hashPassword(password));
		newUser.setToken(generateToken(username, password));
		return newUser;
	}

	/**
	 * This method Authenticates the user from provided
	 * usernameAndPasswordEncrypted and generates a token.
	 * 
	 * @param usernameAndPasswordEncrypted
	 * @return user with updated token or null if user could not be authenticated or
	 *         token could not be generated/user updated in database.
	 */
	public User authenticateUser(
			String usernameAndPasswordEncrypted) {
		User user = getUserWithSecret(usernameAndPasswordEncrypted);
		if (user == null) {
			return null;
		}
		String token = generateToken(user.getId(), user.getPassword());
		user.setToken(token);
		if (userService.mergeUser(user))
		{
			return user;
		}
		else
			return null;
	}

	/**
	 * This method returns the user from DB whose credentials are extracted from
	 * usernameAndPasswordEncrypted
	 * 
	 * @param usernameAndPasswordEncrypted
	 * @return existing User or null otherwise
	 */
	public User getUserWithSecret(String usernameAndPasswordEncrypted) {
		Map<String, String> userIdPasswordPair = decryptUserSecret(usernameAndPasswordEncrypted);
		if (userIdPasswordPair == null) {
			return null;
		}
		String username = userIdPasswordPair.get("username");
		String password = userIdPasswordPair.get("password");
		//User user = userService.getUserByUsernameAndPassword(username, password);
		User user = userService.getUserByUsernameAndPassword(username, hashPassword(password));
		if (user != null) {
			return user;
		} else {
			return null;
		}
	}

	/**
	 * This method decrypts the Base54 encoded user secret and returns
	 * uername/userID and password.
	 * 
	 * @param usernameAndPasswordEncrypted
	 *            - The Base54 encrypted secret where username and password are
	 *            separated by ":"
	 * @return Map of String and String pairs that should contain "username" and
	 *         "password" keys. Returns null if it could not decode the secret.
	 */
	public Map<String, String> decryptUserSecret(
			String usernameAndPasswordEncrypted) {
		Map<String, String> map = new HashMap<>();
		String usernameAndPassword;
		try {
			usernameAndPassword = new String(Base64.decode(usernameAndPasswordEncrypted));
			StringTokenizer tokenizer = new StringTokenizer(
					usernameAndPassword, ":");
			String username = tokenizer.nextToken();
			String password = tokenizer.nextToken();
			map.put("username", username);
			map.put("password", password);
			return map;
		} catch (IOException | NoSuchElementException e) {
			return null;
		}
	}

	/**
	 * This method generates the token based on provided username and password
	 * 
	 * @param username
	 * @param password
	 * @return token as String or null if Token could not be generated Ex:
	 *         missing username
	 */
	public String generateToken(String username, String password) {
		if (username.isEmpty() || password.isEmpty()) {
			return null;
		} else {
			return hashPassword(username + (Math.random() * 50000) + password);
		}
	}

	/**
	 * This method generates the hash made from the provided password. This hash
	 * is then to be stored in the database instead of the actual password. At
	 * every user login the hash is generated and compared with the existing
	 * hash stored as password in database. If they match, user has provided a
	 * valid password.
	 * 
	 * @param password
	 * @return hashed pasword as String or null if hash could not be generated
	 */
	public String hashPassword(String password) {
		byte[] salt = String.valueOf(
				"AbCdEfGhIjKlMnOpRsTuVxQwZ0123456789").getBytes();
		int iteration = 1000;
		return getHash(iteration, password, salt);
	}

	/**
	 * This method generates the hash of a password
	 * 
	 * @param iterationNb
	 *            - the amount of iterations plaintext hash is rehashed (higher
	 *            the number, slower the hash generator, but better protection
	 *            against brute force attack)
	 * @param password
	 *            - the actual unencrypted user password
	 * @param salt
	 *            - the byte aray used to salt the hash function
	 * @return hash as String or null if hash could not be generated. Ex: SHA-1
	 *         algorithm is not available
	 */
	public String getHash(int iterationNb, String password, byte[] salt) {
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-1");
			digest.reset();
			digest.update(salt);
			byte[] input = digest.digest(password.getBytes("UTF-8"));
			for (int i = 0; i < iterationNb; i++) {
				digest.reset();
				input = digest.digest(input);
			}
			return new String(Base64.encodeBytes(input,Base64.URL_SAFE));
		} catch (NoSuchAlgorithmException | IOException e) {
			return null;
		}
	}

	/**
	 * This method authorizes access to a resource coming directly from http
	 * client.
	 * 
	 * @param headers
	 *            - the HttpHeaders headers associated with the HTTPRequest.
	 *            Expected headers are: "identity" and "token", where "identity"
	 *            is the username or some unique user identifier and "token" is
	 *            the server-generated hash which replaces the standard user
	 *            password. The combination of the two is typically stored in
	 *            the cookie on the client.
	 * @param denyBelow
	 *            - The UserType required to access the resource. Used to deny
	 *            access to users with lower user type.
	 * @return - True if user has access to the resource, False otherwise
	 */
	public boolean isAllowed(HttpHeaders headers, UserType denyBelow) {
		String userName = null;
		String token = null;
		if (headers != null) {
			List<String> authHeader = headers.getRequestHeader("identity");
			List<String> tokenHeader = headers.getRequestHeader("token");
			if (authHeader == null || authHeader.isEmpty()
					|| tokenHeader == null || tokenHeader.isEmpty())
				return false;
			else {
				userName = authHeader.get(0);
				token = tokenHeader.get(0);
				return isSuchUserInDBWithAccessRights(userName, token,
						denyBelow);
			}
		}
		return false;
	}

	/**
	 * This method compares the username - token combination of a user that may
	 * or may not exist in the database with the required UserType "denyBelow".
	 * Such user must exist in database and must have UserType equal or greater
	 * to the one required.
	 * 
	 * @param userName
	 *            - The unique user identifier
	 * @param token
	 *            - Server-generated token accompanying any client request
	 * @param denyBelow
	 *            - The UserType to which to compare user's UserType
	 * @return True if User exists and has correct access rights, False if user
	 *         either does not exist or has a lower user type
	 */
	public boolean isSuchUserInDBWithAccessRights(String userName,
			String token, UserType denyBelow) {
		User user = userService.getUserById(userName);
		if (user != null) {
			return user.getToken().equals(token)
					&& user.getUserType().ordinal() <= denyBelow.ordinal();
		} else {
			return false;
		}
	}
}