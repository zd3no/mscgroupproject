
package restMapping;

import java.math.BigInteger;
import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;

public class CellExtractor {

    private DataFormatter formatter = new DataFormatter();

    /**
     * Method to extract a String value from cell.
     * 
     * @param cell
     *            A valid HSSFCell
     * @return The String value of extracted cell value. If null or "(null)", it returns null,
     *         for anything else it returns the toString value.
     */
    public String getStringFromCell(Cell cell) {
        String stringCellValue = formatter.formatCellValue(cell);
        if (cell == null || stringCellValue.equals("(null)")) {
            return null;
        } else {
            return stringCellValue;
        }
    }

    /**
     * Method to extract an integer from CellNode.
     * 
     * @param cell
     *            A valid HSSFCell
     * @return The integer value of extracted cell vaue. Must be a valid numeric value
     *         </p>Ex: 45 and NOT 45.6, String, null or empty. If decimal value is present, it will return the whole part.
     *         Returns null if invalid i.e. is too large to be an Integer
     */
    public Integer getIntegerFromCell(Cell cell) {
        String stringCellValue = formatter.formatCellValue(cell);
        if (cell == null || cell.getCellType() != Cell.CELL_TYPE_NUMERIC) {
            return null;
        } else {
            BigInteger numericValue = new BigInteger(stringCellValue.split("\\.")[0]);
            if (numericValue.compareTo(new BigInteger(String.valueOf(Integer.MAX_VALUE))) == 1) {
                return null;
            } else
                return (int) cell.getNumericCellValue();
        }
    }

    /**
     * Method that extracts a large whole number from the cell.
     * 
     * @param cell
     *            A valid HSSFCell
     * @return The BigInteger value of the cell provided. Must be a valid whole number.
     *         </p>Ex: 48216168421 and NOT 546.484, String, empty or null. Returns null for invalid cell.
     */
    public BigInteger getBigIntegerFromCell(Cell cell) {
        String stringCellValue = formatter.formatCellValue(cell);
        if (cell == null || cell.getCellType() != Cell.CELL_TYPE_NUMERIC) {
            return null;
        } else {
            if (stringCellValue.contains("."))
                return null;
            else
                return new BigInteger(stringCellValue);
        }
    }

    /**
     * Method that extracts a date from the cell.
     * 
     * @param cell
     *            A valid HSSFCell
     * @return The Date value of the cell provided. Must be a valid Date. Returns null otherwise.
     */
    public Date getDateFromCell(Cell cell) {
        if (cell == null) {
            return null;
        }
        Date date;
        try {
            date = cell.getDateCellValue();
            return date;
        } catch (NumberFormatException | IllegalStateException e) {
            return null;
        }
    }

}
