package model;

/**
 * Each user type has a priority assigned to it.
 */
public enum UserType {
	ADMINISTRATION, MANAGEMENT, SERVICE, SUPPORT;
}
