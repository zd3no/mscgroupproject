package model;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="UserEquipment")
@NamedQuery(name="UserEquipment.findAll", query="SELECT u FROM UserEquipment u")
public class UserEquipment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(columnDefinition="NUMERIC(50)", name="typeAllocationCode")
	private BigInteger typeAllocationCode;
	private String marketingName;
	private String manufacturer;
	private String accessCapability;
	private String model;
	private String vendorName;
	private String userEquipmentType;
	private String operatingSystem;
	private String inputMode;

	public UserEquipment(BigInteger typeAllocationCode) {
		this.typeAllocationCode = typeAllocationCode;
	}
	
	public UserEquipment() {
	}

	public BigInteger getTypeAllocationCode() {
		return this.typeAllocationCode;
	}

	public String getAccessCapability() {
		return this.accessCapability;
	}

	public void setAccessCapability(String accessCapability) {
		this.accessCapability = accessCapability;
	}

	public String getInputMode() {
		return this.inputMode;
	}

	public void setInputMode(String inputMode) {
		this.inputMode = inputMode;
	}

	public String getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getMarketingName() {
		return this.marketingName;
	}

	public void setMarketingName(String marketingName) {
		this.marketingName = marketingName;
	}

	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getOperatingSystem() {
		return this.operatingSystem;
	}

	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}

	public String getUserEquipmentType() {
		return this.userEquipmentType;
	}

	public void setUserEquipmentType(String userEquipmentType) {
		this.userEquipmentType = userEquipmentType;
	}

	public String getVendorName() {
		return this.vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((accessCapability == null) ? 0 : accessCapability.hashCode());
		result = prime * result
				+ ((inputMode == null) ? 0 : inputMode.hashCode());
		result = prime * result
				+ ((manufacturer == null) ? 0 : manufacturer.hashCode());
		result = prime * result
				+ ((marketingName == null) ? 0 : marketingName.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result
				+ ((operatingSystem == null) ? 0 : operatingSystem.hashCode());
		result = prime
				* result
				+ ((typeAllocationCode == null) ? 0 : typeAllocationCode
						.hashCode());
		result = prime
				* result
				+ ((userEquipmentType == null) ? 0 : userEquipmentType
						.hashCode());
		result = prime * result
				+ ((vendorName == null) ? 0 : vendorName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserEquipment other = (UserEquipment) obj;
		if (accessCapability == null) {
			if (other.accessCapability != null)
				return false;
		} else if (!accessCapability.equals(other.accessCapability))
			return false;
		if (inputMode == null) {
			if (other.inputMode != null)
				return false;
		} else if (!inputMode.equals(other.inputMode))
			return false;
		if (manufacturer == null) {
			if (other.manufacturer != null)
				return false;
		} else if (!manufacturer.equals(other.manufacturer))
			return false;
		if (marketingName == null) {
			if (other.marketingName != null)
				return false;
		} else if (!marketingName.equals(other.marketingName))
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (operatingSystem == null) {
			if (other.operatingSystem != null)
				return false;
		} else if (!operatingSystem.equals(other.operatingSystem))
			return false;
		if (typeAllocationCode == null) {
			if (other.typeAllocationCode != null)
				return false;
		} else if (!typeAllocationCode.equals(other.typeAllocationCode))
			return false;
		if (userEquipmentType == null) {
			if (other.userEquipmentType != null)
				return false;
		} else if (!userEquipmentType.equals(other.userEquipmentType))
			return false;
		if (vendorName == null) {
			if (other.vendorName != null)
				return false;
		} else if (!vendorName.equals(other.vendorName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserEquipment [typeAllocationCode=" + typeAllocationCode
				+ ", marketingName=" + marketingName + ", manufacturer="
				+ manufacturer + ", accessCapability=" + accessCapability
				+ ", model=" + model + ", vendorName=" + vendorName
				+ ", userEquipmentType=" + userEquipmentType
				+ ", operatingSystem=" + operatingSystem + ", inputMode="
				+ inputMode + "]";
	}

}