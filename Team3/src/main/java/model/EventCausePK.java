package model;

import java.io.Serializable;

import javax.persistence.*;

@Embeddable
public class EventCausePK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="causeCode")
	private Integer causeCode;
	@Column(name="eventId")
	private Integer eventId;

	public EventCausePK() {
	}
	
	public EventCausePK(Integer causeCode, Integer eventId) {
		this.causeCode = causeCode;
		this.eventId = eventId;
	}
	
	public Integer getCauseCode() {
		return this.causeCode;
	}
	public Integer getEventId() {
		return this.eventId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((causeCode == null) ? 0 : causeCode.hashCode());
		result = prime * result + ((eventId == null) ? 0 : eventId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventCausePK other = (EventCausePK) obj;
		if (causeCode == null) {
			if (other.causeCode != null)
				return false;
		} else if (!causeCode.equals(other.causeCode))
			return false;
		if (eventId == null) {
			if (other.eventId != null)
				return false;
		} else if (!eventId.equals(other.eventId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "EventCausePK [causeCode=" + causeCode + ", eventId=" + eventId
				+ "]";
	}
}