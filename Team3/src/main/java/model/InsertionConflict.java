package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
/**
 * Class provides insertionConflict objects, to represent data which is flagged as inconsistent from  the data source file.
 * There are five attributes, an id (int), originTable (String) which is the source table of the error, inputString (String)
 * which is the inconsistent data, errorDescription (String) and timeStamp (Date) which holds the date-time of when the error was logged.
 * NOTE: TimeStamp is automatically set from constructor but can be overwritten.
 * @author dee
 *
 */
@Entity
@Table(name="InsertionConflict")
@NamedQuery(name="InsertionConflict.findAll", query="SELECT i FROM InsertionConflict i")
public class InsertionConflict implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	private String originTable;
	@Column(columnDefinition = "text") private String inputString;
	@Column(columnDefinition = "text") private String errorDescription;
	private Date timeStamp;

	public InsertionConflict() {
		timeStamp = new Date();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOriginTable() {
		return originTable;
	}

	public void setOriginTable(String originTable) {
		this.originTable = originTable;
	}

	public String getInputString() {
		return inputString;
	}

	public void setInputString(String inputString) {
		this.inputString = inputString;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((errorDescription == null) ? 0 : errorDescription.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((inputString == null) ? 0 : inputString.hashCode());
		result = prime * result
				+ ((originTable == null) ? 0 : originTable.hashCode());
		result = prime * result
				+ ((timeStamp == null) ? 0 : timeStamp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InsertionConflict other = (InsertionConflict) obj;
		if (errorDescription == null) {
			if (other.errorDescription != null)
				return false;
		} else if (!errorDescription.equals(other.errorDescription))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (inputString == null) {
			if (other.inputString != null)
				return false;
		} else if (!inputString.equals(other.inputString))
			return false;
		if (originTable == null) {
			if (other.originTable != null)
				return false;
		} else if (!originTable.equals(other.originTable))
			return false;
		if (timeStamp == null) {
			if (other.timeStamp != null)
				return false;
		} else if (!timeStamp.equals(other.timeStamp))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "InsertionConflict [id=" + id + ", originTable=" + originTable
				+ ", inputString=" + inputString + ", errorDescription="
				+ errorDescription + ", timeStamp=" + timeStamp + "]";
	}
	
}