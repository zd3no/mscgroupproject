package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="CountryNetworkCode")
@NamedQuery(name="CountryNetworkCode.findAll", query="SELECT m FROM CountryNetworkCode m")
public class CountryNetworkCode implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId @Column(name="id")
	private CountryNetworkCodePK id;
	private String country;
	private String operator;

	public CountryNetworkCode() {
	}
	
	public CountryNetworkCode(CountryNetworkCodePK id) {
		this.id = id;
	}

	public CountryNetworkCodePK getId() {
		return this.id;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((operator == null) ? 0 : operator.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CountryNetworkCode other = (CountryNetworkCode) obj;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (operator == null) {
			if (other.operator != null)
				return false;
		} else if (!operator.equals(other.operator))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CountryNetworkCode [id=" + id + ", country=" + country
				+ ", operator=" + operator + "]";
	}

}