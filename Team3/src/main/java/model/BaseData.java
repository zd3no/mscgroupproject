
package model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "BaseData")
@NamedQueries({
    @NamedQuery(name = "BaseData.findAll", query = "SELECT b FROM BaseData b"),
    @NamedQuery(name = "BaseData.getBaseDataById", query = "SELECT b FROM BaseData b WHERE b.id=:id")
})
public class BaseData implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private int id;

    private Integer duration;

    private Integer cellId;

    private String networkElementVersion;

    @Temporal(TemporalType.TIMESTAMP)
    private Date timeStamp;

    @Column(columnDefinition = "NUMERIC(50)")
    private BigInteger imsi;

    @Column(columnDefinition = "NUMERIC(50)")
    private BigInteger hier3Id;

    @Column(columnDefinition = "NUMERIC(50)")
    private BigInteger hier32Id;

    @Column(columnDefinition = "NUMERIC(50)")
    private BigInteger hier321Id;

    @ManyToOne
    @JoinColumn(name = "typeAllocationCode", referencedColumnName = "typeAllocationCode", nullable = false, updatable = false)
    private UserEquipment userEquipment;

    @ManyToOne
    @JoinColumns({
        @JoinColumn(name = "causeCode", referencedColumnName = "causeCode", nullable = false, updatable = false),
        @JoinColumn(name = "eventId", referencedColumnName = "eventId", nullable = false, updatable = false) })
    private EventCause eventCause;

    @ManyToOne
    @JoinColumn(name = "failureId", referencedColumnName = "failureId", nullable = false, updatable = false)
    private FailureClass failureClass;

    @ManyToOne
    @JoinColumns({
        @JoinColumn(name = "mobileCountryCode", referencedColumnName = "mobileCountryCode", nullable = false, updatable = false),
        @JoinColumn(name = "mobileNetworkCode", referencedColumnName = "mobileNetworkCode", nullable = false, updatable = false)
    })
    private CountryNetworkCode countryNetworkCode;

    public BaseData() {}

    public BaseData(Integer duration, Date timeStamp, String networkElementVersion, BigInteger imsi, Integer cellId, BigInteger hier3Id,
            BigInteger hier32Id, BigInteger hier321Id, UserEquipment userEquipment, EventCause eventCause, FailureClass failureClass,
            CountryNetworkCode countryNetworkCode) {
        super();
        this.duration = duration;
        this.timeStamp = timeStamp;
        this.networkElementVersion = networkElementVersion;
        this.imsi = imsi;
        this.cellId = cellId;
        this.hier3Id = hier3Id;
        this.hier32Id = hier32Id;
        this.hier321Id = hier321Id;
        this.userEquipment = userEquipment;
        this.eventCause = eventCause;
        this.failureClass = failureClass;
        this.countryNetworkCode = countryNetworkCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getNetworkElementVersion() {
        return networkElementVersion;
    }

    public void setNetworkElementVersion(String networkElementVersion) {
        this.networkElementVersion = networkElementVersion;
    }

    public BigInteger getImsi() {
        return imsi;
    }

    public void setImsi(BigInteger imsi) {
        this.imsi = imsi;
    }

    public Integer getCellId() {
        return cellId;
    }

    public void setCellId(Integer cellId) {
        this.cellId = cellId;
    }

    public BigInteger getHier3Id() {
        return hier3Id;
    }

    public void setHier3Id(BigInteger hier3Id) {
        this.hier3Id = hier3Id;
    }

    public BigInteger getHier32Id() {
        return hier32Id;
    }

    public void setHier32Id(BigInteger hier32Id) {
        this.hier32Id = hier32Id;
    }

    public BigInteger getHier321Id() {
        return hier321Id;
    }

    public void setHier321Id(BigInteger hier321Id) {
        this.hier321Id = hier321Id;
    }

    public UserEquipment getUserEquipment() {
        return userEquipment;
    }

    public void setUserEquipment(UserEquipment userEquipment) {
        this.userEquipment = userEquipment;
    }

    public EventCause getEventCause() {
        return eventCause;
    }

    public void setEventCause(EventCause eventCause) {
        this.eventCause = eventCause;
    }

    public FailureClass getFailureClass() {
        return failureClass;
    }

    public void setFailureClass(FailureClass failureClass) {
        this.failureClass = failureClass;
    }

    public CountryNetworkCode getCountryNetworkCode() {
        return countryNetworkCode;
    }

    public void setCountryNetworkCode(CountryNetworkCode countryNetworkCode) {
        this.countryNetworkCode = countryNetworkCode;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BaseData other = (BaseData) obj;
        if (id != other.id)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "BaseData [id=" + id + ", duration=" + duration + ", timeStamp="
                + timeStamp + ", networkElementVersion="
                + networkElementVersion + ", imsi=" + imsi + ", cellId="
                + cellId + ", hier3Id=" + hier3Id + ", hier32Id=" + hier32Id
                + ", hier321Id=" + hier321Id + ", userEquipment="
                + userEquipment + ", eventCause=" + eventCause
                + ", failureClass=" + failureClass + ", countryNetworkCode="
                + countryNetworkCode + "]";
    }

}
