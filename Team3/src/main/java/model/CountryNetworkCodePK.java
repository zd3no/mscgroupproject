package model;

import java.io.Serializable;

import javax.persistence.*;

@Embeddable
public class CountryNetworkCodePK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="mobileCountryCode")
	private Integer mobileCountryCode;
	@Column(name="mobileNetworkCode")
	private Integer mobileNetworkCode;

	public CountryNetworkCodePK() {
	}
	
	public CountryNetworkCodePK(Integer mobileCountryCode, Integer mobileNetworkCode) {
		this.mobileCountryCode = mobileCountryCode;
		this.mobileNetworkCode = mobileNetworkCode;
	}

	public Integer getMobileCountryCode() {
		return mobileCountryCode;
	}

	public Integer getMobileNetworkCode() {
		return mobileNetworkCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((mobileCountryCode == null) ? 0 : mobileCountryCode
						.hashCode());
		result = prime
				* result
				+ ((mobileNetworkCode == null) ? 0 : mobileNetworkCode
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CountryNetworkCodePK other = (CountryNetworkCodePK) obj;
		if (mobileCountryCode == null) {
			if (other.mobileCountryCode != null)
				return false;
		} else if (!mobileCountryCode.equals(other.mobileCountryCode))
			return false;
		if (mobileNetworkCode == null) {
			if (other.mobileNetworkCode != null)
				return false;
		} else if (!mobileNetworkCode.equals(other.mobileNetworkCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CountryNetworkCodePK [mobileCountryCode=" + mobileCountryCode
				+ ", mobileNetworkCode=" + mobileNetworkCode + "]";
	}
}