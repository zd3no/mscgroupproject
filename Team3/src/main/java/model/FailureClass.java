
package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "FailureClass")
@NamedQuery(name = "FailureClass.findAll", query = "SELECT f FROM FailureClass f")
public class FailureClass implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "failureId")
    private Integer failureId;

    private String description;

    public FailureClass() {}

    public FailureClass(Integer failureId) {
        this.failureId = failureId;
    }

    public Integer getFailureId() {
        return failureId;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((description == null) ? 0 : description.hashCode());
        result = prime * result
                + ((failureId == null) ? 0 : failureId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FailureClass other = (FailureClass) obj;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (failureId == null) {
            if (other.failureId != null)
                return false;
        } else if (!failureId.equals(other.failureId))
            return false;
        return true;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "FailureClass [failureId=" + failureId + ", description="
                + description + "]";
    }

}
