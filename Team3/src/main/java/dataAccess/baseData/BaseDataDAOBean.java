
package dataAccess.baseData;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.BaseData;
import model.EventCause;
import model.FailureClass;
import dataAccess.QueryCommand;
import dataAccess.QueryCommand.QueryCommandBuilder;

@Local
@Stateless
@SuppressWarnings("unchecked")
public class BaseDataDAOBean implements BaseDataDAO {

    private static int currentId = 0;

    private QueryCommand queryCommand;

    @PersistenceContext(unitName = "primary")
    EntityManager em;

    public BaseDataDAOBean() {}

    @Override
    public Collection<BaseData> getAllBaseData() {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b")
                .from("BaseData b")
                .build();
        return em.createQuery(queryCommand.getCommandString()).getResultList();
    }

    @Override
    public BaseData getBaseDataById(Integer id) {
        return em.find(BaseData.class, id);
    }

    @Override
    public void persistBaseData(BaseData baseData) {
        if (baseData == null) {
            return;
        }
        setCurrentId();
        currentId++;
        baseData.setId(currentId);
        em.persist(baseData);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void persistBaseData(Collection<BaseData> baseDataRows) {
        setCurrentId();
        for (BaseData baseData : baseDataRows) {
            currentId++;
            baseData.setId(currentId);
            em.persist(baseData);
        }
        em.flush();
    }

    @Override
    public boolean removeBaseData(BaseData baseData) {
        if (baseData == null) {
            return false;
        }
        BaseData baseDataLocal = getBaseDataById(baseData.getId());
        if (baseDataLocal != null) {
            em.remove(baseDataLocal);
            return true;
        }
        return false;
    }

    @Override
    public boolean mergeBaseData(BaseData baseData) {
        if (baseData == null || baseData.getCountryNetworkCode() == null
                || baseData.getEventCause() == null
                || baseData.getUserEquipment() == null
                || baseData.getFailureClass() == null) {
            return false;
        }
        em.merge(baseData);
        return true;
    }

    @Override
    public Collection<Object[]> getNumberOfFailuresAndDurationForEachImsiBetweenPeriodTime(
            Date dateFrom, Date dateTo) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("count(b), sum(b.duration),  b.imsi")
                .from("BaseData b")
                .where("b.timeStamp")
                .between(":dateFrom AND :dateTo")
                .groupBy("b.imsi")
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("dateFrom", dateFrom)
                .setParameter("dateTo", dateTo).getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    public Collection<Object[]> getImsiWithFailuresForGivenDate(Date dateFrom,
            Date dateTo) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.imsi, b.userEquipment.accessCapability, b.userEquipment.manufacturer, b.userEquipment.model"
                        + ", b.countryNetworkCode.country, b.countryNetworkCode.operator")
                .from("BaseData as b")
                .where("b.timeStamp")
                .between(":dateFrom AND :dateTo")
                .groupBy("b.imsi")
                .build();
        Collection<Object[]> list = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("dateFrom", dateFrom)
                .setParameter("dateTo", dateTo)
                .getResultList();
        return list;
    }

    @Override
    public Collection<Object[]> getAllUniqueCauseCodeAndFailureClassByImsi(
            BigInteger imsi) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.failureClass.description, b.eventCause.description, COUNT(b)")
                .from("BaseData b")
                .where("b.imsi=:imsi")
                .groupBy("b.eventCause, b.failureClass")
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("imsi", imsi)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Collection<Object[]> getLastTenFailuresBasedOnIMSIAndFailureClassAndCauseCode(
            BigInteger imsi, String failureClassDescription,
            String eventCauseDescription) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.timeStamp, b.cellId, b.networkElementVersion, b.userEquipment.accessCapability, b.userEquipment.manufacturer"
                        + ", b.userEquipment.model, b.countryNetworkCode.country, b.countryNetworkCode.operator")
                .from("BaseData b")
                .where("b.imsi=:imsi AND b.failureClass.description=:failureClassDescription AND b.eventCause.description=:eventCauseDescription")
                .orderBy("b.timeStamp")
                .descrease()
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("imsi", imsi)
                .setParameter("failureClassDescription", failureClassDescription)
                .setParameter("eventCauseDescription", eventCauseDescription)
                .setMaxResults(10)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Collection<Object[]> getAllImsiByFailureClass(int failureId) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.imsi, ue.accessCapability, ue.manufacturer, ue.model, cnc.country, cnc.operator")
                .from("BaseData as b inner join b.userEquipment as ue inner join b.countryNetworkCode as cnc")
                .where(" b.failureClass.failureId=:failureId")
                .groupBy("b.imsi")
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("failureId", failureId)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Collection<EventCause> getAllEventCauseByImsi(BigInteger imsi) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.eventCause")
                .from("BaseData b")
                .where("b.imsi=:imsi")
                .groupBy(" b.eventCause")
                .build();
        Collection<EventCause> eventCauses = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("imsi", imsi)
                .getResultList();
        if (eventCauses.size() == 0)
            return new ArrayList<EventCause>();
        return eventCauses;
    }

    @Override
    public Collection<Object[]> getAllImsiBetweenPeriodTime(Date dateFrom, Date dateTo) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.imsi")
                .from("BaseData b")
                .where("b.timeStamp")
                .between(":dateFrom AND :dateTo")
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("dateFrom", dateFrom)
                .setParameter("dateTo", dateTo)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Long getNumberOfFailuresByModelBetweenPeriodTime(String model, Date dateFrom, Date dateTo) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("count(b)")
                .from("BaseData b")
                .where("b.userEquipment.model=:model AND b.timeStamp")
                .between(":dateFrom AND :dateTo")
                .build();
        Long numberFailure = (Long) em
                .createQuery(queryCommand.getCommandString())
                .setParameter("model", model)
                .setParameter("dateFrom", dateFrom)
                .setParameter("dateTo", dateTo)
                .getSingleResult();
        return numberFailure;
    }

    @Override
    public Collection<Object[]> getUniqueEventCauseAndNumberOccurrencesByPhoneModel(
            String model) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.eventCause.id.eventId, b.eventCause.id.causeCode, b.eventCause.description, count(b)")
                .from("BaseData b")
                .where("b.userEquipment.model=:model")
                .groupBy("b.eventCause")
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("model", model)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Collection<Object[]> getNumberOfFailuresByImsiBetweenPeriodTime(BigInteger imsi,
            Date dateFrom, Date dateTo) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.imsi, count(b), ue.accessCapability, ue.manufacturer, ue.model, cnc.country, cnc.operator")
                .from("BaseData AS b INNER JOIN b.userEquipment AS ue INNER JOIN b.countryNetworkCode AS cnc")
                .where("b.timeStamp")
                .between(":dateFrom AND :dateTo AND b.imsi=:imsi")
                .groupBy("b.imsi")
                .build();
        return em
                .createQuery(queryCommand.getCommandString())
                .setParameter("imsi", imsi)
                .setParameter("dateFrom", dateFrom)
                .setParameter("dateTo", dateTo).getResultList();
    }

    @Override
    public Collection<Object[]> getTop10UniqueMarketOperatorAndCellIdBetweenPeriodTime(
            Date dateFrom, Date dateTo) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.countryNetworkCode.country, b.countryNetworkCode.operator, b.cellId, count(b)")
                .from("BaseData b")
                .where("b.timeStamp")
                .between(":dateFrom AND :dateTo")
                .groupBy("b.countryNetworkCode, b.cellId")
                .orderBy("count(b)")
                .descrease()
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("dateFrom", dateFrom)
                .setParameter("dateTo", dateTo)
                .setMaxResults(10)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Collection<Object[]> getTop10ImsiBetweenPeriodTime(Date dateFrom, Date dateTo) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.imsi, count(b), ue.accessCapability, ue.manufacturer, ue.model, cnc.country, cnc.operator")
                .from("BaseData AS b INNER JOIN b.userEquipment AS ue INNER JOIN b.countryNetworkCode AS cnc")
                .where("b.timeStamp")
                .between(":dateFrom AND :dateTo")
                .groupBy("b.imsi")
                .orderBy("count(b)")
                .descrease()
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("dateFrom", dateFrom)
                .setParameter("dateTo", dateTo)
                .setMaxResults(10)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Map<FailureClass, Integer> getTotalNumberOfIMSIForAllFailureClasses(
            Collection<FailureClass> failureClasses) {
        Map<FailureClass, Integer> mapOfIMSIFailuresPerFailureClass = new HashMap<>();
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("DISTINCT b.imsi")
                .from("BaseData b")
                .where("b.failureClass=:failureClass")
                .build();
        for (FailureClass failureClass : failureClasses) {
            Integer numberOfIMSI = em
                    .createQuery(queryCommand.getCommandString())
                    .setParameter("failureClass", failureClass)
                    .getResultList()
                    .size();
            mapOfIMSIFailuresPerFailureClass.put(failureClass, numberOfIMSI);
        }
        return mapOfIMSIFailuresPerFailureClass;
    }

    @Override
    public Collection<Object[]> getNumberOfFailureForEachFailureClassByEventCauseAndImsi(
            Integer eventId, Integer causeCode, BigInteger imsi) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.failureClass.description, count(b)")
                .from("BaseData b")
                .where("b.eventCause.id.eventId=:eventId AND b.eventCause.id.causeCode=:causeCode AND b.imsi=:imsi")
                .groupBy("b.failureClass")
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("eventId", eventId)
                .setParameter("causeCode", causeCode)
                .setParameter("imsi", imsi)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Collection<Object[]> getAllFailureByEventCauseFailureClassAndImsi(
            Integer eventId, Integer causeCode, BigInteger imsi, String failureDescription) {
        queryCommand =
                QueryCommandBuilder
                        .newQueryCommand()
                        .select("b.timeStamp, b.cellId, b.countryNetworkCode.country,  b.countryNetworkCode.operator, b.failureClass.description"
                                + ", b.userEquipment.accessCapability, b.userEquipment.model")
                        .from("BaseData b")
                        .where("b.eventCause.id.eventId=:eventId AND b.eventCause.id.causeCode=:causeCode AND b.imsi=:imsi AND b.failureClass.description=:description")
                        .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("eventId", eventId)
                .setParameter("causeCode", causeCode)
                .setParameter("imsi", imsi)
                .setParameter("description", failureDescription)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Collection<BigInteger> getAllUniqueIMSIs() {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("DISTINCT b.imsi")
                .from("BaseData b")
                .build();
        return em
                .createQuery(queryCommand.getCommandString())
                .getResultList();
    }

    @Override
    public Collection<Object[]> getNumberOfFailureForEachCellIdByEventCauseAndImsi(
            Integer eventId, Integer causeCode, BigInteger imsi) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.cellId, count(b)")
                .from("BaseData b")
                .where("b.eventCause.id.eventId=:eventId AND b.eventCause.id.causeCode=:causeCode AND b.imsi=:imsi")
                .groupBy("b.cellId")
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("eventId", eventId)
                .setParameter("causeCode", causeCode)
                .setParameter("imsi", imsi)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Collection<Object[]> getAllFailureByEventCauseCellIdAndImsi(
            Integer eventId, Integer causeCode, BigInteger imsi, Integer cellId) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.timeStamp, b.cellId, b.countryNetworkCode.country, b.countryNetworkCode.operator, b.failureClass.description"
                        + ", b.userEquipment.accessCapability,  b.userEquipment.model")
                .from("BaseData b")
                .where("b.eventCause.id.eventId=:eventId AND b.eventCause.id.causeCode=:causeCode AND b.imsi=:imsi AND b.cellId=:cellId")
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("eventId", eventId)
                .setParameter("causeCode", causeCode)
                .setParameter("imsi", imsi)
                .setParameter("cellId", cellId)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    public Collection<Object[]> getFailureDescriptionBetweenPeriod(
            BigInteger imsi, Date from, Date to) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.timeStamp, b.failureClass.description, b.eventCause.description, b.cellId, b.networkElementVersion")
                .from("BaseData b")
                .where("b.imsi=:imsi AND b.timeStamp")
                .between(":from AND :to")
                .build();
        return em
                .createQuery(queryCommand.getCommandString())
                .setParameter("imsi", imsi)
                .setParameter("from", from)
                .setParameter("to", to)
                .getResultList();
    }

    @Override
    public Collection<String> getUniquePhones() {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("DISTINCT ue.model")
                .from("UserEquipment ue")
                .build();
        return em
                .createQuery(queryCommand.getCommandString())
                .getResultList();
    }

    @Override
    public Collection<Object[]> getEventCauseAndNumberOccurrencesByImsiBetweenTimePeriod(
            BigInteger imsi, Date dateFrom, Date dateTo) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.eventCause.id.eventId,b.eventCause.id.causeCode, b.eventCause.description, count(b)")
                .from("BaseData b")
                .where("b.imsi=:imsi AND b.timeStamp")
                .between(":dateFrom AND :dateTo")
                .groupBy("b.eventCause")
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("imsi", imsi)
                .setParameter("dateFrom", dateFrom)
                .setParameter("dateTo", dateTo)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Collection<Object[]> getAllFailureAndNumberOccurencesByImsiBetweenTimePeriod(
            BigInteger imsi, Date dateFrom, Date dateTo) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.failureClass.description, count(b)")
                .from("BaseData b")
                .where("b.imsi=:imsi AND b.timeStamp")
                .between(":dateFrom AND :dateTo")
                .groupBy("b.failureClass")
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("imsi", imsi)
                .setParameter("dateFrom", dateFrom)
                .setParameter("dateTo", dateTo)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Collection<Object[]> getEventCauseAndNumberOccurrencesByPhoneModelBetweenTimePeriod(
            String model, Date dateFrom, Date dateTo) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.eventCause.id.eventId, b.eventCause.id.causeCode, b.eventCause.description, count(b)")
                .from("BaseData b")
                .where("b.userEquipment.model=:model AND b.timeStamp")
                .between(":dateFrom AND :dateTo")
                .groupBy("b.eventCause")
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("model", model)
                .setParameter("dateFrom", dateFrom)
                .setParameter("dateTo", dateTo)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Collection<Object[]> getAllFailureAndNumberOccurencesByPhoneModelBetweenTimePeriod(
            String model, Date dateFrom, Date dateTo) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.failureClass.description, count(b)")
                .from("BaseData b")
                .where("b.userEquipment.model=:model AND b.timeStamp")
                .between(":dateFrom AND :dateTo")
                .groupBy("b.failureClass")
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("model", model)
                .setParameter("dateFrom", dateFrom)
                .setParameter("dateTo", dateTo)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Collection<Object[]> getAllFailureByEventCauseImsiBetweenTimePeriod(
            String description, BigInteger imsi, Date dateFrom, Date dateTo) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.timeStamp, b.cellId, b.countryNetworkCode.country, b.countryNetworkCode.operator, b.failureClass.description"
                        + ", b.userEquipment.accessCapability, b.userEquipment.model, b.networkElementVersion")
                .from("BaseData b")
                .where("b.eventCause.description=:description AND b.imsi=:imsi AND b.timeStamp")
                .between(":dateFrom AND :dateTo")
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("description", description)
                .setParameter("imsi", imsi)
                .setParameter("dateFrom", dateFrom)
                .setParameter("dateTo", dateTo)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Collection<Object[]> getAllFailureByFailureClassImsiBetweenTimePeriod(
            String failureDescription, BigInteger imsi, Date dateFrom, Date dateTo) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.timeStamp, b.cellId, b.countryNetworkCode.country, b.networkElementVersion, b.countryNetworkCode.operator"
                        + ", b.userEquipment.accessCapability, b.userEquipment.model, b.networkElementVersion")
                .from("BaseData b")
                .where("b.failureClass.description=:failureDescription AND b.imsi=:imsi AND b.timeStamp")
                .between(":dateFrom AND :dateTo")
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("failureDescription", failureDescription)
                .setParameter("imsi", imsi)
                .setParameter("dateFrom", dateFrom)
                .setParameter("dateTo", dateTo)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Collection<Object[]> getAllFailureByEventCausePhoneModelBetweenTimePeriod(
            String description, String model, Date dateFrom, Date dateTo) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.timeStamp, b.imsi , b.cellId, b.countryNetworkCode.country, b.countryNetworkCode.operator"
                        + ", b.failureClass.description, b.networkElementVersion")
                .from("BaseData b")
                .where("b.eventCause.description=:description AND b.userEquipment.model=:model AND b.timeStamp")
                .between(":dateFrom AND :dateTo")
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("description", description)
                .setParameter("model", model)
                .setParameter("dateFrom", dateFrom)
                .setParameter("dateTo", dateTo)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Collection<Object[]> getAllFailureByFailureClassPhoneModelBetweenTimePeriod(
            String failureDescription, String model, Date dateFrom, Date dateTo) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.timeStamp, b.imsi, b.cellId, b.countryNetworkCode.country, b.countryNetworkCode.operator"
                        + ", b.eventCause.description, b.networkElementVersion")
                .from("BaseData b")
                .where("b.failureClass.description=:failureDescription AND b.userEquipment.model=:model AND b.timeStamp")
                .between(":dateFrom AND :dateTo")
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("failureDescription", failureDescription)
                .setParameter("model", model)
                .setParameter("dateFrom", dateFrom)
                .setParameter("dateTo", dateTo)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Collection<Object[]> getFailuresByPhoneModel(String phone, Integer eventId, Integer causeCode) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.timeStamp, b.failureClass.description, b.cellId, b.networkElementVersion, b.imsi, b.countryNetworkCode.operator"
                        + ", b.countryNetworkCode.country, b.userEquipment.userEquipmentType, b.userEquipment.operatingSystem"
                        + ", b.userEquipment.accessCapability")
                .from("BaseData as b")
                .where("b.userEquipment.model = :phone AND b.eventCause.id.eventId =:eventId AND b.eventCause.id.causeCode=:causeCode")
                .orderBy("b.timeStamp")
                .descrease()
                .build();
        return em
                .createQuery(queryCommand.getCommandString())
                .setParameter("phone", phone)
                .setParameter("eventId", eventId)
                .setParameter("causeCode", causeCode)
                .getResultList();
    }

    @Override
    public Collection<Object[]> getAllFailureByImsiBetweenTimePeriod(
            BigInteger imsi, Date dateFrom, Date dateTo) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select(" b.timeStamp, b.cellId, b.duration, b.eventCause.description, b.failureClass.description, b.countryNetworkCode.country"
                        + ",  b.countryNetworkCode.operator, b.userEquipment.accessCapability")
                .from("BaseData b")
                .where("b.imsi=:imsi AND b.timeStamp")
                .between(":dateFrom AND :dateTo")
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setParameter("imsi", imsi)
                .setParameter("dateFrom", dateFrom)
                .setParameter("dateTo", dateTo)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Collection<Object[]> getTop10UniqueMarketOperatorAndCellIds() {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("b.countryNetworkCode.country, b.countryNetworkCode.operator, b.cellId, count(b)")
                .from("BaseData b")
                .groupBy("b.countryNetworkCode, b.cellId")
                .orderBy("count(b)")
                .descrease()
                .build();
        Collection<Object[]> objects = em
                .createQuery(queryCommand.getCommandString())
                .setMaxResults(10)
                .getResultList();
        if (objects.size() == 0)
            return new ArrayList<Object[]>();
        return objects;
    }

    @Override
    public Long getFailuresForThisCell(Integer cellId, String market) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("count(b)")
                .from("BaseData b")
                .where("b.cellId=:cellId and b.countryNetworkCode.country=:market")
                .build();
        return (Long) em
                .createQuery(queryCommand.getCommandString())
                .setParameter("cellId", cellId)
                .setParameter("market", market)
                .getSingleResult();
    }

    @Override
    public Long getFailuresForThisCellCountryOperator(Integer cellId, String market, String operator) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("count(b)")
                .from("BaseData b")
                .where("b.cellId=:cellId and b.countryNetworkCode.country=:market and b.countryNetworkCode.operator=:operator")
                .build();
        return (Long) em
                .createQuery(queryCommand.getCommandString())
                .setParameter("cellId", cellId)
                .setParameter("operator", operator)
                .setParameter("market", market)
                .getSingleResult();
    }

    @Override
    public Long getNumberOfFailuresForThisMarket(String market) {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("count(b)")
                .from("BaseData b")
                .where("b.countryNetworkCode.country=:market")
                .build();
        return (Long) em.createQuery(queryCommand.getCommandString())
                .setParameter("market", market)
                .getSingleResult();
    }

    private void setCurrentId() {
        if (currentId == 0) {
            queryCommand = QueryCommandBuilder.newQueryCommand()
                    .select("b.id")
                    .from("BaseData b")
                    .orderBy("b.id")
                    .descrease()
                    .build();
            List<Integer> lastId = em
                    .createQuery(queryCommand.getCommandString())
                    .setMaxResults(1)
                    .getResultList();
            if (lastId.size() != 0) {
                currentId = lastId.get(0);
            }
        }
    }
}
