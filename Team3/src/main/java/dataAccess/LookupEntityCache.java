
package dataAccess;

import java.math.BigInteger;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.annotation.PostConstruct;
import javax.ejb.Startup;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.CountryNetworkCode;
import model.CountryNetworkCodePK;
import model.EventCause;
import model.EventCausePK;
import model.FailureClass;
import model.InsertionConflict;
import model.User;
import model.UserEquipment;

/**
 * LookUpEntityCache provides concurrent hash maps for the objects created from the Failure Class, Event Cause,
 * User Equipment and CountryNetwork codes data. Provides methods for each of the classes to add them to the
 * relevant hash map (if not already present) and subsequently add them to the DB. Similar methods are provided to remove
 * and merge the objects from the relevant hash map and the database. Generic persist, remove and merge methods are provided
 * which identify the class of an entity and call the relevant method for that class.
 */
@Singleton
@Startup
public class LookupEntityCache {

    private static final ConcurrentHashMap<Integer, EventCause> eventCauseMap = new ConcurrentHashMap<Integer, EventCause>();
    private static final ConcurrentHashMap<Integer, FailureClass> failureClassMap = new ConcurrentHashMap<Integer, FailureClass>();
    private static final ConcurrentHashMap<Integer, CountryNetworkCode> countryNetworkCodeMap = new ConcurrentHashMap<Integer, CountryNetworkCode>();
    private static final ConcurrentHashMap<BigInteger, UserEquipment> userEquipmentMap = new ConcurrentHashMap<BigInteger, UserEquipment>();
    private static final ConcurrentHashMap<String, User> userMap = new ConcurrentHashMap<String, User>();
    private static final Collection<InsertionConflict> insertionConflicts = new CopyOnWriteArrayList<InsertionConflict>();

    @PersistenceContext(unitName = "primary")
    EntityManager em;

    /**
     * Initiates and loads the content of all hash maps from the database.
     */
    @PostConstruct
    private void init() {
        eventCauseMap.clear();
        failureClassMap.clear();
        countryNetworkCodeMap.clear();
        userEquipmentMap.clear();
        userMap.clear();

        Collection<EventCause> eventCauses = em.createQuery("SELECT e FROM EventCause e", EventCause.class).getResultList();
        Collection<FailureClass> failureClasses = em.createQuery("SELECT f FROM FailureClass f", FailureClass.class).getResultList();
        Collection<CountryNetworkCode> countryNetworkCodes =
                em.createQuery("SELECT c FROM CountryNetworkCode c", CountryNetworkCode.class).getResultList();
        Collection<UserEquipment> userEquipments = em.createQuery("SELECT u FROM UserEquipment u", UserEquipment.class).getResultList();
        Collection<User> users = em.createQuery("SELECT u FROM User u", User.class).getResultList();

        for (EventCause eventCause : eventCauses) {
            eventCauseMap.put(eventCause.getId().hashCode(), eventCause);
        }
        for (FailureClass failureClass : failureClasses) {
            failureClassMap.put(failureClass.getFailureId(), failureClass);
        }
        for (CountryNetworkCode countryNetworkCode : countryNetworkCodes) {
            countryNetworkCodeMap.put(countryNetworkCode.getId().hashCode(), countryNetworkCode);
        }
        for (UserEquipment userEquipment : userEquipments) {
            userEquipmentMap.put(userEquipment.getTypeAllocationCode(), userEquipment);
        }
        for (User user : users) {
            userMap.put(user.getId(), user);
        }
        eventCauses = null;
        failureClasses = null;
        countryNetworkCodes = null;
        userEquipments = null;
        users = null;
    }

    /**
     * Method that persists the desired entity to the cache and database.
     * 
     * @param entity
     *            Non-null entity with a valid id (primary key)
     * @param entityClass
     *            The class of the entity to persist. Supports: EventCause, FailureClass, UserEquipment, CoutnryNetworkCode and User
     * @return True if persist is successful, False otherwise.
     */
    public boolean persist(Object entity, Class<?> entityClass) {
        if (entity == null) {
            return false;
        }
        if (entityClass.equals(EventCause.class)) {
            return persistEventCause((EventCause) entity);
        } else if (entityClass.equals(FailureClass.class)) {
            return persistFailureClass((FailureClass) entity);
        } else if (entityClass.equals(UserEquipment.class)) {
            return persistUserEquipment((UserEquipment) entity);
        } else if (entityClass.equals(CountryNetworkCode.class)) {
            return persistCountryNetworkCode((CountryNetworkCode) entity);
        } else if (entityClass.equals(User.class)) {
            return persistUser((User) entity);
        } else
            return false;
    }

    /**
     * Method that merges the desired entity to the cache and database. If such entity is already in the cache, it will be replaced, otherwise it will
     * be added and
     * subsequently persisted to database.
     * 
     * @param entity
     *            Non-null entity with a valid id (primary key)
     * @param entityClass
     *            The class of the entity to persist. Supports: EventCause, FailureClass, UserEquipment, CoutnryNetworkCode and User
     * @return True if update is successful, False otherwise.
     */
    public boolean merge(Object entity, Class<?> entityClass) {
        if (entity == null) {
            return false;
        }
        if (entityClass.equals(EventCause.class)) {
            return persistAndReplaceEventCause((EventCause) entity);
        } else if (entityClass.equals(FailureClass.class)) {
            return persistAndReplaceFailureClass((FailureClass) entity);
        } else if (entityClass.equals(UserEquipment.class)) {
            return persistAndReplaceUserEquipment((UserEquipment) entity);
        } else if (entityClass.equals(CountryNetworkCode.class)) {
            return persistAndReplaceCountryNetworkCode((CountryNetworkCode) entity);
        } else if (entityClass.equals(User.class)) {
            return persistAndReplaceUser((User) entity);
        } else
            return false;
    }

    /**
     * Method that removes the desired entity to the cache and database.
     * 
     * @param entity
     *            Non-null entity with a valid id (primary key)
     * @param entityClass
     *            The class of the entity to persist. Supports: EventCause, FailureClass, UserEquipment, CoutnryNetworkCode and User
     * @return True if removal is successful, False otherwise.
     */
    public boolean remove(Object entity, Class<?> entityClass) {
        if (entity == null) {
            return false;
        }
        if (entityClass.equals(EventCause.class)) {
            return removeEventCause((EventCause) entity);
        } else if (entityClass.equals(FailureClass.class)) {
            return removeFailureClass((FailureClass) entity);
        } else if (entityClass.equals(UserEquipment.class)) {
            return removeUserEquipment((UserEquipment) entity);
        } else if (entityClass.equals(CountryNetworkCode.class)) {
            return removeCountryNetworkCode((CountryNetworkCode) entity);
        } else if (entityClass.equals(User.class)) {
            return removeUser((User) entity);
        } else
            return false;
    }

    private boolean persistEventCause(EventCause entity) {
        if (entity.getId() == null) {
            return false;
        }
        if (!eventCauseMap.containsKey(entity.getId().hashCode())) {
            eventCauseMap.put(entity.getId().hashCode(), entity);
            em.persist(entity);
            return true;
        } else
            return false;
    }

    private boolean persistFailureClass(FailureClass entity) {
        if (entity.getFailureId() == null) {
            return false;
        }
        if (!failureClassMap.containsKey(entity.getFailureId())) {
            failureClassMap.put(entity.getFailureId(), entity);
            em.persist(entity);
            return true;
        } else
            return false;
    }

    private boolean persistCountryNetworkCode(CountryNetworkCode entity) {
        if (entity.getId() == null) {
            return false;
        }
        if (!countryNetworkCodeMap.containsKey(entity.getId().hashCode())) {
            countryNetworkCodeMap.put(entity.getId().hashCode(), entity);
            em.persist(entity);
            return true;
        } else
            return false;
    }

    private boolean persistUserEquipment(UserEquipment entity) {
        if (entity.getTypeAllocationCode() == null) {
            return false;
        }
        if (!userEquipmentMap.containsKey(entity.getTypeAllocationCode())) {
            userEquipmentMap.put(entity.getTypeAllocationCode(), entity);
            em.persist(entity);
            return true;
        } else
            return false;
    }

    private boolean persistUser(User entity) {
        if (entity.getId() == null) {
            return false;
        }
        if (!userMap.containsKey(entity.getId())) {
            userMap.put(entity.getId(), entity);
            em.persist(entity);
            return true;
        }
        return false;
    }

    private boolean removeEventCause(EventCause entity) {
        if (entity.getId() == null) {
            return false;
        }
        if (eventCauseMap.remove(entity.getId().hashCode(), entity)) {
            if (em.contains(entity)) {
                em.remove(entity);

            } else {
                em.remove(em.merge(entity));
            }
            return true;
        } else
            return false;
    }

    private boolean removeFailureClass(FailureClass entity) {
        if (entity.getFailureId() == null) {
            return false;
        }
        if (failureClassMap.remove(entity.getFailureId(), entity)) {
            FailureClass failureClass = em.find(FailureClass.class, entity.getFailureId());
            em.remove(failureClass);
            return true;
        } else
            return false;
    }

    private boolean removeCountryNetworkCode(CountryNetworkCode entity) {
        if (entity.getId() == null) {
            return false;
        }
        if (countryNetworkCodeMap.remove(entity.getId().hashCode(), entity)) {
            CountryNetworkCode countryNetworkCode = em.find(CountryNetworkCode.class, entity.getId());
            em.remove(countryNetworkCode);
            return true;
        } else
            return false;
    }

    private boolean removeUserEquipment(UserEquipment entity) {
        if (entity.getTypeAllocationCode() == null) {
            return false;
        }
        if (userEquipmentMap.remove(entity.getTypeAllocationCode(), entity)) {
            UserEquipment userEquipment = em.find(UserEquipment.class, entity.getTypeAllocationCode());
            em.remove(userEquipment);
            return true;
        } else
            return false;
    }

    private boolean removeUser(User entity) {
        if (entity.getId() == null) {
            return false;
        }
        if (userMap.remove(entity.getId(), entity)) {
            User user = em.find(User.class, entity.getId());
            em.remove(user);
            return true;
        }
        return false;
    }

    /**
     * This method merges a EventCause to the database and cache. The EventCause Id must be unchanged and existent in the cache
     * 
     * @param entity
     *            The EventCause entity
     * @return true if EventCause was updated, false otherwise
     */
    private boolean persistAndReplaceEventCause(EventCause entity) {
        if (entity.getId() == null) {
            return false;
        }
        if (eventCauseMap.containsKey(entity.getId().hashCode())) {
            eventCauseMap.replace(entity.getId().hashCode(), entity);
            em.merge(entity);
            return true;
        } else {
            eventCauseMap.put(entity.getId().hashCode(), entity);
            em.merge(entity);
            return true;
        }
    }

    /**
     * This method merges a FailureClass to the database and cache. The FailureClass Id must be unchanged and existent in the cache
     * 
     * @param entity
     *            The FailureClass entity
     * @return true if FailureClass was updated, false otherwise
     */
    private boolean persistAndReplaceFailureClass(FailureClass entity) {
        if (entity.getFailureId() == null) {
            return false;
        }
        if (failureClassMap.containsKey(entity.getFailureId())) {
            failureClassMap.replace(entity.getFailureId(), entity);
            em.merge(entity);
            return true;
        } else {
            failureClassMap.put(entity.getFailureId(), entity);
            em.merge(entity);
            return true;
        }
    }

    /**
     * This method merges a CountryNetworkCode to the database and cache. The CountryNetworkCode Id must be unchanged and existent in the cache
     * 
     * @param entity
     *            The CountryNetworkCode entity
     * @return true if CountryNetworkCode was updated, false otherwise
     */
    private boolean persistAndReplaceCountryNetworkCode(CountryNetworkCode entity) {
        if (entity.getId() == null) {
            return false;
        }
        if (countryNetworkCodeMap.containsKey(entity.getId().hashCode())) {
            countryNetworkCodeMap.replace(entity.getId().hashCode(), entity);
            em.merge(entity);
            return true;
        } else {
            countryNetworkCodeMap.put(entity.getId().hashCode(), entity);
            em.merge(entity);
            return true;
        }
    }

    /**
     * This method merges a UserEquipment to the database and cache. The UserEquipment Id must be unchanged and existent in the cache
     * 
     * @param entity
     *            The UserEquipment entity
     * @return true if UserEquipment was updated, false otherwise
     */
    private boolean persistAndReplaceUserEquipment(UserEquipment entity) {
        if (entity.getTypeAllocationCode() == null) {
            return false;
        }
        if (userEquipmentMap.containsKey(entity.getTypeAllocationCode())) {
            userEquipmentMap.replace(entity.getTypeAllocationCode(), entity);
            em.merge(entity);
            return true;
        } else {
            userEquipmentMap.put(entity.getTypeAllocationCode(), entity);
            em.merge(entity);
            return true;
        }
    }

    /**
     * This method merges a User to the database and cache. The User's Id must be unchanged and existent in the cache
     * 
     * @param entity
     *            The user entity
     * @return true if user was updated, false otherwise
     */
    private boolean persistAndReplaceUser(User entity) {
        if (entity.getId() == null) {
            return false;
        }
        if (userMap.containsKey(entity.getId())) {
            userMap.replace(entity.getId(), entity);
            em.merge(entity);
            return true;
        } else {
            userMap.put(entity.getId(), entity);
            em.merge(entity);
            return true;
        }
    }

    /**
     * Get the eventCauseMap from cache. <p><b>DO NOT ADD OR REMOVE ELEMENTS DIRECTLY FROM THIS MAP!!!</b>
     * 
     * @return eventCauseMap ConcurrentHashMap
     */
    public final ConcurrentHashMap<Integer, EventCause> getEventCauseMap() {
        return eventCauseMap;
    }

    /**
     * Get the failureClassMap from cache. <p><b>DO NOT ADD OR REMOVE ELEMENTS DIRECTLY FROM THIS MAP!!!</b>
     * 
     * @return failureClassMap ConcurrentHashMap
     */
    public final ConcurrentHashMap<Integer, FailureClass> getFailureClassMap() {
        return failureClassMap;
    }

    /**
     * Get the countryNetworkCodeMap from cache. <p><b>DO NOT ADD OR REMOVE ELEMENTS DIRECTLY FROM THIS MAP!!!</b>
     * 
     * @return countryNetworkCodeMap ConcurrentHashMap
     */
    public final ConcurrentHashMap<Integer, CountryNetworkCode> getCountryNetworkCodeMap() {
        return countryNetworkCodeMap;
    }

    /**
     * Get the userEquipmentMap from cache. <p><b>DO NOT ADD OR REMOVE ELEMENTS DIRECTLY FROM THIS MAP!!!</b>
     * 
     * @return userEquipmentMap ConcurrentHashMap
     */
    public final ConcurrentHashMap<BigInteger, UserEquipment> getUserEquipmentMap() {
        return userEquipmentMap;
    }

    /**
     * Get the userMap from cache. <p><b>DO NOT ADD OR REMOVE ELEMENTS DIRECTLY FROM THIS MAP!!!</b>
     * 
     * @return userMap ConcurrentHashMap
     */
    public final ConcurrentHashMap<String, User> getUserMap() {
        return userMap;
    }

    /**
     * Gets the EventCause entity from the cache based on the provided eventId and causeCode
     * <p><b>NOTE:</b> Editing properties on this object without merging afterwards will leave the cache - database unsynchronized.
     * <p>ALWAYS merge entity if you make changes to keep the cache-db synchronized.
     * 
     * @param eventId
     * @param causeCode
     * @return EventCause if such entity exists in the cache, NULL otherwise
     */
    public final EventCause getEventCause(Integer eventId, Integer causeCode) {
        if (eventId != null && causeCode != null) {
            EventCausePK pk = new EventCausePK(causeCode, eventId);
            return eventCauseMap.get(pk.hashCode());
        } else {
            return null;
        }
    }

    /**
     * Gets the CountryNetworkCode entity from the cache based on the provided mobileCountryCode and mobileNetworkCode
     * <p><b>NOTE:</b> Editing properties on this object without merging afterwards will leave the cache - database unsynchronized.
     * <p>ALWAYS merge entity if you make changes to keep the cache-db synchronized.
     * 
     * @param mobileCountryCode
     * @param mobileNetworkCode
     * @return CountryNetworkCode if such entity exists in the cache, NULL otherwise
     */
    public final CountryNetworkCode getCountryNetworkCode(Integer mobileCountryCode, Integer mobileNetworkCode) {
        if (mobileCountryCode != null && mobileNetworkCode != null) {
            CountryNetworkCodePK pk = new CountryNetworkCodePK(mobileCountryCode, mobileNetworkCode);
            return countryNetworkCodeMap.get(pk.hashCode());
        } else {
            return null;
        }
    }

    /**
     * Gets the UserEquipment entity from the cache based on the provided typeAllocationCode
     * <p><b>NOTE:</b> Editing properties on this object without merging afterwards will leave the cache - database unsynchronized.
     * <p>ALWAYS merge entity if you make changes to keep the cache-db synchronized.
     * 
     * @param typeAllocationCode
     * @return UserEquipment if such entity exists in the cache, NULL otherwise
     */
    public final UserEquipment getUserEquipment(BigInteger typeAllocationCode) {
        if (typeAllocationCode != null) {
            return userEquipmentMap.get(typeAllocationCode);
        } else {
            return null;
        }
    }

    /**
     * Gets the FailureClass entity from the cache based on the provided failureId
     * <p><b>NOTE:</b> Editing properties on this object without merging afterwards will leave the cache - database unsynchronized.
     * <p>ALWAYS merge entity if you make changes to keep the cache-db synchronized.
     * 
     * @param failureId
     * @return FailureClass if such entity exists in the cache, NULL otherwise
     */
    public final FailureClass getFailureClass(Integer failureId) {
        if (failureId != null) {
            return failureClassMap.get(failureId);
        } else {
            return null;
        }
    }

    /**
     * Gets the User entity from the cache based on the provided id
     * <p><b>NOTE:</b> Editing properties on this object without merging afterwards will leave the cache - database unsynchronized.
     * <p>ALWAYS merge entity if you make changes to keep the cache-db synchronized.
     * 
     * @param id
     * @return User if such entity exists in the cache, NULL otherwise
     */
    public final User getUser(String id) {
        if (id != null) {
            return userMap.get(id);
        }
        return null;
    }

    /**
     * Gets the InsertionConflict collection that currently resides in cache. Please note that this collection is not persisted in the database.
     * 
     * @return Collection of InsertionConflict
     */
    public Collection<InsertionConflict> getInsertionConflicts() {
        return insertionConflicts;
    }

    /**
     * Adds InsertionConflict to the collection of InsertionConflict that is temporarilu stored in cache. This action does not add the
     * InsertionConflict to
     * Database and must be persisted separately.
     * 
     * @param conflict
     */
    public void addInsertionConflict(InsertionConflict conflict) {
        if (conflict != null)
            insertionConflicts.add(conflict);
    }

    /**
     * Deletes the current Collection of InsertionConflict from cache. This method should be called immediately after you have persisted
     * InsertionConflicts to Database.
     */
    public void resetInsertionConflicts() {
        insertionConflicts.clear();
    }

}
