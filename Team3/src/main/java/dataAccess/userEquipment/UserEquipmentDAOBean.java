
package dataAccess.userEquipment;

import java.math.BigInteger;
import java.util.Collection;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.UserEquipment;
import serviceLayer.entityCache.EntityCache;
import dataAccess.QueryCommand;
import dataAccess.QueryCommand.QueryCommandBuilder;

@Local
@Stateless
@SuppressWarnings("unchecked")
public class UserEquipmentDAOBean implements UserEquipmentDAO {

    @Inject
    EntityCache lookupEntityCache;

    @PersistenceContext(unitName = "primary")
    EntityManager em;

    private QueryCommand queryCommand;

    public UserEquipmentDAOBean() {}

    @Override
    public Collection<UserEquipment> getAllUserEquipment() {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("u")
                .from("UserEquipment u")
                .build();
        return em.createQuery(queryCommand.getCommandString()).getResultList();
    }

    @Override
    public UserEquipment getUserEquipmentById(BigInteger typeAllocationCode) {
        return lookupEntityCache.getUserEquipment(typeAllocationCode);
    }

    @Override
    public void persistUserEquipment(UserEquipment userEquipment) {
        lookupEntityCache.persist(userEquipment, UserEquipment.class);
    }

    @Override
    public void persistUserEquipments(Collection<UserEquipment> userEquipments) {
        if (userEquipments != null)
            for (UserEquipment userEquipment : userEquipments) {
                lookupEntityCache.persist(userEquipment, UserEquipment.class);
            }
    }

    @Override
    public boolean removeUserEquipment(UserEquipment userEquipment) {
        return lookupEntityCache.remove(userEquipment, UserEquipment.class);
    }

    @Override
    public boolean mergeUserEquipment(UserEquipment userEquipment) {
        return lookupEntityCache.merge(userEquipment, UserEquipment.class);
    }

}
