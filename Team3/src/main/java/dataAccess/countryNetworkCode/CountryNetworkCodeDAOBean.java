
package dataAccess.countryNetworkCode;

import java.util.Collection;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.CountryNetworkCode;
import serviceLayer.entityCache.EntityCache;
import dataAccess.QueryCommand;
import dataAccess.QueryCommand.QueryCommandBuilder;

@Local
@Stateless
@SuppressWarnings("unchecked")
public class CountryNetworkCodeDAOBean implements CountryNetworkCodeDAO {

    @Inject
    EntityCache lookupEntityCache;

    @PersistenceContext(unitName = "primary")
    EntityManager em;

    private QueryCommand queryCommand;

    public CountryNetworkCodeDAOBean() {}

    @Override
    public Collection<CountryNetworkCode> getAllCountryNetworkCode() {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("c")
                .from("CountryNetworkCode c")
                .build();
        return em.createQuery(queryCommand.getCommandString()).getResultList();
    }

    @Override
    public CountryNetworkCode getCountryNetworkCodeById(Integer mobileCountryCode, Integer mobileNetworkCode) {
        return lookupEntityCache.getCountryNetworkCode(mobileCountryCode, mobileNetworkCode);
    }

    @Override
    public void persistCountryNetworkCode(CountryNetworkCode countryNetworkCode) {
        lookupEntityCache.persist(countryNetworkCode, CountryNetworkCode.class);
    }

    @Override
    public void persistCountryNetworkCodes(Collection<CountryNetworkCode> countryNetworkCodes) {
        for (CountryNetworkCode countryNetworkCode : countryNetworkCodes) {
            lookupEntityCache.persist(countryNetworkCode, CountryNetworkCode.class);
        }
    }

    @Override
    public boolean removeCountryNetworkCode(CountryNetworkCode countryNetworkCode) {
        return lookupEntityCache.remove(countryNetworkCode, CountryNetworkCode.class);
    }

    @Override
    public boolean mergeCountryNetworkCode(CountryNetworkCode countryNetworkCode) {
        return lookupEntityCache.merge(countryNetworkCode, CountryNetworkCode.class);
    }

}
