package dataAccess.insertionConflict;

import java.util.Collection;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.InsertionConflict;

@Local
@Stateless
public class InsertionConflictDAOBean implements InsertionConflictDAO {

	@PersistenceContext(unitName = "primary")
	EntityManager em;
	
	@Override
	public void logValidationError(InsertionConflict insertionConflict){
		insertionConflict.setId(getLastIdFromDatabase()+1);
		em.persist(insertionConflict);
	}

	@Override
	public void logValidationError(Collection<InsertionConflict> insertionConflicts){
		Integer id = getLastIdFromDatabase();
		for (InsertionConflict insertionConflict : insertionConflicts) {
			id++;
			insertionConflict.setId(id);
			em.persist(insertionConflict);
		}
	}
	
	/**
	 * Extracts the current highest id from InsertionConflict table
	 * @return id (int)
	 */
	private Integer getLastIdFromDatabase() {
		List<Integer> lastId = em.createQuery("SELECT i.id FROM InsertionConflict i order by i.id desc",
				Integer.class).setMaxResults(1).getResultList();
		int id = 0;
		if (lastId.size() != 0) {
			id = lastId.get(0);
		}
		return id;
	}
	
	@Override
	public void logPersistenceError(InsertionConflict insertionConflict){
		insertionConflict.setId(getLastIdFromDatabase()+1);
		em.persist(insertionConflict);
	}

	/*@Override
	public Collection<InsertionConflict> getAllInsertionConflict() {
		return em.createQuery("SELECT i FROM InsertionConflict i",InsertionConflict.class).getResultList();
	}*/
}
