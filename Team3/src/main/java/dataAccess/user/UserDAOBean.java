
package dataAccess.user;

import java.util.Collection;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.User;
import dataAccess.LookupEntityCache;
import dataAccess.QueryCommand;
import dataAccess.QueryCommand.QueryCommandBuilder;

@Local
@Stateless
@SuppressWarnings("unchecked")
public class UserDAOBean implements UserDAO {

    @Inject
    LookupEntityCache lookupEntityCache;

    @PersistenceContext(unitName = "primary")
    EntityManager em;

    private QueryCommand queryCommand;

    @Override
    public Collection<User> getAllUser() {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("u")
                .from("User u")
                .build();
        return em.createQuery(queryCommand.getCommandString()).getResultList();
    }

    @Override
    public User getUserById(String id) {
        return lookupEntityCache.getUser(id);
    }

    @Override
    public void persistenceUser(User user) {
        if (user == null) {
            return;
        }
        lookupEntityCache.persist(user, User.class);
    }

    @Override
    public boolean removeUser(User user) {
        return lookupEntityCache.remove(user, User.class);
    }

    @Override
    public boolean mergeUser(User user) {
        return lookupEntityCache.merge(user, User.class);
    }

    @Override
    public User getUserByUsernameAndPassword(String username, String password) {
        if (username != null && password != null) {
            User user = lookupEntityCache.getUser(username);
            if (user != null && user.getPassword().equals(password)) {
                return user;
            } else {
                return null;
            }
        }
        return null;
    }

}
