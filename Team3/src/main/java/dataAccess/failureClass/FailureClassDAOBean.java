
package dataAccess.failureClass;

import java.util.Collection;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.FailureClass;
import dataAccess.LookupEntityCache;
import dataAccess.QueryCommand;
import dataAccess.QueryCommand.QueryCommandBuilder;

@Local
@Stateless
@SuppressWarnings("unchecked")
public class FailureClassDAOBean implements FailureClassDAO {

    @Inject
    LookupEntityCache lookupEntityCache;

    @PersistenceContext(unitName = "primary")
    EntityManager em;

    private QueryCommand queryCommand;

    @Override
    public Collection<FailureClass> getAllFailureClasses() {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("f")
                .from("FailureClass f")
                .build();
        return em.createQuery(queryCommand.getCommandString()).getResultList();
    }

    @Override
    public FailureClass getFailureClassById(Integer failureId) {
        return lookupEntityCache.getFailureClass(failureId);
    }

    @Override
    public void persistFailureClass(FailureClass failureClass) {
        lookupEntityCache.persist(failureClass, FailureClass.class);
    }

    @Override
    public void persistFailureClass(Collection<FailureClass> failureClasses) {
        for (FailureClass failureClass : failureClasses) {
            lookupEntityCache.persist(failureClass, FailureClass.class);
        }
    }

    @Override
    public boolean removeFailureClass(FailureClass failureClass) {
        return lookupEntityCache.remove(failureClass, FailureClass.class);
    }

    @Override
    public boolean mergeFailureClass(FailureClass failureClass) {
        return lookupEntityCache.merge(failureClass, FailureClass.class);
    }

}
