
package dataAccess.eventCause;

import java.util.Collection;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.EventCause;
import serviceLayer.entityCache.EntityCache;
import dataAccess.QueryCommand;
import dataAccess.QueryCommand.QueryCommandBuilder;

@Local
@Stateless
@SuppressWarnings("unchecked")
public class EventCauseDAOBean implements EventCauseDAO {

    @Inject
    EntityCache lookupEntityCache;

    @PersistenceContext(unitName = "primary")
    EntityManager em;

    private QueryCommand queryCommand;

    public EventCauseDAOBean() {}

    @Override
    public Collection<EventCause> getAllEventCauses() {
        queryCommand = QueryCommandBuilder.newQueryCommand()
                .select("e")
                .from("EventCause e")
                .build();
        return em.createQuery(queryCommand.getCommandString()).getResultList();
    }

    @Override
    public EventCause getEventCauseById(Integer eventId, Integer causeCode) {
        return lookupEntityCache.getEventCause(eventId, causeCode);
    }

    @Override
    public void persistEventCode(EventCause eventCause) {
        lookupEntityCache.persist(eventCause, EventCause.class);
    }

    @Override
    public boolean removeEventCode(EventCause eventCause) {
        return lookupEntityCache.remove(eventCause, EventCause.class);
    }

    @Override
    public boolean mergeEventCode(EventCause eventCause) {
        return lookupEntityCache.merge(eventCause, EventCause.class);
    }

    @Override
    public void persistEventCodes(Collection<EventCause> eventCauses) {
        if (eventCauses != null)
            for (EventCause eventCause : eventCauses) {
                lookupEntityCache.persist(eventCause, EventCause.class);
            }
    }

}
